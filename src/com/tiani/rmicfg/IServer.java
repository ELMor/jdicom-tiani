/* IServer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.rmicfg;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Properties;

public interface IServer extends Remote
{
    public String getType() throws RemoteException;
    
    public void setProperties(Properties properties) throws RemoteException;
    
    public Properties getProperties() throws RemoteException;
    
    public String[] getPropertyNames() throws RemoteException;
    
    public void start() throws RemoteException;
    
    public void start(Properties properties) throws RemoteException;
    
    public void stop(boolean bool, boolean bool_0_) throws RemoteException;
    
    public boolean isRunning() throws RemoteException;
}
