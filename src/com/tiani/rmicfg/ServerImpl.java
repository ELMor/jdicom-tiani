/* ServerImpl - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.rmicfg;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;

public class ServerImpl extends UnicastRemoteObject implements IServer
{
    private IServer _local;
    
    public ServerImpl(IServer iserver) throws RemoteException {
	_local = iserver;
    }
    
    public String getType() throws RemoteException {
	return _local.getType();
    }
    
    public String[] getPropertyNames() throws RemoteException {
	return _local.getPropertyNames();
    }
    
    public void setProperties(Properties properties) throws RemoteException {
	_local.setProperties(properties);
    }
    
    public Properties getProperties() throws RemoteException {
	return _local.getProperties();
    }
    
    public void start() throws RemoteException {
	_local.start();
    }
    
    public void start(Properties properties) throws RemoteException {
	_local.start(properties);
    }
    
    public void stop(boolean bool, boolean bool_0_) throws RemoteException {
	_local.stop(bool, bool_0_);
    }
    
    public boolean isRunning() throws RemoteException {
	return _local.isRunning();
    }
}
