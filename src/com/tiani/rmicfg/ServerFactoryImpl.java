/* ServerFactoryImpl - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.rmicfg;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Hashtable;

import com.kcmultimedia.demo.SCMEvent;
import com.kcmultimedia.demo.SCMEventListener;
import com.kcmultimedia.demo.SCMEventManager;

public class ServerFactoryImpl extends UnicastRemoteObject
    implements IServerFactory, SCMEventListener
{
    private final Hashtable _registry = new Hashtable();
    private final String[] _classNames;
    
    public ServerFactoryImpl(String[] strings) throws RemoteException {
	_classNames = strings;
	SCMEventManager scmeventmanager = SCMEventManager.getInstance();
	scmeventmanager.addSCMEventListener(this);
    }
    
    public IServer createServer(String string, String string_0_)
	throws RemoteException {
	try {
	    ServerImpl serverimpl
		= new ServerImpl((IServer)
				 Class.forName(string).newInstance());
	    _registry.put(string_0_, serverimpl);
	    return serverimpl;
	} catch (Exception exception) {
	    System.err.println(exception);
	    throw new RemoteException("", exception);
	}
    }
    
    public String[] listServerNames() throws RemoteException {
	String[] strings = new String[_registry.size()];
	Enumeration enumeration = _registry.keys();
	for (int i = 0; i < strings.length; i++)
	    strings[i] = (String) enumeration.nextElement();
	return strings;
    }
    
    public IServer getServer(String string) throws RemoteException {
	return (IServer) _registry.get(string);
    }
    
    public IServer removeServer(String string) throws RemoteException {
	return (IServer) _registry.remove(string);
    }
    
    public String[] listClassNames() throws RemoteException {
	return _classNames;
    }
    
    public void handleSCMEvent(SCMEvent scmevent) {
	if (scmevent.getID() != 1) {
	    /* empty */
	}
    }
}
