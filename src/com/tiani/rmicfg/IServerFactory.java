/* IServerFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.rmicfg;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServerFactory extends Remote
{
    public IServer createServer(String string, String string_0_)
	throws RemoteException;
    
    public IServer getServer(String string) throws RemoteException;
    
    public IServer removeServer(String string) throws RemoteException;
    
    public String[] listServerNames() throws RemoteException;
    
    public String[] listClassNames() throws RemoteException;
}
