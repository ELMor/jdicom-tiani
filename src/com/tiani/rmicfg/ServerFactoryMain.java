/* ServerFactoryMain - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.rmicfg;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Hashtable;

import com.tiani.dicom.util.CommandLineParser;

import examples.classServer.ClassFileServer;

public class ServerFactoryMain
{
    private String name = "ServerFactory";
    private String httpHost = null;
    private int httpPort = 2001;
    private String classpath = "";
    private final String[] classnames;
    private static final String USAGE
	= "Usage: ServerFactoryMain [-codebase <rmiCodebase>] [-http <host>[:<port>]] [-name <name>] -server <srvClass1> [<srvClass2> ..]\nwith\n-codebase              overwrites system property java.rmi.server.codebase with specified\n  <rmiCodebase>        URL or filepath\n-http <host>[:<port>]  start http server listen at specified port (default: 2001)\n-name <name>           name used for binding with the (local) rmi registry (default: \"ServerFactory\")\n-server <classname#>   server class name";
    private static final String[] OPTIONS
	= { "-codebase", "-http", "-name", "-server" };
    private static final int[] PARAMNUM = { 1, 1, 1 };
    
    public static void main(String[] strings) {
	try {
	    LocateRegistry.createRegistry(1099);
	    System.out.println("Start rmiregistry listening on port 1099");
	} catch (RemoteException remoteexception) {
	    /* empty */
	}
	try {
	    for (int i = 0; i < strings.length; i++)
		System.out.println("args[" + i + "]=" + strings[i]);
	    new ServerFactoryMain
		(CommandLineParser.parse(strings, OPTIONS, PARAMNUM))
		.execute();
	} catch (IllegalArgumentException illegalargumentexception) {
	    System.out.println(illegalargumentexception);
	    System.out.println
		("Usage: ServerFactoryMain [-codebase <rmiCodebase>] [-http <host>[:<port>]] [-name <name>] -server <srvClass1> [<srvClass2> ..]\nwith\n-codebase              overwrites system property java.rmi.server.codebase with specified\n  <rmiCodebase>        URL or filepath\n-http <host>[:<port>]  start http server listen at specified port (default: 2001)\n-name <name>           name used for binding with the (local) rmi registry (default: \"ServerFactory\")\n-server <classname#>   server class name");
	} catch (Throwable throwable) {
	    throwable.printStackTrace(System.out);
	}
    }
    
    private ServerFactoryMain(Hashtable hashtable)
	throws IllegalArgumentException {
	if (hashtable.containsKey("-name"))
	    name = CommandLineParser.get(hashtable, "-name", 0);
	classnames = (String[]) hashtable.get("-server");
	if (hashtable.containsKey("-http")) {
	    httpHost = CommandLineParser.get(hashtable, "-http", 0);
	    int i = httpHost.indexOf(':');
	    if (i != -1) {
		httpPort = Integer.parseInt(httpHost.substring(i + 1));
		httpHost = httpHost.substring(0, i);
	    }
	}
	if (classnames == null || classnames.length == 0)
	    throw new IllegalArgumentException("Missing server class name(s)");
	if (hashtable.containsKey("-codebase")) {
	    String string = CommandLineParser.get(hashtable, "-codebase", 0);
	    try {
		if (string.indexOf(':') > 2)
		    new URL(string);
		else if (httpHost == null)
		    string = new File(string).toURL().toString();
		else {
		    classpath = string;
		    string = "http://" + httpHost + ':' + httpPort + '/';
		}
		System.setProperty("java.rmi.server.codebase", string);
		System.out.println("Set java.rmi.server.codebase=" + string);
	    } catch (MalformedURLException malformedurlexception) {
		throw new IllegalArgumentException("Malformed codebase URL - "
						   + string);
	    }
	}
    }
    
    private void execute()
	throws IOException, ClassNotFoundException, InstantiationException,
	       IllegalAccessException, MalformedURLException, RemoteException {
	if (httpHost != null) {
	    new ClassFileServer(httpPort, classpath);
	    System.out
		.println("Start http server listening at port " + httpPort);
	}
	for (int i = 0; i < classnames.length; i++) {
	    IServer iserver
		= (IServer) Class.forName(classnames[i]).newInstance();
	}
	ServerFactoryImpl serverfactoryimpl
	    = new ServerFactoryImpl(classnames);
	Naming.rebind(name, serverfactoryimpl);
	System.out.println("Waiting for invocations from clients...");
    }
}
