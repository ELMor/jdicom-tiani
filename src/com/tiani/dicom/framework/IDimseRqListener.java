/* IDimseRqListener - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

public interface IDimseRqListener
{
    public void handleRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException;
    
    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException;
}
