/* DimseExchange - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;

public class DimseExchange implements Runnable
{
    private static final IDimseRspListener _DEF_RSP_LISTENER = new IDimseRspListener() {
	public void handleRSP(DimseExchange dimseexchange, int i, int i_0_,
			      DicomMessage dicommessage) {
	    /* empty */
	}
    };
    private VerboseAssociation _as;
    private int _artim = 1000;
    private DimseRqManager _rqManager;
    private boolean _queueRQ;
    private boolean _queueRSP;
    private BlockingHashtable _dimseSCUs = null;
    private BlockingVector _queue = null;
    private Socket _socket;
    private String _remoteAET;
    private String _localAET;
    private Scheduler _scheduler = new Scheduler();
    
    private final class Scheduler implements Runnable
    {
	private Scheduler() {
	    /* empty */
	}
	
	public void run() {
	    try {
		if (Debug.DEBUG > 1)
		    Debug.out.println
			("jdicom: " + _remoteAET
			 + " Enter DimseExchange.Scheduler.run()");
		DicomMessage dicommessage;
		while ((dicommessage = (DicomMessage) _queue.peek()) != null) {
		    if ((dicommessage.getI(3) & 0x8000) == 0)
			_rqManager.handleRQ(DimseExchange.this, dicommessage);
		    else
			DimseExchange.this.handleRSP(DimseExchange.this,
						     dicommessage);
		    _queue.pop();
		}
		if (Debug.DEBUG > 1)
		    Debug.out.println
			("jdicom: " + _remoteAET
			 + " Leave DimseExchange.Scheduler.run()");
	    } catch (Exception exception) {
		if (Debug.DEBUG > 2)
		    exception.printStackTrace(Debug.out);
		Debug.out.println("jdicom: Scheduler.run: "
				  + exception.toString() + ": "
				  + exception.getMessage());
	    } finally {
		_queue = null;
	    }
	}
    }
    
    private final class BlockingHashtable extends Hashtable
    {
	private int _maxOperationsInvoked;
	
	public BlockingHashtable(int i) {
	    _maxOperationsInvoked = i;
	}
	
	public synchronized Object add(Object object, Object object_1_)
	    throws InterruptedException {
	    if (_maxOperationsInvoked != 0) {
		while (_as.isOpen() && super.size() >= _maxOperationsInvoked)
		    this.wait();
	    }
	    return super.put(object, object_1_);
	}
	
	public synchronized Object remove(Object object) {
	    Object object_2_ = super.remove(object);
	    this.notifyAll();
	    return object_2_;
	}
	
	public synchronized void waitUntilEmpty() throws InterruptedException {
	    while (_as.isOpen() && !super.isEmpty())
		this.wait();
	}
    }
    
    private final class BlockingVector extends Vector
    {
	private BlockingVector() {
	    /* empty */
	}
	
	public synchronized Object peek() throws InterruptedException {
	    while (this.isEmpty())
		this.wait();
	    return this.firstElement();
	}
	
	public synchronized void pop() {
	    this.removeElementAt(0);
	    this.notifyAll();
	}
	
	public synchronized void write(Object object) {
	    this.addElement(object);
	    this.notifyAll();
	}
	
	public synchronized void waitUntilEmpty() throws InterruptedException {
	    while (!this.isEmpty())
		this.wait();
	}
    }
    
    public DimseExchange(VerboseAssociation verboseassociation,
			 DimseRqManager dimserqmanager, boolean bool,
			 boolean bool_3_, int i) {
	_as = verboseassociation;
	_rqManager = dimserqmanager;
	_queueRQ = bool;
	_queueRSP = bool_3_;
	_dimseSCUs = new BlockingHashtable(i);
	_socket = _as.socket();
	_localAET = _as.localAET();
	_remoteAET = _as.remoteAET();
    }
    
    public void setARTIM(int i) {
	_artim = i;
    }
    
    public final VerboseAssociation getAssociation() {
	return _as;
    }
    
    public final String localAET() {
	return _localAET;
    }
    
    public final String remoteAET() {
	return _remoteAET;
    }
    
    public final int nextMessageID() {
	return _as.nextMessageID();
    }
    
    public final boolean isOpen() {
	return _as.isOpen();
    }
    
    public final byte getPresentationContext(int i, int i_4_)
	throws IllegalValueException {
	return _as.getPresentationContext(i, i_4_);
    }
    
    public final byte getPresentationContext(int i)
	throws IllegalValueException {
	return _as.getPresentationContext(i);
    }
    
    public final UIDEntry getCurrentAbstractSyntax()
	throws IllegalValueException {
	return _as.getCurrentAbstractSyntax();
    }
    
    public final byte[] listAcceptedPresentationContexts(int i)
	throws IllegalValueException {
	return _as.listAcceptedPresentationContexts(i);
    }
    
    public final UIDEntry[] listAcceptedTransferSyntaxes(int i)
	throws IllegalValueException {
	return _as.listAcceptedTransferSyntaxes(i);
    }
    
    public final UIDEntry getTransferSyntax(byte i)
	throws IllegalValueException {
	return _as.getTransferSyntax(i);
    }
    
    public void addAssociationListener
	(IAssociationListener iassociationlistener) {
	_as.addAssociationListener(iassociationlistener);
    }
    
    public void sendCancelRQ(int i, int i_5_)
	throws IOException, IllegalValueException, DicomException {
	DicomMessage dicommessage = new DicomMessage(4095, i, null);
	_as.send(i_5_, dicommessage, null);
    }
    
    public void sendRQ
	(int i, int i_6_, DicomMessage dicommessage,
	 IDimseRspListener idimsersplistener)
	throws InterruptedException, IOException, IllegalValueException,
	       DicomException {
	_dimseSCUs.add(new Integer(i),
		       (idimsersplistener != null ? idimsersplistener
			: _DEF_RSP_LISTENER));
	_as.send(i_6_, dicommessage, dicommessage.getDataset());
    }
    
    public DicomMessage sendRQ(int i, int i_7_, DicomMessage dicommessage)
	throws InterruptedException, IOException, IllegalValueException,
	       DicomException {
	FutureDimseRsp futuredimsersp = new FutureDimseRsp();
	_as.addAssociationListener(futuredimsersp);
	try {
	    sendRQ(i, i_7_, dicommessage, futuredimsersp);
	    return futuredimsersp.getResponse();
	} finally {
	    _as.removeAssociationListener(futuredimsersp);
	}
    }
    
    public void sendRQ
	(int i, DicomMessage dicommessage, IDimseRspListener idimsersplistener)
	throws InterruptedException, IOException, IllegalValueException,
	       DicomException {
	_dimseSCUs.add(new Integer(i),
		       (idimsersplistener != null ? idimsersplistener
			: _DEF_RSP_LISTENER));
	_as.sendInPresentationContext(dicommessage.getPresentationContext(),
				      dicommessage, dicommessage.getDataset());
    }
    
    public DicomMessage sendRQ(int i, DicomMessage dicommessage)
	throws InterruptedException, IOException, IllegalValueException,
	       DicomException {
	FutureDimseRsp futuredimsersp = new FutureDimseRsp();
	_as.addAssociationListener(futuredimsersp);
	try {
	    sendRQ(i, dicommessage, futuredimsersp);
	    return futuredimsersp.getResponse();
	} finally {
	    _as.removeAssociationListener(futuredimsersp);
	}
    }
    
    public DicomMessage cecho() throws IOException, IllegalValueException,
				       DicomException, InterruptedException {
	int i = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(48, i, null);
	dicommessage.affectedSOPclassUID("1.2.840.10008.1.1");
	return sendRQ(i, 4097, dicommessage);
    }
    
    public DicomMessage cstore
	(byte i, String string, String string_8_, int i_9_,
	 DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_10_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 1, i_10_, dicomobject);
	dicommessage.affectedSOP(string, string_8_);
	dicommessage.priority(i_9_);
	return sendRQ(i_10_, dicommessage);
    }
    
    public DicomMessage cstore
	(byte i, String string, String string_11_, int i_12_,
	 String string_13_, int i_14_, DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_15_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 1, i_15_, dicomobject);
	dicommessage.affectedSOP(string, string_11_);
	dicommessage.priority(i_12_);
	dicommessage.moveOriginator(string_13_, i_14_);
	return sendRQ(i_15_, dicommessage);
    }
    
    public DicomMessage neventReport
	(byte i, String string, String string_16_, int i_17_,
	 DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_18_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 256, i_18_, dicomobject);
	dicommessage.affectedSOP(string, string_16_);
	dicommessage.set(15, new Integer(i_17_));
	return sendRQ(i_18_, dicommessage);
    }
    
    public DicomMessage ncreate
	(byte i, String string, String string_19_, DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_20_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 320, i_20_, dicomobject);
	dicommessage.affectedSOP(string, string_19_);
	return sendRQ(i_20_, dicommessage);
    }
    
    public DicomMessage nset
	(byte i, String string, String string_21_, DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_22_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 288, i_22_, dicomobject);
	dicommessage.requestedSOP(string, string_21_);
	return sendRQ(i_22_, dicommessage);
    }
    
    public DicomMessage nget
	(byte i, String string, String string_23_, int[] is)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_24_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 272, i_24_, null);
	dicommessage.requestedSOP(string, string_23_);
	if (is != null)
	    dicommessage.attributeIdList(is);
	return sendRQ(i_24_, dicommessage);
    }
    
    public DicomMessage naction
	(byte i, String string, String string_25_, int i_26_,
	 DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_27_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 304, i_27_, dicomobject);
	dicommessage.requestedSOP(string, string_25_);
	dicommessage.set(18, new Integer(i_26_));
	return sendRQ(i_27_, dicommessage);
    }
    
    public DicomMessage ndelete(byte i, String string, String string_28_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_29_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 336, i_29_, null);
	dicommessage.requestedSOP(string, string_28_);
	return sendRQ(i_29_, dicommessage);
    }
    
    public void cstoreAsync
	(byte i, String string, String string_30_, int i_31_,
	 DicomObject dicomobject, IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_32_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 1, i_32_, dicomobject);
	dicommessage.affectedSOP(string, string_30_);
	dicommessage.priority(i_31_);
	sendRQ(i_32_, dicommessage, idimsersplistener);
    }
    
    public void cstoreAsync
	(byte i, String string, String string_33_, int i_34_,
	 String string_35_, int i_36_, DicomObject dicomobject,
	 IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_37_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 1, i_37_, dicomobject);
	dicommessage.affectedSOP(string, string_33_);
	dicommessage.priority(i_34_);
	dicommessage.moveOriginator(string_35_, i_36_);
	sendRQ(i_37_, dicommessage, idimsersplistener);
    }
    
    public void neventReportAsync
	(byte i, String string, String string_38_, int i_39_,
	 DicomObject dicomobject, IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_40_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 256, i_40_, dicomobject);
	dicommessage.affectedSOP(string, string_38_);
	dicommessage.set(15, new Integer(i_39_));
	sendRQ(i_40_, dicommessage, idimsersplistener);
    }
    
    public void ncreateAsync
	(byte i, String string, String string_41_, DicomObject dicomobject,
	 IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_42_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 320, i_42_, dicomobject);
	dicommessage.affectedSOP(string, string_41_);
	sendRQ(i_42_, dicommessage, idimsersplistener);
    }
    
    public void nsetAsync
	(byte i, String string, String string_43_, DicomObject dicomobject,
	 IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_44_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 288, i_44_, dicomobject);
	dicommessage.requestedSOP(string, string_43_);
	sendRQ(i_44_, dicommessage, idimsersplistener);
    }
    
    public void ngetAsync
	(byte i, String string, String string_45_, int[] is,
	 IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_46_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 272, i_46_, null);
	dicommessage.requestedSOP(string, string_45_);
	if (is != null)
	    dicommessage.attributeIdList(is);
	sendRQ(i_46_, dicommessage, idimsersplistener);
    }
    
    public void nactionAsync
	(byte i, String string, String string_47_, int i_48_,
	 DicomObject dicomobject, IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_49_ = nextMessageID();
	DicomMessage dicommessage
	    = new DicomMessage(i, 304, i_49_, dicomobject);
	dicommessage.requestedSOP(string, string_47_);
	dicommessage.set(18, new Integer(i_48_));
	sendRQ(i_49_, dicommessage, idimsersplistener);
    }
    
    public void ndeleteAsync
	(byte i, String string, String string_50_,
	 IDimseRspListener idimsersplistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	int i_51_ = nextMessageID();
	DicomMessage dicommessage = new DicomMessage(i, 336, i_51_, null);
	dicommessage.requestedSOP(string, string_50_);
	sendRQ(i_51_, dicommessage, idimsersplistener);
    }
    
    public void waitForAllRSP() throws InterruptedException {
	_dimseSCUs.waitUntilEmpty();
    }
    
    public void run() {
	try {
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: " + _remoteAET
				  + " Enter DimseExchange.run()");
	    if (_queueRQ || _queueRSP) {
		Thread thread = new Thread(_scheduler);
		_queue = new BlockingVector();
		thread.start();
	    }
	    while (_as.isOpen()) {
		if (Debug.DEBUG > 1)
		    Debug.out
			.println("jdicom: " + _remoteAET + " Waiting for PDU");
		int i = _as.peek();
		if (Debug.DEBUG > 1)
		    Debug.out
			.println("jdicom: " + _remoteAET + " PDU received");
		switch (i) {
		case 10: {
		    DicomObject dicomobject = _as.receiveCommand();
		    DicomObject dicomobject_52_ = null;
		    if (dicomobject.getI(8) != 257)
			dicomobject_52_ = _as.receiveData();
		    DicomMessage dicommessage
			= (new DicomMessage
			   ((byte) _as.getCurrentPresentationContext(),
			    _as.getCurrentAbstractSyntax(), dicomobject,
			    dicomobject_52_));
		    int i_53_ = dicomobject.getI(3);
		    if ((i_53_ & 0x8000) == 0) {
			if (_rqManager == null)
			    throw new DicomException
				      ("Error: Received unexpected DIMSE-RQ");
			if (_queueRQ && i_53_ != 4095)
			    _queue.write(dicommessage);
			else
			    _rqManager.handleRQ(this, dicommessage);
		    } else if (_queueRSP)
			_queue.write(dicommessage);
		    else
			handleRSP(this, dicommessage);
		    break;
		}
		case 11:
		    _as.receiveReleaseRequest();
		    _as.sendReleaseResponse();
		    _as.closesocket(_artim);
		    return;
		case 13:
		    _as.receiveReleaseResponse();
		    _as.closesocket();
		    return;
		case 12:
		    _as.receiveAbort();
		    _as.closesocket();
		    return;
		default:
		    Debug.out.println
			("jdicom: DimseExchange.run: receive unrecognized PDU 0x"
			 + Integer.toHexString(i));
		    _as.sendAbort(Abort.DICOM_UL_SERVICE_PROVIDER,
				  Abort.UNRECOGNIZED_PDU);
		    _as.closesocket(_artim);
		    return;
		}
	    }
	} catch (Throwable throwable) {
	    if (Debug.DEBUG > 1)
		throwable.printStackTrace(Debug.out);
	    else
		Debug.out.println("jdicom: " + throwable.getMessage());
	    if (_as.isOpen()) {
		try {
		    _as.sendAbort(Abort.DICOM_UL_SERVICE_PROVIDER,
				  Abort.REASON_NOT_SPECIFIED);
		    _as.closesocket(_artim);
		} catch (Exception exception) {
		    if (_as.isOpen()) {
			try {
			    _as.closesocket();
			} catch (Exception exception_54_) {
			    /* empty */
			}
		    }
		}
	    }
	} finally {
	    synchronized (_dimseSCUs) {
		_dimseSCUs.notifyAll();
	    }
	    if (_queue != null)
		_queue.write(null);
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: " + _remoteAET
				  + " Leave DimseExchange.run()");
	}
    }
    
    public void releaseAssoc()
	throws InterruptedException, IOException, IllegalValueException {
	if (!_as.isOpen())
	    throw new IOException("jdicom: Association already closed");
	if (Debug.DEBUG > 2)
	    Debug.out.println("jdicom: Enter _dimseSCUs.waitUntilEmpty(");
	_dimseSCUs.waitUntilEmpty();
	if (_queue != null) {
	    if (Debug.DEBUG > 2)
		Debug.out.println("jdicom: Enter _queue.waitUntilEmpty()");
	    _queue.waitUntilEmpty();
	}
	if (_as.isOpen()) {
	    if (Debug.DEBUG > 2)
		Debug.out.println("jdicom: Enter _as.sendReleaseRequest()");
	    _as.sendReleaseRequest();
	}
	if (Debug.DEBUG > 2)
	    Debug.out.println("jdicom: Leave DimseExchange.releaseAssoc()");
    }
    
    private void handleRSP
	(DimseExchange dimseexchange_55_, DicomMessage dicommessage)
	throws IOException, UnknownUIDException, IllegalValueException,
	       DicomException {
	int i = dicommessage.getI(5);
	int i_56_ = dicommessage.getI(9);
	IDimseRspListener idimsersplistener
	    = (Status.isPending(i_56_)
	       ? (IDimseRspListener) _dimseSCUs.get(new Integer(i))
	       : (IDimseRspListener) _dimseSCUs.remove(new Integer(i)));
	if (idimsersplistener == null)
	    throw new DicomException
		      ("jdicom: Error: Received unexpected DIMSE-RSP");
	idimsersplistener.handleRSP(dimseexchange_55_, i, i_56_, dicommessage);
    }
}
