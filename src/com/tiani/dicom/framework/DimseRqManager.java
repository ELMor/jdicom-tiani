/* DimseRqManager - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

public class DimseRqManager
{
    private Hashtable cEchoScpReg = new Hashtable();
    private Hashtable cStoreScpReg = new Hashtable();
    private Hashtable cFindScpReg = new Hashtable();
    private Hashtable cGetScpReg = new Hashtable();
    private Hashtable cMoveScpReg = new Hashtable();
    private Hashtable nEventReportScuReg = new Hashtable();
    private Hashtable nGetScpReg = new Hashtable();
    private Hashtable nSetScpReg = new Hashtable();
    private Hashtable nActionScpReg = new Hashtable();
    private Hashtable nCreateScpReg = new Hashtable();
    private Hashtable nDeleteScpReg = new Hashtable();
    
    public void regCEchoScp(String string, IDimseRqListener idimserqlistener) {
	cEchoScpReg.put(string, idimserqlistener);
    }
    
    public void regCStoreScp(String string,
			     IDimseRqListener idimserqlistener) {
	cStoreScpReg.put(string, idimserqlistener);
    }
    
    public void regCFindScp(String string, IDimseRqListener idimserqlistener) {
	cFindScpReg.put(string, idimserqlistener);
    }
    
    public void regCGetScp(String string, IDimseRqListener idimserqlistener) {
	cGetScpReg.put(string, idimserqlistener);
    }
    
    public void regCMoveScp(String string, IDimseRqListener idimserqlistener) {
	cMoveScpReg.put(string, idimserqlistener);
    }
    
    public void regNEventReportScu(String string,
				   IDimseRqListener idimserqlistener) {
	nEventReportScuReg.put(string, idimserqlistener);
    }
    
    public void regNGetScp(String string, IDimseRqListener idimserqlistener) {
	nGetScpReg.put(string, idimserqlistener);
    }
    
    public void regNSetScp(String string, IDimseRqListener idimserqlistener) {
	nSetScpReg.put(string, idimserqlistener);
    }
    
    public void regNActionScp(String string,
			      IDimseRqListener idimserqlistener) {
	nActionScpReg.put(string, idimserqlistener);
    }
    
    public void regNCreateScp(String string,
			      IDimseRqListener idimserqlistener) {
	nCreateScpReg.put(string, idimserqlistener);
    }
    
    public void regNDeleteScp(String string,
			      IDimseRqListener idimserqlistener) {
	nDeleteScpReg.put(string, idimserqlistener);
    }
    
    public void handleRQ
	(DimseExchange dimseexchange, DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	Object object = null;
	Object object_0_ = null;
	int i = dicommessage.getI(3);
	if (i == 4095) {
	    int i_1_ = dicommessage.getI(5);
	    boolean bool
		= (handleCancelRQwith(dimseexchange, i_1_, cFindScpReg)
		   || handleCancelRQwith(dimseexchange, i_1_, cGetScpReg)
		   || handleCancelRQwith(dimseexchange, i_1_, cMoveScpReg));
	} else {
	    String string;
	    IDimseRqListener idimserqlistener;
	    switch (i) {
	    case 48:
		string = dicommessage.getS(1);
		idimserqlistener = (IDimseRqListener) cEchoScpReg.get(string);
		break;
	    case 1:
		string = dicommessage.getS(1);
		idimserqlistener = (IDimseRqListener) cStoreScpReg.get(string);
		break;
	    case 16:
		string = dicommessage.getS(1);
		idimserqlistener = (IDimseRqListener) cGetScpReg.get(string);
		break;
	    case 32:
		string = dicommessage.getS(1);
		idimserqlistener = (IDimseRqListener) cFindScpReg.get(string);
		break;
	    case 33:
		string = dicommessage.getS(1);
		idimserqlistener = (IDimseRqListener) cMoveScpReg.get(string);
		break;
	    case 256:
		string = dicommessage.getS(1);
		idimserqlistener
		    = (IDimseRqListener) nEventReportScuReg.get(string);
		break;
	    case 320:
		string = dicommessage.getS(1);
		idimserqlistener
		    = (IDimseRqListener) nCreateScpReg.get(string);
		break;
	    case 272:
		string = dicommessage.getS(2);
		idimserqlistener = (IDimseRqListener) nGetScpReg.get(string);
		break;
	    case 288:
		string = dicommessage.getS(2);
		idimserqlistener = (IDimseRqListener) nSetScpReg.get(string);
		break;
	    case 304:
		string = dicommessage.getS(2);
		idimserqlistener
		    = (IDimseRqListener) nActionScpReg.get(string);
		break;
	    case 336:
		string = dicommessage.getS(2);
		idimserqlistener
		    = (IDimseRqListener) nDeleteScpReg.get(string);
		break;
	    default:
		throw new DicomException();
	    }
	    int i_2_ = dicommessage.getI(4);
	    if (idimserqlistener != null)
		idimserqlistener.handleRQ(dimseexchange, i_2_, string,
					  dicommessage);
	    else
		sopClassNotSupported(dimseexchange, i_2_, string,
				     dicommessage);
	}
    }
    
    protected boolean handleCancelRQwith
	(DimseExchange dimseexchange, int i, Hashtable hashtable)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	Enumeration enumeration = hashtable.elements();
	while (enumeration.hasMoreElements()) {
	    if (((IDimseRqListener) enumeration.nextElement())
		    .handleCancelRQ(dimseexchange, i))
		return true;
	}
	return false;
    }
    
    protected void sopClassNotSupported
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	DicomMessage dicommessage_3_
	    = new DicomMessage(dicommessage.getPresentationContext(),
			       dicommessage.getAbstractSyntax(),
			       dicommessage.getI(3) | 0x8000, i, null);
	dicommessage_3_.status(290);
	dimseexchange.getAssociation().sendMessage(dicommessage_3_);
    }
}
