/* IAcceptancePolicy - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

public interface IAcceptancePolicy
{
    public Response prepareResponse
	(VerboseAssociation verboseassociation, Request request)
	throws IllegalValueException;
}
