/* Acceptor - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;

public class Acceptor implements Runnable
{
    private ServerSocket _serverSocket;
    private boolean _startThread;
    private String _calledTitle;
    private String[] _callingTitles;
    private int _artimRelease = 1000;
    private int _artimAssoc = 2000;
    private int[] _asids = null;
    private int _maxOperationsInvoked = 1;
    private int _maxOperationsPerformed = 1;
    private IAcceptorListener _acceptorListener;
    private int _tsid = 8193;
    private boolean _rejectnocontext = true;
    private Vector _associationListeners;
    private Vector _acceptorChilds;
    private IAcceptancePolicy _acceptancePolicy;
    
    private class AcceptorChild implements Runnable
    {
	Socket _socket;
	VerboseAssociation _as = null;
	
	AcceptorChild(Socket socket) {
	    _socket = socket;
	}
	
	public void abort() throws IOException {
	    if (_as != null && _as.isOpen()) {
		_as.sendAbort(Abort.DICOM_UL_SERVICE_PROVIDER,
			      Abort.DICOM_UL_SERVICE_USER);
		_as.closesocket();
	    }
	}
	
	public void run() {
	    _acceptorChilds.addElement(this);
	    try {
		if (Debug.DEBUG > 1)
		    Debug.out.println("jdicom: " + _socket.getInetAddress()
				      + " Enter AcceptorChild.run()");
		_as = new VerboseAssociation(_socket);
		Enumeration enumeration = _associationListeners.elements();
		while (enumeration.hasMoreElements())
		    _as.addAssociationListener((IAssociationListener)
					       enumeration.nextElement());
		_socket.setSoTimeout(_artimAssoc);
		Request request;
		try {
		    request = _as.receiveAssociateRequest();
		} catch (InterruptedIOException interruptedioexception) {
		    _socket.close();
		    throw new DicomException
			      ("ARTIM timer expired before receiving A-ASSOCIATE-RQ");
		}
		_socket.setSoTimeout(0);
		Response response
		    = _acceptancePolicy.prepareResponse(_as, request);
		_as.sendAssociateResponse(response);
		if (response instanceof Acknowledge)
		    _acceptorListener.handle((Acknowledge) response, _as);
		else
		    _as.closesocket(_artimRelease);
		if (Debug.DEBUG > 1)
		    Debug.out.println("jdicom: " + _socket.getInetAddress()
				      + " Leave AcceptorChild.run()");
	    } catch (Exception exception) {
		exception.printStackTrace(Debug.out);
	    } finally {
		_as = null;
		_acceptorChilds.removeElement(this);
	    }
	}
    }
    
    /**
     * @deprecated
     */
    public Acceptor(ServerSocket serversocket, boolean bool, int[] is, int i,
		    int i_0_, IAcceptorListener iacceptorlistener) {
	_associationListeners = new Vector();
	_acceptorChilds = new Vector();
	_acceptancePolicy = new IAcceptancePolicy() {
	    public Response prepareResponse
		(VerboseAssociation verboseassociation, Request request)
		throws IllegalValueException {
		Response response
		    = ResponsePolicy.prepareResponse(request, _calledTitle,
						     _callingTitles, _asids,
						     _tsid, _rejectnocontext);
		if (!(response instanceof Acknowledge))
		    return response;
		int i_2_ = request.getMaxOperationsInvoked();
		int i_3_ = request.getMaxOperationsPerformed();
		int i_4_ = (i_2_ == 0 || (_maxOperationsInvoked != 0
					  && i_2_ > _maxOperationsInvoked)
			    ? _maxOperationsInvoked : i_2_);
		int i_5_ = (i_3_ == 0 || (_maxOperationsPerformed != 0
					  && i_3_ > _maxOperationsPerformed)
			    ? _maxOperationsPerformed : i_3_);
		Acknowledge acknowledge = (Acknowledge) response;
		acknowledge.setMaxOperationsInvoked(i_4_);
		acknowledge.setMaxOperationsPerformed(i_5_);
		return acknowledge;
	    }
	};
	_serverSocket = serversocket;
	_startThread = bool;
	_asids = is;
	_maxOperationsInvoked = i;
	_maxOperationsPerformed = i_0_;
	_acceptorListener = iacceptorlistener;
    }
    
    public Acceptor(ServerSocket serversocket, boolean bool,
		    IAcceptancePolicy iacceptancepolicy,
		    IAcceptorListener iacceptorlistener) {
	_associationListeners = new Vector();
	_acceptorChilds = new Vector();
	_acceptancePolicy = new IAcceptancePolicy() {
	    public Response prepareResponse
		(VerboseAssociation verboseassociation, Request request)
		throws IllegalValueException {
		Response response
		    = ResponsePolicy.prepareResponse(request, _calledTitle,
						     _callingTitles, _asids,
						     _tsid, _rejectnocontext);
		if (!(response instanceof Acknowledge))
		    return response;
		int i = request.getMaxOperationsInvoked();
		int i_7_ = request.getMaxOperationsPerformed();
		int i_8_ = (i == 0 || (_maxOperationsInvoked != 0
				       && i > _maxOperationsInvoked)
			    ? _maxOperationsInvoked : i);
		int i_9_ = (i_7_ == 0 || (_maxOperationsPerformed != 0
					  && i_7_ > _maxOperationsPerformed)
			    ? _maxOperationsPerformed : i_7_);
		Acknowledge acknowledge = (Acknowledge) response;
		acknowledge.setMaxOperationsInvoked(i_8_);
		acknowledge.setMaxOperationsPerformed(i_9_);
		return acknowledge;
	    }
	};
	_serverSocket = serversocket;
	_startThread = bool;
	_acceptancePolicy = iacceptancepolicy;
	_acceptorListener = iacceptorlistener;
    }
    
    public void setARTIM(int i) {
	setARTIM(i, i);
    }
    
    public void setARTIM(int i, int i_10_) {
	_artimAssoc = i;
	_artimRelease = i_10_;
    }
    
    public void setStartThread(boolean bool) {
	_startThread = bool;
    }
    
    public boolean isStartThread(boolean bool) {
	return _startThread;
    }
    
    public void setAcceptorListener(IAcceptorListener iacceptorlistener) {
	_acceptorListener = iacceptorlistener;
    }
    
    /**
     * @deprecated
     */
    public void setCalledTitle(String string) {
	_calledTitle = string;
    }
    
    /**
     * @deprecated
     */
    public void setCallingTitles(String[] strings) {
	_callingTitles = strings;
    }
    
    public void setAcceptancePolicy(IAcceptancePolicy iacceptancepolicy) {
	_acceptancePolicy = iacceptancepolicy;
    }
    
    public IAcceptancePolicy getAcceptancePolicy() {
	return _acceptancePolicy;
    }
    
    public void addAssociationListener
	(IAssociationListener iassociationlistener) {
	_associationListeners.addElement(iassociationlistener);
    }
    
    public void removeAssociationListener
	(IAssociationListener iassociationlistener) {
	_associationListeners.removeElement(iassociationlistener);
    }
    
    public void run() {
	try {
	    for (;;) {
		Socket socket = _serverSocket.accept();
		AcceptorChild acceptorchild = new AcceptorChild(socket);
		if (_startThread) {
		    if (Debug.DEBUG > 1)
			Debug.out.println
			    ("jdicom: " + socket.getInetAddress()
			     + " Start new Thread for AcceptorChild.run()");
		    Thread thread = new Thread(acceptorchild);
		    thread.start();
		} else
		    acceptorchild.run();
	    }
	} catch (Exception exception) {
	    Debug.out.println("jdicom: " + exception);
	}
    }
    
    public void stop(boolean bool) {
	try {
	    _serverSocket.close();
	    if (bool) {
		AcceptorChild[] acceptorchilds;
		synchronized (_acceptorChilds) {
		    acceptorchilds = new AcceptorChild[_acceptorChilds.size()];
		    _acceptorChilds.copyInto(acceptorchilds);
		}
		for (int i = 0; i < acceptorchilds.length; i++)
		    acceptorchilds[i].abort();
	    }
	} catch (IOException ioexception) {
	    Debug.out.println("jdicom: " + ioexception);
	}
    }
}
