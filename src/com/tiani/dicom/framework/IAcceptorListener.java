/* IAcceptorListener - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;

public interface IAcceptorListener
{
    public void handle
	(Acknowledge acknowledge, VerboseAssociation verboseassociation)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException;
}
