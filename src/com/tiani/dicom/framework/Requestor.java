/* Requestor - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

public class Requestor
{
    private Socket _socket;
    private Request _request;
    private Response _response;
    private Vector _associationListeners = new Vector();
    
    public Requestor(Socket socket, Request request) {
	_socket = socket;
	_request = request;
	_response = null;
    }
    
    public void addAssociationListener
	(IAssociationListener iassociationlistener) {
	_associationListeners.addElement(iassociationlistener);
    }
    
    public VerboseAssociation openAssoc()
	throws UnknownHostException, IOException, UnknownUIDException,
	       IllegalValueException {
	VerboseAssociation verboseassociation
	    = new VerboseAssociation(_socket);
	Enumeration enumeration = _associationListeners.elements();
	while (enumeration.hasMoreElements())
	    verboseassociation.addAssociationListener((IAssociationListener)
						      enumeration
							  .nextElement());
	verboseassociation.sendAssociateRequest(_request);
	_response = verboseassociation.receiveAssociateResponse();
	if (!(_response instanceof Acknowledge))
	    return null;
	return verboseassociation;
    }
    
    public final Response response() {
	return _response;
    }
}
