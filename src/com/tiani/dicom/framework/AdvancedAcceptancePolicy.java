/* AdvancedAcceptancePolicy - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.net.InetAddress;
import java.util.Hashtable;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Reject;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

public class AdvancedAcceptancePolicy implements IAcceptancePolicy
{
    private Vector _calledtitles = null;
    private Vector _callingtitles = null;
    private final Hashtable _aetSpecificPolicy = new Hashtable();
    private final Hashtable _ipcheck = new Hashtable();
    private final Vector _abstractsyntaxes = new Vector();
    private final Vector _transfersyntaxes = new Vector();
    private int _maxopinvoked = 1;
    private int _maxopperformed = 1;
    private int _maxpdusize = 0;
    private final Vector _asroles = new Vector();
    private final Vector _scuroles = new Vector();
    private final Vector _scproles = new Vector();
    public static final Reject APPLICATIONCONTEXTNAME_NOT_SUPPORTED
	= new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER,
		     Reject.USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED);
    public static final Reject CALLED_AETITLE_NOT_RECOGNIZED
	= new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER,
		     Reject.USER_CALLED_AETITLE_NOT_RECOGNIZED);
    public static final Reject CALLING_AETITLE_NOT_RECOGNIZED
	= new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER,
		     Reject.USER_CALLING_AETITLE_NOT_RECOGNIZED);
    public static final Reject NO_REASON_GIVEN
	= new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER,
		     Reject.USER_NO_REASON_GIVEN);
    
    public String[] getCallingTitles() {
	if (_callingtitles == null)
	    return null;
	String[] strings = new String[_callingtitles.size()];
	_callingtitles.copyInto(strings);
	return strings;
    }
    
    public String getCalledTitle() {
	if (_calledtitles == null || _calledtitles.size() == 0)
	    return null;
	return (String) _calledtitles.elementAt(0);
    }
    
    public String[] getCalledTitles() {
	if (_calledtitles == null)
	    return null;
	String[] strings = new String[_calledtitles.size()];
	_calledtitles.copyInto(strings);
	return strings;
    }
    
    public void setCalledTitle(String string) {
	if (string == null)
	    _calledtitles = null;
	else
	    setCalledTitles(new String[] { string });
    }
    
    public void setCalledTitles(String[] strings) {
	if (strings == null)
	    _calledtitles = null;
	else {
	    _calledtitles = new Vector();
	    for (int i = 0; i < strings.length; i++)
		_calledtitles.addElement(strings[i]);
	}
    }
    
    public void addCalledTitle(String string) {
	if (_calledtitles == null)
	    _calledtitles = new Vector();
	if (_calledtitles.indexOf(string) == -1)
	    _calledtitles.addElement(string);
    }
    
    public boolean removeCalledTitle(String string) {
	if (_calledtitles == null)
	    return false;
	return _calledtitles.removeElement(string);
    }
    
    public void setCallingTitles(String[] strings) {
	if (strings == null)
	    _callingtitles = null;
	else {
	    _callingtitles = new Vector();
	    for (int i = 0; i < strings.length; i++)
		_callingtitles.addElement(strings[i]);
	}
    }
    
    public void addCallingTitle(String string) {
	if (_callingtitles == null)
	    _callingtitles = new Vector();
	if (_callingtitles.indexOf(string) == -1)
	    _callingtitles.addElement(string);
    }
    
    public boolean removeCallingTitle(String string) {
	if (_callingtitles == null)
	    return false;
	return _callingtitles.removeElement(string);
    }
    
    public void setMaxOperationsInvoked(int i) {
	_maxopinvoked = i;
    }
    
    public int getMaxOperationsInvoked() {
	return _maxopinvoked;
    }
    
    public void setMaxOperationsPerformed(int i) {
	_maxopperformed = i;
    }
    
    public int getMaxOperationsPerformed() {
	return _maxopperformed;
    }
    
    public void setMaxPduSize(int i) {
	_maxpdusize = i;
    }
    
    public int getMaxPduSize() {
	return _maxpdusize;
    }
    
    public int getScuRole(int i) throws IllegalValueException {
	int i_0_ = _asroles.indexOf(UID.getUIDEntry(i));
	if (i_0_ == -1)
	    return -1;
	return ((Integer) _scuroles.elementAt(i_0_)).intValue();
    }
    
    public int getScpRole(int i) throws IllegalValueException {
	int i_1_ = _asroles.indexOf(UID.getUIDEntry(i));
	if (i_1_ == -1)
	    return -1;
	return ((Integer) _scproles.elementAt(i_1_)).intValue();
    }
    
    public int getAbstractSyntaxes() {
	return _abstractsyntaxes.size();
    }
    
    public UIDEntry getAbstractSyntax(int i) {
	return (UIDEntry) _abstractsyntaxes.elementAt(i);
    }
    
    public int getTransferSyntaxes(int i) {
	return ((Vector) _transfersyntaxes.elementAt(i)).size();
    }
    
    public UIDEntry getTransferSyntax(int i, int i_2_) {
	return ((UIDEntry)
		((Vector) _transfersyntaxes.elementAt(i)).elementAt(i_2_));
    }
    
    public void addPresentationContext(int i, int[] is)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	UIDEntry[] uidentrys = new UIDEntry[is.length];
	for (int i_3_ = 0; i_3_ < is.length; i_3_++)
	    uidentrys[i_3_] = UID.getUIDEntry(is[i_3_]);
	int i_4_ = _abstractsyntaxes.indexOf(uidentry);
	Vector vector;
	if (i_4_ == -1) {
	    _abstractsyntaxes.addElement(uidentry);
	    _transfersyntaxes.addElement(vector = new Vector());
	} else
	    vector = (Vector) _transfersyntaxes.elementAt(i_4_);
	for (int i_5_ = 0; i_5_ < uidentrys.length; i_5_++)
	    vector.addElement(uidentrys[i_5_]);
    }
    
    public void setScuScpRoleSelection(int i, int i_6_, int i_7_)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	int i_8_ = _asroles.indexOf(uidentry);
	if (i_8_ != -1) {
	    _scuroles.setElementAt(new Integer(i_6_), i_8_);
	    _scproles.setElementAt(new Integer(i_7_), i_8_);
	} else {
	    _asroles.addElement(uidentry);
	    _scuroles.addElement(new Integer(i_6_));
	    _scproles.addElement(new Integer(i_7_));
	}
    }
    
    public void setPolicyForCallingTitle
	(String string, AdvancedAcceptancePolicy advancedacceptancepolicy_9_) {
	_aetSpecificPolicy.put(string, advancedacceptancepolicy_9_);
    }
    
    public AdvancedAcceptancePolicy removePolicyForCallingTitle
	(String string) {
	return (AdvancedAcceptancePolicy) _aetSpecificPolicy.remove(string);
    }
    
    public void setIpCheckForCallingTitle(String string,
					  InetAddress inetaddress) {
	_ipcheck.put(string, inetaddress);
    }
    
    public InetAddress removeIpCheckForCallingTitle(String string) {
	return (InetAddress) _ipcheck.remove(string);
    }
    
    public void removeIpChecks() {
	_ipcheck.clear();
    }
    
    public Response prepareResponse
	(VerboseAssociation verboseassociation, Request request)
	throws IllegalValueException {
	if (request.isPrivateApplicationContext())
	    return APPLICATIONCONTEXTNAME_NOT_SUPPORTED;
	if (_calledtitles != null
	    && _calledtitles.indexOf(request.getCalledTitle()) == -1)
	    return CALLED_AETITLE_NOT_RECOGNIZED;
	String string = request.getCallingTitle();
	InetAddress inetaddress = (InetAddress) _ipcheck.get(string);
	if (_callingtitles != null && _callingtitles.indexOf(string) == -1
	    || (inetaddress != null
		&& !inetaddress
			.equals(verboseassociation.socket().getInetAddress())))
	    return CALLING_AETITLE_NOT_RECOGNIZED;
	AdvancedAcceptancePolicy advancedacceptancepolicy_10_
	    = (AdvancedAcceptancePolicy) _aetSpecificPolicy.get(string);
	if (advancedacceptancepolicy_10_ == null)
	    advancedacceptancepolicy_10_ = this;
	Acknowledge acknowledge = new Acknowledge();
	acknowledge.setCalledTitle(request.getCalledTitle());
	acknowledge.setCallingTitle(request.getCallingTitle());
	acknowledge.setMaxPduSize(_maxpdusize);
	return ((advancedacceptancepolicy_10_.negotiate(request, acknowledge)
		 > 0)
		? (Response) acknowledge : NO_REASON_GIVEN);
    }
    
    private int negotiate(Request request, Acknowledge acknowledge)
	throws IllegalValueException {
	int i = 0;
	int i_11_ = request.getPresentationContexts();
	for (int i_12_ = 0; i_12_ < i_11_; i_12_++) {
	    if (negotiateTransferSyntax(request, acknowledge, i_12_))
		i++;
	}
	if (i == 0)
	    return 0;
	negotiateAsyncOperations(request, acknowledge);
	return i;
    }
    
    private boolean negotiateTransferSyntax
	(Request request, Acknowledge acknowledge, int i)
	throws IllegalValueException {
	UIDEntry uidentry = request.getAbstractSyntax(i);
	int i_13_ = _abstractsyntaxes.indexOf(uidentry);
	if (i_13_ == -1) {
	    acknowledge.addPresentationContext
		(request.getPresentationContextID(i), 3, 8193);
	    return false;
	}
	int i_14_ = request.getScuRole(uidentry);
	int i_15_ = request.getScpRole(uidentry);
	if (i_14_ != -1) {
	    int i_16_ = 1;
	    int i_17_ = 0;
	    int i_18_ = _asroles.indexOf(uidentry);
	    if (i_18_ != -1) {
		i_16_ = ((Integer) _scuroles.elementAt(i_18_)).intValue();
		i_17_ = ((Integer) _scproles.elementAt(i_18_)).intValue();
	    }
	    i_16_ &= i_14_;
	    i_17_ &= i_15_;
	    if (i_16_ == 0 && i_17_ == 0) {
		acknowledge.addPresentationContext
		    (request.getPresentationContextID(i), 3, 8193);
		return false;
	    }
	    acknowledge.setScuScpRoleSelection(uidentry.getConstant(), i_16_,
					       i_17_);
	}
	int i_19_ = request.getTransferSyntaxes(i);
	Vector vector = (Vector) _transfersyntaxes.elementAt(i_13_);
	int i_20_ = vector.size();
	for (int i_21_ = 0; i_21_ < i_20_; i_21_++) {
	    for (int i_22_ = 0; i_22_ < i_19_; i_22_++) {
		UIDEntry uidentry_23_ = (UIDEntry) vector.elementAt(i_21_);
		if (uidentry_23_.equals(request.getTransferSyntax(i, i_22_))) {
		    acknowledge.addPresentationContext
			(request.getPresentationContextID(i), 0,
			 uidentry_23_.getConstant());
		    return true;
		}
	    }
	}
	acknowledge.addPresentationContext(request.getPresentationContextID(i),
					   4, 8193);
	return false;
    }
    
    private void negotiateAsyncOperations(Request request,
					  Acknowledge acknowledge) {
	int i = request.getMaxOperationsInvoked();
	int i_24_ = request.getMaxOperationsPerformed();
	acknowledge.setMaxOperationsInvoked(i == 0 || (_maxopinvoked != 0
						       && _maxopinvoked < i)
					    ? _maxopinvoked : i);
	acknowledge.setMaxOperationsPerformed((i_24_ == 0
					       || (_maxopperformed != 0
						   && _maxopperformed < i_24_))
					      ? _maxopperformed : i_24_);
    }
}
