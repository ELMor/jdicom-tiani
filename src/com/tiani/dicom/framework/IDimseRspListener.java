/* IDimseRspListener - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

public interface IDimseRspListener
{
    public void handleRSP
	(DimseExchange dimseexchange, int i, int i_0_,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException;
}
