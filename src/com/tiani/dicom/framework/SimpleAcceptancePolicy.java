/* SimpleAcceptancePolicy - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;

public class SimpleAcceptancePolicy implements IAcceptancePolicy
{
    private String _calledTitle;
    private String[] _callingTitles;
    private int[] _asids;
    private int _maxPduSize;
    private int _maxOpInvoked;
    private int _maxOpPerformed;
    
    public SimpleAcceptancePolicy(String string, String[] strings, int[] is,
				  int i, int i_0_, int i_1_) {
	_calledTitle = string;
	_callingTitles = strings;
	_asids = is;
	_maxPduSize = i;
	_maxOpInvoked = i_0_;
	_maxOpPerformed = i_1_;
    }
    
    public void setCalledTitle(String string) {
	_calledTitle = string;
    }
    
    public void setCallingTitles(String[] strings) {
	_callingTitles = strings;
    }
    
    public void setAbstractSyntaxes(int[] is) {
	_asids = is;
    }
    
    public void setMaxPduSize(int i) {
	_maxPduSize = i;
    }
    
    public void setMaxOpInvoked(int i) {
	_maxOpInvoked = i;
    }
    
    public void setMaxOpPerformed(int i) {
	_maxOpPerformed = i;
    }
    
    public String getCalledTitle() {
	return _calledTitle;
    }
    
    public String[] getCallingTitles() {
	return _callingTitles;
    }
    
    public int[] getAbstractSyntaxes() {
	return _asids;
    }
    
    public int getMaxPduSize() {
	return _maxPduSize;
    }
    
    public int getMaxOpInvoked() {
	return _maxOpInvoked;
    }
    
    public int getMaxOpPerformed() {
	return _maxOpPerformed;
    }
    
    public Response prepareResponse
	(VerboseAssociation verboseassociation, Request request)
	throws IllegalValueException {
	Response response
	    = ResponsePolicy.prepareResponse(request, _calledTitle,
					     _callingTitles, _asids, 8193,
					     true);
	if (!(response instanceof Acknowledge))
	    return response;
	Acknowledge acknowledge = (Acknowledge) response;
	acknowledge.setMaxPduSize(_maxPduSize);
	int i = request.getMaxOperationsInvoked();
	int i_2_ = request.getMaxOperationsPerformed();
	int i_3_ = (i == 0 || _maxOpInvoked != 0 && i > _maxOpInvoked
		    ? _maxOpInvoked : i);
	int i_4_ = (i_2_ == 0 || _maxOpPerformed != 0 && i_2_ > _maxOpPerformed
		    ? _maxOpPerformed : i_2_);
	acknowledge.setMaxOperationsInvoked(i_3_);
	acknowledge.setMaxOperationsPerformed(i_4_);
	return acknowledge;
    }
}
