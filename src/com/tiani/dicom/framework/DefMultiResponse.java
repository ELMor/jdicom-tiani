/* DefMultiResponse - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.util.Enumeration;

import com.archimed.dicom.DicomObject;

public class DefMultiResponse implements IMultiResponse
{
    private Enumeration enum;
    private boolean canceled = false;
    
    public DefMultiResponse(Enumeration enumeration) {
	enum = enumeration;
    }
    
    public int nextResponse(DimseExchange dimseexchange,
			    DicomMessage dicommessage) {
	if (!enum.hasMoreElements())
	    return 0;
	if (canceled)
	    return 65024;
	dicommessage.setDataset((DicomObject) enum.nextElement());
	return 65280;
    }
    
    public void cancel() {
	canceled = true;
    }
}
