/* DicomMessage - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UIDEntry;

public final class DicomMessage extends DicomObject
{
    public static final int CSTORERQ = 1;
    public static final int CSTORERSP = 32769;
    public static final int CGETRQ = 16;
    public static final int CGETRSP = 32784;
    public static final int CFINDRQ = 32;
    public static final int CFINDRSP = 32800;
    public static final int CMOVERQ = 33;
    public static final int CMOVERSP = 32801;
    public static final int CECHORQ = 48;
    public static final int CECHORSP = 32816;
    public static final int NEVENTREPORTRQ = 256;
    public static final int NEVENTREPORTRSP = 33024;
    public static final int NGETRQ = 272;
    public static final int NGETRSP = 33040;
    public static final int NSETRQ = 288;
    public static final int NSETRSP = 33056;
    public static final int NACTIONRQ = 304;
    public static final int NACTIONRSP = 33072;
    public static final int NCREATERQ = 320;
    public static final int NCREATERSP = 33088;
    public static final int NDELETERQ = 336;
    public static final int NDELETERSP = 33104;
    public static final int CCANCELRQ = 4095;
    public static final int LOW = 2;
    public static final int MEDIUM = 0;
    public static final int HIGH = 1;
    public static final int NODATASET = 257;
    public static final int YESDATASET = 65278;
    public static final int SOPCLASSNOTSUPPORTED = 290;
    private byte _pcid = 0;
    private UIDEntry _asuid = null;
    private DicomObject _dataset = null;
    
    public DicomMessage(byte i, UIDEntry uidentry, DicomObject dicomobject,
			DicomObject dicomobject_0_) {
	_pcid = i;
	_asuid = uidentry;
	this.addGroups(dicomobject);
	_dataset = dicomobject_0_;
    }
    
    public DicomMessage(byte i, UIDEntry uidentry, int i_1_, int i_2_,
			DicomObject dicomobject) throws DicomException {
	_pcid = i;
	_asuid = uidentry;
	this.set(3, new Integer(i_1_));
	this.set((i_1_ & 0x8e00) == 0 ? 4 : 5, new Integer(i_2_));
	setDataset(dicomobject);
    }
    
    public DicomMessage
	(byte i, int i_3_, int i_4_, DicomObject dicomobject)
	throws DicomException {
	this(i, null, i_3_, i_4_, dicomobject);
    }
    
    public DicomMessage(int i, int i_5_, DicomObject dicomobject)
	throws DicomException {
	this((byte) 0, null, i, i_5_, dicomobject);
    }
    
    public void setDataset(DicomObject dicomobject) {
	try {
	    this.set(8, new Integer((_dataset = dicomobject) == null ? 257
				    : 65278));
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.getMessage());
	}
    }
    
    public byte getPresentationContext() {
	return _pcid;
    }
    
    public UIDEntry getAbstractSyntax() {
	return _asuid;
    }
    
    public DicomObject getDataset() {
	return _dataset;
    }
    
    public void affectedSOPclassUID(String string) throws DicomException {
	this.set(1, string);
    }
    
    public void affectedSOP(String string, String string_6_)
	throws DicomException {
	this.set(1, string);
	this.set(13, string_6_);
    }
    
    public void requestedSOP(String string, String string_7_)
	throws DicomException {
	this.set(2, string);
	this.set(14, string_7_);
    }
    
    public void priority(int i) throws DicomException {
	this.set(7, new Integer(i));
    }
    
    public void eventTypeID(int i) throws DicomException {
	this.set(15, new Integer(i));
    }
    
    public void actionTypeID(int i) throws DicomException {
	this.set(18, new Integer(i));
    }
    
    public void status(int i) throws DicomException {
	this.set(9, new Integer(i));
    }
    
    public void moveDestination(String string) throws DicomException {
	this.set(6, string);
    }
    
    public void attributeIdList(int[] is) throws DicomException {
	for (int i = 0; i < is.length; i++)
	    this.append(16, new Integer(is[i]));
    }
    
    public void moveOriginator(String string, int i) throws DicomException {
	this.set(23, string);
	this.set(24, new Integer(i));
    }
}
