/* Status - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.util.Hashtable;

import com.archimed.dicom.DicomException;

public class Status
{
    private static Hashtable defaultTab = new Hashtable();
    private static Hashtable specificTabs = new Hashtable();
    
    private static int maskBits(int i) {
	int i_0_;
	switch (i_0_ = i & 0xff00) {
	case 42752:
	case 43264:
	    return i_0_;
	default:
	    switch (i_0_ = i & 0xf000) {
	    case 49152:
		return i_0_;
	    default:
		return 0;
	    }
	}
    }
    
    public static boolean isPending(int i) {
	return i == 65280 || i == 65281;
    }
    
    public static StatusEntry getStatusEntry(int i) {
	StatusEntry statusentry = getStatusEntry(defaultTab, i);
	if (statusentry == null)
	    statusentry = new StatusEntry(i, 0, "?");
	return statusentry;
    }
    
    private static StatusEntry getStatusEntry(Hashtable hashtable, int i) {
	StatusEntry statusentry = (StatusEntry) hashtable.get(new Integer(i));
	if (statusentry == null && (i = maskBits(i)) != 0)
	    statusentry = (StatusEntry) hashtable.get(new Integer(i));
	return statusentry;
    }
    
    public static StatusEntry getStatusEntry(int i, int i_1_)
	throws DicomException {
	StatusEntry statusentry = null;
	Hashtable hashtable = (Hashtable) specificTabs.get(new Integer(i));
	if (hashtable != null)
	    statusentry = getStatusEntry(hashtable, i_1_);
	if (statusentry == null)
	    statusentry = getStatusEntry(i_1_);
	return statusentry;
    }
    
    public static String toString(int i) {
	String string = Integer.toHexString(i);
	return "status #" + "0000".substring(string.length()) + string + "H";
    }
    
    static {
	defaultTab.put(new Integer(0), new StatusEntry(0, 1, "Success"));
	defaultTab.put
	    (new Integer(1),
	     (new StatusEntry
	      (1, 1,
	       "Success: Partial Study Content exists on system supporting SCP")));
	defaultTab.put
	    (new Integer(2),
	     (new StatusEntry
	      (2, 1,
	       "Success: None of the Study Content exists on system supporting SCP")));
	defaultTab.put
	    (new Integer(3),
	     (new StatusEntry
	      (3, 1,
	       "Success: It is StatusEntry.UNKNOWN whether or not study content exists on system supporting SCP")));
	defaultTab.put(new Integer(261),
		       new StatusEntry(261, 5, "Failure: No such attribute"));
	defaultTab.put(new Integer(262),
		       new StatusEntry(262, 5,
				       "Failure: Invalid attribute value"));
	defaultTab.put(new Integer(263),
		       new StatusEntry(263, 4,
				       "Warning: Attribute list error"));
	defaultTab.put
	    (new Integer(272),
	     new StatusEntry(272, 5,
			     "Failure: Processing StatusEntry.FAILURE"));
	defaultTab.put(new Integer(273),
		       new StatusEntry(273, 5,
				       "Failure: Duplicate SOP instance"));
	defaultTab.put(new Integer(274),
		       new StatusEntry(274, 5,
				       "Failure: No such object instance"));
	defaultTab.put(new Integer(275),
		       new StatusEntry(275, 5, "Failure: No such event type"));
	defaultTab.put(new Integer(276),
		       new StatusEntry(276, 5, "Failure: No such argument"));
	defaultTab.put(new Integer(277),
		       new StatusEntry(277, 5,
				       "Failure: Invalid argument value"));
	defaultTab.put
	    (new Integer(278),
	     new StatusEntry(278, 4, "Warning: Attribute Value Out of Range"));
	defaultTab.put(new Integer(279),
		       new StatusEntry(279, 5,
				       "Failure: Invalid object instance"));
	defaultTab.put(new Integer(280),
		       new StatusEntry(280, 5, "Failure: No such SOP class"));
	defaultTab.put(new Integer(281),
		       new StatusEntry(281, 5,
				       "Failure: Class-instance conflict"));
	defaultTab.put(new Integer(288),
		       new StatusEntry(288, 5, "Failure: Missing attribute"));
	defaultTab.put(new Integer(289),
		       new StatusEntry(289, 5,
				       "Failure: Missing attribute value"));
	defaultTab.put(new Integer(290),
		       new StatusEntry(290, 5,
				       "Refused: SOP class not supported"));
	defaultTab.put(new Integer(528),
		       new StatusEntry(528, 5,
				       "Failure: Duplicate invocation"));
	defaultTab.put(new Integer(529),
		       new StatusEntry(529, 5,
				       "Failure: Unrecognized operation"));
	defaultTab.put(new Integer(530),
		       new StatusEntry(530, 5, "Failure: Mistyped argument"));
	defaultTab.put(new Integer(531),
		       new StatusEntry(531, 5,
				       "Failure: Resource limitation"));
	defaultTab.put(new Integer(42752),
		       new StatusEntry(42752, 5, "Refused: Out of Resources"));
	defaultTab.put
	    (new Integer(43264),
	     new StatusEntry(43264, 5,
			     "Failure: Data Set does not match SOP Class"));
	defaultTab.put(new Integer(45056),
		       new StatusEntry(45056, 4,
				       "Warning: Coercion of Data Elements"));
	defaultTab.put(new Integer(45062),
		       new StatusEntry(45062, 4,
				       "Warning: Elements Discarded"));
	defaultTab.put
	    (new Integer(45063),
	     new StatusEntry(45063, 4,
			     "Warning: Data Set does not match SOP Class"));
	defaultTab.put
	    (new Integer(46592),
	     new StatusEntry(46592, 4,
			     "Warning: Memory allocation not supported"));
	defaultTab.put
	    (new Integer(46593),
	     (new StatusEntry
	      (46593, 4,
	       "Warning: Film session printing (collation) is not supported")));
	defaultTab.put
	    (new Integer(46594),
	     (new StatusEntry
	      (46594, 4,
	       "Warning: Film Session SOP Instance hierarchy does not contain Image Box SOP Instances (empty page)")));
	defaultTab.put
	    (new Integer(46595),
	     (new StatusEntry
	      (46595, 4,
	       "Warning: Film Box SOP Instance hierarchy does not contain Image Box SOP Instances (empty page)")));
	defaultTab.put
	    (new Integer(46596),
	     (new StatusEntry
	      (46596, 4,
	       "Warning: Image size is larger than image box size, the image has been demagnified")));
	defaultTab.put
	    (new Integer(46597),
	     (new StatusEntry
	      (46597, 4,
	       "Warning: Requested Min Density or Max Density outside of printers operating range")));
	defaultTab.put
	    (new Integer(46601),
	     (new StatusEntry
	      (46601, 4,
	       "Warning: Image size is larger than the Image Box size. The Image has been cropped to fit")));
	defaultTab.put
	    (new Integer(46602),
	     (new StatusEntry
	      (46602, 4,
	       "Warning: Image size is larger than the Image Box size. Image or Combined Print Image has been decimated to fit")));
	defaultTab.put
	    (new Integer(50688),
	     (new StatusEntry
	      (50688, 5,
	       "Failure: Film Session SOP Instance hierarchy does not contain Film Box SOP Instances")));
	defaultTab.put
	    (new Integer(50689),
	     (new StatusEntry
	      (50689, 5,
	       "Failure: Unable to create Print Job SOP Instance; print queue is full")));
	defaultTab.put
	    (new Integer(50690),
	     (new StatusEntry
	      (50690, 5,
	       "Failure: Unable to create Print Job SOP Instance; print queue is full")));
	defaultTab.put
	    (new Integer(50691),
	     (new StatusEntry
	      (50691, 5,
	       "Failure: Image size is larger than image box size")));
	defaultTab.put(new Integer(49152),
		       new StatusEntry(49152, 5,
				       "Failure: Cannot understand"));
	defaultTab.put
	    (new Integer(50693),
	     (new StatusEntry
	      (50693, 5,
	       "Failure: Insufficient memory in printer to store the image")));
	defaultTab.put
	    (new Integer(50707),
	     (new StatusEntry
	      (50707, 5,
	       "Failure: Combined Print Image size is larger than the Image Box size")));
	defaultTab.put
	    (new Integer(50710),
	     (new StatusEntry
	      (50710, 5,
	       "Failure: There is an existing Film Box that has not been printed and N-ACTION at the Film Session level is not supported")));
	defaultTab.put(new Integer(65024),
		       new StatusEntry(65024, 3, "StatusEntry.CANCEL"));
	defaultTab.put(new Integer(65280),
		       new StatusEntry(65280, 2, "StatusEntry.PENDING"));
	defaultTab.put
	    (new Integer(65281),
	     (new StatusEntry
	      (65281, 2,
	       "StatusEntry.PENDING - one or more Optional Keys were not supported")));
	Hashtable hashtable = new Hashtable(11);
	hashtable.put
	    (new Integer(42753),
	     (new StatusEntry
	      (42753, 5,
	       "Refused: Out of Resources - Unable to calculate number of matches")));
	hashtable.put
	    (new Integer(42754),
	     (new StatusEntry
	      (42754, 5,
	       "Refused: Out of Resources - Unable to perform sub-operations")));
	hashtable.put
	    (new Integer(43009),
	     new StatusEntry(43009, 5,
			     "Refused: Move Destination StatusEntry.UNKNOWN"));
	hashtable.put
	    (new Integer(43264),
	     new StatusEntry(43264, 5,
			     "Failure: Identifier does not match SOP Class"));
	hashtable.put
	    (new Integer(45056),
	     (new StatusEntry
	      (45056, 4,
	       "Warning: Sub-operations Complete - One or more Failures")));
	hashtable.put(new Integer(49152),
		      new StatusEntry(49152, 5, "Failure: Unable to process"));
	specificTabs.put(new Integer(4132), hashtable);
	specificTabs.put(new Integer(4133), hashtable);
	specificTabs.put(new Integer(4134), hashtable);
	specificTabs.put(new Integer(4135), hashtable);
	specificTabs.put(new Integer(4136), hashtable);
	specificTabs.put(new Integer(4137), hashtable);
	specificTabs.put(new Integer(4138), hashtable);
	specificTabs.put(new Integer(4139), hashtable);
	specificTabs.put(new Integer(4140), hashtable);
	Hashtable hashtable_2_ = new Hashtable(31);
	hashtable_2_.put
	    (new Integer(46596),
	     (new StatusEntry
	      (46596, 4,
	       "Warning: Annotation Box not supported, image printed without annotation")));
	hashtable_2_.put
	    (new Integer(46597),
	     (new StatusEntry
	      (46597, 4,
	       "Warning: Image Overlay Box not supported, image printed without overlay")));
	hashtable_2_.put
	    (new Integer(46598),
	     (new StatusEntry
	      (46598, 4,
	       "Warning: Presentation LUT not supported, image printed without applying any Presentation LUT")));
	hashtable_2_.put
	    (new Integer(46600),
	     (new StatusEntry
	      (46600, 4,
	       "Warning: Presentation LUT not supported at Image Box level, image printed with Film Box Presentation LUT")));
	hashtable_2_.put
	    (new Integer(50688),
	     (new StatusEntry
	      (50688, 5,
	       "Failure: Stored Print Storage SOP Instance does not contain Film Boxes")));
	hashtable_2_.put
	    (new Integer(50695),
	     (new StatusEntry
	      (50695, 5,
	       "Failure: Stored Print Storage SOP Instance not available from Retrieve AE")));
	hashtable_2_.put
	    (new Integer(50696),
	     (new StatusEntry
	      (50696, 5,
	       "Failure: Image SOP Instance not available from Retrieve AE")));
	hashtable_2_.put
	    (new Integer(50697),
	     (new StatusEntry
	      (50697, 5,
	       "Failure: StatusEntry.FAILURE in retrieving Stored Print Storage SOP Instance")));
	hashtable_2_.put
	    (new Integer(50698),
	     (new StatusEntry
	      (50698, 5,
	       "Failure: StatusEntry.FAILURE in retrieving Image SOP Instance")));
	hashtable_2_.put(new Integer(50699),
			 (new StatusEntry
			  (50699, 5,
			   "Failure: StatusEntry.UNKNOWN Retrieve AE title")));
	hashtable_2_.put
	    (new Integer(50700),
	     (new StatusEntry
	      (50700, 5,
	       "Failure: Print request rejected because printer cannot handle color image")));
	hashtable_2_.put
	    (new Integer(50701),
	     (new StatusEntry
	      (50701, 5,
	       "Failure: Stored Print Storage SOP Instance does not contain Image Boxes (empty page)")));
	hashtable_2_.put
	    (new Integer(50702),
	     new StatusEntry(50702, 5,
			     "Failure: Annotation Box not supported"));
	hashtable_2_.put
	    (new Integer(50703),
	     new StatusEntry(50703, 5,
			     "Failure: Image Overlay Box not supported"));
	hashtable_2_.put
	    (new Integer(50704),
	     new StatusEntry(50704, 5,
			     "Failure: Presentation LUT not supported"));
	hashtable_2_.put
	    (new Integer(50707),
	     (new StatusEntry
	      (50707, 5,
	       "Failure: Combined Print Image size is larger than the Image Box size")));
	hashtable_2_.put
	    (new Integer(50708),
	     (new StatusEntry
	      (50708, 5,
	       "Failure: Presentation LUT not supported at Image Box level")));
	hashtable_2_.put
	    (new Integer(50709),
	     (new StatusEntry
	      (50709, 5,
	       "Failure: Unable to establish an Association with the Retrieve AE")));
	specificTabs.put(new Integer(4150), hashtable_2_);
	Hashtable hashtable_3_ = new Hashtable(3);
	hashtable_3_.put
	    (new Integer(50710),
	     (new StatusEntry
	      (50710, 5,
	       "Failure: Combined Print Image requires cropping to fit Image Box. This is not supported in this SOP Class")));
	specificTabs.put(new Integer(4160), hashtable_3_);
    }
}
