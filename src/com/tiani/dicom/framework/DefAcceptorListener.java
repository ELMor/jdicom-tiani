/* DefAcceptorListener - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;

public class DefAcceptorListener implements IAcceptorListener
{
    private boolean _startThread;
    private int _artim;
    private DimseRqManager _rqManager;
    private boolean _queueRQ;
    private boolean _queueRSP;
    
    public DefAcceptorListener(boolean bool, DimseRqManager dimserqmanager,
			       boolean bool_0_, boolean bool_1_) {
	_startThread = bool;
	_rqManager = dimserqmanager;
	_queueRQ = bool_0_;
	_queueRSP = bool_1_;
    }
    
    public void setARTIM(int i) {
	_artim = i;
    }
    
    public void setStartThread(boolean bool) {
	_startThread = bool;
    }
    
    public boolean isStartThread() {
	return _startThread;
    }
    
    public void setRqManager(DimseRqManager dimserqmanager) {
	_rqManager = dimserqmanager;
    }
    
    public DimseRqManager getRqManager() {
	return _rqManager;
    }
    
    public void setQueueRQ(boolean bool) {
	_queueRQ = bool;
    }
    
    public boolean isQueueRQ() {
	return _queueRQ;
    }
    
    public void setQueueRSP(boolean bool) {
	_queueRSP = bool;
    }
    
    public boolean isQueueRSP() {
	return _queueRSP;
    }
    
    public void handle
	(Acknowledge acknowledge, VerboseAssociation verboseassociation)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	DimseExchange dimseexchange
	    = new DimseExchange(verboseassociation, _rqManager, _queueRQ,
				_queueRSP,
				acknowledge.getMaxOperationsPerformed());
	dimseexchange.setARTIM(_artim);
	if (_startThread) {
	    if (Debug.DEBUG > 1)
		Debug.out.println
		    ("jdicom: " + verboseassociation.remoteAET()
		     + " Start new Thread for DimseExchange.run()");
	    Thread thread = new Thread(dimseexchange);
	    thread.start();
	} else
	    dimseexchange.run();
    }
}
