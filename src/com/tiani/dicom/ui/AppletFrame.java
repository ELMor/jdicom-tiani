/* AppletFrame - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.applet.AudioClip;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;

import javax.swing.JFrame;

public class AppletFrame extends JFrame implements AppletStub, AppletContext
{
    public AppletFrame(String string, Applet applet, int i, int i_0_) {
	this.setTitle(string);
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	Dimension dimension = toolkit.getScreenSize();
	this.setLocation((dimension.width - i) / 2,
			 (dimension.height - i_0_) / 2);
	this.setSize(i, i_0_);
	this.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent windowevent) {
		System.exit(0);
	    }
	});
	Container container = this.getContentPane();
	container.add(applet);
	applet.setStub(this);
	applet.init();
	this.show();
	applet.start();
    }
    
    public AppletFrame(Applet applet, int i, int i_2_) {
	this.setTitle(applet.getClass().getName());
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	Dimension dimension = toolkit.getScreenSize();
	this.setLocation((dimension.width - i) / 2,
			 (dimension.height - i_2_) / 2);
	this.setSize(i, i_2_);
	this.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent windowevent) {
		System.exit(0);
	    }
	});
	Container container = this.getContentPane();
	container.add(applet);
	applet.setStub(this);
	applet.init();
	this.show();
	applet.start();
    }
    
    public AppletFrame(String string, Applet applet, int i, int i_4_,
		       WindowListener windowlistener) {
	this.setTitle(string);
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	Dimension dimension = toolkit.getScreenSize();
	this.setLocation((dimension.width - i) / 2,
			 (dimension.height - i_4_) / 2);
	this.setSize(i, i_4_);
	this.addWindowListener(windowlistener);
	Container container = this.getContentPane();
	container.add(applet);
	applet.setStub(this);
	applet.init();
	this.show();
	applet.start();
    }
    
    public boolean isActive() {
	return true;
    }
    
    public URL getDocumentBase() {
	return null;
    }
    
    public URL getCodeBase() {
	return null;
    }
    
    public String getParameter(String string) {
	return "";
    }
    
    public AppletContext getAppletContext() {
	return this;
    }
    
    public void appletResize(int i, int i_5_) {
	/* empty */
    }
    
    public AudioClip getAudioClip(URL url) {
	return null;
    }
    
    public Image getImage(URL url) {
	return null;
    }
    
    public Applet getApplet(String string) {
	return null;
    }
    
    public Enumeration getApplets() {
	return null;
    }
    
    public void showDocument(URL url) {
	/* empty */
    }
    
    public void showDocument(URL url, String string) {
	/* empty */
    }
    
    public void showStatus(String string) {
	/* empty */
    }
    
    public void setStream(String string, InputStream inputstream) {
	/* empty */
    }
    
    public InputStream getStream(String string) {
	return null;
    }
    
    public Iterator getStreamKeys() {
	return null;
    }
}
