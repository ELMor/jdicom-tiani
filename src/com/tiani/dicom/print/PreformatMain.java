/* PreformatMain - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.print;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.media.FileMetaInformation;

public class PreformatMain
{
    private static final String USAGE
	= "Usage: PreformatMain <src> <dest> <bitDepth> [<pLUT>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n<bitDepth>   maximal number of stored bits of resulting pixel data\n<pLUT>       file path of Presentation LUT to apply on image";
    
    public static void main(String[] strings) {
	if (strings.length < 3)
	    System.out.println
		("Usage: PreformatMain <src> <dest> <bitDepth> [<pLUT>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n<bitDepth>   maximal number of stored bits of resulting pixel data\n<pLUT>       file path of Presentation LUT to apply on image");
	else {
	    try {
		Debug.DEBUG = 3;
		DicomObject dicomobject = load(strings[0]);
		int i = Integer.parseInt(strings[2]);
		DicomObject dicomobject_0_
		    = strings.length > 3 ? load(strings[3]) : null;
		DicomObject dicomobject_1_
		    = PrintManagementUtils.preformatGrayscale(dicomobject,
							      dicomobject_0_,
							      i, 0);
		DicomObject dicomobject_2_
		    = PrintManagementUtils
			  .getPixelModule(dicomobject_1_.removeGroup(40));
		dicomobject_1_.addGroups(dicomobject_2_);
		save(strings[1], dicomobject_1_);
	    } catch (Throwable throwable) {
		throwable.printStackTrace(System.out);
	    }
	}
    }
    
    private static DicomObject load(String string)
	throws IOException, DicomException {
	DicomObject dicomobject = new DicomObject();
	FileInputStream fileinputstream = new FileInputStream(string);
	TianiInputStream tianiinputstream
	    = new TianiInputStream(fileinputstream);
	try {
	    tianiinputstream.read(dicomobject);
	} finally {
	    tianiinputstream.close();
	}
	return dicomobject;
    }
    
    private static void save(String string, DicomObject dicomobject)
	throws IOException, DicomException {
	FileOutputStream fileoutputstream = new FileOutputStream(string);
	dicomobject.setFileMetaInformation
	    (new FileMetaInformation(dicomobject, "1.2.840.10008.1.2.1"));
	try {
	    dicomobject.write(fileoutputstream, true, 8194, false);
	} finally {
	    fileoutputstream.close();
	}
    }
}
