/* PrintManagementUtils - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.print;
import java.util.Properties;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.tiani.dicom.util.EnumPMI;
import com.tiani.dicom.util.LUT;
import com.tiani.dicom.util.LUTFactory;
import com.tiani.dicom.util.PixelMatrix;

public class PrintManagementUtils
{
    private static final int[] PLUTMODULE_DNAMES = { 1239, 1240 };
    private static final int[] PIXELMODULE_DNAMES
	= { 461, 462, 466, 467, 473, 475, 476, 477, 478, 1184 };
    public static final int INFLATE_ALWAYS = 0;
    public static final int INFLATE_IF_NONLINEAR = 1;
    public static final int INFLATE_NEVER = 2;
    
    private PrintManagementUtils() {
	/* empty */
    }
    
    private static void set(DicomObject dicomobject, int i, String string)
	throws DicomException {
	if (string != null && string.length() > 0)
	    dicomobject.set(i, string);
    }
    
    public static DicomObject createGammaPresentationLUT(int i, int i_0_,
							 double d) {
	try {
	    DicomObject dicomobject = new DicomObject();
	    dicomobject.set(1239, createGammaLUTSequenceItem(i, i_0_, d));
	    return dicomobject;
	} catch (DicomException dicomexception) {
	    dicomexception.printStackTrace(Debug.out);
	    throw new RuntimeException(dicomexception.getMessage());
	}
    }
    
    public static DicomObject createGammaLUTSequenceItem(int i, int i_1_,
							 double d) {
	if (i <= 0 || i > 65535)
	    throw new IllegalArgumentException("Illegal value for LUT len "
					       + i);
	if (i_1_ < 8 || i_1_ > 16)
	    throw new IllegalArgumentException
		      ("Illegal value for LUT bitDepth " + i_1_);
	if (d < 0.1 || d > 10.0)
	    throw new IllegalArgumentException("Illegal value for LUT gamma "
					       + d);
	int i_2_ = (1 << i_1_) - 1;
	double d_3_ = 1.0 / d;
	double d_4_ = (double) (1 << i_1_) / Math.pow((double) (i - 1), d_3_);
	try {
	    DicomObject dicomobject = new DicomObject();
	    byte[] is = new byte[i << 1];
	    int i_5_ = 0;
	    for (int i_6_ = 0; i_6_ < i; i_6_++) {
		int i_7_ = Math.min(i_2_, (int) (d_4_ * Math.pow((double) i_6_,
								 d_3_)));
		is[i_5_++] = (byte) i_7_;
		is[i_5_++] = (byte) (i_7_ >> 8);
	    }
	    dicomobject.set(509, is);
	    dicomobject.set(506, new Integer(i), 0);
	    dicomobject.set(506, new Integer(0), 1);
	    dicomobject.set(506, new Integer(i_1_), 2);
	    dicomobject.set(507, "Gamma [" + d + "] correction LUT");
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: Generate Gamma LUT: len=" + i
				  + ", bitDepth=" + i_1_ + ", gamma=" + d);
	    return dicomobject;
	} catch (DicomException dicomexception) {
	    dicomexception.printStackTrace(Debug.out);
	    throw new RuntimeException(dicomexception.getMessage());
	}
    }
    
    public static DicomObject preformatGrayscale
	(DicomObject dicomobject, DicomObject dicomobject_8_, int i, int i_9_,
	 boolean bool)
	throws DicomException, IllegalValueException {
	if (i < 8 || i > 16)
	    throw new IllegalArgumentException("Illegal bitDepth - " + i);
	String string = (String) dicomobject.get(462);
	int i_10_ = EnumPMI.getConstant(string);
	if (!EnumPMI.isGrayscale(i_10_))
	    throw new IllegalArgumentException
		      ("Cannot preformat images with Photometric Interpretation - "
		       + string);
	boolean bool_11_ = i_10_ == 0;
	int i_12_ = dicomobject.getI(461);
	if (i_12_ != 1)
	    throw new DicomException("Illegal Samples Per Pixel - " + i_12_);
	int i_13_ = dicomobject.getI(475);
	if (i_13_ != 8 && i_13_ != 16)
	    throw new IllegalArgumentException("Cannot preformat images with "
					       + i_13_ + " bits allocated");
	int i_14_ = Math.min(i, i_13_);
	if (i_9_ == 0
	    || i_9_ == 1 && (dicomobject.getSize(505) > 0
			     || dicomobject.getSize(510) > 0
			     || (dicomobject_8_ != null
				 && dicomobject_8_.getSize(1239) > 0)))
	    i_14_ = i;
	PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
	if (Debug.DEBUG > 1)
	    Debug.out.println("jdicom: " + string + "-" + pixelmatrix);
	boolean bool_15_ = LUTFactory.isXRaySOPClass(dicomobject);
	if (dicomobject_8_ == null && bool && dicomobject.getSize(510) <= 0
	    && (dicomobject.getSize(487) <= 0
		|| dicomobject.getSize(488) <= 0)) {
	    DicomObject dicomobject_16_ = null;
	    Float var_float = null;
	    Float var_float_17_ = null;
	    if (!bool_15_) {
		dicomobject_16_ = (DicomObject) dicomobject.get(505);
		var_float = (Float) dicomobject.get(490);
		var_float_17_ = (Float) dicomobject.get(489);
	    }
	    int[] is = (dicomobject_16_ != null
			? LUTFactory.calcMinMax(dicomobject_16_)
			: pixelmatrix.calcMinMax());
	    float f = (float) (is[1] + is[0]) / 2.0F;
	    float f_18_ = (float) (is[1] - is[0]);
	    if (var_float != null && var_float_17_ != null) {
		f = f * var_float.floatValue() + var_float_17_.floatValue();
		f_18_ *= var_float.floatValue();
	    }
	    dicomobject.set(487, new Float(f));
	    dicomobject.set(488, new Float(f_18_));
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: Generate VOI LUT: c=" + f + ", w="
				  + f_18_);
	}
	int i_19_ = (1 << i_14_) - 1;
	int i_20_ = pixelmatrix.getNumberOfSamples();
	byte[] is;
	if (i_13_ > 8) {
	    if (i_14_ > 8) {
		LUT.Short var_short
		    = LUTFactory.createShortLUT(pixelmatrix.getMinVal(),
						pixelmatrix.getMaxVal(), i_19_,
						bool_11_, dicomobject,
						dicomobject, dicomobject_8_);
		if (Debug.DEBUG > 1)
		    Debug.out.println("jdicom: Apply Total LUT:" + var_short);
		is = new byte[2 * i_20_];
		int i_21_ = 0;
		if (pixelmatrix instanceof PixelMatrix.UnsignedShort) {
		    PixelMatrix.UnsignedShort unsignedshort
			= (PixelMatrix.UnsignedShort) pixelmatrix;
		    for (int i_22_ = 0; i_22_ < i_20_; i_22_++) {
			int i_23_
			    = var_short
				  .lookupShort(unsignedshort.getSample(i_22_));
			is[i_21_++] = (byte) i_23_;
			is[i_21_++] = (byte) (i_23_ >> 8);
		    }
		} else {
		    PixelMatrix.SignedShort signedshort
			= (PixelMatrix.SignedShort) pixelmatrix;
		    for (int i_24_ = 0; i_24_ < i_20_; i_24_++) {
			int i_25_
			    = var_short
				  .lookupShort(signedshort.getSample(i_24_));
			is[i_21_++] = (byte) i_25_;
			is[i_21_++] = (byte) (i_25_ >> 8);
		    }
		}
		dicomobject.set(475, new Integer(16));
	    } else {
		LUT.Byte1 byte1
		    = LUTFactory.createByteLUT(pixelmatrix.getMinVal(),
					       pixelmatrix.getMaxVal(), i_19_,
					       bool_11_,
					       bool_15_ ? null : dicomobject,
					       dicomobject, dicomobject_8_);
		if (Debug.DEBUG > 1)
		    Debug.out.println("jdicom: Apply Total LUT:" + byte1);
		is = new byte[i_20_];
		if (pixelmatrix instanceof PixelMatrix.UnsignedShort) {
		    PixelMatrix.UnsignedShort unsignedshort
			= (PixelMatrix.UnsignedShort) pixelmatrix;
		    for (int i_26_ = 0; i_26_ < i_20_; i_26_++)
			is[i_26_]
			    = byte1.lookupByte(unsignedshort.getSample(i_26_));
		} else {
		    PixelMatrix.SignedShort signedshort
			= (PixelMatrix.SignedShort) pixelmatrix;
		    for (int i_27_ = 0; i_27_ < i_20_; i_27_++)
			is[i_27_]
			    = byte1.lookupByte(signedshort.getSample(i_27_));
		}
		dicomobject.set(475, new Integer(8));
	    }
	} else if (i_14_ > 8) {
	    LUT.Short var_short
		= LUTFactory.createShortLUT(pixelmatrix.getMinVal(),
					    pixelmatrix.getMaxVal(), i_19_,
					    bool_11_, dicomobject, dicomobject,
					    dicomobject_8_);
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: Apply Total LUT:" + var_short);
	    is = new byte[2 * i_20_];
	    int i_28_ = 0;
	    if (pixelmatrix instanceof PixelMatrix.UnsignedByte) {
		PixelMatrix.UnsignedByte unsignedbyte
		    = (PixelMatrix.UnsignedByte) pixelmatrix;
		for (int i_29_ = 0; i_29_ < i_20_; i_29_++) {
		    int i_30_
			= var_short.lookupShort(unsignedbyte.getSample(i_29_));
		    is[i_28_++] = (byte) i_30_;
		    is[i_28_++] = (byte) (i_30_ >> 8);
		}
	    } else {
		PixelMatrix.SignedByte signedbyte
		    = (PixelMatrix.SignedByte) pixelmatrix;
		for (int i_31_ = 0; i_31_ < i_20_; i_31_++) {
		    int i_32_
			= var_short.lookupShort(signedbyte.getSample(i_31_));
		    is[i_28_++] = (byte) i_32_;
		    is[i_28_++] = (byte) (i_32_ >> 8);
		}
	    }
	    dicomobject.set(475, new Integer(16));
	} else {
	    LUT.Byte1 byte1
		= LUTFactory.createByteLUT(pixelmatrix.getMinVal(),
					   pixelmatrix.getMaxVal(), i_19_,
					   bool_11_, dicomobject, dicomobject,
					   dicomobject_8_);
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: Apply Total LUT:" + byte1);
	    is = new byte[i_20_];
	    if (pixelmatrix instanceof PixelMatrix.UnsignedByte) {
		PixelMatrix.UnsignedByte unsignedbyte
		    = (PixelMatrix.UnsignedByte) pixelmatrix;
		for (int i_33_ = 0; i_33_ < i_20_; i_33_++)
		    is[i_33_]
			= byte1.lookupByte(unsignedbyte.getSample(i_33_));
	    } else {
		PixelMatrix.SignedByte signedbyte
		    = (PixelMatrix.SignedByte) pixelmatrix;
		for (int i_34_ = 0; i_34_ < i_20_; i_34_++)
		    is[i_34_] = byte1.lookupByte(signedbyte.getSample(i_34_));
	    }
	    dicomobject.set(475, new Integer(8));
	}
	dicomobject.set(462, "MONOCHROME2");
	dicomobject.set(476, new Integer(i_14_));
	dicomobject.set(477, new Integer(i_14_ - 1));
	dicomobject.set(478, new Integer(0));
	dicomobject.set(1184, is);
	return dicomobject;
    }
    
    public static DicomObject preformatGrayscale
	(DicomObject dicomobject, DicomObject dicomobject_35_, int i,
	 int i_36_)
	throws DicomException, IllegalValueException {
	return preformatGrayscale(dicomobject, dicomobject_35_, i, i_36_,
				  true);
    }
    
    public static boolean setPixelAspectRatio
	(DicomObject dicomobject, boolean bool) throws DicomException {
	if (dicomobject.getSize(473) == 2)
	    return false;
	Float var_float;
	Float var_float_37_;
	if (dicomobject.getSize(470) == 2) {
	    var_float = (Float) dicomobject.get(470, 0);
	    var_float_37_ = (Float) dicomobject.get(470, 1);
	} else if (dicomobject.getSize(308) == 2) {
	    var_float = (Float) dicomobject.get(308, 0);
	    var_float_37_ = (Float) dicomobject.get(308, 1);
	} else
	    return false;
	int i = (int) (var_float.floatValue() * 1000.0F);
	int i_38_ = (int) (var_float_37_.floatValue() * 1000.0F);
	if (i == i_38_) {
	    if (!bool)
		return false;
	    i = i_38_ = 1;
	}
	dicomobject.set(473, new Integer(i), 0);
	dicomobject.set(473, new Integer(i_38_), 1);
	return true;
    }
    
    public static boolean setPixelAspectRatio(DicomObject dicomobject)
	throws DicomException {
	return setPixelAspectRatio(dicomobject, true);
    }
    
    public static DicomObject getPixelModule(DicomObject dicomobject)
	throws DicomException {
	DicomObject dicomobject_39_ = new DicomObject();
	copyAttributs(dicomobject, dicomobject_39_, PIXELMODULE_DNAMES);
	if (dicomobject.getI(461) > 1)
	    dicomobject_39_.set(463, dicomobject.get(463));
	return dicomobject_39_;
    }
    
    public static DicomObject getPresentationLUTModule
	(DicomObject dicomobject) throws DicomException {
	DicomObject dicomobject_40_ = new DicomObject();
	copyAttributs(dicomobject, dicomobject_40_, PLUTMODULE_DNAMES);
	return dicomobject_40_;
    }
    
    static void copyAttributs
	(DicomObject dicomobject, DicomObject dicomobject_41_, int[] is)
	throws DicomException {
	for (int i = 0; i < is.length; i++) {
	    int i_43_;
	    int i_42_ = dicomobject.getSize(i_43_ = is[i]);
	    dicomobject_41_.deleteItem(i_43_);
	    for (int i_44_ = 0; i_44_ < i_42_; i_44_++)
		dicomobject_41_.append(i_43_, dicomobject.get(i_43_, i_44_));
	}
    }
    
    public static String getRequestedImageSize
	(DicomObject dicomobject, float f) throws DicomException {
	Float var_float = (Float) dicomobject.get(470, 1);
	if (var_float == null)
	    var_float = (Float) dicomobject.get(308, 1);
	if (var_float != null)
	    return "" + ((float) dicomobject.getI(467) * var_float.floatValue()
			 * f);
	return null;
    }
    
    /**
     * @deprecated
     */
    public static void setFilmSession
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	setFilmSessionCreateAttribs(dicomobject, properties);
    }
    
    public static void setFilmSessionCreateAttribs
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	set(dicomobject, 705,
	    properties.getProperty("Session.NumberOfCopies"));
	set(dicomobject, 706, properties.getProperty("Session.PrintPriority"));
	set(dicomobject, 707, properties.getProperty("Session.MediumType"));
	set(dicomobject, 708,
	    properties.getProperty("Session.FilmDestination"));
	set(dicomobject, 709,
	    properties.getProperty("Session.FilmSessionLabel"));
	set(dicomobject, 710,
	    properties.getProperty("Session.MemoryAllocation"));
	set(dicomobject, 756, properties.getProperty("Session.OwnerID"));
    }
    
    public static void setFilmSessionSetAttribs
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	setFilmSessionCreateAttribs(dicomobject, properties);
    }
    
    /**
     * @deprecated
     */
    public static void setFilmBox
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	setFilmBoxCreateAttribs(dicomobject, properties);
    }
    
    public static void setFilmBoxCreateAttribs
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	set(dicomobject, 713,
	    properties.getProperty("FilmBox.ImageDisplayFormat"));
	set(dicomobject, 715,
	    properties.getProperty("FilmBox.FilmOrientation"));
	set(dicomobject, 716, properties.getProperty("FilmBox.FilmSizeID"));
	set(dicomobject, 1363,
	    properties.getProperty("FilmBox.RequestedResolutionID"));
	set(dicomobject, 714,
	    properties.getProperty("FilmBox.AnnotationDisplayFormatID"));
	setFilmBoxSetAttribs(dicomobject, properties);
    }
    
    public static void setFilmBoxSetAttribs
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	set(dicomobject, 717,
	    properties.getProperty("FilmBox.MagnificationType"));
	set(dicomobject, 718, properties.getProperty("FilmBox.SmoothingType"));
	set(dicomobject, 719, properties.getProperty("FilmBox.BorderDensity"));
	set(dicomobject, 720,
	    properties.getProperty("FilmBox.EmptyImageDensity"));
	set(dicomobject, 721, properties.getProperty("FilmBox.MinDensity"));
	set(dicomobject, 722, properties.getProperty("FilmBox.MaxDensity"));
	set(dicomobject, 723, properties.getProperty("FilmBox.Trim"));
	set(dicomobject, 724,
	    properties.getProperty("FilmBox.ConfigurationInformation"));
	set(dicomobject, 1236, properties.getProperty("FilmBox.Illumination"));
	set(dicomobject, 1237,
	    properties.getProperty("FilmBox.ReflectedAmbientLight"));
    }
    
    /**
     * @deprecated
     */
    public static void setImageBox
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	setImageBoxSetAttribs(dicomobject, properties);
    }
    
    public static void setImageBoxSetAttribs
	(DicomObject dicomobject, Properties properties)
	throws DicomException {
	set(dicomobject, 730, properties.getProperty("ImageBox.Polarity"));
	set(dicomobject, 717,
	    properties.getProperty("ImageBox.MagnificationType"));
	set(dicomobject, 718,
	    properties.getProperty("ImageBox.SmoothingType"));
	set(dicomobject, 721, properties.getProperty("ImageBox.MinDensity"));
	set(dicomobject, 722, properties.getProperty("ImageBox.MaxDensity"));
	set(dicomobject, 724,
	    properties.getProperty("ImageBox.ConfigurationInformation"));
	set(dicomobject, 1362,
	    properties.getProperty("ImageBox.RequestedDecimateCropBehavior"));
	set(dicomobject, 731,
	    properties.getProperty("ImageBox.RequestedImageSize"));
    }
    
    public static void addReferencedPresentationLUT
	(DicomObject dicomobject, String string)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject_45_ = new DicomObject();
	dicomobject_45_.set(115, UID.toString(4145));
	dicomobject_45_.set(116, string);
	dicomobject.append(1241, dicomobject_45_);
    }
    
    public static void addReferencedImageOverlayBox
	(DicomObject dicomobject, String string)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject_46_ = new DicomObject();
	dicomobject_46_.set(62, UID.toString(4160));
	dicomobject_46_.set(63, string);
	dicomobject.append(734, dicomobject_46_);
    }
    
    public static DicomObject rescaleLUTtoFitBitDepth
	(DicomObject dicomobject, int i) throws DicomException {
	if (dicomobject.getSize(506) != 3)
	    throw new DicomException("Missing or Invalid LUT Descriptor");
	int i_47_ = 1 << i;
	int i_48_ = dicomobject.getI(506, 0);
	if (i_47_ == i_48_)
	    return dicomobject;
	LUT.Short var_short = (LUT.Short) LUTFactory.createLUT(dicomobject);
	LUT.Short var_short_49_ = (LUT.Short) var_short.rescaleLength(i_47_);
	if (Debug.DEBUG > 1)
	    Debug.out.println("jdicom: Rescaled LUT to Fit Bit Depth: "
			      + var_short_49_);
	dicomobject.set(506, new Integer(i_47_), 0);
	dicomobject.deleteItem(509);
	byte[] is = new byte[i_47_ << 1];
	int i_50_ = 0;
	for (int i_51_ = 0; i_51_ < i_47_; i_51_++) {
	    int i_52_ = var_short_49_.lookupShort(i_51_);
	    is[i_50_++] = (byte) i_52_;
	    is[i_50_++] = (byte) (i_52_ >> 8);
	}
	dicomobject.set(509, is);
	return dicomobject;
    }
    
    public static DicomObject preformatColor(DicomObject dicomobject)
	throws DicomException {
	PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
	if (pixelmatrix.isRGB()) {
	    if (!(pixelmatrix instanceof PixelMatrix.UnsignedByte))
		throw new UnsupportedOperationException("RGB with "
							+ pixelmatrix);
	    return (pixelmatrix.isColorByPlane() ? dicomobject
		    : fromPixelToPlane(dicomobject));
	}
	if (!pixelmatrix.isPaletteColor())
	    throw new IllegalArgumentException
		      ("Photometric Interpretation "
		       + pixelmatrix.getPhotometricInterpretation());
	byte[] is = new byte[pixelmatrix.getPixelsPerFrame() * 3];
	fillPaneFromPalette(pixelmatrix, pixelmatrix.getRedPalette(), is, 0);
	fillPaneFromPalette(pixelmatrix, pixelmatrix.getGreenPalette(), is, 1);
	fillPaneFromPalette(pixelmatrix, pixelmatrix.getBluePalette(), is, 2);
	dicomobject.set(461, new Integer(3));
	dicomobject.set(462, "RGB");
	dicomobject.set(463, new Integer(1));
	dicomobject.set(475, new Integer(8));
	dicomobject.set(476, new Integer(8));
	dicomobject.set(477, new Integer(7));
	dicomobject.set(1184, is);
	if (Debug.DEBUG > 1)
	    Debug.out.println
		("jdicom: Convert pixel data from PALETTE COLOR to RGB");
	return dicomobject;
    }
    
    private static DicomObject fromPixelToPlane(DicomObject dicomobject)
	throws DicomException {
	byte[] is = (byte[]) dicomobject.get(1184);
	byte[] is_53_ = new byte[is.length];
	int i = is.length / 3;
	int i_54_ = 0;
	int i_55_ = 0;
	int i_56_ = i;
	int i_57_ = i << 1;
	for (int i_58_ = 0; i_58_ < i; i_58_++) {
	    is_53_[i_55_++] = is[i_54_++];
	    is_53_[i_56_++] = is[i_54_++];
	    is_53_[i_57_++] = is[i_54_++];
	}
	dicomobject.set(463, new Integer(1));
	dicomobject.set(1184, is_53_);
	if (Debug.DEBUG > 1)
	    Debug.out.println
		("jdicom: Convert Planar Configuration from pixel to plane");
	return dicomobject;
    }
    
    private static void fillPaneFromPalette
	(PixelMatrix pixelmatrix, byte[] is, byte[] is_59_, int i)
	throws DicomException {
	int i_60_ = 0;
	int i_61_ = pixelmatrix.getPixelsPerFrame();
	int i_62_ = i_61_ * i;
	while (i_60_ < i_61_) {
	    is_59_[i_62_] = is[pixelmatrix.getSample(i_60_)];
	    i_60_++;
	    i_62_++;
	}
    }
    
    public static DicomObject colorToGrayscale(DicomObject dicomobject)
	throws DicomException {
	if (dicomobject.getI(461) != 3)
	    throw new IllegalArgumentException("SamplesPerPixel != 3");
	if (!"RGB".equals(dicomobject.getS(462)))
	    throw new IllegalArgumentException
		      ("dPhotometricInterpretation != \"RGB\"");
	if (dicomobject.getI(475) != 8)
	    throw new IllegalArgumentException("dBitsAllocated != 8");
	if (dicomobject.getI(476) != 8)
	    throw new IllegalArgumentException("dBitsStored != 8");
	if (dicomobject.getI(477) != 7)
	    throw new IllegalArgumentException("dHighBit != 7");
	if (dicomobject.getI(478) != 0)
	    throw new IllegalArgumentException("dPixelRepresentation != 0");
	int i = dicomobject.getI(466) * dicomobject.getI(467);
	int i_63_ = 1;
	int i_64_ = 3;
	if (dicomobject.getI(463) != 0) {
	    i_63_ = i;
	    i_64_ = 1;
	}
	byte[] is = (byte[]) dicomobject.get(1184);
	byte[] is_65_ = new byte[i];
	int i_66_ = 0;
	int i_67_ = i_66_ + i_63_;
	int i_68_ = i_67_ + i_63_;
	int i_69_ = 0;
	while (i_69_ < i) {
	    is_65_[i_69_] = (byte) (((is[i_66_] & 0xff) + (is[i_67_] & 0xff)
				     + (is[i_68_] & 0xff))
				    / 3);
	    i_69_++;
	    i_66_ += i_64_;
	    i_67_ += i_64_;
	    i_68_ += i_64_;
	}
	dicomobject.set(461, new Integer(1));
	dicomobject.set(462, "MONOCHROME2");
	dicomobject.deleteItem(463);
	dicomobject.deleteItem(490);
	dicomobject.deleteItem(489);
	dicomobject.deleteItem(487);
	dicomobject.deleteItem(488);
	dicomobject.set(1184, is_65_);
	return dicomobject;
    }
    
    public static DicomObject grayscaleToColor(DicomObject dicomobject)
	throws DicomException {
	if (dicomobject.getI(461) != 1)
	    throw new IllegalArgumentException("SamplesPerPixel != 1");
	if (!"MONOCHROME2".equals(dicomobject.getS(462)))
	    throw new IllegalArgumentException
		      ("dPhotometricInterpretation != \"MONOCHROME2\"");
	if (dicomobject.getI(475) != 8)
	    throw new IllegalArgumentException("dBitsAllocated != 8");
	if (dicomobject.getI(476) != 8)
	    throw new IllegalArgumentException("dBitsStored != 8");
	if (dicomobject.getI(477) != 7)
	    throw new IllegalArgumentException("dHighBit != 7");
	if (dicomobject.getI(478) != 0)
	    throw new IllegalArgumentException("dPixelRepresentation != 0");
	byte[] is = (byte[]) dicomobject.get(1184);
	int i = is.length;
	byte[] is_70_ = new byte[i * 3];
	System.arraycopy(is, 0, is_70_, 0, i);
	System.arraycopy(is, 0, is_70_, i, i);
	System.arraycopy(is, 0, is_70_, i * 2, i);
	dicomobject.set(461, new Integer(3));
	dicomobject.set(462, "RGB");
	dicomobject.set(463, new Integer(1));
	dicomobject.set(1184, is_70_);
	return dicomobject;
    }
}
