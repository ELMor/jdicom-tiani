/* DirRecordFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import java.util.Hashtable;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.DicomObjectFilterPath;
import com.tiani.dicom.util.DicomObjectPath;
import com.tiani.dicom.util.IDicomObjectFilter;

public class DirRecordFactory
{
    private Hashtable _iods;
    private static Hashtable _defaultIODs = new Hashtable();
    
    public DirRecordFactory() {
	_iods = _defaultIODs;
    }
    
    public DirRecordFactory(Hashtable hashtable) {
	_iods = _defaultIODs;
	_iods = hashtable;
    }
    
    public DicomObject create(String string, DicomObject dicomobject)
	throws DicomException {
	DicomObject dicomobject_0_ = new DicomObject();
	init(dicomobject_0_, string, dicomobject, _iods);
	return dicomobject_0_;
    }
    
    public void init(DicomObject dicomobject, String string,
		     DicomObject dicomobject_1_) throws DicomException {
	init(dicomobject, string, dicomobject_1_, _iods);
    }
    
    public static IDicomObjectFilter createFilter(final String type) {
	return new IDicomObjectFilter() {
	    public boolean accept(DicomObject dicomobject)
		throws DicomException {
		return type.equals(dicomobject.getS(48));
	    }
	};
    }
    
    public static IDicomObjectFilter createFilter
	(final String type, final int dname, final String val) {
	return new IDicomObjectFilter() {
	    public boolean accept(DicomObject dicomobject)
		throws DicomException {
		return (type.equals(dicomobject.getS(48))
			&& val.equals(dicomobject.getS(dname)));
	    }
	};
    }
    
    public static DicomObjectFilterPath createImageFilterPath
	(DicomObject dicomobject) throws DicomException {
	DicomObjectFilterPath dicomobjectfilterpath
	    = new DicomObjectFilterPath();
	dicomobjectfilterpath.addElement(createFilter("PATIENT", 148,
						      dicomobject.getS(148)));
	dicomobjectfilterpath.addElement(createFilter("STUDY", 425,
						      dicomobject.getS(425)));
	dicomobjectfilterpath.addElement(createFilter("SERIES", 426,
						      dicomobject.getS(426)));
	dicomobjectfilterpath.addElement(createFilter("IMAGE", 53,
						      dicomobject.getS(63)));
	return dicomobjectfilterpath;
    }
    
    public DicomObject createImageDR
	(DicomObject dicomobject, String[] strings) throws DicomException {
	DicomObject dicomobject_3_ = dicomobject.getFileMetaInformation();
	FileMetaInformation.check(dicomobject_3_);
	DicomObject dicomobject_4_ = create("IMAGE", dicomobject);
	initReferencedSOP(dicomobject_4_, strings, dicomobject_3_.getS(29),
			  dicomobject_3_.getS(30), dicomobject_3_.getS(31));
	return dicomobject_4_;
    }
    
    public DicomObjectPath createImageDRPath
	(DicomObject dicomobject, String[] strings) throws DicomException {
	DicomObjectPath dicomobjectpath = new DicomObjectPath();
	dicomobjectpath.addElement(create("PATIENT", dicomobject));
	dicomobjectpath.addElement(create("STUDY", dicomobject));
	dicomobjectpath.addElement(create("SERIES", dicomobject));
	dicomobjectpath.addElement(createImageDR(dicomobject, strings));
	return dicomobjectpath;
    }
    
    public static void init(DicomObject dicomobject, String string)
	throws DicomException {
	dicomobject.set(48, string);
	dicomobject.set(46, new Integer(65535));
	dicomobject.set(45, new Integer(0));
	dicomobject.set(47, new Integer(0));
    }
    
    public static DicomObject create(String string) throws DicomException {
	DicomObject dicomobject = new DicomObject();
	init(dicomobject, string);
	return dicomobject;
    }
    
    public static void init(DicomObject dicomobject, String string,
			    DicomObject dicomobject_5_,
			    int[] is) throws DicomException {
	init(dicomobject, string);
	copy(dicomobject, dicomobject_5_, is);
    }
    
    public static DicomObject create(String string, DicomObject dicomobject,
				     int[] is) throws DicomException {
	DicomObject dicomobject_6_ = new DicomObject();
	init(dicomobject_6_, string, dicomobject, is);
	return dicomobject_6_;
    }
    
    public static void init(DicomObject dicomobject, String string,
			    DicomObject dicomobject_7_,
			    Hashtable hashtable) throws DicomException {
	init(dicomobject, string, dicomobject_7_,
	     (int[]) hashtable.get(string));
    }
    
    public static DicomObject create
	(String string, DicomObject dicomobject, Hashtable hashtable)
	throws DicomException {
	DicomObject dicomobject_8_ = new DicomObject();
	init(dicomobject_8_, string, dicomobject, hashtable);
	return dicomobject_8_;
    }
    
    public static void initReferencedSOP
	(DicomObject dicomobject, String[] strings, String string,
	 String string_9_, String string_10_)
	throws DicomException {
	dicomobject.deleteItem(50);
	for (int i = 0; i < strings.length; i++)
	    dicomobject.append(50, strings[i]);
	dicomobject.set(52, string);
	dicomobject.set(53, string_9_);
	dicomobject.set(54, string_10_);
    }
    
    public static void copy
	(DicomObject dicomobject, DicomObject dicomobject_11_, int[] is)
	throws DicomException {
	for (int i = 0; i < is.length; i++)
	    copy(dicomobject, dicomobject_11_, is[i]);
    }
    
    public static void copy
	(DicomObject dicomobject, DicomObject dicomobject_12_, int i)
	throws DicomException {
	if (dicomobject_12_.getSize(i) >= 0)
	    dicomobject.set(i, dicomobject_12_.get(i));
    }
    
    static {
	_defaultIODs.put("PATIENT", new int[] { 57, 147, 148 });
	_defaultIODs.put("STUDY", new int[] { 57, 64, 70, 77, 427, 425, 95 });
	_defaultIODs.put("SERIES", new int[] { 57, 81, 428, 426, 699 });
	_defaultIODs.put("IMAGE", new int[] { 57, 430, 699 });
	_defaultIODs.put("OVERLAY", new int[] { 57, 437, 699 });
	_defaultIODs.put("MODALITY LUT", new int[] { 57, 439 });
	_defaultIODs.put("VOI LUT", new int[] { 57, 439 });
	_defaultIODs.put("CURVE", new int[] { 57, 438 });
	_defaultIODs.put("TOPIC", new int[] { 57, 700, 701, 702, 703 });
	_defaultIODs.put("VISIT", new int[] { 57, 562, 554, 85 });
	_defaultIODs.put("RESULTS", new int[] { 57, 1104, 59 });
	_defaultIODs.put("INTERPRETATION",
			 new int[] { 57, 1111, 1115, 1120, 1121, 1125, 1127,
				     1128 });
	_defaultIODs.put("STUDY COMPONENT", new int[] { 57, 81, 95, 96, 100 });
	_defaultIODs.put("STORED PRINT", new int[] { 57, 430, 699 });
    }
}
