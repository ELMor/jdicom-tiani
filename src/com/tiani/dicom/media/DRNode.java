/* DRNode - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import java.util.Iterator;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.IDicomObjectFilter;

public class DRNode
{
    private final DicomObject dataset;
    private final int hashCode;
    private final String type;
    private final String key;
    private DRNode next = null;
    private DREntity lowerEntity = null;
    private int inUse;
    
    public DRNode(DicomObject dicomobject) throws DicomException {
	dataset = dicomobject;
	type = dicomobject.getS(48);
	key = getKeyFrom(dicomobject, type);
	hashCode = type.hashCode() + (key != null ? key.hashCode() : 0);
	inUse = dicomobject.getI(46);
    }
    
    public final DicomObject getDataset() {
	return dataset;
    }
    
    public final String getType() {
	return type;
    }
    
    public final String getKey() {
	return key;
    }
    
    public final DRNode getNext() {
	return next;
    }
    
    DRNode getLast() {
	DRNode drnode_0_;
	for (drnode_0_ = this; drnode_0_.next != null;
	     drnode_0_ = drnode_0_.next) {
	    /* empty */
	}
	return drnode_0_;
    }
    
    DRNode findInUse() {
	for (DRNode drnode_1_ = this; drnode_1_ != null;
	     drnode_1_ = drnode_1_.next) {
	    if (drnode_1_.inUse != 0)
		return drnode_1_;
	}
	return null;
    }
    
    DRNode find(DRNode drnode_2_) {
	for (DRNode drnode_3_ = this; drnode_3_ != null;
	     drnode_3_ = drnode_3_.next) {
	    if (drnode_3_.inUse != 0 && drnode_3_.equals(drnode_2_))
		return drnode_3_;
	}
	return null;
    }
    
    DRNode find(String string, IDicomObjectFilter idicomobjectfilter)
	throws DicomException {
	for (DRNode drnode_4_ = this; drnode_4_ != null;
	     drnode_4_ = drnode_4_.next) {
	    if (drnode_4_.inUse != 0
		&& (string == null || string.equals(drnode_4_.type))
		&& (idicomobjectfilter == null
		    || idicomobjectfilter.accept(drnode_4_.getDataset())))
		return drnode_4_;
	}
	return null;
    }
    
    DRNode find(String string, String string_5_) {
	for (DRNode drnode_6_ = this; drnode_6_ != null;
	     drnode_6_ = drnode_6_.next) {
	    if (drnode_6_.inUse != 0
		&& (string_5_ == null || string_5_.equals(drnode_6_.key))
		&& (string == null || string.equals(drnode_6_.type)))
		return drnode_6_;
	}
	return null;
    }
    
    DRNode find(String string) {
	for (DRNode drnode_7_ = this; drnode_7_ != null;
	     drnode_7_ = drnode_7_.next) {
	    if (drnode_7_.inUse != 0
		&& (string == null || string.equals(drnode_7_.type)))
		return drnode_7_;
	}
	return null;
    }
    
    public final DREntity getLowerEntity() {
	return lowerEntity;
    }
    
    public final int getInUse() {
	return inUse;
    }
    
    public final boolean isInUse() {
	return inUse != 0;
    }
    
    public final void setNext(DRNode drnode_8_) {
	next = drnode_8_;
    }
    
    public void setNext(DRNode drnode_9_, Long var_long) {
	setNext(drnode_9_);
	try {
	    dataset.set(45, var_long);
	} catch (DicomException dicomexception) {
	    dicomexception.printStackTrace(Debug.out);
	    throw new RuntimeException(dicomexception.toString());
	}
    }
    
    public final void setLowerEntity(DREntity drentity) {
	lowerEntity = drentity;
    }
    
    void setLowerEntity(DREntity drentity, Long var_long) {
	setLowerEntity(drentity);
	try {
	    dataset.set(47, var_long);
	} catch (DicomException dicomexception) {
	    dicomexception.printStackTrace(Debug.out);
	    throw new RuntimeException(dicomexception.toString());
	}
    }
    
    void addLower(DRNode drnode_10_, Long var_long) {
	if (lowerEntity != null)
	    lowerEntity.add(drnode_10_, var_long);
	else
	    setLowerEntity(new DREntity(drnode_10_), var_long);
    }
    
    public final void setInactive() {
	setInUse(0);
    }
    
    public final void setInUse() {
	setInUse(65535);
    }
    
    public void setInUse(int i) {
	try {
	    dataset.set(46, new Integer(i));
	} catch (DicomException dicomexception) {
	    dicomexception.printStackTrace(Debug.out);
	    throw new RuntimeException(dicomexception.toString());
	}
	inUse = i;
    }
    
    public void delete() {
	if (!isInUse())
	    throw new IllegalStateException();
	if (lowerEntity != null) {
	    Iterator iterator = lowerEntity.iterator();
	    while (iterator.hasNext())
		((DRNode) iterator.next()).delete();
	}
	setInactive();
    }
    
    public static String getKeyFrom(DicomObject dicomobject, String string) {
	String string_11_ = (String) dicomobject.get(53);
	if (string_11_ != null)
	    return string_11_;
	int i;
	if ("SERIES".equals(string))
	    i = 426;
	else if ("STUDY".equals(string))
	    i = 425;
	else if ("PATIENT".equals(string))
	    i = 148;
	else
	    i = 63;
	return (String) dicomobject.get(i);
    }
    
    public final int hashCode() {
	return hashCode;
    }
    
    public boolean equals(Object object) {
	if (this == object)
	    return true;
	if (object == null || !(object instanceof DRNode))
	    return false;
	return equals((DRNode) object);
    }
    
    public boolean equals(DRNode drnode_12_) {
	if (dataset == drnode_12_.dataset)
	    return true;
	return (hashCode == drnode_12_.hashCode && key != null
		&& key.equals(drnode_12_.key) && type.equals(drnode_12_.type));
    }
    
    public static DicomObject createComposite
	(DRNode[] drnodes, boolean bool) throws DicomException {
	DicomObject dicomobject = new DicomObject();
	for (int i = 0; i < drnodes.length; i++) {
	    DicomObject dicomobject_13_ = drnodes[i].getDataset();
	    if (bool)
		dicomobject.appendVRs(dicomobject_13_);
	    else
		dicomobject.setVRs(dicomobject_13_);
	}
	return dicomobject;
    }
}
