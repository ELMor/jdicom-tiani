/* FileSet2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;

public class FileSet2
{
    private String fileSetID;
    private File dirFile;
    private File rootDir;
    
    public FileSet2(File file) {
	this(file, "");
    }
    
    public FileSet2(File file, String string) {
	dirFile = file;
	rootDir = new File(file.getParent());
	fileSetID = string;
    }
    
    public File getDirFile() {
	return dirFile;
    }
    
    public File getRootDir() {
	return rootDir;
    }
    
    public String getFileSetID() {
	return fileSetID;
    }
    
    public void setFileSetID(String string) {
	string = string;
    }
    
    public File read(DicomDir2 dicomdir2) throws IOException, DicomException {
	FileInputStream fileinputstream = new FileInputStream(dirFile);
	try {
	    dicomdir2.read(fileinputstream);
	} finally {
	    fileinputstream.close();
	}
	dicomdir2.init();
	fileSetID = (String) dicomdir2.get(38);
	if (fileSetID == null)
	    fileSetID = "";
	if (Debug.DEBUG > 0)
	    logRead(dirFile);
	return dirFile;
    }
    
    public File write(DicomDir2 dicomdir2) throws IOException, DicomException {
	FileOutputStream fileoutputstream = new FileOutputStream(dirFile);
	try {
	    dicomdir2.write(fileoutputstream);
	} finally {
	    fileoutputstream.close();
	}
	if (Debug.DEBUG > 0)
	    logWrite(dirFile);
	return dirFile;
    }
    
    public File getRefFile(DicomObject dicomobject) throws DicomException {
	int i = dicomobject.getSize(50);
	if (i <= 0)
	    return null;
	File file = new File(rootDir, (String) dicomobject.get(50, 0));
	for (int i_0_ = 1; i_0_ < i; i_0_++)
	    file = new File(file, (String) dicomobject.get(50, i_0_));
	return file;
    }
    
    public File read(DicomObject dicomobject, DicomObject dicomobject_1_)
	throws IOException, DicomException {
	File file = getRefFile(dicomobject_1_);
	if (file != null) {
	    FileInputStream fileinputstream = new FileInputStream(file);
	    try {
		dicomobject.read(fileinputstream);
	    } finally {
		fileinputstream.close();
	    }
	    if (Debug.DEBUG > 0)
		logRead(file);
	}
	return file;
    }
    
    public File write(DicomObject dicomobject, DicomObject dicomobject_2_)
	throws IOException, DicomException, UnknownUIDException {
	return write(dicomobject, dicomobject_2_, true);
    }
    
    public File write
	(DicomObject dicomobject, DicomObject dicomobject_3_, boolean bool)
	throws IOException, DicomException, UnknownUIDException {
	File file = getRefFile(dicomobject_3_);
	if (file != null) {
	    String string = dicomobject_3_.getS(54);
	    if (bool && dicomobject.getFileMetaInformation() == null)
		dicomobject.setFileMetaInformation
		    (new FileMetaInformation(dicomobject, string));
	    File file_4_ = new File(file.getParent());
	    file_4_.mkdirs();
	    FileOutputStream fileoutputstream = new FileOutputStream(file);
	    try {
		dicomobject.write(fileoutputstream, bool,
				  UID.getUIDEntry(string).getConstant(),
				  false);
	    } finally {
		fileoutputstream.close();
	    }
	    if (Debug.DEBUG > 0)
		logWrite(file);
	}
	return file;
    }
    
    public File delete(DicomObject dicomobject, boolean bool)
	throws IOException, DicomException {
	File file = getRefFile(dicomobject);
	if (file != null) {
	    file.delete();
	    if (bool) {
		for (File file_5_ = new File(file.getParent());
		     file_5_.list().length == 0;
		     file_5_ = new File(file_5_.getParent()))
		    file_5_.delete();
	    }
	    if (Debug.DEBUG > 0)
		logDelete(file);
	}
	return file;
    }
    
    private void logRead(File file) {
	log(" << M-READ[", file);
    }
    
    private void logWrite(File file) {
	log(" << M-WRITE[", file);
    }
    
    private void logDelete(File file) {
	log(" << M-DELETE[", file);
    }
    
    private void log(String string, File file) {
	Debug.out.println("jdicom: " + fileSetID + string + file + "]");
    }
}
