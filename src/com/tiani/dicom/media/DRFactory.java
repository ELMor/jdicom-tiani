/* DRFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.DicomObjectFilterPath;
import com.tiani.dicom.util.DicomObjectPath;
import com.tiani.dicom.util.IDicomObjectFilter;

public class DRFactory
{
    public static final String PATIENT = "PATIENT";
    public static final String STUDY = "STUDY";
    public static final String SERIES = "SERIES";
    public static final String IMAGE = "IMAGE";
    public static final String OVERLAY = "OVERLAY";
    public static final String MODALITY_LUT = "MODALITY LUT";
    public static final String VOI_LUT = "VOI LUT";
    public static final String CURVE = "CURVE";
    public static final String TOPIC = "TOPIC";
    public static final String VISIT = "VISIT";
    public static final String RESULT = "RESULT";
    public static final String INTERPRETATION = "INTERPRETATION";
    public static final String STUDY_COMPONENT = "STUDY COMPONENT";
    public static final String STORED_PRINT = "STORED PRINT";
    public static final String RT_DOSE = "RT DOSE";
    public static final String RT_STRUCTURE_SET = "RT STRUCTURE SET";
    public static final String RT_PLAN = "RT PLAN";
    public static final String RT_TREAT_RECORD = "RT TREAT RECORD";
    public static final String PRESENTATION = "PRESENTATION";
    public static final String SR_DOCUMENT = "SR DOCUMENT";
    public static final String KO_DOCUMENT = "KEY OBJECT DOC";
    public static final String RAW_DATA = "RAW DATA";
    public static final String[] IMAGE_TYPE_PATH
	= { "PATIENT", "STUDY", "SERIES", "IMAGE" };
    public static final String[] INSTANCE_TYPE_PATH
	= { "PATIENT", "STUDY", "SERIES", null };
    public static final int IN_USE = 65535;
    public static final int INACTIVE = 0;
    private static final String[] IMAGE_UIDS
	= { "1.2.840.10008.5.1.4.1.1.1", "1.2.840.10008.5.1.4.1.1.2",
	    "1.2.840.10008.5.1.4.1.1.3.1", "1.2.840.10008.5.1.4.1.1.4",
	    "1.2.840.10008.5.1.4.1.1.6.1", "1.2.840.10008.5.1.4.1.1.7",
	    "1.2.840.10008.5.1.4.1.1.12.1", "1.2.840.10008.5.1.4.1.1.12.2",
	    "1.2.840.10008.5.1.4.1.1.12.3", "1.2.840.10008.5.1.4.1.1.20",
	    "1.2.840.10008.5.1.1.29", "1.2.840.10008.5.1.1.20",
	    "1.2.840.10008.5.1.4.1.1.128", "1.2.840.10008.5.1.4.1.1.481.1",
	    "1.2.840.10008.5.1.4.1.1.3", "1.2.840.10008.5.1.4.1.1.5",
	    "1.2.840.10008.5.1.4.1.1.6", "1.2.840.10008.5.1.4.1.1.77.1.1",
	    "1.2.840.10008.5.1.4.1.1.77.1.2", "1.2.840.10008.5.1.4.1.1.77.1.3",
	    "1.2.840.10008.5.1.4.1.1.77.1.4", "1.2.840.10008.5.1.4.1.1.1.1",
	    "1.2.840.10008.5.1.4.1.1.1.1.1", "1.2.840.10008.5.1.4.1.1.1.2",
	    "1.2.840.10008.5.1.4.1.1.1.2.1", "1.2.840.10008.5.1.4.1.1.1.3",
	    "1.2.840.10008.5.1.4.1.1.1.3.1" };
    private static final String PRESENTATION_UID
	= "1.2.840.10008.5.1.4.1.1.11.1";
    private static final String RT_DOSE_UID = "1.2.840.10008.5.1.4.1.1.481.2";
    private static final String RT_STRUCTURE_SET_UID
	= "1.2.840.10008.5.1.4.1.1.481.3";
    private static final String RT_PLAN_UID = "1.2.840.10008.5.1.4.1.1.481.5";
    private static final String[] RT_TREAT_RECORD_UIDS
	= { "1.2.840.10008.5.1.4.1.1.481.4", "1.2.840.10008.5.1.4.1.1.481.6",
	    "1.2.840.10008.5.1.4.1.1.481.7" };
    private static final String[] SR_DOCUMENT_UIDS
	= { "1.2.840.10008.5.1.4.1.1.88.11", "1.2.840.10008.5.1.4.1.1.88.22",
	    "1.2.840.10008.5.1.4.1.1.88.33" };
    private static final String OVERLAY_UID = "1.2.840.10008.5.1.4.1.1.8";
    private static final String KO_DOCUMENT_UID
	= "1.2.840.10008.5.1.4.1.1.88.59";
    private static final String[] RAW_DATA_UIDs
	= { "1.2.840.10008.5.1.4.1.1.66", "1.2.826.0.1.3680043.2.242.1.1" };
    private static final String[] CURVE_UIDS
	= { "1.2.840.10008.5.1.4.1.1.9", "1.2.840.10008.5.1.4.1.1.129" };
    private static final String MODALITY_LUT_UID
	= "1.2.840.10008.5.1.4.1.1.10";
    private static final String VOI_LUT_UID = "1.2.840.10008.5.1.4.1.1.11";
    private static final String STORED_PRINT_UID = "1.2.840.10008.5.1.1.27";
    public static final IDicomObjectFilter PATIENT_FILTER
	= createFilter("PATIENT");
    public static final IDicomObjectFilter STUDY_FILTER
	= createFilter("STUDY");
    public static final IDicomObjectFilter SERIES_FILTER
	= createFilter("SERIES");
    private Hashtable _tags;
    private Hashtable _groups;
    
    public static String typeOf(String string) {
	if (Arrays.asList(IMAGE_UIDS).indexOf(string) != -1)
	    return "IMAGE";
	if ("1.2.840.10008.5.1.4.1.1.88.59".equals(string))
	    return "KEY OBJECT DOC";
	if ("1.2.840.10008.5.1.4.1.1.8".equals(string))
	    return "OVERLAY";
	if ("1.2.840.10008.5.1.4.1.1.10".equals(string))
	    return "MODALITY LUT";
	if ("1.2.840.10008.5.1.4.1.1.11".equals(string))
	    return "VOI LUT";
	if (Arrays.asList(CURVE_UIDS).indexOf(string) != -1)
	    return "CURVE";
	if ("1.2.840.10008.5.1.1.27".equals(string))
	    return "STORED PRINT";
	if ("1.2.840.10008.5.1.4.1.1.481.2".equals(string))
	    return "RT DOSE";
	if ("1.2.840.10008.5.1.4.1.1.481.5".equals(string))
	    return "RT PLAN";
	if ("1.2.840.10008.5.1.1.27".equals(string))
	    return "STORED PRINT";
	if (Arrays.asList(RT_TREAT_RECORD_UIDS).indexOf(string) != -1)
	    return "RT TREAT RECORD";
	if ("1.2.840.10008.5.1.4.1.1.11.1".equals(string))
	    return "PRESENTATION";
	if (Arrays.asList(SR_DOCUMENT_UIDS).indexOf(string) != -1)
	    return "SR DOCUMENT";
	if (Arrays.asList(RAW_DATA_UIDs).indexOf(string) != -1)
	    return "RAW DATA";
	return null;
    }
    
    public DRFactory() {
	this(loadDefProperties());
    }
    
    public DRFactory(Hashtable hashtable, Hashtable hashtable_0_) {
	_tags = hashtable;
	_groups = hashtable_0_;
    }
    
    public final Hashtable getTags() {
	return _tags;
    }
    
    public DRFactory(Properties properties) {
	_tags = parseProperties(properties, "");
	_groups = parseProperties(properties, "gr.");
    }
    
    public DicomObject create(String string, DicomObject dicomobject)
	throws DicomException {
	DicomObject dicomobject_1_ = new DicomObject();
	init(dicomobject_1_, string, dicomobject, _tags, _groups);
	return dicomobject_1_;
    }
    
    public void init(DicomObject dicomobject, String string,
		     DicomObject dicomobject_2_) throws DicomException {
	init(dicomobject, string, dicomobject_2_, _tags, _groups);
    }
    
    public static IDicomObjectFilter createFilter(final String type) {
	return new IDicomObjectFilter() {
	    public boolean accept(DicomObject dicomobject)
		throws DicomException {
		return type.equals(dicomobject.getS(48));
	    }
	};
    }
    
    public static IDicomObjectFilter createRefSOPInstanceFilter
	(final String uid) {
	if (uid == null || uid.length() == 0)
	    return null;
	return new IDicomObjectFilter() {
	    public boolean accept(DicomObject dicomobject)
		throws DicomException {
		return uid.equals(dicomobject.getS(53));
	    }
	};
    }
    
    public static IDicomObjectFilter createRefSOPClassFilter
	(final String uid) {
	if (uid == null || uid.length() == 0)
	    return null;
	return new IDicomObjectFilter() {
	    public boolean accept(DicomObject dicomobject)
		throws DicomException {
		return uid.equals(dicomobject.getS(52));
	    }
	};
    }
    
    public static IDicomObjectFilter createRefSOPFilter(final String classUID,
							final String instUID) {
	if (classUID == null || classUID.length() == 0)
	    return createRefSOPInstanceFilter(instUID);
	if (instUID == null || instUID.length() == 0)
	    return createRefSOPClassFilter(classUID);
	return new IDicomObjectFilter() {
	    public boolean accept(DicomObject dicomobject)
		throws DicomException {
		return (classUID.equals(dicomobject.getS(52))
			&& instUID.equals(dicomobject.getS(53)));
	    }
	};
    }
    
    public static IDicomObjectFilter createFilter
	(final String type, final String val, final int dname,
	 final int index) {
	if (val == null || val.length() == 0)
	    return createFilter(type);
	return new IDicomObjectFilter() {
	    public boolean accept(DicomObject dicomobject)
		throws DicomException {
		return (type.equals(dicomobject.getS(48))
			&& val.equals(dicomobject.getS(dname, index)));
	    }
	};
    }
    
    public static IDicomObjectFilter createFilter(String string,
						  String string_6_, int i) {
	return createFilter(string, string_6_, i, 0);
    }
    
    public static DicomObjectFilterPath createRefSOPInstanceFilterPath
	(DicomObject dicomobject) throws DicomException {
	DicomObjectFilterPath dicomobjectfilterpath
	    = new DicomObjectFilterPath();
	dicomobjectfilterpath
	    .addElement(createFilter("PATIENT", dicomobject.getS(148), 148));
	dicomobjectfilterpath
	    .addElement(createFilter("STUDY", dicomobject.getS(425), 425));
	dicomobjectfilterpath
	    .addElement(createFilter("SERIES", dicomobject.getS(426), 426));
	dicomobjectfilterpath
	    .addElement(createRefSOPInstanceFilter(dicomobject.getS(63)));
	return dicomobjectfilterpath;
    }
    
    public DicomObject createInstanceDR
	(DicomObject dicomobject, String[] strings) throws DicomException {
	String string = typeOf((String) dicomobject.get(62));
	if (string == null)
	    throw new DicomException
		      ("Could not recognized type of DICOM Object");
	return createInstanceDR(string, dicomobject, strings);
    }
    
    public DicomObject createInstanceDR
	(String string, DicomObject dicomobject, String[] strings)
	throws DicomException {
	DicomObject dicomobject_7_ = dicomobject.getFileMetaInformation();
	FileMetaInformation.check(dicomobject_7_);
	DicomObject dicomobject_8_ = create(string, dicomobject);
	initReferencedSOP(dicomobject_8_, strings, dicomobject_7_.getS(29),
			  dicomobject_7_.getS(30), dicomobject_7_.getS(31));
	return dicomobject_8_;
    }
    
    public DicomObjectPath createInstanceDRPath
	(DicomObject dicomobject, String[] strings) throws DicomException {
	String string = typeOf((String) dicomobject.get(62));
	if (string == null)
	    throw new DicomException
		      ("Could not recognized type of DICOM Object");
	return createInstanceDRPath(string, dicomobject, strings);
    }
    
    public DicomObjectPath createInstanceDRPath
	(String string, DicomObject dicomobject, String[] strings)
	throws DicomException {
	DicomObjectPath dicomobjectpath = new DicomObjectPath();
	dicomobjectpath.addElement(create("PATIENT", dicomobject));
	dicomobjectpath.addElement(create("STUDY", dicomobject));
	dicomobjectpath.addElement(create("SERIES", dicomobject));
	dicomobjectpath.addElement(createInstanceDR(string, dicomobject,
						    strings));
	return dicomobjectpath;
    }
    
    public DRNode[] createInstanceDRNodePath
	(DicomObject dicomobject, String[] strings) throws DicomException {
	String string = typeOf((String) dicomobject.get(62));
	if (string == null)
	    throw new DicomException
		      ("Could not recognized type of DICOM Object");
	return createInstanceDRNodePath(string, dicomobject, strings);
    }
    
    public DRNode[] createInstanceDRNodePath
	(String string, DicomObject dicomobject, String[] strings)
	throws DicomException {
	return new DRNode[] { new DRNode(create("PATIENT", dicomobject)),
			      new DRNode(create("STUDY", dicomobject)),
			      new DRNode(create("SERIES", dicomobject)),
			      new DRNode(createInstanceDR(string, dicomobject,
							  strings)) };
    }
    
    public static void init(DicomObject dicomobject, String string)
	throws DicomException {
	dicomobject.set(48, string);
	dicomobject.set(46, new Integer(65535));
	dicomobject.set(45, new Integer(0));
	dicomobject.set(47, new Integer(0));
    }
    
    public static DicomObject create(String string) throws DicomException {
	DicomObject dicomobject = new DicomObject();
	init(dicomobject, string);
	return dicomobject;
    }
    
    public static void init(DicomObject dicomobject, String string,
			    DicomObject dicomobject_9_, int[] is,
			    int[] is_10_) throws DicomException {
	init(dicomobject, string);
	copyGroups(dicomobject, dicomobject_9_, is_10_);
	copy(dicomobject, dicomobject_9_, is);
    }
    
    public static DicomObject create
	(String string, DicomObject dicomobject, int[] is, int[] is_11_)
	throws DicomException {
	DicomObject dicomobject_12_ = new DicomObject();
	init(dicomobject_12_, string, dicomobject, is, is_11_);
	return dicomobject_12_;
    }
    
    public static void init(DicomObject dicomobject, String string,
			    DicomObject dicomobject_13_, Hashtable hashtable,
			    Hashtable hashtable_14_) throws DicomException {
	init(dicomobject, string, dicomobject_13_,
	     (int[]) hashtable.get(string),
	     hashtable_14_ != null ? (int[]) hashtable_14_.get(string) : null);
    }
    
    public static DicomObject create
	(String string, DicomObject dicomobject, Hashtable hashtable,
	 Hashtable hashtable_15_)
	throws DicomException {
	DicomObject dicomobject_16_ = new DicomObject();
	init(dicomobject_16_, string, dicomobject, hashtable, hashtable_15_);
	return dicomobject_16_;
    }
    
    public static DicomObject createPrivateDR
	(String[] strings, String string) throws DicomException {
	DicomObject dicomobject = create("PRIVATE");
	initReferencedFile(dicomobject, strings, string);
	return dicomobject;
    }
    
    public static void initReferencedFile
	(DicomObject dicomobject, String[] strings, String string)
	throws DicomException {
	dicomobject.deleteItem(50);
	for (int i = 0; i < strings.length; i++)
	    dicomobject.append(50, strings[i]);
	dicomobject.set(52, string);
    }
    
    public static void initReferencedSOP
	(DicomObject dicomobject, String[] strings, String string,
	 String string_17_, String string_18_)
	throws DicomException {
	initReferencedFile(dicomobject, strings, string);
	dicomobject.set(53, string_17_);
	dicomobject.set(54, string_18_);
    }
    
    public static void copy
	(DicomObject dicomobject, DicomObject dicomobject_19_, int[] is)
	throws DicomException {
	for (int i = 0; i < is.length; i++)
	    copy(dicomobject, dicomobject_19_, is[i]);
    }
    
    public static void copy
	(DicomObject dicomobject, DicomObject dicomobject_20_, int i)
	throws DicomException {
	int i_21_ = i >> 16;
	int i_22_ = i & 0xffff;
	int i_23_ = dicomobject_20_.getSize_ge(i_21_, i_22_);
	for (int i_24_ = 0; i_24_ < i_23_; i_24_++) {
	    try {
		dicomobject.append_ge(i_21_, i_22_,
				      dicomobject_20_.get_ge(i_21_, i_22_,
							     i_24_));
	    } catch (DicomException dicomexception) {
		dicomobject.append_ge(i_21_, i_22_,
				      dicomobject_20_.getS_ge(i_21_, i_22_,
							      i_24_));
	    }
	}
    }
    
    public static void copyGroups
	(DicomObject dicomobject, DicomObject dicomobject_25_, int[] is)
	throws DicomException {
	if (is != null) {
	    for (int i = 0; i < is.length; i++) {
		if (dicomobject_25_.containsGroup(is[i]))
		    dicomobject.addGroups(dicomobject_25_.copyGroup(is[i]));
	    }
	}
    }
    
    private static Properties loadDefProperties() {
	try {
	    Properties properties = new Properties();
	    InputStream inputstream
		= DRFactory.class.getResourceAsStream("DRFactory.properties");
	    properties.load(inputstream);
	    inputstream.close();
	    return properties;
	} catch (Exception exception) {
	    throw new RuntimeException(exception.getMessage());
	}
    }
    
    public static Hashtable parseProperties(Properties properties,
					    String string) {
	Hashtable hashtable = new Hashtable();
	hashtable.put("PATIENT",
		      parseIntProperties(properties, "pat." + string));
	hashtable.put("STUDY",
		      parseIntProperties(properties, "sty." + string));
	hashtable.put("SERIES",
		      parseIntProperties(properties, "ser." + string));
	hashtable.put("IMAGE",
		      parseIntProperties(properties, "img." + string));
	hashtable.put("OVERLAY",
		      parseIntProperties(properties, "ovy." + string));
	hashtable.put("MODALITY LUT",
		      parseIntProperties(properties, "mod." + string));
	hashtable.put("VOI LUT",
		      parseIntProperties(properties, "voi." + string));
	hashtable.put("CURVE",
		      parseIntProperties(properties, "crv." + string));
	hashtable.put("TOPIC",
		      parseIntProperties(properties, "tpc." + string));
	hashtable.put("VISIT",
		      parseIntProperties(properties, "vis." + string));
	hashtable.put("RESULT",
		      parseIntProperties(properties, "res." + string));
	hashtable.put("INTERPRETATION",
		      parseIntProperties(properties, "int." + string));
	hashtable.put("STUDY COMPONENT",
		      parseIntProperties(properties, "cmp." + string));
	hashtable.put("STORED PRINT",
		      parseIntProperties(properties, "prt." + string));
	hashtable.put("RT DOSE",
		      parseIntProperties(properties, "rtd." + string));
	hashtable.put("RT STRUCTURE SET",
		      parseIntProperties(properties, "rts." + string));
	hashtable.put("RT PLAN",
		      parseIntProperties(properties, "rtp." + string));
	hashtable.put("RT TREAT RECORD",
		      parseIntProperties(properties, "rtt." + string));
	hashtable.put("PRESENTATION",
		      parseIntProperties(properties, "prs." + string));
	hashtable.put("SR DOCUMENT",
		      parseIntProperties(properties, "srd." + string));
	hashtable.put("KEY OBJECT DOC",
		      parseIntProperties(properties, "kod." + string));
	hashtable.put("RAW DATA",
		      parseIntProperties(properties, "raw." + string));
	return hashtable;
    }
    
    private static int[] parseIntProperties(Properties properties,
					    String string) {
	Vector vector = new Vector();
	String string_26_;
	for (int i = 1;
	     (string_26_ = properties.getProperty(string + i)) != null; i++)
	    vector.addElement(Integer.valueOf(string_26_, 16));
	int[] is = new int[vector.size()];
	for (int i = 0; i < is.length; i++)
	    is[i] = ((Integer) vector.elementAt(i)).intValue();
	return is;
    }
}
