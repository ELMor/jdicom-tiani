/* DicomDir - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.DicomObjectFilterPath;
import com.tiani.dicom.util.DicomObjectPath;
import com.tiani.dicom.util.IDicomObjectFilter;
import com.tiani.dicom.util.UIDUtils;

public class DicomDir extends DicomObject
{
    private Hashtable _records;
    private int _readLock;
    private int _writeLock;
    
    public DicomDir() {
	_records = new Hashtable(179);
	_readLock = 0;
	_writeLock = 0;
    }
    
    public DicomDir(String string, String string_0_) throws DicomException {
	_records = new Hashtable(179);
	_readLock = 0;
	_writeLock = 0;
	this.set(38, string);
	this.setFileMetaInformation
	    (new FileMetaInformation("1.2.840.10008.1.3.10", string_0_,
				     "1.2.840.10008.1.2.1"));
	reset();
    }
    
    public void reset() throws DicomException {
	this.set(44, null);
	this.set(41, new Long(0L));
	this.set(42, new Long(0L));
	this.set(43, new Integer(0));
	_records.clear();
    }
    
    public DicomDir(String string) throws DicomException {
	this(string, UIDUtils.createUID());
    }
    
    public String getFileSetID() {
	return (String) this.get(38);
    }
    
    public String getFileSetUID() {
	DicomObject dicomobject = this.getFileMetaInformation();
	return dicomobject != null ? (String) dicomobject.get(30) : null;
    }
    
    public void write(OutputStream outputstream)
	throws DicomException, IOException {
	super.write(outputstream, true, 8194, false, false);
    }
    
    public void write(OutputStream outputstream, boolean bool, int i,
		      boolean bool_1_, boolean bool_2_) throws DicomException {
	throw new DicomException
		  ("Invalid use of DicomObject.write instead DicomDir.write");
    }
    
    public void init() {
	int i = this.getSize(44);
	_records = new Hashtable(i > 64 ? i : 64);
	for (int i_3_ = 0; i_3_ < i; i_3_++)
	    _records.put(new Long(this.getOffset(44, i_3_)),
			 this.get(44, i_3_));
    }
    
    public void addRootDR(DicomObject dicomobject) throws DicomException {
	Long var_long = _appendDR(dicomobject);
	Long var_long_4_ = (Long) this.get(41);
	if (var_long_4_.longValue() == 0L)
	    this.set(41, var_long);
	else
	    _setNextOffset(var_long_4_, var_long);
    }
    
    public void addLowerDR
	(DicomObject dicomobject, DicomObject dicomobject_5_)
	throws DicomException {
	Long var_long = _appendDR(dicomobject);
	Long var_long_6_ = (Long) dicomobject_5_.get(47);
	if (var_long_6_.longValue() == 0L)
	    dicomobject_5_.set(47, var_long);
	else
	    _setNextOffset(var_long_6_, var_long);
    }
    
    public DicomObjectPath addRootDRpaths
	(DicomObjectPath dicomobjectpath,
	 DicomObjectFilterPath dicomobjectfilterpath)
	throws DicomException {
	DicomObject[] dicomobjects
	    = listRootDRs(dicomobjectfilterpath.first());
	DicomObject dicomobject;
	switch (dicomobjects.length) {
	case 0:
	    addRootDR(dicomobject = dicomobjectpath.first());
	    break;
	case 1:
	    dicomobject = dicomobjects[0];
	    break;
	default:
	    return null;
	}
	DicomObjectPath dicomobjectpath_7_ = dicomobjectpath.remaining();
	return (dicomobjectpath_7_.isEmpty() ? new DicomObjectPath(dicomobject)
		: new DicomObjectPath(dicomobject,
				      addLowerDRpaths(dicomobjectpath_7_,
						      dicomobjectfilterpath
							  .remaining(),
						      dicomobject)));
    }
    
    public DicomObjectPath addLowerDRpaths
	(DicomObjectPath dicomobjectpath,
	 DicomObjectFilterPath dicomobjectfilterpath, DicomObject dicomobject)
	throws DicomException {
	DicomObject[] dicomobjects
	    = listLowerDRs(dicomobjectfilterpath.first(), dicomobject);
	DicomObject dicomobject_8_;
	switch (dicomobjects.length) {
	case 0:
	    addLowerDR(dicomobject_8_ = dicomobjectpath.first(), dicomobject);
	    break;
	case 1:
	    dicomobject_8_ = dicomobjects[0];
	    break;
	default:
	    return null;
	}
	DicomObjectPath dicomobjectpath_9_ = dicomobjectpath.remaining();
	return (dicomobjectpath_9_.isEmpty()
		? new DicomObjectPath(dicomobject_8_)
		: new DicomObjectPath(dicomobject_8_,
				      addLowerDRpaths(dicomobjectpath_9_,
						      dicomobjectfilterpath
							  .remaining(),
						      dicomobject_8_)));
    }
    
    public DicomObject getLastDR() throws DicomException {
	return _findNext((Long) this.get(42), _records, false);
    }
    
    public DicomObject getFirstDR(boolean bool) throws DicomException {
	return _findNext((Long) this.get(41), _records, bool);
    }
    
    public DicomObject getFirstDR() throws DicomException {
	return getFirstDR(true);
    }
    
    public DicomObject getNextDR(DicomObject dicomobject, boolean bool)
	throws DicomException {
	return _findNext((Long) dicomobject.get(45), _records, bool);
    }
    
    public DicomObject getNextDR(DicomObject dicomobject)
	throws DicomException {
	return getNextDR(dicomobject, true);
    }
    
    public DicomObject getLowerDR(DicomObject dicomobject, boolean bool)
	throws DicomException {
	return _findNext((Long) dicomobject.get(47), _records, bool);
    }
    
    public DicomObject getLowerDR(DicomObject dicomobject)
	throws DicomException {
	return getLowerDR(dicomobject, true);
    }
    
    public DicomObject[] listRootDRs(boolean bool) throws DicomException {
	return listNextDRs(getFirstDR(bool), bool);
    }
    
    public DicomObject[] listRootDRs() throws DicomException {
	return listRootDRs(true);
    }
    
    public DicomObject[] listRootDRs(IDicomObjectFilter idicomobjectfilter,
				     boolean bool) throws DicomException {
	return listNextDRs(idicomobjectfilter, getFirstDR(bool), bool);
    }
    
    public DicomObject[] listRootDRs(IDicomObjectFilter idicomobjectfilter)
	throws DicomException {
	return listRootDRs(idicomobjectfilter, true);
    }
    
    public DicomObject[] listLowerDRs
	(DicomObject dicomobject, boolean bool) throws DicomException {
	return listNextDRs(getLowerDR(dicomobject, bool), bool);
    }
    
    public DicomObject[] listLowerDRs(DicomObject dicomobject)
	throws DicomException {
	return listLowerDRs(dicomobject, true);
    }
    
    public DicomObject[] listLowerDRs(IDicomObjectFilter idicomobjectfilter,
				      DicomObject dicomobject,
				      boolean bool) throws DicomException {
	return listNextDRs(idicomobjectfilter, getLowerDR(dicomobject, bool),
			   bool);
    }
    
    public DicomObject[] listLowerDRs
	(IDicomObjectFilter idicomobjectfilter, DicomObject dicomobject)
	throws DicomException {
	return listLowerDRs(idicomobjectfilter, dicomobject, true);
    }
    
    public DicomObject[] listNextDRs(DicomObject dicomobject, boolean bool)
	throws DicomException {
	return listNextDRs(null, dicomobject, bool);
    }
    
    public DicomObject[] listNextDRs(DicomObject dicomobject)
	throws DicomException {
	return listNextDRs(dicomobject, true);
    }
    
    public DicomObjectPath[] listRootDRpaths
	(DicomObjectFilterPath dicomobjectfilterpath, boolean bool)
	throws DicomException {
	Vector vector = new Vector();
	if (!dicomobjectfilterpath.isEmpty()) {
	    DicomObject[] dicomobjects
		= listRootDRs(dicomobjectfilterpath.first(), bool);
	    DicomObjectFilterPath dicomobjectfilterpath_10_
		= dicomobjectfilterpath.remaining();
	    for (int i = 0; i < dicomobjects.length; i++) {
		if (!dicomobjectfilterpath_10_.isEmpty()) {
		    DicomObjectPath[] dicomobjectpaths
			= listLowerDRpaths(dicomobjectfilterpath_10_,
					   dicomobjects[i], bool);
		    if (dicomobjectpaths.length > 0) {
			for (int i_11_ = 0; i_11_ < dicomobjectpaths.length;
			     i_11_++)
			    vector.addElement
				(new DicomObjectPath(dicomobjects[i],
						     dicomobjectpaths[i_11_]));
		    }
		} else
		    vector.addElement(new DicomObjectPath(dicomobjects[i]));
	    }
	}
	DicomObjectPath[] dicomobjectpaths
	    = new DicomObjectPath[vector.size()];
	vector.copyInto(dicomobjectpaths);
	return dicomobjectpaths;
    }
    
    public DicomObjectPath[] listRootDRpaths
	(DicomObjectFilterPath dicomobjectfilterpath) throws DicomException {
	return listRootDRpaths(dicomobjectfilterpath, true);
    }
    
    public DicomObjectPath[] listLowerDRpaths
	(DicomObjectFilterPath dicomobjectfilterpath, DicomObject dicomobject,
	 boolean bool)
	throws DicomException {
	Vector vector = new Vector();
	if (!dicomobjectfilterpath.isEmpty()) {
	    DicomObject[] dicomobjects
		= listLowerDRs(dicomobjectfilterpath.first(), dicomobject,
			       bool);
	    DicomObjectFilterPath dicomobjectfilterpath_12_
		= dicomobjectfilterpath.remaining();
	    for (int i = 0; i < dicomobjects.length; i++) {
		if (!dicomobjectfilterpath_12_.isEmpty()) {
		    DicomObjectPath[] dicomobjectpaths
			= listLowerDRpaths(dicomobjectfilterpath_12_,
					   dicomobjects[i], bool);
		    if (dicomobjectpaths.length > 0) {
			for (int i_13_ = 0; i_13_ < dicomobjectpaths.length;
			     i_13_++)
			    vector.addElement
				(new DicomObjectPath(dicomobjects[i],
						     dicomobjectpaths[i_13_]));
		    }
		} else
		    vector.addElement(new DicomObjectPath(dicomobjects[i]));
	    }
	}
	DicomObjectPath[] dicomobjectpaths
	    = new DicomObjectPath[vector.size()];
	vector.copyInto(dicomobjectpaths);
	return dicomobjectpaths;
    }
    
    public DicomObjectPath[] listLowerDRpaths
	(DicomObjectFilterPath dicomobjectfilterpath, DicomObject dicomobject)
	throws DicomException {
	return listLowerDRpaths(dicomobjectfilterpath, dicomobject, true);
    }
    
    public DicomObject[] listNextDRs(IDicomObjectFilter idicomobjectfilter,
				     DicomObject dicomobject,
				     boolean bool) throws DicomException {
	Vector vector = new Vector();
	for (DicomObject dicomobject_14_ = dicomobject;
	     dicomobject_14_ != null;
	     dicomobject_14_ = getNextDR(dicomobject_14_, bool)) {
	    if (idicomobjectfilter == null
		|| idicomobjectfilter.accept(dicomobject_14_))
		vector.addElement(dicomobject_14_);
	}
	DicomObject[] dicomobjects = new DicomObject[vector.size()];
	vector.copyInto(dicomobjects);
	return dicomobjects;
    }
    
    public DicomObject[] listNextDRs
	(IDicomObjectFilter idicomobjectfilter, DicomObject dicomobject)
	throws DicomException {
	return listNextDRs(idicomobjectfilter, dicomobject, true);
    }
    
    public Vector deleteDR(DicomObject dicomobject) throws DicomException {
	Vector vector = new Vector();
	vector.addElement(dicomobject);
	for (DicomObject dicomobject_15_ = getLowerDR(dicomobject, true);
	     dicomobject_15_ != null;
	     dicomobject_15_ = getNextDR(dicomobject_15_, true)) {
	    Enumeration enumeration = deleteDR(dicomobject_15_).elements();
	    while (enumeration.hasMoreElements())
		vector.addElement(enumeration.nextElement());
	}
	dicomobject.set(46, new Integer(0));
	return vector;
    }
    
    public Vector deleteDRs(DicomObjectFilterPath dicomobjectfilterpath)
	throws DicomException {
	Vector vector = new Vector();
	DicomObjectPath[] dicomobjectpaths
	    = listRootDRpaths(dicomobjectfilterpath);
	for (int i = 0; i < dicomobjectpaths.length; i++) {
	    Enumeration enumeration
		= deleteDR(dicomobjectpaths[i].last()).elements();
	    while (enumeration.hasMoreElements())
		vector.addElement(enumeration.nextElement());
	}
	return vector;
    }
    
    public void replaceDR
	(DicomObject dicomobject, DicomObject dicomobject_16_)
	throws DicomException {
	Long var_long = (Long) dicomobject.get(47);
	dicomobject.set(46, new Integer(0));
	dicomobject.set(47, new Long(0L));
	dicomobject_16_.set(47, var_long);
	Long var_long_17_ = _appendDR(dicomobject_16_);
	Long var_long_18_ = (Long) dicomobject.get(45);
	if (var_long_18_.longValue() == 0L)
	    dicomobject.set(45, var_long_17_);
	else
	    _setNextOffset(var_long_18_, var_long_17_);
    }
    
    public void clearDRs() throws DicomException {
	_records.clear();
	this.set(44, null);
	this.set(41, new Long(0L));
	this.set(42, new Long(0L));
    }
    
    public boolean isCompactable() throws DicomException {
	Enumeration enumeration = _records.elements();
	while (enumeration.hasMoreElements()) {
	    DicomObject dicomobject = (DicomObject) enumeration.nextElement();
	    if (dicomobject.getI(46) == 0)
		return true;
	}
	return false;
    }
    
    public void compact() throws DicomException {
	DicomObject dicomobject = getFirstDR(true);
	Hashtable hashtable = _records;
	_records = new Hashtable(179);
	clearDRs();
	Vector vector = new Vector();
	Long var_long;
	for (/**/; dicomobject != null;
	     dicomobject = _findNext(var_long, hashtable, true)) {
	    vector.addElement(dicomobject);
	    addRootDR(dicomobject);
	    var_long = (Long) dicomobject.get(45);
	    dicomobject.set(45, new Long(0L));
	}
	Enumeration enumeration = vector.elements();
	while (enumeration.hasMoreElements()) {
	    DicomObject dicomobject_19_
		= (DicomObject) enumeration.nextElement();
	    _compactChilds(dicomobject_19_, hashtable);
	}
    }
    
    public synchronized int addReadLock() {
	while (_writeLock > 0) {
	    try {
		this.wait();
	    } catch (InterruptedException interruptedexception) {
		/* empty */
	    }
	}
	return ++_readLock;
    }
    
    public synchronized int addWriteLock() {
	while (_writeLock > 0 || _readLock > 0) {
	    try {
		this.wait();
	    } catch (InterruptedException interruptedexception) {
		/* empty */
	    }
	}
	return ++_writeLock;
    }
    
    public synchronized int releaseReadLock() {
	_readLock--;
	this.notifyAll();
	return _readLock;
    }
    
    public synchronized int releaseWriteLock() {
	_writeLock--;
	this.notifyAll();
	return _writeLock;
    }
    
    private Long _appendDR(DicomObject dicomobject) throws DicomException {
	this.append(44, dicomobject);
	Long var_long = new Long(this.calculateOffset(44, _records.size(),
						      8194, false, false));
	this.set(42, var_long);
	_records.put(var_long, dicomobject);
	return var_long;
    }
    
    private void _setNextOffset(Long var_long, Long var_long_20_)
	throws DicomException {
	Object object = null;
	DicomObject dicomobject;
	do {
	    if ((dicomobject = (DicomObject) _records.get(var_long)) == null)
		throw new DicomException("No Directory Record at offset "
					 + var_long);
	} while ((var_long = (Long) dicomobject.get(45)).longValue() != 0L);
	dicomobject.set(45, var_long_20_);
    }
    
    private void _compactChilds
	(DicomObject dicomobject, Hashtable hashtable) throws DicomException {
	Long var_long = (Long) dicomobject.get(47);
	dicomobject.set(47, new Long(0L));
	for (DicomObject dicomobject_21_ = _findNext(var_long, hashtable,
						     true);
	     dicomobject_21_ != null;
	     dicomobject_21_ = _findNext(var_long, hashtable, true)) {
	    addLowerDR(dicomobject_21_, dicomobject);
	    _compactChilds(dicomobject_21_, hashtable);
	    var_long = (Long) dicomobject_21_.get(45);
	    dicomobject_21_.set(45, new Long(0L));
	}
    }
    
    private DicomObject _findNext(Long var_long, Hashtable hashtable,
				  boolean bool) throws DicomException {
	if (var_long.longValue() == 0L)
	    return null;
	DicomObject dicomobject = (DicomObject) hashtable.get(var_long);
	if (dicomobject == null)
	    throw new DicomException("No Directory Record at offset "
				     + var_long);
	return (!bool || dicomobject.getI(46) != 0 ? dicomobject
		: _findNext((Long) dicomobject.get(45), hashtable, bool));
    }
}
