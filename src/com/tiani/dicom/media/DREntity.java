/* DREntity - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import java.util.Iterator;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.tiani.dicom.util.IDicomObjectFilter;

public class DREntity
{
    private final DRNode first;
    private DRNode last;
    
    class DRIterator implements Iterator
    {
	private DRNode next;
	private DRNode remove;
	private final String type;
	private final IDicomObjectFilter filter;
	
	DRIterator(String string, IDicomObjectFilter idicomobjectfilter)
	    throws DicomException {
	    type = string;
	    filter = idicomobjectfilter;
	    next = first.find(type, filter);
	    remove = null;
	}
	
	public Object next() {
	    remove = next;
	    next = next.getNext();
	    if (next != null) {
		try {
		    next = next.find(type, filter);
		} catch (DicomException dicomexception) {
		    dicomexception.printStackTrace(Debug.out);
		    next = null;
		}
	    }
	    return remove;
	}
	
	public boolean hasNext() {
	    return next != null;
	}
	
	public void remove() {
	    if (remove == null)
		throw new IllegalStateException();
	    remove.delete();
	    remove = null;
	}
    }
    
    public DREntity(DRNode drnode) {
	first = drnode;
	last = drnode.getLast();
    }
    
    public final DRNode getFirst() {
	return first;
    }
    
    public void add(DRNode drnode, Long var_long) {
	last.setNext(drnode, var_long);
	last = drnode.getLast();
    }
    
    public DRNode find(DRNode drnode) {
	return first.find(drnode);
    }
    
    public DRNode find(String string, String string_0_) {
	return first.find(string, string_0_);
    }
    
    public int getCount() {
	int i = 0;
	for (DRNode drnode = first; drnode != null; drnode = drnode.getNext())
	    i++;
	return i;
    }
    
    public int getCount(String string) {
	try {
	    return getCount(string, null);
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.toString());
	}
    }
    
    public int getCount
	(String string, IDicomObjectFilter idicomobjectfilter)
	throws DicomException {
	int i = 0;
	DRNode drnode;
	for (drnode = first;
	     (drnode != null
	      && (drnode = drnode.find(string, idicomobjectfilter)) != null);
	     drnode = drnode.getNext())
	    i++;
	return i;
    }
    
    public Iterator iterator() {
	try {
	    return new DRIterator(null, null);
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.toString());
	}
    }
    
    public Iterator iterator(String string) {
	try {
	    return new DRIterator(string, null);
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.toString());
	}
    }
    
    public Iterator iterator
	(String string, IDicomObjectFilter idicomobjectfilter)
	throws DicomException {
	return new DRIterator(string, idicomobjectfilter);
    }
}
