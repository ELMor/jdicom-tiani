/* StorageCmtConstants - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;

public interface StorageCmtConstants
{
    public static final String SOP_CLASS_UID = "1.2.840.10008.1.20.1";
    public static final String SOP_INSTANCE_UID = "1.2.840.10008.1.20.1.1";
    public static final int PROCESSING_FAILURE = 272;
    public static final int NO_SUCH_OBJECT_INSTANCE = 274;
    public static final int RESOURCE_LIMITATION = 275;
    public static final int REFERENCED_SOP_CLASS_NOT_SUPPORTED = 290;
    public static final int CLASS_INSTANCE_CONFLICT = 281;
    public static final int DUPLICATE_TRANSACTION_UID = 305;
}
