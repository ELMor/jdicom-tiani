/* SkipHeader - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public final class SkipHeader extends DataInputStream
{
    private static byte[] MAGI = { 77, 65, 71, 73 };
    private static byte[] DICM = { 68, 73, 67, 77 };
    private static byte[] GR0008 = { 8, 0, 0, 0 };
    private static final String TS_LE = "1.2.840.10008.1.2";
    private String ts = "1.2.840.10008.1.2";
    private String mgCompress = null;
    private byte[] b4 = new byte[4];
    public static boolean skipGroups = false;
    private InputStream myIn;
    
    public SkipHeader(InputStream inputstream) {
	super(inputstream);
	myIn = inputstream;
    }
    
    public InputStream getUnderlyingStream() {
	return myIn;
    }
    
    public String getTransferSyntax() {
	return ts;
    }
    
    public String getMgCompress() {
	return mgCompress;
    }
    
    public static void main(String[] strings) {
	try {
	    BufferedInputStream bufferedinputstream
		= new BufferedInputStream(new FileInputStream(strings[0]));
	    try {
		SkipHeader skipheader = new SkipHeader(bufferedinputstream);
		int i = skipheader.seekPixelData();
		System.out.println("instance.seekPixelData() returns " + i);
		System.out.println("instance.getTransferSyntax() returns "
				   + skipheader.getTransferSyntax());
		System.out.println("instance.getMgCompress() returns "
				   + skipheader.getMgCompress());
		if (strings.length > 1) {
		    System.out
			.println("write retaining file date to " + strings[1]);
		    BufferedOutputStream bufferedoutputstream
			= new BufferedOutputStream(new FileOutputStream(strings
									[1]));
		    try {
			int i_0_;
			while ((i_0_ = bufferedinputstream.read()) != -1)
			    bufferedoutputstream.write(i_0_);
		    } finally {
			bufferedinputstream.close();
		    }
		}
	    } finally {
		bufferedinputstream.close();
	    }
	} catch (Exception exception) {
	    exception.printStackTrace();
	}
    }
    
    private void skipFully(int i) throws IOException {
	int i_2_;
	for (int i_1_ = i; i_1_ > 0; i_1_ -= i_2_) {
	    if ((i_2_ = this.skipBytes(i)) == 0)
		throw new EOFException();
	}
    }
    
    private int readVRLen() throws IOException {
	int i = readLen();
	switch (i & 0xffff) {
	case 16975:
	case 20053:
	case 20819:
	case 21589:
	case 22351:
	    return readLen();
	default:
	    return i >> 16;
	}
    }
    
    private int readTag() throws IOException {
	this.readFully(b4);
	return (((b4[0] & 0xff) << 16) + ((b4[1] & 0xff) << 24)
		+ ((b4[2] & 0xff) << 0) + ((b4[3] & 0xff) << 8));
    }
    
    private int readLen() throws IOException {
	this.readFully(b4);
	return (((b4[0] & 0xff) << 0) + ((b4[1] & 0xff) << 8)
		+ ((b4[2] & 0xff) << 16) + ((b4[3] & 0xff) << 24));
    }
    
    private boolean isExplicit(int i) {
	return ts != "1.2.840.10008.1.2" || (i & ~0xffff) == 131072;
    }
    
    private void skipAsciiHeader() throws IOException {
	String string;
	while (!"ENDINFO".equals(string = this.readLine())) {
	    if (string.startsWith("COMPRESSION"))
		mgCompress = string.substring(12);
	}
    }
    
    private void skipFileMetaInfo() throws IOException {
	this.skipBytes(124);
	this.readFully(b4, 0, 4);
	if (!Arrays.equals(b4, DICM))
	    throw new IOException("DICOM Format error!");
	this.skipBytes(12);
	parseUntil(131088);
	int i = readVRLen();
	byte[] is = new byte[i];
	this.readFully(is, 0, i);
	if (is[i - 1] == 0)
	    i--;
	String string = new String(is, 0, 0, i);
	if (!"1.2.840.10008.1.2".equals(string))
	    ts = string;
    }
    
    public int seekPixelData_skipAsciiHeader() throws IOException {
	parseUntil(2145386512);
	if (ts != "1.2.840.10008.1.2")
	    this.readFully(b4);
	return readLen();
    }
    
    public int seekPixelData() throws IOException {
	this.readFully(b4, 0, 4);
	if (Arrays.equals(b4, MAGI))
	    skipAsciiHeader();
	else if (Arrays.equals(b4, GR0008)) {
	    this.readFully(b4, 0, 4);
	    this.skipBytes(readLen());
	} else
	    skipFileMetaInfo();
	parseUntil(2145386512);
	if (ts != "1.2.840.10008.1.2")
	    this.readFully(b4);
	return readLen();
    }
    
    private void parseUntil(int i) throws IOException {
	int i_3_;
	while ((i_3_ = readTag()) != i) {
	    int i_4_ = isExplicit(i_3_) ? readVRLen() : readLen();
	    if (skipGroups && (i_3_ & 0xffff) == 0 && i_3_ != 2145386496) {
		if (i_4_ != 4)
		    throw new IOException("DICOM Format error!");
		this.skipBytes(readLen());
	    } else if (i_4_ != -1)
		this.skipBytes(i_4_);
	    else
		parseSQ();
	}
    }
    
    private void parseSQ() throws IOException {
	int i;
	while ((i = readTag()) == -73728) {
	    int i_5_ = readLen();
	    if (i_5_ != -1)
		this.skipBytes(i_5_);
	    else {
		parseUntil(-73715);
		if (readLen() != 0)
		    throw new IOException("DICOM Format error!");
	    }
	}
	if (i != -73507 || readLen() != 0)
	    throw new IOException("DICOM Format error!");
    }
}
