/* StorageCmtSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.io.IOException;
import java.net.Socket;
import java.util.Hashtable;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;

public class StorageCmtSCP implements IDimseRqListener, StorageCmtConstants
{
    public static final int BEFORE_ACTION_RSP = 10;
    public static final int AFTER_ACTION_RSP = 11;
    public static final int OPEN_ASSOC = 20;
    public static final int REUSE_ASSOC = 21;
    public static final int TRY_REUSE_ASSOC = 22;
    private IStrategy strategy;
    private AETable aets;
    private int[] tsids = { 8193 };
    private int timing = 10;
    private int assocMode = 22;
    private int artim = 1000;
    private int maxPduSize = 32768;
    
    private class Context
    {
	DicomObject actionInfo;
	DicomObject result;
	Hashtable cookie = new Hashtable();
	AET aet = null;
	int status = 0;
	String remoteAET;
	String localAET;
	
	Context(DimseExchange dimseexchange, String string, String string_0_,
		DicomMessage dicommessage,
		DicomMessage dicommessage_1_) throws DicomException {
	    actionInfo = dicommessage.getDataset();
	    result = new DicomObject();
	    result.set(118, actionInfo.get(118));
	    remoteAET = dimseexchange.remoteAET();
	    localAET = dimseexchange.localAET();
	    if (!"1.2.840.10008.1.20.1.1".equals(string_0_))
		status = 274;
	    else if (dicommessage.getSize(18) != 1)
		status = 530;
	    else if (dicommessage.getI(18) != 1) {
		dicommessage_1_.set(18, dicommessage.get(18));
		status = 276;
	    } else if (actionInfo.getSize(118) != 1
		       || actionInfo.getSize(121) < 1)
		status = 277;
	    else if (assocMode != 21
		     && (aet = aets.lookup(remoteAET)) == null) {
		dicommessage_1_.set
		    (11, ("Unrecognized AET of Storage Commitment SCU - "
			  + remoteAET));
		status = 272;
	    } else {
		status = strategy.prepare(dimseexchange, actionInfo,
					  dicommessage_1_, cookie);
		if (status == 0 && timing == 10)
		    status = strategy.commit(dimseexchange, actionInfo,
					     dicommessage_1_, cookie, result);
	    }
	    dicommessage_1_.set(9, new Integer(status));
	}
	
	void postProcess(DimseExchange dimseexchange) {
	    if (Status.getStatusEntry(status).getType() != 5) {
		if (timing == 11)
		    status = strategy.commit(dimseexchange, actionInfo, null,
					     cookie, result);
		if (assocMode != 20) {
		    try {
			sendResult(dimseexchange);
			return;
		    } catch (Exception exception) {
			if (assocMode == 21) {
			    Debug.out.println
				("jdicom: ERROR: reuse of association failed: "
				 + exception.getMessage());
			    return;
			}
			if (Debug.DEBUG > 0) {
			    Debug.out.println
				("jdicom: WARNING: reuse of association failed: "
				 + exception.getMessage());
			    Debug.out.println
				("jdicom: -> initiate separate association to send commitment result");
			}
		    }
		}
		try {
		    DimseExchange dimseexchange_2_ = connect();
		    sendResult(dimseexchange_2_);
		    dimseexchange_2_.releaseAssoc();
		} catch (Exception exception) {
		    Debug.out.println
			("jdicom: ERROR: send commitment result in separate association failed: "
			 + exception.getMessage());
		}
	    }
	}
	
	private void sendResult(DimseExchange dimseexchange)
	    throws IOException, IllegalValueException, DicomException,
		   InterruptedException {
	    int i = result.getSize(120) == -1 ? 1 : 2;
	    byte i_3_ = dimseexchange.getPresentationContext(4100, 8193);
	    DicomMessage dicommessage
		= dimseexchange.neventReport(i_3_, "1.2.840.10008.1.20.1",
					     "1.2.840.10008.1.20.1.1", i,
					     result);
	    int i_4_ = dicommessage.getI(9);
	    if (i_4_ != 0)
		Debug.out.println
		    ("jdicom: WARNING: receive N-EVENT-REPORT RSP with status "
		     + Status.getStatusEntry(i_4_));
	}
	
	private DimseExchange connect()
	    throws IOException, IllegalValueException, DicomException,
		   UnknownUIDException {
	    Request request = new Request();
	    request.setCalledTitle(remoteAET);
	    request.setCallingTitle(localAET);
	    request.setMaxPduSize(maxPduSize);
	    request.addPresentationContext(4100, tsids);
	    request.setScuScpRoleSelection(4100, 0, 1);
	    Socket socket = new Socket(aet.host, aet.port);
	    Requestor requestor = new Requestor(socket, request);
	    VerboseAssociation verboseassociation = requestor.openAssoc();
	    if (verboseassociation == null)
		throw new DicomException
			  ("Association rejected by StorageCommitment - "
			   + requestor.response());
	    DimseExchange dimseexchange
		= new DimseExchange(verboseassociation, null, false, false, 1);
	    dimseexchange.setARTIM(artim);
	    new Thread(dimseexchange).start();
	    return dimseexchange;
	}
    }
    
    public static interface IStrategy
    {
	public int prepare(DimseExchange dimseexchange,
			   DicomObject dicomobject, DicomMessage dicommessage,
			   Hashtable hashtable);
	
	public int commit(DimseExchange dimseexchange, DicomObject dicomobject,
			  DicomMessage dicommessage, Hashtable hashtable,
			  DicomObject dicomobject_5_);
    }
    
    public StorageCmtSCP(IStrategy istrategy, AETable aetable) {
	strategy = istrategy;
	aets = aetable;
    }
    
    public void setStrategy(IStrategy istrategy) {
	strategy = istrategy;
    }
    
    public void setAEaets(AETable aetable) {
	aets = aetable;
    }
    
    public void setTiming(int i) {
	timing = i;
    }
    
    public void setAssocMode(int i) {
	assocMode = i;
    }
    
    public void setTransferSyntaxes(int[] is) {
	tsids = is;
    }
    
    public void setARTIM(int i) {
	artim = i;
    }
    
    public void setMaxPduSize(int i) {
	maxPduSize = i;
    }
    
    public void handleRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	if (!"1.2.840.10008.1.20.1".equals(string))
	    throw new RuntimeException
		      ("com.tiani.dicom.service.StorageCommitmentSCP was registered under wrong SOP Class UID: "
		       + string);
	String string_6_ = dicommessage.getS(14);
	DicomMessage dicommessage_7_
	    = new DicomMessage(dicommessage.getPresentationContext(),
			       dicommessage.getAbstractSyntax(), 33072, i,
			       null);
	dicommessage_7_.affectedSOP(string, string_6_);
	Context context = new Context(dimseexchange, string, string_6_,
				      dicommessage, dicommessage_7_);
	dimseexchange.getAssociation().sendMessage(dicommessage_7_);
	context.postProcess(dimseexchange);
    }
    
    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	throw new DicomException("Error: cancel request for N-ACTION service");
    }
}
