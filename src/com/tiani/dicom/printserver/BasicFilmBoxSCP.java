/* BasicFilmBoxSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.printserver;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.framework.DefNCreateSCP;
import com.tiani.dicom.framework.DefNSetSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.UIDUtils;

class BasicFilmBoxSCP
{
    private Param param;
    private static final String[] DISPLAY_FORMATS
	= { "STANDARD", "ROW", "COL" };
    
    public BasicFilmBoxSCP(Param param) {
	this.param = param;
    }
    
    public void setParam(Param param) {
	this.param = param;
    }
    
    public IDimseRqListener getColorImageBoxNSetListener() {
	return new DefNSetSCP() {
	    protected int set
		(DimseExchange dimseexchange, String string, String string_1_,
		 DicomMessage dicommessage, DicomMessage dicommessage_2_)
		throws DicomException {
		DicomObject dicomobject = dicommessage.getDataset();
		if (dicomobject == null)
		    return 288;
		return BasicFilmBoxSCP.this.store(string_1_,
						  "1.2.840.10008.5.1.1.30",
						  ((DicomObject)
						   dicomobject.get(733)));
	    }
	};
    }
    
    public IDimseRqListener getGrayscaleImageBoxNSetListener() {
	return new DefNSetSCP() {
	    protected int set
		(DimseExchange dimseexchange, String string, String string_4_,
		 DicomMessage dicommessage, DicomMessage dicommessage_5_)
		throws DicomException {
		DicomObject dicomobject = dicommessage.getDataset();
		if (dicomobject == null)
		    return 288;
		return BasicFilmBoxSCP.this.store(string_4_,
						  "1.2.840.10008.5.1.1.29",
						  ((DicomObject)
						   dicomobject.get(732)));
	    }
	};
    }
    
    private int store(String string, String string_6_,
		      DicomObject dicomobject) {
	if (dicomobject == null)
	    return 288;
	if (param.isStoreHC()) {
	    try {
		storeHardcopyImage(string,
				   makeHardcopyImage(string_6_, dicomobject));
	    } catch (Exception exception) {
		exception.printStackTrace(Debug.out);
	    }
	}
	return 0;
    }
    
    private DicomObject makeHardcopyImage
	(String string, DicomObject dicomobject) throws DicomException {
	dicomobject.set(147, null);
	dicomobject.set(148, null);
	dicomobject.set(150, null);
	dicomobject.set(152, null);
	dicomobject.set(425, UIDUtils.createUID());
	dicomobject.set(64, null);
	dicomobject.set(70, null);
	dicomobject.set(88, null);
	dicomobject.set(427, null);
	dicomobject.set(77, null);
	dicomobject.set(81, "HC");
	dicomobject.set(426, UIDUtils.createUID());
	dicomobject.set(428, null);
	dicomobject.set(84, null);
	dicomobject.set(430, null);
	dicomobject.set(62, string);
	dicomobject.set(63, UIDUtils.createUID());
	return dicomobject;
    }
    
    private void storeHardcopyImage(String string, DicomObject dicomobject)
	throws DicomException, IOException {
	File file = new File(param.getStoreHCPath());
	if (!file.exists())
	    file.mkdirs();
	UIDEntry uidentry = param.getStoreHCTS();
	FileMetaInformation filemetainformation
	    = new FileMetaInformation(dicomobject, uidentry.getValue());
	dicomobject.setFileMetaInformation(filemetainformation);
	File file_7_ = new File(file, string);
	FileOutputStream fileoutputstream = new FileOutputStream(file_7_);
	try {
	    dicomobject.write(fileoutputstream, true, uidentry.getConstant(),
			      false);
	    if (Debug.DEBUG > 0)
		Debug.out.println("Stored Hardcopy image to " + file_7_);
	} finally {
	    fileoutputstream.close();
	}
    }
    
    public IDimseRqListener getNCreateListener() {
	return new DefNCreateSCP() {
	    protected int create
		(DimseExchange dimseexchange, String string, String string_9_,
		 DicomMessage dicommessage, DicomMessage dicommessage_10_)
		throws DicomException {
		if (string_9_ == null)
		    dicommessage_10_.set(13, UIDUtils.createUID());
		DicomObject dicomobject = dicommessage.getDataset();
		switch (dicommessage.getAbstractSyntax().getConstant()) {
		case 12292:
		    BasicFilmBoxSCP.this
			.setImageBoxSeq(dicomobject, "1.2.840.10008.5.1.1.4");
		    break;
		case 12294:
		    BasicFilmBoxSCP.this.setImageBoxSeq
			(dicomobject, "1.2.840.10008.5.1.1.4.1");
		    break;
		}
		dicommessage_10_.setDataset(dicomobject);
		return 0;
	    }
	};
    }
    
    private void setImageBoxSeq(DicomObject dicomobject, String string)
	throws DicomException {
	int i = 0;
	switch (Param.indexOfIn(dicomobject.getS(713), DISPLAY_FORMATS)) {
	case 0:
	    i = getProduct(dicomobject.getS(713, 1));
	    break;
	case 1:
	case 2:
	    i = getSum(dicomobject.getS(713, 1));
	    break;
	}
	while (i-- > 0) {
	    DicomObject dicomobject_11_ = new DicomObject();
	    dicomobject_11_.set(115, string);
	    dicomobject_11_.set(116, UIDUtils.createUID());
	    dicomobject.append(726, dicomobject_11_);
	}
    }
    
    private static int getProduct(String string) {
	StringTokenizer stringtokenizer = new StringTokenizer(string, ", ");
	try {
	    if (stringtokenizer.countTokens() == 2)
		return (Integer.parseInt(stringtokenizer.nextToken())
			* Integer.parseInt(stringtokenizer.nextToken()));
	} catch (NumberFormatException numberformatexception) {
	    /* empty */
	}
	return 0;
    }
    
    private static int getSum(String string) {
	int i = 0;
	StringTokenizer stringtokenizer = new StringTokenizer(string, ", ");
	try {
	    while (stringtokenizer.hasMoreTokens())
		i += Integer.parseInt(stringtokenizer.nextToken());
	    return i;
	} catch (NumberFormatException numberformatexception) {
	    return 0;
	}
    }
}
