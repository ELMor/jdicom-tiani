/* PrintServerDummy - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.printserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.Properties;

import com.archimed.dicom.Debug;
import com.kcmultimedia.demo.SCMEvent;
import com.kcmultimedia.demo.SCMEventListener;
import com.kcmultimedia.demo.SCMEventManager;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DefNActionSCP;
import com.tiani.dicom.framework.DefNCreateSCP;
import com.tiani.dicom.framework.DefNDeleteSCP;
import com.tiani.dicom.framework.DefNGetSCP;
import com.tiani.dicom.framework.DefNSetSCP;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.IAcceptancePolicy;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.SimpleAcceptancePolicy;
import com.tiani.rmicfg.IServer;

public class PrintServerDummy implements IServer, SCMEventListener
{
    private static final IDimseRqListener _ncreateSCP = new DefNCreateSCP();
    private static final IDimseRqListener _ndeleteSCP = new DefNDeleteSCP();
    private static final IDimseRqListener _nactionSCP = new DefNActionSCP();
    private static final IDimseRqListener _nsetSCP = new DefNSetSCP();
    private static final IDimseRqListener _ngetSCP = new DefNGetSCP();
    private Param _param;
    private Acceptor _acceptor = null;
    private Thread _thread = null;
    private BasicFilmBoxSCP _filmBoxSCP;
    private PrinterSCP _printerSCP;
    private DimseRqManager _rqManager;
    private DefAcceptorListener _acceptorListener;
    
    public PrintServerDummy() {
	this(null);
    }
    
    public PrintServerDummy(Properties properties) {
	SCMEventManager scmeventmanager = SCMEventManager.getInstance();
	scmeventmanager.addSCMEventListener(this);
	_param
	    = new Param(properties != null ? properties : loadDefProperties());
	Debug.DEBUG = _param.getVerbose();
	_filmBoxSCP = new BasicFilmBoxSCP(_param);
	_printerSCP = new PrinterSCP(_param);
	_rqManager = createRqManager(_filmBoxSCP, _printerSCP);
    }
    
    public String getType() throws RemoteException {
	return "PrintServerDummy";
    }
    
    public void setProperties(Properties properties) throws RemoteException {
	_param = new Param(properties);
	Debug.DEBUG = _param.getVerbose();
	synchronized (this) {
	    if (_thread != null) {
		_acceptor.setARTIM(_param.getAssocTimeout(),
				   _param.getReleaseTimeout());
		_acceptor.setStartThread(_param.isMultiThreadTCP());
		_acceptorListener.setStartThread(_param.isMultiThreadAssoc());
		_acceptorListener.setQueueRQ(_param.isQueueRQ());
		_acceptorListener.setARTIM(_param.getReleaseTimeout());
		_acceptor.setAcceptancePolicy(createPolicy());
	    }
	}
	_printerSCP.setParam(_param);
	_filmBoxSCP.setParam(_param);
    }
    
    public String[] getPropertyNames() throws RemoteException {
	return Param.KEYS;
    }
    
    public Properties getProperties() throws RemoteException {
	return _param.getProperties();
    }
    
    public void start() throws RemoteException {
	if (_thread != null)
	    throw new IllegalStateException("server is running");
	try {
	    Debug.out.println("Start PrintServerDummy v1.4.5 at "
			      + new GregorianCalendar().getTime() + " with "
			      + _param);
	    Debug.DEBUG = _param.getVerbose();
	    _acceptorListener
		= new DefAcceptorListener(_param.isMultiThreadAssoc(),
					  _rqManager, _param.isQueueRQ(),
					  false);
	    _acceptor = new Acceptor(new ServerSocket(_param.getPort()),
				     _param.isMultiThreadTCP(), createPolicy(),
				     _acceptorListener);
	    _thread = new Thread(_acceptor);
	    _thread.start();
	    Debug.out.println("Waiting for invocations from clients...");
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
    }
    
    public void start(Properties properties) throws RemoteException {
	setProperties(properties);
	start();
    }
    
    public void stop(boolean bool, boolean bool_0_) throws RemoteException {
	try {
	    if (_thread == null)
		throw new IllegalStateException("server is not running");
	    _acceptor.stop(bool);
	    if (bool_0_)
		_thread.join();
	    synchronized (this) {
		_thread = null;
		_acceptor = null;
		_acceptorListener = null;
	    }
	    Debug.out.println("Stopped PrintServerDummy v1.4.5 at "
			      + new GregorianCalendar().getTime());
	} catch (Throwable throwable) {
	    throwable.printStackTrace(Debug.out);
	    throw new RemoteException("", throwable);
	}
    }
    
    public boolean isRunning() throws RemoteException {
	return _thread != null;
    }
    
    public static void setLog(PrintStream printstream) {
	Debug.out = printstream;
    }
    
    public void handleSCMEvent(SCMEvent scmevent) {
	if (scmevent.getID() == 1) {
	    try {
		if (isRunning())
		    stop(true, false);
	    } catch (Exception exception) {
		exception.printStackTrace(System.out);
	    }
	}
    }
    
    public static void main(String[] strings) {
	try {
	    String string = (strings.length > 0 ? strings[0]
			     : "PrintServerDummy.properties");
	    new PrintServerDummy(loadProperties(string)).start();
	} catch (Throwable throwable) {
	    System.out.println(throwable);
	}
    }
    
    static Properties loadProperties(String string) {
	try {
	    Properties properties = new Properties();
	    File file = new File(string);
	    System.out
		.println("load properties from " + file.getAbsolutePath());
	    FileInputStream fileinputstream = new FileInputStream(file);
	    try {
		properties.load(fileinputstream);
		return properties;
	    } finally {
		fileinputstream.close();
	    }
	} catch (Exception exception) {
	    System.out.println(exception);
	    return null;
	}
    }
    
    private static Properties loadDefProperties() {
	Properties properties = new Properties();
	InputStream inputstream
	    = PrintServerDummy.class
		  .getResourceAsStream("PrintServerDummy.properties");
	try {
	    properties.load(inputstream);
	    inputstream.close();
	    Debug.out.println("Load default properties");
	} catch (Exception exception) {
	    throw new RuntimeException
		      ("PrintServerDummy.class.getResourceAsStream(\"PrintServerDummy.properties\") failed!");
	}
	return properties;
    }
    
    private IAcceptancePolicy createPolicy() {
	int i = 0;
	int[] is = new int[4];
	if (_param.isVerification())
	    is[i++] = 4097;
	if (_param.isBasicGrayscalePrintManagement())
	    is[i++] = 12292;
	if (_param.isBasicColorPrintManagement())
	    is[i++] = 12294;
	if (_param.isPresentationLUT())
	    is[i++] = 4145;
	int[] is_1_ = new int[i];
	System.arraycopy(is, 0, is_1_, 0, i);
	return new SimpleAcceptancePolicy(_param.getCalledTitle(),
					  _param.getCallingTitles(), is_1_,
					  _param.getMaxPduSize(),
					  _param.getMaxInvoke(), 1);
    }
    
    private DimseRqManager createRqManager(BasicFilmBoxSCP basicfilmboxscp,
					   PrinterSCP printerscp) {
	DimseRqManager dimserqmanager = new DimseRqManager();
	dimserqmanager.regCEchoScp("1.2.840.10008.1.1",
				   DefCEchoSCP.getInstance());
	dimserqmanager.regNCreateScp("1.2.840.10008.5.1.1.23", _ncreateSCP);
	dimserqmanager.regNDeleteScp("1.2.840.10008.5.1.1.23", _ndeleteSCP);
	dimserqmanager.regNCreateScp("1.2.840.10008.5.1.1.1", _ncreateSCP);
	dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.1", _nsetSCP);
	dimserqmanager.regNDeleteScp("1.2.840.10008.5.1.1.1", _ndeleteSCP);
	dimserqmanager.regNActionScp("1.2.840.10008.5.1.1.1", _nactionSCP);
	dimserqmanager.regNCreateScp("1.2.840.10008.5.1.1.2",
				     _filmBoxSCP.getNCreateListener());
	dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.2", _nsetSCP);
	dimserqmanager.regNDeleteScp("1.2.840.10008.5.1.1.2", _ndeleteSCP);
	dimserqmanager.regNActionScp("1.2.840.10008.5.1.1.2", _nactionSCP);
	dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.4",
				  _filmBoxSCP
				      .getGrayscaleImageBoxNSetListener());
	dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.4.1",
				  _filmBoxSCP.getColorImageBoxNSetListener());
	dimserqmanager.regNGetScp("1.2.840.10008.5.1.1.16",
				  printerscp.getNGetListener());
	return dimserqmanager;
    }
}
