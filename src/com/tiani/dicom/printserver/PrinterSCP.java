/* PrinterSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.printserver;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefNGetSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;

class PrinterSCP
{
    private Param _param;
    
    public PrinterSCP(Param param) {
	_param = param;
    }
    
    public void setParam(Param param) {
	_param = param;
    }
    
    public IDimseRqListener getNGetListener() {
	return new DefNGetSCP() {
	    protected int get
		(DimseExchange dimseexchange, String string, String string_1_,
		 DicomMessage dicommessage, DicomMessage dicommessage_2_)
		throws DicomException {
		DicomObject dicomobject = PrinterSCP.this.createAttributs();
		DicomObject dicomobject_3_ = dicomobject;
		int i = dicommessage.getSize(16);
		if (i > 0) {
		    dicomobject_3_ = new DicomObject();
		    for (int i_4_ = 0; i_4_ < i; i_4_++)
			copyAttrib(dicommessage.getI(16, i_4_), dicomobject,
				   dicomobject_3_);
		}
		dicommessage_2_.setDataset(dicomobject_3_);
		return 0;
	    }
	};
    }
    
    private DicomObject createAttributs() throws DicomException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(760, _param.getPrinterStatus());
	dicomobject.set(761, _param.getPrinterStatusInfo());
	dicomobject.set(762, _param.getPrinterName());
	dicomobject.set(84, _param.getManufacturer());
	dicomobject.set(105, _param.getManufacturerModelName());
	dicomobject.set(228, _param.getDeviceSerialNumber());
	dicomobject.set(236, _param.getSoftwareVersion());
	dicomobject.set(317, _param.getDateOfLastCalibration());
	dicomobject.set(318, _param.getTimeOfLastCalibration());
	return dicomobject;
    }
    
    private static void copyAttrib
	(int i, DicomObject dicomobject, DicomObject dicomobject_5_)
	throws DicomException {
	int i_6_ = i >> 16;
	int i_7_ = i & 0xffff;
	dicomobject_5_.set_ge(i_6_, i_7_, dicomobject.get_ge(i_6_, i_7_));
    }
}
