/* AttribOverlayFactoryMain - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.overlay;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.util.CommandLineParser;

public class AttribOverlayFactoryMain
{
    private final AttribOverlayFactory _factory;
    private final int _rows;
    private final int _columns;
    private final int _repeat;
    private final boolean _burnin;
    private final String _src;
    private final String _dest;
    private static final String USAGE
	= "Usage: AttribOverlayFactoryMain <src> <dest> [-prop <propFile>] [-burnin]\n                         [-dim <rows> <columns>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n-prop        use specified properties instead default\n <propFile>  properties file path\n-burnin      burn overlays into pixeldata\n-dim         specifies overlay dimension (default equal as pixel data)\n <rows>      number of rows\n <columns>   number of columns\n-repeat      repeat operation <count> times";
    private static final String[] OPTIONS
	= { "", "-prop", "-burnin", "-dim", "-repeat" };
    private static final int[] PARAMNUM = { 2, 1, 0, 2, 1 };
    
    public static void main(String[] strings) {
	try {
	    new AttribOverlayFactoryMain
		(CommandLineParser.parse(strings, OPTIONS, PARAMNUM))
		.execute();
	} catch (IllegalArgumentException illegalargumentexception) {
	    illegalargumentexception.printStackTrace(System.out);
	    System.out.println(illegalargumentexception);
	    System.out.println
		("Usage: AttribOverlayFactoryMain <src> <dest> [-prop <propFile>] [-burnin]\n                         [-dim <rows> <columns>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n-prop        use specified properties instead default\n <propFile>  properties file path\n-burnin      burn overlays into pixeldata\n-dim         specifies overlay dimension (default equal as pixel data)\n <rows>      number of rows\n <columns>   number of columns\n-repeat      repeat operation <count> times");
	} catch (Throwable throwable) {
	    throwable.printStackTrace(System.out);
	}
    }
    
    private AttribOverlayFactoryMain(Hashtable hashtable)
	throws IOException, DicomException {
	_src = CommandLineParser.get(hashtable, "", 0);
	_dest = CommandLineParser.get(hashtable, "", 1);
	_factory
	    = (new AttribOverlayFactory
	       (hashtable.containsKey("-prop")
		? loadProperties(CommandLineParser.get(hashtable, "-prop", 0))
		: null));
	if (hashtable.containsKey("-dim")) {
	    _rows = Integer.parseInt(CommandLineParser.get(hashtable, "-dim",
							   0));
	    _columns = Integer.parseInt(CommandLineParser.get(hashtable,
							      "-dim", 1));
	} else {
	    _rows = 0;
	    _columns = 0;
	}
	_burnin = hashtable.containsKey("-burnin");
	_repeat = (hashtable.containsKey("-repeat")
		   ? Integer.parseInt(CommandLineParser.get(hashtable,
							    "-repeat", 0))
		   : 1);
    }
    
    private void execute() throws IOException, DicomException {
	for (int i = 0; i < _repeat; i++) {
	    FileInputStream fileinputstream = new FileInputStream(_src);
	    DicomObject dicomobject = new DicomObject();
	    try {
		TianiInputStream tianiinputstream
		    = new TianiInputStream(fileinputstream);
		tianiinputstream.read(dicomobject, true);
	    } finally {
		fileinputstream.close();
	    }
	    int i_0_ = _rows > 0 ? _rows : dicomobject.getI(466);
	    int i_1_ = _columns > 0 ? _columns : dicomobject.getI(467) & ~0x7;
	    long l = System.currentTimeMillis();
	    _factory.createOverlay(i_0_, i_1_, dicomobject,
				   Overlay.getFreeOverlayGroup(dicomobject));
	    long l_2_ = System.currentTimeMillis();
	    System.out.println("createOverlay takes "
			       + (float) (l_2_ - l) / 1000.0F + " s");
	    if (_burnin) {
		BurnInOverlay.burnInAll(dicomobject);
		long l_3_ = System.currentTimeMillis();
		System.out.println("burn in takes "
				   + (float) (l_3_ - l_2_) / 1000.0F + " s");
	    }
	    FileOutputStream fileoutputstream = new FileOutputStream(_dest);
	    try {
		dicomobject.write(fileoutputstream, true);
	    } finally {
		fileoutputstream.close();
	    }
	}
    }
    
    private static Properties loadProperties(String string)
	throws IOException {
	FileInputStream fileinputstream = new FileInputStream(string);
	Properties properties = new Properties();
	try {
	    properties.load(fileinputstream);
	} finally {
	    fileinputstream.close();
	}
	return properties;
    }
}
