/* AttribOverlayFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.overlay;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.StringTokenizer;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class AttribOverlayFactory
{
    private static final String[] POS_PO_CH = { "L", "P", "H" };
    private static final String[] NEG_PO_CH = { "R", "A", "F" };
    private final Font _font;
    private final float _minFontSize;
    private final float _maxFontSize;
    private final float _relFontSize;
    private final float _relLineGap;
    private final float _patOrient;
    private final float _relTickLen;
    private final float _relTickWidth;
    private final float _relInset;
    private final Properties _prop;
    private static final Properties _defProp = new Properties();
    private static final int LEFT = 0;
    private static final int CENTER = 1;
    private static final int RIGHT = 2;
    private static final byte[] REVERSE_BYTE;
    
    public AttribOverlayFactory(Properties properties) {
	_prop
	    = properties != null ? (Properties) properties.clone() : _defProp;
	_font = Font.decode(_prop.getProperty("font"));
	_relFontSize = (float) _font.getSize() / 512.0F;
	_minFontSize
	    = (_prop.containsKey("font.min")
	       ? Float.parseFloat(_prop.getProperty("font.min")) : 8.0F);
	_maxFontSize
	    = (_prop.containsKey("font.max")
	       ? Float.parseFloat(_prop.getProperty("font.max")) : 100.0F);
	_relLineGap
	    = (_prop.containsKey("lineGap")
	       ? Float.parseFloat(_prop.getProperty("lineGap")) : 0.0F);
	_patOrient
	    = (_prop.containsKey("patOrient")
	       ? Float.parseFloat(_prop.getProperty("patOrient")) : 0.0F);
	_relTickLen
	    = (_prop.containsKey("tick.len")
	       ? Float.parseFloat(_prop.getProperty("tick.len")) / 512.0F
	       : 0.016F);
	_relTickWidth
	    = (_prop.containsKey("tick.width")
	       ? Float.parseFloat(_prop.getProperty("tick.width")) / 512.0F
	       : 0.0040F);
	_relInset = (_prop.containsKey("inset")
		     ? Float.parseFloat(_prop.getProperty("inset")) / 512.0F
		     : 0.0080F);
    }
    
    public void createOverlay(int i, int i_0_, DicomObject dicomobject,
			      int i_1_) throws DicomException {
	if ((i_0_ & 0x3) != 0)
	    throw new IllegalArgumentException("only columns = 4xN supported");
	byte[] is = createOverlayData(i, i_0_, dicomobject);
	dicomobject.set_ge(i_1_, 16, new Integer(i));
	dicomobject.set_ge(i_1_, 17, new Integer(i_0_));
	dicomobject.set_ge(i_1_, 64, "G");
	dicomobject.set_ge(i_1_, 69, "AUTOMATED");
	dicomobject.set_ge(i_1_, 80, new Integer(1), 0);
	dicomobject.set_ge(i_1_, 80, new Integer(1), 1);
	dicomobject.set_ge(i_1_, 256, new Integer(1));
	dicomobject.set_ge(i_1_, 258, new Integer(0));
	dicomobject.set_ge(i_1_, 12288, is);
    }
    
    private void addPatOrient(DicomObject dicomobject) throws DicomException {
	dicomobject.set(436, (getPatOrient(dicomobject, 0, 0)
			      + getPatOrient(dicomobject, 1, 1)
			      + getPatOrient(dicomobject, 2, 2)));
	dicomobject.set(436, (getPatOrient(dicomobject, 3, 0)
			      + getPatOrient(dicomobject, 4, 1)
			      + getPatOrient(dicomobject, 5, 2)), 1);
    }
    
    private String getPatOrient(DicomObject dicomobject, int i, int i_2_) {
	float f = ((Float) dicomobject.get(441, i)).floatValue();
	if (f > _patOrient)
	    return POS_PO_CH[i_2_];
	if (-f > _patOrient)
	    return NEG_PO_CH[i_2_];
	return "";
    }
    
    private byte[] createOverlayData(int i, int i_3_,
				     DicomObject dicomobject) {
	if (_patOrient > 0.0F && dicomobject.getSize(436) <= 0
	    && dicomobject.getSize(441) == 6) {
	    try {
		addPatOrient(dicomobject);
	    } catch (DicomException dicomexception) {
		Debug.out.println("jdicom: " + dicomexception);
	    }
	}
	float f = Math.min(_maxFontSize,
			   Math.max(_minFontSize, _relFontSize * (float) i));
	float f_4_ = f + _relLineGap * (float) i;
	float f_5_ = _relInset * (float) i;
	BufferedImage bufferedimage = new BufferedImage(i_3_, i, 12);
	Graphics2D graphics2d = bufferedimage.createGraphics();
	Font font = _font.deriveFont(f);
	graphics2d.setFont(font);
	graphics2d.setPaintMode();
	float f_6_ = (float) i - f_5_;
	float f_7_ = (float) i_3_ - f_5_;
	float f_8_ = (f_6_ - (float) countLines("e.") * f_4_) / 2.0F;
	float f_9_ = (f_6_ - (float) countLines("w.") * f_4_) / 2.0F;
	float f_10_ = f_6_ - (float) countLines("se.") * f_4_;
	float f_11_ = f_6_ - (float) countLines("sw.") * f_4_;
	float f_12_ = f_6_ - (float) countLines("s.") * f_4_;
	draw("nw.", dicomobject, graphics2d, font, f_5_, f_5_, f_4_, 0);
	draw("n.", dicomobject, graphics2d, font, (float) (i_3_ / 2), f_5_,
	     f_4_, 1);
	draw("ne.", dicomobject, graphics2d, font, f_7_, f_5_, f_4_, 2);
	draw("w.", dicomobject, graphics2d, font, f_5_, f_9_, f_4_, 0);
	draw("e.", dicomobject, graphics2d, font, f_7_, f_8_, f_4_, 2);
	draw("sw.", dicomobject, graphics2d, font, f_5_, f_11_, f_4_, 0);
	draw("s.", dicomobject, graphics2d, font, (float) (i_3_ / 2), f_12_,
	     f_4_, 1);
	draw("se.", dicomobject, graphics2d, font, f_7_, f_10_, f_4_, 2);
	float f_13_ = getVScaleStep(dicomobject);
	if (f_13_ != 0.0F) {
	    int i_14_ = Math.max((int) (_relTickWidth * (float) i), 1);
	    int i_15_ = Math.max((int) (_relTickLen * (float) i), 3);
	    int i_16_;
	    if ((i_16_ = getIntParam("scale.nee")) > 0)
		drawScale(dicomobject, graphics2d, f_7_, f_8_,
			  f_7_ - (float) i_15_, i_16_, f_13_, i_15_, i_14_);
	    if ((i_16_ = getIntParam("scale.see")) > 0)
		drawScale(dicomobject, graphics2d, f_7_, f_10_,
			  f_7_ - (float) i_15_, i_16_, f_13_, i_15_, i_14_);
	    if ((i_16_ = getIntParam("scale.sww")) > 0)
		drawScale(dicomobject, graphics2d, f_5_, f_11_, f_5_, i_16_,
			  f_13_, i_15_, i_14_);
	    if ((i_16_ = getIntParam("scale.nww")) > 0)
		drawScale(dicomobject, graphics2d, f_5_, f_9_, f_5_, i_16_,
			  f_13_, i_15_, i_14_);
	}
	byte[] is
	    = ((DataBufferByte) bufferedimage.getRaster().getDataBuffer())
		  .getData();
	for (int i_17_ = 0; i_17_ < is.length; i_17_++)
	    is[i_17_] = REVERSE_BYTE[is[i_17_] & 0xff];
	return is;
    }
    
    private void drawScale(DicomObject dicomobject, Graphics2D graphics2d,
			   float f, float f_18_, float f_19_, int i,
			   float f_20_, int i_21_, int i_22_) {
	float f_23_ = f_18_;
	int i_24_ = 0;
	while (i_24_ <= i) {
	    graphics2d.fillRect((int) f_19_, (int) f_23_, i_21_, i_22_);
	    i_24_++;
	    f_23_ -= f_20_;
	}
	f_23_ += f_20_;
	graphics2d.fillRect((int) f, (int) f_23_, i_22_,
			    (int) (f_18_ - f_23_ + (float) i_22_));
    }
    
    private void draw(String string, DicomObject dicomobject,
		      Graphics2D graphics2d, Font font, float f, float f_25_,
		      float f_26_, int i) {
	float f_27_ = f_25_ + f_26_;
	int i_28_ = 1;
	String string_29_;
	while ((string_29_ = _prop.getProperty(string + i_28_)) != null) {
	    try {
		String string_30_ = createLine(string + i_28_ + '.',
					       string_29_, dicomobject);
		if (string_30_.length() > 0) {
		    java.awt.geom.Rectangle2D rectangle2d
			= font.getStringBounds(string_30_,
					       graphics2d
						   .getFontRenderContext());
		    graphics2d.drawString(string_30_,
					  f - ((float) rectangle2d.getWidth()
					       * (float) i / 2.0F),
					  f_27_);
		}
	    } catch (Exception exception) {
		Debug.out.println("jdicom: " + exception);
	    }
	    i_28_++;
	    f_27_ += f_26_;
	}
    }
    
    private static float getVScaleStep(DicomObject dicomobject) {
	try {
	    Float var_float = (Float) dicomobject.get(470, 0);
	    if (var_float == null)
		var_float = (Float) dicomobject.get(308, 0);
	    return var_float != null ? 10.0F / var_float.floatValue() : 0.0F;
	} catch (Exception exception) {
	    Debug.out.println("jdicom: " + exception);
	    return 0.0F;
	}
    }
    
    private int getIntParam(String string) {
	String string_31_ = _prop.getProperty(string);
	return string_31_ != null ? Integer.parseInt(string_31_) : -1;
    }
    
    private int countLines(String string) {
	int i;
	for (i = 0; _prop.containsKey(string + (i + 1)); i++) {
	    /* empty */
	}
	return i;
    }
    
    private String createLine(String string, String string_32_,
			      DicomObject dicomobject) throws DicomException {
	if (string_32_.indexOf('%') == -1)
	    return string_32_;
	StringBuffer stringbuffer = new StringBuffer();
	StringTokenizer stringtokenizer = new StringTokenizer(string_32_, "%");
	if (string_32_.charAt(0) != '%')
	    stringbuffer.append(stringtokenizer.nextToken());
	while (stringtokenizer.hasMoreTokens()) {
	    String string_33_ = stringtokenizer.nextToken();
	    String string_34_ = string + string_33_.charAt(0);
	    String string_35_ = _prop.getProperty(string_34_);
	    if (string_35_ == null || string_35_.length() == 0)
		throw new IllegalArgumentException("Missing property - "
						   + string_34_);
	    stringbuffer.append(getStrFrom(dicomobject, string_35_));
	    stringbuffer.append(string_33_.substring(1));
	}
	return stringbuffer.toString();
    }
    
    private static String getStrFrom
	(DicomObject dicomobject, String string) throws DicomException {
	StringTokenizer stringtokenizer = new StringTokenizer(string, "-");
	int i = Integer.parseInt(stringtokenizer.nextToken(), 16);
	int i_36_ = i >> 16;
	int i_37_ = i & 0xffff;
	int i_38_ = (stringtokenizer.hasMoreTokens()
		     ? Integer.parseInt(stringtokenizer.nextToken()) - 1 : 0);
	String string_39_ = dicomobject.getS_ge(i_36_, i_37_, i_38_);
	return string_39_ != null ? string_39_ : "";
    }
    
    static {
	try {
	    InputStream inputstream
		= AttribOverlayFactory.class
		      .getResourceAsStream("AttribOverlayFactory.properties");
	    _defProp.load(inputstream);
	    inputstream.close();
	} catch (IOException ioexception) {
	    throw new RuntimeException(ioexception.toString());
	}
	REVERSE_BYTE
	    = (new byte[]
	       { 0, -128, 64, -64, 32, -96, 96, -32, 16, -112, 80, -48, 48,
		 -80, 112, -16, 8, -120, 72, -56, 40, -88, 104, -24, 24, -104,
		 88, -40, 56, -72, 120, -8, 4, -124, 68, -60, 36, -92, 100,
		 -28, 20, -108, 84, -44, 52, -76, 116, -12, 12, -116, 76, -52,
		 44, -84, 108, -20, 28, -100, 92, -36, 60, -68, 124, -4, 2,
		 -126, 66, -62, 34, -94, 98, -30, 18, -110, 82, -46, 50, -78,
		 114, -14, 10, -118, 74, -54, 42, -86, 106, -22, 26, -102, 90,
		 -38, 58, -70, 122, -6, 6, -122, 70, -58, 38, -90, 102, -26,
		 22, -106, 86, -42, 54, -74, 118, -10, 14, -114, 78, -50, 46,
		 -82, 110, -18, 30, -98, 94, -34, 62, -66, 126, -2, 1, -127,
		 65, -63, 33, -95, 97, -31, 17, -111, 81, -47, 49, -79, 113,
		 -15, 9, -119, 73, -55, 41, -87, 105, -23, 25, -103, 89, -39,
		 57, -71, 121, -7, 5, -123, 69, -59, 37, -91, 101, -27, 21,
		 -107, 85, -43, 53, -75, 117, -11, 13, -115, 77, -51, 45, -83,
		 109, -19, 29, -99, 93, -35, 61, -67, 125, -3, 3, -125, 67,
		 -61, 35, -93, 99, -29, 19, -109, 83, -45, 51, -77, 115, -13,
		 11, -117, 75, -53, 43, -85, 107, -21, 27, -101, 91, -37, 59,
		 -69, 123, -5, 7, -121, 71, -57, 39, -89, 103, -25, 23, -105,
		 87, -41, 55, -73, 119, -9, 15, -113, 79, -49, 47, -81, 111,
		 -17, 31, -97, 95, -33, 63, -65, 127, -1 });
    }
}
