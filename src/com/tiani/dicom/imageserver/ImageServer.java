/* ImageServer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.Properties;

import com.archimed.dicom.Debug;
import com.kcmultimedia.demo.SCMEvent;
import com.kcmultimedia.demo.SCMEventListener;
import com.kcmultimedia.demo.SCMEventManager;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.AdvancedAcceptancePolicy;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.service.DimseRouter;
import com.tiani.dicom.service.StorageCmtSCP;
import com.tiani.dicom.util.AETable;
import com.tiani.rmicfg.IServer;

public class ImageServer implements IServer, SCMEventListener
{
    private Param param;
    private final AETable aets = new AETable();
    private final StorageManagerFactory storageMgrFty;
    private final StorageSCP storageSCP;
    private final QuerySCP querySCP;
    private final RetrieveSCP retrieveSCP;
    private final StgCmtSCPStrategy stgCmtSCPStrategy;
    private final StorageCmtSCP storageCmtSCP;
    private final StudyContentNotificationSCU scnSCU;
    private final FwdConnectionPool fwdConnectionPool;
    private final FwdConnectionFactory fwdStorageCmtConnectionFactory;
    private final FwdConnectionFactory fwdQueryConnectionFactory;
    private final FwdConnectionFactory fwdRetrieveConnectionFactory;
    private final DimseRouter storageCmtRouter;
    private final DimseRouter queryRouter;
    private final DimseRouter retrieveRouter;
    private Acceptor acceptor = null;
    private Thread thread = null;
    private final DimseRqManager rqManager = new DimseRqManager();
    private final AdvancedAcceptancePolicy acceptancePolicy
	= new AdvancedAcceptancePolicy();
    private DefAcceptorListener acceptorListener = null;
    static final int[] IMAGE_STORAGE_AS
	= { 4118, 4119, 4120, 4121, 4122, 4123, 4128, 4129, 4130, 4131, 4148,
	    4149, 4151, 4153, 4157, 4158, 4159, 4161, 4162, 4163, 4164, 4176,
	    4177, 4178, 4179, 4180, 4181 };
    static final int[] NON_IMAGE_STORAGE_AS
	= { 4165, 4154, 4155, 4156, 4182, 4183, 4184, 4166, 4167, 4168, 4194,
	    4124, 4125, 4126, 4127, 4152, 4147, 4196, 4195 };
    
    public ImageServer() {
	this(null);
    }
    
    public ImageServer(Properties properties) {
	SCMEventManager scmeventmanager = SCMEventManager.getInstance();
	scmeventmanager.addSCMEventListener(this);
	param
	    = new Param(properties != null ? properties : loadDefProperties());
	Debug.DEBUG = param.getVerbose();
	updateAETs(aets, param.getAETProperties());
	storageMgrFty = new StorageManagerFactory(param);
	scnSCU = new StudyContentNotificationSCU(param, storageMgrFty, aets);
	storageSCP = new StorageSCP(param, storageMgrFty, scnSCU);
	querySCP = new QuerySCP(param, storageMgrFty);
	retrieveSCP = new RetrieveSCP(param, storageMgrFty, aets);
	stgCmtSCPStrategy = new StgCmtSCPStrategy(storageMgrFty, aets);
	storageCmtSCP = new StorageCmtSCP(stgCmtSCPStrategy, aets);
	fwdConnectionPool = new FwdConnectionPool();
	fwdStorageCmtConnectionFactory
	    = new FwdConnectionFactory.StorageCmt(param, aets,
						  fwdConnectionPool);
	fwdQueryConnectionFactory
	    = new FwdConnectionFactory.Query(param, aets, fwdConnectionPool);
	fwdRetrieveConnectionFactory
	    = new FwdConnectionFactory.Retrieve(param, aets,
						fwdConnectionPool);
	storageCmtRouter
	    = new DimseRouter(fwdStorageCmtConnectionFactory, storageCmtSCP);
	queryRouter = new DimseRouter(fwdQueryConnectionFactory, querySCP);
	retrieveRouter
	    = new DimseRouter(fwdRetrieveConnectionFactory, retrieveSCP);
	updateStorageCmtSCP(param);
	initRqManager();
	updateAcceptancePolicy();
	acceptorListener
	    = new DefAcceptorListener(param.isMultiThreadAssoc(), rqManager,
				      param.isQueueRQ(), false);
	acceptorListener.setARTIM(param.getReleaseTimeout());
    }
    
    private void updateAcceptancePolicy() {
	try {
	    acceptancePolicy.setCalledTitles(param.getCalledTitles());
	    acceptancePolicy.setCallingTitles(param.getCallingTitles());
	    acceptancePolicy.setMaxPduSize(param.getMaxPduSize());
	    acceptancePolicy
		.setMaxOperationsInvoked(param.getMaxOperationsInvoked());
	    if (param.isVerification())
		acceptancePolicy
		    .addPresentationContext(4097, param.getVerificationTS());
	    if (param.isStorage()) {
		for (int i = 0; i < IMAGE_STORAGE_AS.length; i++)
		    acceptancePolicy.addPresentationContext
			(IMAGE_STORAGE_AS[i], param.getImageStorageTS());
		for (int i = 0; i < NON_IMAGE_STORAGE_AS.length; i++)
		    acceptancePolicy.addPresentationContext
			(NON_IMAGE_STORAGE_AS[i],
			 param.getNonImageStorageTS());
	    }
	    if (param.isStorageCmt())
		acceptancePolicy
		    .addPresentationContext(4100, param.getStorageCmtTS());
	    if (param.isQRPatRoot()) {
		acceptancePolicy
		    .addPresentationContext(4132, param.getQueryRetrieveTS());
		acceptancePolicy
		    .addPresentationContext(4133, param.getQueryRetrieveTS());
	    }
	    if (param.isQRStudyRoot()) {
		acceptancePolicy
		    .addPresentationContext(4135, param.getQueryRetrieveTS());
		acceptancePolicy
		    .addPresentationContext(4136, param.getQueryRetrieveTS());
	    }
	    if (param.isQRPatStudyOnly()) {
		acceptancePolicy
		    .addPresentationContext(4138, param.getQueryRetrieveTS());
		acceptancePolicy
		    .addPresentationContext(4139, param.getQueryRetrieveTS());
	    }
	} catch (Exception exception) {
	    throw new RuntimeException(exception.toString());
	}
    }
    
    private void initRqManager() {
	rqManager.regCEchoScp("1.2.840.10008.1.1", DefCEchoSCP.getInstance());
	rqManager.regCFindScp("1.2.840.10008.5.1.4.1.2.1.1", queryRouter);
	rqManager.regCMoveScp("1.2.840.10008.5.1.4.1.2.1.2", retrieveRouter);
	rqManager.regCFindScp("1.2.840.10008.5.1.4.1.2.2.1", queryRouter);
	rqManager.regCMoveScp("1.2.840.10008.5.1.4.1.2.2.2", retrieveRouter);
	rqManager.regCFindScp("1.2.840.10008.5.1.4.1.2.3.1", queryRouter);
	rqManager.regCMoveScp("1.2.840.10008.5.1.4.1.2.3.2", retrieveRouter);
	rqManager.regNActionScp("1.2.840.10008.1.20.1", storageCmtRouter);
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.2",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.3.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.4",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.6.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.7",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.2",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.3",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.20",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.1.29",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.1.20",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.128",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.3",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.5",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.6",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.2",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.3",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.4",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.1.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.2",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.2.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.3",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.3.1",
			       storageSCP.getImageSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.11.1",
			       storageSCP.getGspsSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.2",
			       storageSCP.getRtDoseSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.3",
			       storageSCP.getRtStructuredSetSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.5",
			       storageSCP.getRtPlanSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.4",
			       storageSCP.getRtTreatRecordSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.6",
			       storageSCP.getRtTreatRecordSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.7",
			       storageSCP.getRtTreatRecordSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.11",
			       storageSCP.getSrSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.22",
			       storageSCP.getSrSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.33",
			       storageSCP.getSrSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.59",
			       storageSCP.getKoSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.8",
			       storageSCP.getOverlaySCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.9",
			       storageSCP.getCurveSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.10",
			       storageSCP.getModalityLutSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.11",
			       storageSCP.getVoiLutSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.129",
			       storageSCP.getCurveSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.1.27",
			       storageSCP.getStoredPrintSCP());
	rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.66",
			       storageSCP.getRawDataSCP());
	rqManager.regCStoreScp("1.2.826.0.1.3680043.2.242.1.1",
			       storageSCP.getRawDataSCP());
    }
    
    static void updateAETs(AETable aetable, String string) {
	aetable.clear();
	if (string != null) {
	    try {
		InputStream inputstream
		    = (string.indexOf(':') > 1
		       ? (InputStream) new URL(string).openStream()
		       : new FileInputStream(string));
		try {
		    aetable.load(inputstream);
		    if (Debug.DEBUG > 0)
			Debug.out
			    .println("Load AET properties from " + string);
		} finally {
		    inputstream.close();
		}
		if (Debug.DEBUG > 0)
		    aetable.list(Debug.out);
	    } catch (Exception exception) {
		Debug.out.println(exception);
	    }
	}
    }
    
    private void updateStorageCmtSCP(Param param) {
	storageCmtSCP.setTiming(param.getStorageCmtTiming());
	storageCmtSCP.setAssocMode(param.getStorageCmtAssocMode());
	storageCmtSCP.setMaxPduSize(param.getMaxPduSize());
	storageCmtSCP.setARTIM(param.getReleaseTimeout());
    }
    
    public String getType() throws RemoteException {
	return "ImageServer";
    }
    
    public void setProperties(Properties properties) throws RemoteException {
	param = new Param(properties);
	Debug.DEBUG = param.getVerbose();
	updateAETs(aets, param.getAETProperties());
	updateStorageCmtSCP(param);
	storageMgrFty.setParam(param);
	scnSCU.setParam(param);
	storageSCP.setParam(param);
	querySCP.setParam(param);
	retrieveSCP.setParam(param);
	fwdQueryConnectionFactory.setParam(param);
	fwdRetrieveConnectionFactory.setParam(param);
	updateAcceptancePolicy();
	acceptorListener.setQueueRQ(param.isQueueRQ());
	acceptorListener.setARTIM(param.getReleaseTimeout());
	acceptorListener.setStartThread(param.isMultiThreadAssoc());
	if (acceptor != null) {
	    acceptor.setARTIM(param.getReleaseTimeout());
	    acceptor.setStartThread(param.isMultiThreadTCP());
	}
    }
    
    public String[] getPropertyNames() throws RemoteException {
	return Param.KEYS;
    }
    
    public Properties getProperties() throws RemoteException {
	return param.getProperties();
    }
    
    public void start() throws RemoteException {
	if (thread != null)
	    throw new IllegalStateException("server is running");
	try {
	    Debug.out.println("Start ImageServer v1.7.29 at "
			      + new GregorianCalendar().getTime() + " with "
			      + param);
	    acceptor = new Acceptor(new ServerSocket(param.getPort()),
				    param.isMultiThreadTCP(), acceptancePolicy,
				    acceptorListener);
	    acceptor.setARTIM(param.getAssocTimeout(),
			      param.getReleaseTimeout());
	    acceptor.addAssociationListener(storageSCP);
	    thread = new Thread(acceptor);
	    thread.start();
	    Debug.out.println("Waiting for invocations from clients...");
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
    }
    
    public void start(Properties properties) throws RemoteException {
	setProperties(properties);
	start();
    }
    
    public void stop(boolean bool, boolean bool_0_) throws RemoteException {
	try {
	    if (thread == null)
		throw new IllegalStateException("server is not running");
	    acceptor.stop(bool);
	    if (bool_0_)
		thread.join();
	    synchronized (this) {
		thread = null;
		acceptor = null;
	    }
	    Debug.out.println("Stopped ImageServer v1.7.31 at "
			      + new GregorianCalendar().getTime());
	} catch (Throwable throwable) {
	    throwable.printStackTrace(Debug.out);
	    throw new RemoteException("", throwable);
	}
    }
    
    public boolean isRunning() throws RemoteException {
	return thread != null;
    }
    
    public static void setLog(PrintStream printstream) {
	Debug.out = printstream;
    }
    
    private static Properties loadDefProperties() {
	Properties properties = new Properties();
	InputStream inputstream
	    = ImageServer.class.getResourceAsStream("ImageServer.properties");
	try {
	    properties.load(inputstream);
	    inputstream.close();
	    Debug.out.println("Load default properties");
	} catch (Exception exception) {
	    throw new RuntimeException
		      ("ImageServer.class.getResourceAsStream(\"ImageServer.properties\") failed!");
	}
	return properties;
    }
    
    static Properties loadProperties(String string) {
	try {
	    Properties properties = new Properties();
	    File file = new File(string);
	    System.out
		.println("load properties from " + file.getAbsolutePath());
	    FileInputStream fileinputstream = new FileInputStream(file);
	    try {
		properties.load(fileinputstream);
		return properties;
	    } finally {
		fileinputstream.close();
	    }
	} catch (Exception exception) {
	    System.out.println(exception);
	    return null;
	}
    }
    
    public static void main(String[] strings) {
	try {
	    String string
		= strings.length > 0 ? strings[0] : "ImageServer.properties";
	    new ImageServer(loadProperties(string)).start();
	} catch (Throwable throwable) {
	    System.out.println(throwable);
	}
    }
    
    public void handleSCMEvent(SCMEvent scmevent) {
	if (scmevent.getID() == 1) {
	    try {
		if (isRunning())
		    stop(true, false);
	    } catch (Exception exception) {
		exception.printStackTrace(System.out);
	    }
	}
    }
}
