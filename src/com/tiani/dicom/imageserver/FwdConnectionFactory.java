/* FwdConnectionFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.io.IOException;
import java.net.Socket;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.service.DimseRouter;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;

abstract class FwdConnectionFactory implements DimseRouter.IConnectionFactory
{
    protected Param param;
    private final AETable aets;
    private final FwdConnectionPool pool;
    private static final int[] AS_IDS
	= { 4132, 4135, 4138, 4133, 4136, 4139, 4100 };
    private static final int[] ONLY_DEF_TS = { 8193 };
    
    static class Retrieve extends FwdConnectionFactory
    {
	Retrieve(Param param, AETable aetable,
		 FwdConnectionPool fwdconnectionpool) {
	    super(param, aetable, fwdconnectionpool);
	}
	
	protected boolean isFwd() {
	    return param.isFwdRetrieve();
	}
    }
    
    static class Query extends FwdConnectionFactory
    {
	Query(Param param, AETable aetable,
	      FwdConnectionPool fwdconnectionpool) {
	    super(param, aetable, fwdconnectionpool);
	}
	
	protected boolean isFwd() {
	    return param.isFwdQuery();
	}
    }
    
    static class StorageCmt extends FwdConnectionFactory
    {
	StorageCmt(Param param, AETable aetable,
		   FwdConnectionPool fwdconnectionpool) {
	    super(param, aetable, fwdconnectionpool);
	}
	
	protected boolean isFwd() {
	    return param.isFwdStorageCmt();
	}
    }
    
    protected FwdConnectionFactory(Param param, AETable aetable,
				   FwdConnectionPool fwdconnectionpool) {
	aets = aetable;
	pool = fwdconnectionpool;
	setParam(param);
    }
    
    protected abstract boolean isFwd();
    
    public void setParam(Param param) {
	this.param = param;
    }
    
    public void receivedRSP(DimseExchange dimseexchange,
			    DimseExchange dimseexchange_0_, int i, int i_1_,
			    DicomMessage dicommessage) {
	if (!param.isFwdKeepOpen() && !Status.isPending(i_1_))
	    pool.removeAndRelease(dimseexchange.getAssociation());
    }
    
    private Request createRequest(VerboseAssociation verboseassociation)
	throws IllegalValueException {
	Request request = new Request();
	request.setCalledTitle(param.getFwdAET());
	request.setCallingTitle(param.isFwdCallingTitle()
				? verboseassociation.remoteAET()
				: verboseassociation.localAET());
	request.setMaxPduSize(param.getMaxPduSize());
	if (param.getMaxOperationsInvoked() != 1) {
	    request.setMaxOperationsInvoked(param.getMaxOperationsInvoked());
	    request.setMaxOperationsPerformed(1);
	}
	for (int i = 0; i < AS_IDS.length; i++)
	    request.addPresentationContext(AS_IDS[i], ONLY_DEF_TS);
	return request;
    }
    
    public DimseExchange getConnectionForDimseRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, IllegalValueException, UnknownUIDException {
	if (!isFwd())
	    return null;
	VerboseAssociation verboseassociation = dimseexchange.getAssociation();
	DimseExchange dimseexchange_2_ = pool.get(verboseassociation);
	if (dimseexchange_2_ != null)
	    return dimseexchange_2_;
	AET aet = aets.lookup(param.getFwdAET());
	if (aet == null)
	    throw new IOException("Unknown AET: " + param.getFwdAET());
	Socket socket = new Socket(aet.host, aet.port);
	Requestor requestor
	    = new Requestor(socket, createRequest(verboseassociation));
	VerboseAssociation verboseassociation_3_ = requestor.openAssoc();
	if (verboseassociation_3_ == null)
	    throw new IOException("Association rejected: "
				  + requestor.response());
	Acknowledge acknowledge = (Acknowledge) requestor.response();
	dimseexchange_2_
	    = new DimseExchange(verboseassociation_3_, null, false, false,
				acknowledge.getMaxOperationsInvoked());
	new Thread(dimseexchange_2_).start();
	pool.put(verboseassociation, dimseexchange_2_);
	return dimseexchange_2_;
    }
    
    public DimseExchange getConnectionForCancelRQ
	(DimseExchange dimseexchange, int i) throws IOException {
	if (!isFwd())
	    return null;
	DimseExchange dimseexchange_4_
	    = pool.get(dimseexchange.getAssociation());
	if (dimseexchange_4_ != null)
	    return dimseexchange_4_;
	throw new IOException("No connection for cancel request available");
    }
}
