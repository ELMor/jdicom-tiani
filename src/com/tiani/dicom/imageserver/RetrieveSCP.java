/* RetrieveSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.util.ArrayList;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefCMoveSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IMultiResponse;
import com.tiani.dicom.framework.SingleResponse;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.service.MoveStorageSCU;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import com.tiani.dicom.util.AndDicomObjectFilter;
import com.tiani.dicom.util.DOFFactory;
import com.tiani.dicom.util.IDicomObjectFilter;

class RetrieveSCP extends DefCMoveSCP
{
    private AETable aets;
    private StorageManagerFactory storageMgrFty;
    private Param param;
    
    public RetrieveSCP(Param param,
		       StorageManagerFactory storagemanagerfactory,
		       AETable aetable) {
	aets = aetable;
	storageMgrFty = storagemanagerfactory;
	setParam(param);
    }
    
    public void setParam(Param param) {
	this.param = param;
    }
    
    protected IMultiResponse query(DimseExchange dimseexchange, int i,
				   String string, DicomMessage dicommessage) {
	String string_0_ = dimseexchange.localAET();
	String string_1_ = (String) dicommessage.get(6);
	if (string_1_ == null || string_1_.length() == 0)
	    return new SingleResponse(43264);
	AET aet = aets.lookup(string_1_);
	if (aet == null)
	    return new SingleResponse(43009);
	StorageManager storagemanager;
	ArrayList arraylist;
	try {
	    storagemanager
		= storageMgrFty.getStorageManagerForAET(string_0_, false);
	    if (storagemanager == null)
		return SingleResponse.NO_MATCH;
	    DicomObject dicomobject = dicommessage.getDataset();
	    String string_2_ = dicomobject.getS(78);
	    IDicomObjectFilter[] idicomobjectfilters
		= new IDicomObjectFilter[4];
	    idicomobjectfilters[0]
		= DOFFactory.getPatientDOFFactory().getFilter(dicomobject);
	    if (!"PATIENT".equals(string_2_)) {
		idicomobjectfilters[1]
		    = DOFFactory.getStudyDOFFactory().getFilter(dicomobject);
		if (!"STUDY".equals(string_2_)) {
		    idicomobjectfilters[2] = DOFFactory.getSeriesDOFFactory
						 ().getFilter(dicomobject);
		    if (!"SERIES".equals(string_2_)) {
			idicomobjectfilters[3] = DOFFactory.getImageDOFFactory
						     ().getFilter(dicomobject);
			IDicomObjectFilter idicomobjectfilter
			    = (DRFactory.createRefSOPFilter
			       ((String) dicomobject.get(62),
				(String) dicomobject.get(63)));
			if (idicomobjectfilter != null)
			    idicomobjectfilters[3]
				= (new AndDicomObjectFilter
				   (new IDicomObjectFilter[]
				    { idicomobjectfilters[3],
				      idicomobjectfilter }));
		    }
		}
	    }
	    DicomDir2 dicomdir2 = storagemanager.getDirInfo();
	    dicomdir2.addReadLock();
	    try {
		arraylist
		    = dicomdir2.listNodePaths(DRFactory.INSTANCE_TYPE_PATH,
					      idicomobjectfilters);
	    } finally {
		dicomdir2.releaseReadLock();
	    }
	    if (arraylist.isEmpty())
		return SingleResponse.NO_MATCH;
	} catch (Exception exception) {
	    Debug.out.println(exception);
	    return new SingleResponse(42753);
	}
	try {
	    MoveStgSCUStrategy movestgscustrategy
		= new MoveStgSCUStrategy(arraylist,
					 storagemanager.getFileset());
	    MoveStorageSCU movestoragescu
		= new MoveStorageSCU(movestgscustrategy,
				     dimseexchange.remoteAET(),
				     dicommessage.getI(4),
				     movestgscustrategy.getInstanceUIDs(),
				     movestgscustrategy.getASIDs());
	    movestoragescu.setMaxNumOpInvoked(param.getMaxOperationsInvoked());
	    movestoragescu.setMaxPduSize(param.getMaxPduSize());
	    movestoragescu.setPriority(param.getMovePriority());
	    movestoragescu.setARTIM(param.getReleaseTimeout());
	    com.archimed.dicom.network.Response response
		= movestoragescu.connect(aet.host, aet.port, aet.title,
					 dimseexchange.localAET(),
					 movestgscustrategy.getASIDs());
	    if (!movestoragescu.isConnected()) {
		Debug.out
		    .println("Could not connect to move destination: " + aet);
		return new SingleResponse(49152);
	    }
	    return movestoragescu;
	} catch (Exception exception) {
	    Debug.out.println(exception);
	    return new SingleResponse(49152);
	}
    }
}
