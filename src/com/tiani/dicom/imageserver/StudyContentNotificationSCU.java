/* StudyContentNotificationSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.media.DREntity;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.service.SimpleStorageSCU;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import com.tiani.dicom.util.BasicStudyDescriptor;
import com.tiani.dicom.util.UIDUtils;

class StudyContentNotificationSCU
{
    private static final int[] ONLY_SCN_ASID = { 4099 };
    private Param param;
    private final AETable aets;
    private final StorageManagerFactory storageMgrFty;
    private final Hashtable assocMap = new Hashtable();
    
    private class AssocMapItem
    {
	DicomDir2 dirInfo = null;
	DicomObject retrImageInfo = null;
	final Hashtable studyDescriptors = new Hashtable();
	
	public AssocMapItem(VerboseAssociation verboseassociation) {
	    try {
		StorageManager storagemanager
		    = storageMgrFty.getStorageManagerForAET(verboseassociation
								.localAET(),
							    false);
		if (storagemanager != null) {
		    dirInfo = storagemanager.getDirInfo();
		    retrImageInfo
			= (BasicStudyDescriptor.getRetrieveInfo
			   (verboseassociation.localAET(),
			    dirInfo.getFileSetID(), dirInfo.getFileSetUID()));
		}
	    } catch (Exception exception) {
		Debug.out.println(exception);
	    }
	}
	
	public boolean addRetrImage(DicomObject dicomobject)
	    throws DicomException {
	    String string = (String) dicomobject.get(425);
	    BasicStudyDescriptor basicstudydescriptor
		= (BasicStudyDescriptor) studyDescriptors.get(string);
	    if (basicstudydescriptor == null) {
		basicstudydescriptor
		    = new BasicStudyDescriptor(UIDUtils.createUID());
		studyDescriptors.put(string, basicstudydescriptor);
	    }
	    return basicstudydescriptor.addRetrImage(dicomobject,
						     retrImageInfo);
	}
	
	public Hashtable getStudyDescriptors() {
	    if (param.isSCNTotal() && dirInfo != null) {
		try {
		    return loadStudyDescriptors();
		} catch (Exception exception) {
		    Debug.out.println(exception);
		}
	    }
	    return studyDescriptors;
	}
	
	private DRNode findStudyNode(String string, String string_0_) {
	    DREntity drentity = dirInfo.getRootEntity();
	    DRNode drnode = drentity.find("PATIENT", string);
	    if (drnode == null)
		return null;
	    DREntity drentity_1_ = drnode.getLowerEntity();
	    if (drentity_1_ == null)
		return null;
	    return drentity_1_.find("STUDY", string_0_);
	}
	
	private Hashtable loadStudyDescriptors() throws DicomException {
	    Hashtable hashtable = new Hashtable();
	    dirInfo.addReadLock();
	    try {
		Enumeration enumeration = studyDescriptors.elements();
		while (enumeration.hasMoreElements()) {
		    BasicStudyDescriptor basicstudydescriptor
			= (BasicStudyDescriptor) enumeration.nextElement();
		    String string = basicstudydescriptor.getS(148);
		    String string_2_ = basicstudydescriptor.getS(147);
		    String string_3_ = basicstudydescriptor.getS(427);
		    String string_4_ = basicstudydescriptor.getS(425);
		    DRNode drnode = findStudyNode(string, string_4_);
		    if (drnode != null) {
			BasicStudyDescriptor basicstudydescriptor_5_
			    = new BasicStudyDescriptor(UIDUtils.createUID());
			hashtable.put(string_4_, basicstudydescriptor_5_);
			DREntity drentity = drnode.getLowerEntity();
			if (drentity != null) {
			    Iterator iterator = drentity.iterator("SERIES");
			    while (iterator.hasNext()) {
				DRNode drnode_6_ = (DRNode) iterator.next();
				DicomObject dicomobject
				    = drnode_6_.getDataset();
				String string_7_
				    = (String) dicomobject.get(426);
				DREntity drentity_8_
				    = drnode_6_.getLowerEntity();
				if (drentity_8_ != null) {
				    Iterator iterator_9_
					= drentity_8_.iterator();
				    while (iterator_9_.hasNext()) {
					DRNode drnode_10_
					    = (DRNode) iterator_9_.next();
					DicomObject dicomobject_11_
					    = drnode_10_.getDataset();
					DicomObject dicomobject_12_
					    = new DicomObject();
					dicomobject_12_.set(147, string_2_);
					dicomobject_12_.set(148, string);
					dicomobject_12_.set(425, string_4_);
					dicomobject_12_.set(427, string_3_);
					dicomobject_12_.set(426, string_7_);
					dicomobject_12_
					    .set(62, dicomobject_11_.get(52));
					dicomobject_12_
					    .set(63, dicomobject_11_.get(53));
					basicstudydescriptor_5_.addRetrImage
					    (dicomobject_12_, retrImageInfo);
				    }
				}
			    }
			}
		    }
		}
	    } finally {
		dirInfo.releaseReadLock();
	    }
	    return hashtable;
	}
    }
    
    public StudyContentNotificationSCU
	(Param param, StorageManagerFactory storagemanagerfactory,
	 AETable aetable) {
	aets = aetable;
	storageMgrFty = storagemanagerfactory;
	setParam(param);
    }
    
    public void setParam(Param param) {
	this.param = param;
    }
    
    public void add(VerboseAssociation verboseassociation,
		    DicomObject dicomobject) throws DicomException {
	AssocMapItem assocmapitem
	    = (AssocMapItem) assocMap.get(verboseassociation);
	if (assocmapitem == null) {
	    assocmapitem = new AssocMapItem(verboseassociation);
	    assocMap.put(verboseassociation, assocmapitem);
	}
	assocmapitem.addRetrImage(dicomobject);
    }
    
    public void execute(VerboseAssociation verboseassociation) {
	AssocMapItem assocmapitem
	    = (AssocMapItem) assocMap.remove(verboseassociation);
	if (assocmapitem != null && param.isSCN()) {
	    String string = param.getSCNAET();
	    AET aet = aets.lookup(string);
	    if (aet == null) {
		Debug.out.println("WARNING: SCN.SCP[" + string
				  + "] is not a specified AET.");
		Debug.out
		    .println("Cannot perform Study Content Notification.");
	    } else {
		try {
		    Hashtable hashtable = assocmapitem.getStudyDescriptors();
		    VerboseAssociation verboseassociation_13_
			= SimpleStorageSCU.openAssociation(aet.host, aet.port,
							   aet.title,
							   verboseassociation
							       .localAET(),
							   ONLY_SCN_ASID);
		    if (verboseassociation_13_ == null)
			Debug.out.println
			    ("Could not open Association for Study Content Notification to "
			     + aet.title);
		    else {
			try {
			    Enumeration enumeration = hashtable.elements();
			    while (enumeration.hasMoreElements())
				SimpleStorageSCU.sendDicomObject
				    (verboseassociation_13_,
				     (DicomObject) enumeration.nextElement());
			} finally {
			    SimpleStorageSCU
				.closeAssociation(verboseassociation_13_);
			}
		    }
		} catch (Exception exception) {
		    Debug.out.println(exception);
		}
	    }
	}
    }
}
