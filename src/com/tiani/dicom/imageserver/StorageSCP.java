/* StorageSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DefCStoreSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.iod.CompositeIOD;
import com.tiani.dicom.iod.IODError;
import com.tiani.dicom.iod.IODErrorSummary;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.service.IconFactory;
import com.tiani.dicom.util.LUT;
import com.tiani.dicom.util.LUTFactory;
import com.tiani.dicom.util.PixelMatrix;

class StorageSCP implements IAssociationListener
{
    private final StudyContentNotificationSCU scnSCU;
    private StorageManagerFactory storageMgrFty;
    private Param param;
    static final int ERR_RESSOURCE_ACCESS_FILESET = 42753;
    static final int ERR_MISSING_CLASS_UID = 43265;
    static final int ERR_MISSING_INST_UID = 43266;
    static final int ERR_MISSING_SERIES_UID = 43267;
    static final int ERR_MISSING_STUDY_UID = 43268;
    static final int ERR_UNMATCH_CLASS_UID = 43281;
    static final int ERR_UNMATCH_INST_UID = 43282;
    static final int ERR_UNMATCH_TYPE1_ATTR = 43283;
    static final int WARN_COERCION_OF_DATA = 45056;
    static final int WARN_UNMATCH_ATTR = 45063;
    private final IDimseRqListener imageSCP = new ImageStoreSCP();
    private final IDimseRqListener gspsSCP = new GspsStoreSCP();
    private final IDimseRqListener rtDoseSCP = new RtDoseStoreSCP();
    private final IDimseRqListener rtPlanSCP = new RtPlanStoreSCP();
    private final IDimseRqListener rtStructuredSetSCP
	= new RtStructuredSetStoreSCP();
    private final IDimseRqListener rtTreatRecordSCP
	= new RtTreatRecordStoreSCP();
    private final IDimseRqListener overlaySCP = new OverlayStoreSCP();
    private final IDimseRqListener curveSCP = new CurveStoreSCP();
    private final IDimseRqListener voiLutSCP = new VoiLutStoreSCP();
    private final IDimseRqListener modalityLutSCP = new ModalityLutStoreSCP();
    private final IDimseRqListener srSCP = new SrStoreSCP();
    private final IDimseRqListener koSCP = new KoStoreSCP();
    private final IDimseRqListener storedPrintSCP = new StoredPrintSCP();
    private final IDimseRqListener rawDataSCP = new RawDataSCP();
    
    private final class RawDataSCP extends MyCStoreSCP
    {
	private RawDataSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "RAW DATA";
	}
    }
    
    private final class StoredPrintSCP extends MyCStoreSCP
    {
	private StoredPrintSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "STORED PRINT";
	}
    }
    
    private final class KoStoreSCP extends MyCStoreSCP
    {
	private KoStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "KEY OBJECT DOC";
	}
    }
    
    private final class SrStoreSCP extends MyCStoreSCP
    {
	private SrStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "SR DOCUMENT";
	}
    }
    
    private final class ModalityLutStoreSCP extends MyCStoreSCP
    {
	private ModalityLutStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "MODALITY LUT";
	}
    }
    
    private final class VoiLutStoreSCP extends MyCStoreSCP
    {
	private VoiLutStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "VOI LUT";
	}
    }
    
    private final class CurveStoreSCP extends MyCStoreSCP
    {
	private CurveStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "CURVE";
	}
    }
    
    private final class OverlayStoreSCP extends MyCStoreSCP
    {
	private OverlayStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "OVERLAY";
	}
    }
    
    private final class RtTreatRecordStoreSCP extends MyCStoreSCP
    {
	private RtTreatRecordStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "RT TREAT RECORD";
	}
    }
    
    private final class RtStructuredSetStoreSCP extends MyCStoreSCP
    {
	private RtStructuredSetStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "RT STRUCTURE SET";
	}
    }
    
    private final class RtPlanStoreSCP extends MyCStoreSCP
    {
	private RtPlanStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "RT PLAN";
	}
    }
    
    private final class RtDoseStoreSCP extends MyCStoreSCP
    {
	private RtDoseStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "RT DOSE";
	}
    }
    
    private final class GspsStoreSCP extends MyCStoreSCP
    {
	private GspsStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "PRESENTATION";
	}
    }
    
    private final class ImageStoreSCP extends MyCStoreSCP
    {
	private ImageStoreSCP() {
	    super();
	}
	
	public String getDRType() {
	    return "IMAGE";
	}
	
	protected void adjustTS(StoreContext storecontext)
	    throws DicomException, UnknownUIDException, IOException {
	    UIDEntry uidentry = param.getMediaTS4Img();
	    if (uidentry != null) {
		DicomObject dicomobject
		    = storecontext.ds.getFileMetaInformation();
		String string = dicomobject.getS(31);
		if (!uidentry.getValue().equals(string)) {
		    int i = UID.getUIDEntry(string).getConstant();
		    if (i != 8195 && i != 8194 && i != 8193) {
			float f = (float) getPixelSize(storecontext.ds);
			Compression compression
			    = new Compression(storecontext.ds);
			compression.decompress();
			if (Debug.DEBUG > 1)
			    Debug.out.println
				("Finished Decompression 1 : "
				 + (float) getPixelSize(storecontext.ds) / f);
		    }
		    storecontext.pm = PixelMatrix.create(storecontext.ds);
		    if (param.isCompress()) {
			Compression compression
			    = new Compression(storecontext.ds);
			compression.compress(uidentry.getConstant());
			if (Debug.DEBUG > 1)
			    Debug.out.println
				("Finished Compression "
				 + ((float) storecontext.pm
						.getPixelData().length
				    / (float) getPixelSize(storecontext.ds))
				 + " : 1");
		    }
		    storecontext.ds.getFileMetaInformation()
			.set(31, param.getMediaTS4Img().getValue());
		}
	    }
	}
	
	protected void setPixelMatrixAndLUT(StoreContext storecontext)
	    throws DicomException, UnknownUIDException, IOException {
	    if (storecontext.pm == null) {
		DicomObject dicomobject
		    = storecontext.ds.getFileMetaInformation();
		String string = dicomobject.getS(31);
		int i = UID.getUIDEntry(string).getConstant();
		if (i != 8195 && i != 8194 && i != 8193) {
		    byte[][] is = getPixelData(storecontext.ds);
		    float f = (float) getPixelSize(storecontext.ds);
		    Compression compression = new Compression(storecontext.ds);
		    compression.decompress();
		    if (Debug.DEBUG > 1)
			Debug.out.println("Finished Decompression 1 : "
					  + (float) getPixelSize(storecontext
								 .ds) / f);
		    storecontext.pm = PixelMatrix.create(storecontext.ds);
		    setPixelData(storecontext.ds, is);
		    dicomobject.set(31, string);
		} else
		    storecontext.pm = PixelMatrix.create(storecontext.ds);
	    }
	    if (storecontext.lut == null && storecontext.pm.isMonochrome())
		storecontext.lut = LUTFactory.createByteLUT(storecontext.pm,
							    storecontext.ds);
	}
	
	protected void createIcon(DicomObject dicomobject,
				  StoreContext storecontext) {
	    IconFactory iconfactory = param.getIconFactory();
	    if (iconfactory != null) {
		try {
		    setPixelMatrixAndLUT(storecontext);
		    dicomobject.set
			(699,
			 iconfactory.createIconDicomObject(storecontext.pm,
							   storecontext.lut));
		    if (Debug.DEBUG > 1)
			Debug.out.println("Created icon for received image");
		} catch (Exception exception) {
		    exception.printStackTrace(Debug.out);
		    Debug.out.println
			("Failed to create icon for received image: "
			 + exception);
		}
	    }
	}
    }
    
    private abstract class MyCStoreSCP extends DefCStoreSCP
    {
	private MyCStoreSCP() {
	    /* empty */
	}
	
	public abstract String getDRType();
	
	protected void adjustTS(StoreContext storecontext)
	    throws DicomException, UnknownUIDException, IOException {
	    UIDEntry uidentry = param.getMediaTS4NonImg();
	    if (uidentry != null)
		storecontext.ds.getFileMetaInformation()
		    .set(31, uidentry.getValue());
	}
	
	protected void setPixelMatrixAndLUT(StoreContext storecontext)
	    throws DicomException, UnknownUIDException, IOException {
	    /* empty */
	}
	
	private boolean storeToFileset
	    (String string, StoreContext storecontext)
	    throws StorageException {
	    try {
		String string_0_ = (String) storecontext.ds.get(425);
		StorageManager storagemanager
		    = storageMgrFty.getStorageManagerForAET(string, true);
		DicomDir2 dicomdir2 = storagemanager.getDirInfo();
		DRNode[] drnodes
		    = (param.getDRFactory().createInstanceDRNodePath
		       (getDRType(), storecontext.ds,
			(StorageSCP.this.createFileSetID
			 (storagemanager.getRootDir(), storecontext.ds,
			  param.getFilenameExt()))));
		dicomdir2.addWriteLock();
		try {
		    int i = dicomdir2.addNodePath(drnodes);
		    if (i == 0) {
			if (Debug.DEBUG > 0)
			    Debug.out.println
				("Received data object already in fileset. - Rejected");
			return false;
		    }
		    createIcon(drnodes[3].getDataset(), storecontext);
		    storagemanager.setDirty(true);
		} finally {
		    dicomdir2.releaseWriteLock();
		}
		storagemanager.write(storecontext.ds, drnodes[3].getDataset(),
				     string_0_);
	    } catch (Exception exception) {
		Debug.out.println(exception);
		throw new StorageException(42753, null);
	    }
	    return true;
	}
	
	protected void createIcon(DicomObject dicomobject,
				  StoreContext storecontext) {
	    /* empty */
	}
	
	protected int store
	    (DimseExchange dimseexchange, String string, String string_1_,
	     DicomMessage dicommessage, DicomMessage dicommessage_2_)
	    throws IOException, DicomException, IllegalValueException,
		   UnknownUIDException {
	    dicommessage_2_.set(9, new Integer(0));
	    DicomObject dicomobject = dicommessage.getDataset();
	    try {
		StorageSCP.this.checkUIDs(string, string_1_, dicomobject);
		if (param.isStorageValidate())
		    StorageSCP.this.validate(string, dicomobject,
					     dicommessage_2_);
		dicomobject.setFileMetaInformation
		    (new FileMetaInformation
		     (dicomobject,
		      dimseexchange.getAssociation().getTransferSyntax
			  (dicommessage.getPresentationContext()).getValue()));
		String string_3_ = dimseexchange.localAET();
		WebsiteBuilder websitebuilder = param.getWebsiteBuilder();
		StoreContext storecontext = new StoreContext(dicomobject);
		if (param.isFileset()) {
		    adjustTS(storecontext);
		    StorageSCP.this.adjustDRKeyAttribs(dicomobject,
						       dicommessage_2_);
		    if (!storeToFileset(string_3_, storecontext))
			websitebuilder = null;
		}
		if (websitebuilder != null) {
		    try {
			setPixelMatrixAndLUT(storecontext);
			websitebuilder.store(string_3_, dicomobject,
					     storecontext.pm, storecontext.lut,
					     getDRType());
		    } catch (Exception exception) {
			Debug.out.println
			    ("Failed to create icon for received image: ");
			exception.printStackTrace(Debug.out);
		    }
		}
		scnSCU.add(dimseexchange.getAssociation(), dicomobject);
	    } catch (StorageException storageexception) {
		storageexception.copyTo(dicommessage_2_);
	    }
	    return dicommessage_2_.getI(9);
	}
    }
    
    private static final class StoreContext
    {
	DicomObject ds;
	PixelMatrix pm;
	LUT.Byte1 lut;
	
	StoreContext(DicomObject dicomobject) {
	    ds = dicomobject;
	}
    }
    
    private static class StorageException extends Exception
    {
	final int status;
	
	StorageException(int i, String string) {
	    super(string);
	    status = i;
	}
	
	void copyTo(DicomMessage dicommessage) {
	    try {
		dicommessage.set(9, new Integer(status));
		if (this.getMessage() != null)
		    dicommessage.set(11, this.getMessage());
	    } catch (DicomException dicomexception) {
		throw new RuntimeException(dicomexception.toString());
	    }
	}
    }
    
    public StorageSCP
	(Param param, StorageManagerFactory storagemanagerfactory,
	 StudyContentNotificationSCU studycontentnotificationscu) {
	storageMgrFty = storagemanagerfactory;
	scnSCU = studycontentnotificationscu;
	setParam(param);
    }
    
    public void setParam(Param param) {
	this.param = param;
    }
    
    public IDimseRqListener getImageSCP() {
	return imageSCP;
    }
    
    public IDimseRqListener getGspsSCP() {
	return gspsSCP;
    }
    
    public IDimseRqListener getRtDoseSCP() {
	return rtDoseSCP;
    }
    
    public IDimseRqListener getRtPlanSCP() {
	return rtPlanSCP;
    }
    
    public IDimseRqListener getRtStructuredSetSCP() {
	return rtStructuredSetSCP;
    }
    
    public IDimseRqListener getRtTreatRecordSCP() {
	return rtTreatRecordSCP;
    }
    
    public IDimseRqListener getOverlaySCP() {
	return overlaySCP;
    }
    
    public IDimseRqListener getCurveSCP() {
	return curveSCP;
    }
    
    public IDimseRqListener getVoiLutSCP() {
	return voiLutSCP;
    }
    
    public IDimseRqListener getModalityLutSCP() {
	return modalityLutSCP;
    }
    
    public IDimseRqListener getSrSCP() {
	return srSCP;
    }
    
    public IDimseRqListener getKoSCP() {
	return koSCP;
    }
    
    public IDimseRqListener getStoredPrintSCP() {
	return storedPrintSCP;
    }
    
    public IDimseRqListener getRawDataSCP() {
	return rawDataSCP;
    }
    
    private void checkExist(int i, DicomObject dicomobject, int i_4_)
	throws StorageException, DicomException {
	if (dicomobject.getSize(i) <= 0)
	    throw new StorageException(i_4_,
				       ("Missing " + DDict.getDescription(i)
					+ " in data set"));
    }
    
    private void checkUIDs
	(String string, String string_5_, DicomObject dicomobject)
	throws StorageException, DicomException {
	checkExist(62, dicomobject, 43265);
	checkExist(63, dicomobject, 43266);
	checkExist(425, dicomobject, 43268);
	checkExist(426, dicomobject, 43267);
	if (!string.equals((String) dicomobject.get(62)))
	    throw new StorageException
		      (43281,
		       "SOP Class UID in data set differs from value in command");
	if (!string_5_.equals((String) dicomobject.get(63)))
	    throw new StorageException
		      (43282,
		       "SOP Instance UID in data set differs from value in command");
    }
    
    private void validate
	(String string, DicomObject dicomobject, DicomMessage dicommessage)
	throws StorageException, DicomException {
	CompositeIOD compositeiod = CompositeIOD.getIOD(string, dicomobject);
	if (compositeiod == null) {
	    if (Debug.DEBUG > 0)
		Debug.out.println
		    ("Validation of SOPclass " + string
		     + " not supported by this version of ImageServer");
	} else {
	    Vector vector = new Vector();
	    if (!compositeiod.validate(dicomobject, vector, null)) {
		if (Debug.DEBUG > 0) {
		    Debug.out.println("ERROR detected for SOPclass " + string
				      + " :");
		    Enumeration enumeration = vector.elements();
		    while (enumeration.hasMoreElements())
			Debug.out.println(enumeration.nextElement());
		}
		IODErrorSummary ioderrorsummary = new IODErrorSummary(vector);
		Enumeration enumeration = ioderrorsummary.offendingElements();
		while (enumeration.hasMoreElements())
		    dicommessage.append(10, enumeration.nextElement());
		boolean bool = ioderrorsummary.type(1) > 0;
		int i = vector.size();
		StorageException storageexception
		    = (new StorageException
		       (bool ? 43283 : 45063,
			(i == 1 ? ((IODError) vector.elementAt(0)).toString()
			 : "" + i + "elements does not match SOP class")));
		if (bool)
		    throw storageexception;
		storageexception.copyTo(dicommessage);
	    }
	}
    }
    
    private static int getPixelSize(DicomObject dicomobject) {
	int i = 0;
	int i_6_ = dicomobject.getSize(1184);
	while (i_6_-- > 0)
	    i += ((byte[]) dicomobject.get(1184, i_6_)).length;
	return i > 0 ? i : 1;
    }
    
    private static byte[][] getPixelData(DicomObject dicomobject) {
	byte[][] is = new byte[dicomobject.getSize(1184)][];
	for (int i = 0; i < is.length; i++)
	    is[i] = (byte[]) dicomobject.get(1184, i);
	return is;
    }
    
    private static void setPixelData(DicomObject dicomobject, byte[][] is)
	throws DicomException {
	dicomobject.deleteItem(1184);
	for (int i = 0; i < is.length; i++)
	    dicomobject.append(1184, is[i]);
    }
    
    private void adjustDRKeyAttribs(DicomObject dicomobject,
				    DicomMessage dicommessage) {
	assureDRKeyAttrib(148, dicomobject, dicommessage, "<null>");
	assureDRKeyAttrib(427, dicomobject, dicommessage, "<null>");
	assureDRKeyAttrib(428, dicomobject, dicommessage, new Integer(0));
	assureDRKeyAttrib(430, dicomobject, dicommessage, new Integer(0));
    }
    
    private void assureDRKeyAttrib(int i, DicomObject dicomobject,
				   DicomMessage dicommessage, Object object) {
	if (dicomobject.getSize(i) <= 0) {
	    String string = "set " + DDict.getDescription(i) + " to " + object;
	    try {
		dicomobject.set(i, object);
		if (!param.isWarningAsSuccess()) {
		    dicommessage.set(9, new Integer(45056));
		    dicommessage.set(11, string);
		}
	    } catch (DicomException dicomexception) {
		Debug.out.println(string + " throws " + dicomexception);
	    }
	}
    }
    
    private String[] createFileSetID(File file, DicomObject dicomobject,
				     String string) throws DicomException {
	String[] strings = new String[param.isFilesetSplitSeries() ? 4 : 3];
	int i = 0;
	File file_7_
	    = new File(file, strings[i++] = toFileID(dicomobject.getS(147)));
	file_7_
	    = new File(file_7_, strings[i++] = toFileID(dicomobject.getS(64)));
	if (param.isFilesetSplitSeries())
	    file_7_
		= new File(file_7_,
			   strings[i++] = toFileID(dicomobject.getS(81)
						   + dicomobject.getS(428)));
	File file_8_
	    = new File(file_7_,
		       (strings[i]
			= (leadingZeros(toFileID(dicomobject.getS(430)))
			   + string)));
	int i_9_ = 1;
	while (file_8_.exists()) {
	    file_8_ = new File(file_7_,
			       strings[i] = (leadingZeros(String.valueOf(i_9_))
					     + string));
	    i_9_++;
	}
	return strings;
    }
    
    private String leadingZeros(String string) {
	return "00000000".substring(string.length()) + string;
    }
    
    private String toFileID(String string) {
	StringBuffer stringbuffer = new StringBuffer();
	if (string != null) {
	    for (int i = 0; i < string.length() && stringbuffer.length() < 8;
		 i++) {
		char c;
		if (Character.isLetterOrDigit(c = string.charAt(i)))
		    stringbuffer.append(Character.toUpperCase(c));
	    }
	}
	return stringbuffer.length() > 0 ? stringbuffer.toString() : "NULL";
    }
    
    public void associateRequestReceived(VerboseAssociation verboseassociation,
					 Request request) {
	/* empty */
    }
    
    public void associateResponseReceived
	(VerboseAssociation verboseassociation, Response response) {
	/* empty */
    }
    
    public void associateRequestSent(VerboseAssociation verboseassociation,
				     Request request) {
	/* empty */
    }
    
    public void associateResponseSent(VerboseAssociation verboseassociation,
				      Response response) {
	/* empty */
    }
    
    public void releaseRequestReceived(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseReceived
	(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseRequestSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void abortReceived(VerboseAssociation verboseassociation,
			      Abort abort) {
	/* empty */
    }
    
    public void abortSent(VerboseAssociation verboseassociation, int i,
			  int i_10_) {
	/* empty */
    }
    
    public void socketClosed(VerboseAssociation verboseassociation) {
	try {
	    StorageManager storagemanager
		= storageMgrFty.getStorageManagerForAET(verboseassociation
							    .localAET(),
							false);
	    if (storagemanager != null) {
		storagemanager.compact();
		storagemanager.writeDicomDir();
	    }
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
	scnSCU.execute(verboseassociation);
    }
}
