/* ImageServerApplet - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;

import com.archimed.dicom.Debug;
import com.tiani.dicom.ui.AETableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import com.tiani.dicom.util.CheckParam;

public class ImageServerApplet extends JApplet
{
    private final ActionListener onAddAET;
    private final ActionListener onDelAET;
    private final ChangeListener onTabbedPane;
    private Properties _params;
    private AETable _aets;
    private AETableModel _aetTableModel;
    private JTable _aetTable;
    private String _aetSrcPath;
    private ImageServer _server;
    private JButton _applyButton;
    private JButton _startButton;
    private JButton _stopButton;
    private JButton _addAETButton;
    private JButton _delAETButton;
    private JTabbedPane _tabbedPane;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;
    
    public ImageServerApplet() {
	onAddAET = new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		_aetTableModel.addAET(new AET("AETitle", "localhost", 104));
	    }
	};
	onDelAET = new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		int i = _aetTable.getSelectedRow();
		if (i != -1)
		    _aetTableModel.deleteAET(i);
	    }
	};
	onTabbedPane = new ChangeListener() {
	    public void stateChanged(ChangeEvent changeevent) {
		if (_tabbedPane.getSelectedIndex() == 1) {
		    String string = _params.getProperty("AET.Properties");
		    if (string != _aetSrcPath) {
			ImageServer.updateAETs(_aets, _aetSrcPath = string);
			_aetTableModel.update();
		    }
		}
		ImageServerApplet.this.enableButtons();
	    }
	};
	_params = null;
	_aets = new AETable();
	_aetTableModel = new AETableModel(_aets);
	_aetTable = new JTable(_aetTableModel);
	_aetSrcPath = null;
	_server = null;
	_applyButton = new JButton("Apply");
	_startButton = new JButton("Start");
	_stopButton = new JButton("Stop");
	_addAETButton = new JButton("+AET");
	_delAETButton = new JButton("-AET");
	_tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 30000), true);
	_params = null;
    }
    
    ImageServerApplet(Properties properties) {
	onAddAET = new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		_aetTableModel.addAET(new AET("AETitle", "localhost", 104));
	    }
	};
	onDelAET = new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		int i = _aetTable.getSelectedRow();
		if (i != -1)
		    _aetTableModel.deleteAET(i);
	    }
	};
	onTabbedPane = new ChangeListener() {
	    public void stateChanged(ChangeEvent changeevent) {
		if (_tabbedPane.getSelectedIndex() == 1) {
		    String string = _params.getProperty("AET.Properties");
		    if (string != _aetSrcPath) {
			ImageServer.updateAETs(_aets, _aetSrcPath = string);
			_aetTableModel.update();
		    }
		}
		ImageServerApplet.this.enableButtons();
	    }
	};
	_params = null;
	_aets = new AETable();
	_aetTableModel = new AETableModel(_aets);
	_aetTable = new JTable(_aetTableModel);
	_aetSrcPath = null;
	_server = null;
	_applyButton = new JButton("Apply");
	_startButton = new JButton("Start");
	_stopButton = new JButton("Stop");
	_addAETButton = new JButton("+AET");
	_delAETButton = new JButton("-AET");
	_tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 30000), true);
	_params = properties;
    }
    
    public void init() {
	ImageServer.setLog(_log);
	if (isApplet())
	    _params = getAppletParam();
	if (_params != null) {
	    try {
		CheckParam.verify(_params, Param.CHECKS);
	    } catch (Exception exception) {
		_log.println(exception);
		_params = null;
	    }
	}
	_server = new ImageServer(_params);
	try {
	    _params = _server.getProperties();
	} catch (Exception exception) {
	    _log.println(exception);
	}
	_applyButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    ImageServerApplet.this.saveAETs();
		    _server.setProperties(_params);
		} catch (Exception exception) {
		    _tabbedPane.setSelectedIndex(2);
		    _log.println(exception);
		}
	    }
	});
	_startButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    ImageServerApplet.this.saveAETs();
		    _tabbedPane.setSelectedIndex(2);
		    _server.start(_params);
		} catch (Exception exception) {
		    _log.println(exception);
		}
		ImageServerApplet.this.enableButtons();
	    }
	});
	_stopButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    _server.stop(true, false);
		} catch (Exception exception) {
		    _log.println(exception);
		}
		ImageServerApplet.this.enableButtons();
	    }
	});
	_addAETButton.addActionListener(onAddAET);
	_delAETButton.addActionListener(onDelAET);
	_tabbedPane.addChangeListener(onTabbedPane);
	Box box = Box.createHorizontalBox();
	box.add(_applyButton);
	box.add(_startButton);
	box.add(_stopButton);
	box.add(_addAETButton);
	box.add(_delAETButton);
	box.add(Box.createGlue());
	box.add(new JTianiButton(isApplet() ? this.getAppletContext() : null));
	JPropertiesTable jpropertiestable
	    = new JPropertiesTable(Param.KEYS, _params, Param.CHECKS);
	_tabbedPane.add("Props", new JScrollPane(jpropertiestable));
	_tabbedPane.add("AETs", new JScrollPane(_aetTable));
	_tabbedPane.add("Log", new JAutoScrollPane(_logTextArea));
	Container container = this.getContentPane();
	container.add(box, "North");
	container.add(_tabbedPane, "Center");
	enableButtons();
    }
    
    private void saveAETs() throws IOException {
	String string = _params.getProperty("AET.Properties");
	if (string.indexOf(':') > 1) {
	    URL url = new URL(string);
	    if (!url.getProtocol().equals("file")) {
		_log.println("WARNING: Cannot update AETs at " + url);
		return;
	    }
	    string = url.getFile();
	}
	FileOutputStream fileoutputstream = new FileOutputStream(string);
	try {
	    _aets.store(fileoutputstream, "AETs created by ImageServer");
	} finally {
	    fileoutputstream.close();
	}
	if (Debug.DEBUG > 0)
	    Debug.out.println("Stored AETs to " + string);
    }
    
    private void enableButtons() {
	_addAETButton.setEnabled(_tabbedPane.getSelectedIndex() == 1);
	_delAETButton.setEnabled(_tabbedPane.getSelectedIndex() == 1);
	try {
	    _startButton.setEnabled(!_server.isRunning());
	    _stopButton.setEnabled(_server.isRunning());
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private boolean isApplet() {
	return this.getDocumentBase() != null;
    }
    
    Properties getParams() {
	return _params;
    }
    
    private Properties getAppletParam() {
	Properties properties = new Properties();
	for (int i = 0; i < Param.KEYS.length; i++)
	    properties.put(Param.KEYS[i], this.getParameter(Param.KEYS[i]));
	return properties;
    }
}
