/* Param - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.service.IconFactory;
import com.tiani.dicom.util.CheckParam;

final class Param
{
    public static final String AET_PROPERTIES_KEY = "AET.Properties";
    public static final String[] KEYS
	= { "Port", "CalledTitles", "CallingTitles", "AET.Properties",
	    "MultiThreadTCP", "MultiThreadAssoc", "QueueRQ", "MaxOpInvoked",
	    "MaxPduSize", "AssocTimeout[ms]", "ReleaseTimeout[ms]",
	    "Verification", "Verification.TransferSyntax", "Storage",
	    "Storage.WarningAsSuccess", "Storage.Validate",
	    "Storage.TransferSyntax", "StorageCmt",
	    "StorageCmt.TransferSyntax", "StorageCmt.Forward",
	    "StorageCmt.BeforeRSP", "StorageCmt.OpenAssoc", "QR.PatRoot",
	    "QR.StudyRoot", "QR.PatStudyOnly", "QR.TransferSyntax",
	    "QR.Forward.Query", "QR.Forward.Retrieve", "QR.Priority",
	    "Forward.AET", "Forward.KeepOpen", "Forward.CallingTitle", "SCN",
	    "SCN.AET", "SCN.Total", "Fileset", "Fileset.Path",
	    "Fileset.Separate", "Fileset.SplitSeries", "Fileset.ID",
	    "Fileset.Scheme", "Fileset.TransferSyntax", "Fileset.FilenameExt",
	    "Fileset.Icon", "Fileset.Icon.MaxRows", "Fileset.Icon.MaxColumns",
	    "Fileset.Icon.Jpeg", "Fileset.Icon.Jpeg.Quality",
	    "Fileset.Icon.Jpeg.Multiframe", "Fileset.Icon.Color",
	    "Fileset.MaxSize[MB]", "Website", "Website.Path",
	    "Website.Separate", "Website.Scheme", "Website.Jpeg.MaxRows",
	    "Website.Jpeg.MaxColumns", "Website.Jpeg.Multiframe",
	    "Website.Jpeg.Quality", "Website.Jpeg.BurnInOverlays", "Verbose",
	    "DumpCmdsetIntoDir", "DumpDatasetIntoDir" };
    public static final Hashtable CHECKS = new Hashtable();
    private static final String[] PRIORITIES = { "MEDIUM", "HIGH", "LOW" };
    private static final String[] NATIVE_TRANSFER_SYNTAX_NAMES
	= { "ImplicitVRLittleEndian", "ExplicitVRLittleEndian",
	    "ExplicitVRBigEndian" };
    private static final String[] ACCEPT_TRANSFER_SYNTAX_NAMES
	= { "ImplicitVRLittleEndian", "ExplicitVRLittleEndian",
	    "ExplicitVRBigEndian", "RLELossless", "JPEGLossless",
	    "JPEG_Baseline", "AllSupported" };
    private static final int[] IVR_LE_TS = { 8193 };
    private static final int[] EVR_LE_TS = { 8194, 8193 };
    private static final int[] EVR_BE_TS = { 8195, 8194, 8193 };
    private static final int[] RLE_LL_TS = { 8198, 8194, 8193 };
    private static final int[] JPEG_LL_TS = { 8197, 8194, 8193 };
    private static final int[] JPEG_BASELINE_TS = { 8196, 8194, 8193 };
    private static final int[] ALL_TS = { 8197, 8198, 8194, 8195, 8193, 8196 };
    private static final int[] NATIVE_TS = { 8194, 8195, 8193 };
    private static final int[][] IMG_TS
	= { IVR_LE_TS, EVR_LE_TS, EVR_BE_TS, RLE_LL_TS, JPEG_LL_TS,
	    JPEG_BASELINE_TS, ALL_TS };
    private static final int[][] NON_IMG_TS
	= { IVR_LE_TS, EVR_LE_TS, EVR_BE_TS, EVR_LE_TS, EVR_LE_TS, EVR_LE_TS,
	    NATIVE_TS };
    private static final String[] MEDIA_TRANSFER_SYNTAX_NAMES
	= { "ImplicitVRLittleEndian", "ExplicitVRLittleEndian",
	    "ExplicitVRBigEndian", "JPEGLossless", "SameAsReceived",
	    "AcrNema" };
    private static final String[] ICON_COLOR
	= { "ColorToGray", "ColorAsOriginal" };
    private static final String[] COMMIT_ASSOC_MODE_NAMES
	= { "Reuse", "TryReuse", "OpenNew" };
    private static final int[] COMMIT_ASSOC_MODES = { 21, 22, 20 };
    private static final String[] VERBOSE = { "0", "1", "2", "3", "4", "5" };
    private final Properties prop;
    private final int port;
    private final String[] calledTitles;
    private final String[] callingTitles;
    private final String aetProperties;
    private final boolean multiThreadTCP;
    private final boolean multiThreadAssoc;
    private final boolean queueRQ;
    private final int maxOpInvoked;
    private final int maxPduSize;
    private final int assocTimeout;
    private final int releaseTimeout;
    private final boolean verification;
    private final int verificationTS;
    private final boolean storage;
    private final int storageTS;
    private final boolean warningAsSuccess;
    private final boolean storageValidate;
    private final boolean storageCmt;
    private final int storageCmtTS;
    private final int storageCmtTiming;
    private final int storageCmtAssocMode;
    private final boolean qrPatRoot;
    private final boolean qrStudyRoot;
    private final boolean qrPatStudyOnly;
    private final int qrTS;
    private final int qrPriority;
    private final boolean fwdStorageCmt;
    private final boolean fwdQuery;
    private final boolean fwdRetrieve;
    private final boolean fwdKeepOpen;
    private final boolean fwdCallingTitle;
    private final boolean scn;
    private final boolean scnTotal;
    private final boolean fileSet;
    private final boolean fileSetSeparate;
    private final boolean fileSetSplitSeries;
    private final IconFactory iconFactory;
    private final WebsiteBuilder websiteBuilder;
    private final boolean filesetIconJpeg;
    private final UIDEntry mediaTS4Img;
    private final UIDEntry mediaTS4NonImg;
    private final boolean compress;
    private final boolean fmi;
    private final long fileSetMaxSize;
    private final String filenameExt;
    private final DRFactory drFactory;
    private final int verbose;
    
    public Param(Properties properties) throws IllegalArgumentException {
	prop = (Properties) properties.clone();
	CheckParam.verify(properties, CHECKS);
	port = Integer.parseInt(prop.getProperty("Port"));
	String string = prop.getProperty("CalledTitles");
	calledTitles = (string != null && string.length() > 0
			? tokenize(string, "\\/,") : null);
	string = prop.getProperty("CallingTitles");
	callingTitles = (string != null && string.length() > 0
			 ? tokenize(string, "\\/,") : null);
	string = prop.getProperty("AET.Properties");
	aetProperties = string != null && string.length() > 0 ? string : null;
	string = prop.getProperty("DumpCmdsetIntoDir");
	Debug.dumpCmdsetIntoDir
	    = string != null && string.length() > 0 ? string : null;
	string = prop.getProperty("DumpDatasetIntoDir");
	Debug.dumpDatasetIntoDir
	    = string != null && string.length() > 0 ? string : null;
	multiThreadTCP = parseBoolean(prop.getProperty("MultiThreadTCP"));
	multiThreadAssoc = parseBoolean(prop.getProperty("MultiThreadAssoc"));
	queueRQ = parseBoolean(prop.getProperty("QueueRQ"));
	maxOpInvoked
	    = queueRQ ? Integer.parseInt(prop.getProperty("MaxOpInvoked")) : 1;
	maxPduSize = Integer.parseInt(prop.getProperty("MaxPduSize"));
	assocTimeout = Integer.parseInt(prop.getProperty("AssocTimeout[ms]"));
	releaseTimeout
	    = Integer.parseInt(prop.getProperty("ReleaseTimeout[ms]"));
	verification = parseBoolean(prop.getProperty("Verification"));
	verificationTS
	    = indexOfIn(prop.getProperty("Verification.TransferSyntax"),
			NATIVE_TRANSFER_SYNTAX_NAMES);
	storage = parseBoolean(prop.getProperty("Storage"));
	storageTS = indexOfIn(prop.getProperty("Storage.TransferSyntax"),
			      ACCEPT_TRANSFER_SYNTAX_NAMES);
	warningAsSuccess
	    = parseBoolean(prop.getProperty("Storage.WarningAsSuccess"));
	storageValidate = parseBoolean(prop.getProperty("Storage.Validate"));
	storageCmt = parseBoolean(prop.getProperty("StorageCmt"));
	storageCmtTS = indexOfIn(prop.getProperty("StorageCmt.TransferSyntax"),
				 NATIVE_TRANSFER_SYNTAX_NAMES);
	storageCmtTiming
	    = parseBoolean(prop.getProperty("StorageCmt.BeforeRSP")) ? 10 : 11;
	storageCmtAssocMode
	    = (COMMIT_ASSOC_MODES
	       [indexOfIn(prop.getProperty("StorageCmt.OpenAssoc"),
			  COMMIT_ASSOC_MODE_NAMES)]);
	verbose = Integer.parseInt(prop.getProperty("Verbose"));
	qrPatRoot = parseBoolean(prop.getProperty("QR.PatRoot"));
	qrStudyRoot = parseBoolean(prop.getProperty("QR.StudyRoot"));
	qrPatStudyOnly = parseBoolean(prop.getProperty("QR.PatStudyOnly"));
	qrTS = indexOfIn(prop.getProperty("QR.TransferSyntax"),
			 NATIVE_TRANSFER_SYNTAX_NAMES);
	qrPriority = indexOfIn(prop.getProperty("QR.Priority"), PRIORITIES);
	fwdStorageCmt = parseBoolean(prop.getProperty("StorageCmt.Forward"));
	fwdQuery = parseBoolean(prop.getProperty("QR.Forward.Query"));
	fwdRetrieve = parseBoolean(prop.getProperty("QR.Forward.Retrieve"));
	fwdKeepOpen = parseBoolean(prop.getProperty("Forward.KeepOpen"));
	fwdCallingTitle
	    = parseBoolean(prop.getProperty("Forward.CallingTitle"));
	scn = parseBoolean(prop.getProperty("SCN"));
	scnTotal = parseBoolean(prop.getProperty("SCN.Total"));
	fileSet = parseBoolean(prop.getProperty("Fileset"));
	fileSetSeparate = parseBoolean(prop.getProperty("Fileset.Separate"));
	fileSetSplitSeries
	    = parseBoolean(prop.getProperty("Fileset.SplitSeries"));
	try {
	    switch (indexOfIn(prop.getProperty("Fileset.TransferSyntax"),
			      MEDIA_TRANSFER_SYNTAX_NAMES)) {
	    case 0:
		mediaTS4Img = UID.getUIDEntry(8193);
		mediaTS4NonImg = UID.getUIDEntry(8193);
		compress = false;
		fmi = true;
		break;
	    case 1:
		mediaTS4Img = UID.getUIDEntry(8194);
		mediaTS4NonImg = UID.getUIDEntry(8194);
		compress = false;
		fmi = true;
		break;
	    case 2:
		mediaTS4Img = UID.getUIDEntry(8195);
		mediaTS4NonImg = UID.getUIDEntry(8195);
		compress = false;
		fmi = true;
		break;
	    case 3:
		mediaTS4Img = UID.getUIDEntry(8197);
		mediaTS4NonImg = UID.getUIDEntry(8194);
		compress = true;
		fmi = true;
		break;
	    case 4:
		mediaTS4Img = null;
		mediaTS4NonImg = null;
		compress = false;
		fmi = true;
		break;
	    case 5:
		mediaTS4Img = UID.getUIDEntry(8193);
		mediaTS4NonImg = UID.getUIDEntry(8193);
		compress = false;
		fmi = false;
		break;
	    default:
		throw new RuntimeException
			  (prop.getProperty("Fileset.TransferSyntax"));
	    }
	} catch (IllegalValueException illegalvalueexception) {
	    throw new RuntimeException("" + illegalvalueexception);
	}
	iconFactory
	    = (parseBoolean(prop.getProperty("Fileset.Icon"))
	       ? (new IconFactory
		  (Integer.parseInt(prop.getProperty("Fileset.Icon.MaxRows")),
		   Integer
		       .parseInt(prop.getProperty("Fileset.Icon.MaxColumns"))))
	       : null);
	filesetIconJpeg = parseBoolean(prop.getProperty("Fileset.Icon.Jpeg"));
	if (iconFactory != null) {
	    iconFactory.setColorConvert
		(indexOfIn(prop.getProperty("Fileset.Icon.Color"),
			   ICON_COLOR));
	    iconFactory.setJpeg(filesetIconJpeg);
	    iconFactory.setJpegQuality
		((float) (Integer.parseInt
			  (prop.getProperty("Fileset.Icon.Jpeg.Quality")))
		 / 100.0F);
	    iconFactory.setJpegMultiframe
		(parseBoolean
		 (prop.getProperty("Fileset.Icon.Jpeg.Multiframe")));
	}
	fileSetMaxSize
	    = ((long) Integer.parseInt(prop.getProperty("Fileset.MaxSize[MB]"))
	       * 1048576L);
	string = prop.getProperty("Fileset.FilenameExt").trim();
	filenameExt = (string.length() == 0 || string.startsWith(".") ? string
		       : "." + string);
	drFactory = createDRFactory();
	websiteBuilder
	    = (parseBoolean(prop.getProperty("Website"))
	       ? (new WebsiteBuilder
		  (prop.getProperty("Website.Path"),
		   parseBoolean(prop.getProperty("Website.Separate")),
		   Integer.parseInt(prop.getProperty("Website.Jpeg.MaxRows")),
		   Integer
		       .parseInt(prop.getProperty("Website.Jpeg.MaxColumns")),
		   parseBoolean(prop.getProperty("Website.Jpeg.Multiframe")),
		   (float) Integer.parseInt(prop.getProperty
					    ("Website.Jpeg.Quality")) / 100.0F,
		   (parseBoolean
		    (prop.getProperty("Website.Jpeg.BurnInOverlays"))),
		   loadWebsiteScheme(prop.getProperty("Website.Scheme"))))
	       : null);
    }
    
    private Properties loadWebsiteScheme(String string) {
	if (string != null && string.length() > 0) {
	    try {
		Properties properties = new Properties();
		InputStream inputstream
		    = (string.indexOf(':') > 2
		       ? (InputStream) new URL(string).openStream()
		       : new FileInputStream(string));
		try {
		    properties.load(inputstream);
		} finally {
		    inputstream.close();
		}
		if (Debug.DEBUG > 1)
		    Debug.out
			.println("Loaded scheme for website from " + string);
		return properties;
	    } catch (Exception exception) {
		Debug.out.println("WARNING: Loading scheme for website from "
				  + string + " throws" + exception);
		Debug.out.println("-> Use default scheme");
	    }
	}
	return null;
    }
    
    private static int indexOfIn(String string, String[] strings) {
	int i = strings.length;
	while (--i >= 0 && !strings[i].equals(string)) {
	    /* empty */
	}
	return i;
    }
    
    private DRFactory createDRFactory() {
	String string = getFilesetScheme();
	if (string != null && string.length() > 0) {
	    try {
		Properties properties = new Properties();
		InputStream inputstream
		    = (string.indexOf(':') > 2
		       ? (InputStream) new URL(string).openStream()
		       : new FileInputStream(string));
		try {
		    properties.load(inputstream);
		} finally {
		    inputstream.close();
		}
		if (Debug.DEBUG > 1)
		    Debug.out.println
			("Loaded scheme for directory records from " + string);
		return new DRFactory(properties);
	    } catch (Exception exception) {
		Debug.out.println
		    ("WARNING: Loading scheme for directory records from "
		     + string + " throws" + exception);
		Debug.out.println("-> Use default scheme");
	    }
	}
	return new DRFactory();
    }
    
    public DRFactory getDRFactory() {
	return drFactory;
    }
    
    public String toString() {
	StringBuffer stringbuffer = new StringBuffer();
	stringbuffer.append("Param:\n");
	for (int i = 0; i < KEYS.length; i++)
	    stringbuffer.append(KEYS[i]).append('=').append
		(prop.getProperty(KEYS[i])).append('\n');
	return stringbuffer.toString();
    }
    
    public int getPort() {
	return port;
    }
    
    public String[] getCalledTitles() {
	return calledTitles;
    }
    
    public String[] getCallingTitles() {
	return callingTitles;
    }
    
    public String getAETProperties() {
	return aetProperties;
    }
    
    public boolean isMultiThreadTCP() {
	return multiThreadTCP;
    }
    
    public boolean isMultiThreadAssoc() {
	return multiThreadAssoc;
    }
    
    public boolean isQueueRQ() {
	return queueRQ;
    }
    
    public int getMaxOperationsInvoked() {
	return maxOpInvoked;
    }
    
    public int getMaxPduSize() {
	return maxPduSize;
    }
    
    public int getReleaseTimeout() {
	return releaseTimeout;
    }
    
    public int getAssocTimeout() {
	return assocTimeout;
    }
    
    public boolean isVerification() {
	return verification;
    }
    
    public int[] getVerificationTS() {
	return NON_IMG_TS[verificationTS];
    }
    
    public boolean isStorage() {
	return storage;
    }
    
    public int[] getNonImageStorageTS() {
	return NON_IMG_TS[storageTS];
    }
    
    public int[] getImageStorageTS() {
	return IMG_TS[storageTS];
    }
    
    public boolean isWarningAsSuccess() {
	return warningAsSuccess;
    }
    
    public boolean isStorageValidate() {
	return storageValidate;
    }
    
    public boolean isStorageCmt() {
	return storageCmt;
    }
    
    public int[] getStorageCmtTS() {
	return NON_IMG_TS[storageCmtTS];
    }
    
    public int getStorageCmtAssocMode() {
	return storageCmtAssocMode;
    }
    
    public int getStorageCmtTiming() {
	return storageCmtTiming;
    }
    
    public boolean isQRPatRoot() {
	return qrPatRoot;
    }
    
    public boolean isQRStudyRoot() {
	return qrStudyRoot;
    }
    
    public boolean isQRPatStudyOnly() {
	return qrPatStudyOnly;
    }
    
    public int[] getQueryRetrieveTS() {
	return NON_IMG_TS[qrTS];
    }
    
    public int getMovePriority() {
	return qrPriority;
    }
    
    public boolean isFwdStorageCmt() {
	return fwdStorageCmt;
    }
    
    public boolean isFwdQuery() {
	return fwdQuery;
    }
    
    public boolean isFwdRetrieve() {
	return fwdRetrieve;
    }
    
    public boolean isFwdKeepOpen() {
	return fwdKeepOpen;
    }
    
    public boolean isFwdCallingTitle() {
	return fwdCallingTitle;
    }
    
    public String getFwdAET() {
	return prop.getProperty("Forward.AET");
    }
    
    public boolean isSCN() {
	return scn;
    }
    
    public String getSCNAET() {
	return prop.getProperty("SCN.AET");
    }
    
    public boolean isSCNTotal() {
	return scnTotal;
    }
    
    public boolean isFileset() {
	return fileSet;
    }
    
    public boolean isFilesetSeparate() {
	return fileSetSeparate;
    }
    
    public boolean isFilesetSplitSeries() {
	return fileSetSplitSeries;
    }
    
    public String getFilesetPath() {
	return prop.getProperty("Fileset.Path");
    }
    
    public String getFilesetScheme() {
	return prop.getProperty("Fileset.Scheme");
    }
    
    public String getFilesetID() {
	return prop.getProperty("Fileset.ID");
    }
    
    public UIDEntry getMediaTS4Img() {
	return mediaTS4Img;
    }
    
    public UIDEntry getMediaTS4NonImg() {
	return mediaTS4NonImg;
    }
    
    public final boolean isCompress() {
	return compress;
    }
    
    public final boolean isFileMetaInformation() {
	return fmi;
    }
    
    public final String getFilenameExt() {
	return filenameExt;
    }
    
    public final IconFactory getIconFactory() {
	return iconFactory;
    }
    
    public final boolean isFilesetIconJpeg() {
	return filesetIconJpeg;
    }
    
    public final long getFilesetMaxSize() {
	return fileSetMaxSize;
    }
    
    public final WebsiteBuilder getWebsiteBuilder() {
	return websiteBuilder;
    }
    
    public final int getVerbose() {
	return verbose;
    }
    
    public Properties getProperties() {
	return prop;
    }
    
    private static String[] tokenize(String string, String string_0_) {
	StringTokenizer stringtokenizer
	    = new StringTokenizer(string, string_0_);
	String[] strings = new String[stringtokenizer.countTokens()];
	for (int i = 0; i < strings.length; i++)
	    strings[i] = stringtokenizer.nextToken();
	return strings;
    }
    
    private static boolean parseBoolean(String string) {
	return string != null && "true".compareTo(string.toLowerCase()) == 0;
    }
    
    static {
	CHECKS.put("Port", CheckParam.range(100, 65535));
	CHECKS.put("MultiThreadTCP", CheckParam.bool());
	CHECKS.put("MultiThreadAssoc", CheckParam.bool());
	CHECKS.put("QueueRQ", CheckParam.bool());
	CHECKS.put("MaxOpInvoked", CheckParam.range(0, 65535));
	CHECKS.put("MaxPduSize", CheckParam.range(0, 65535));
	CHECKS.put("AssocTimeout[ms]", CheckParam.range(0, 65535));
	CHECKS.put("ReleaseTimeout[ms]", CheckParam.range(0, 65535));
	CHECKS.put("Verification", CheckParam.bool());
	CHECKS.put("Verification.TransferSyntax",
		   CheckParam.enum(NATIVE_TRANSFER_SYNTAX_NAMES));
	CHECKS.put("Storage", CheckParam.bool());
	CHECKS.put("Storage.TransferSyntax",
		   CheckParam.enum(ACCEPT_TRANSFER_SYNTAX_NAMES));
	CHECKS.put("Storage.WarningAsSuccess", CheckParam.bool());
	CHECKS.put("Storage.Validate", CheckParam.bool());
	CHECKS.put("StorageCmt", CheckParam.bool());
	CHECKS.put("StorageCmt.TransferSyntax",
		   CheckParam.enum(NATIVE_TRANSFER_SYNTAX_NAMES));
	CHECKS.put("StorageCmt.Forward", CheckParam.bool());
	CHECKS.put("StorageCmt.BeforeRSP", CheckParam.bool());
	CHECKS.put("StorageCmt.OpenAssoc",
		   CheckParam.enum(COMMIT_ASSOC_MODE_NAMES));
	CHECKS.put("QR.PatRoot", CheckParam.bool());
	CHECKS.put("QR.StudyRoot", CheckParam.bool());
	CHECKS.put("QR.PatStudyOnly", CheckParam.bool());
	CHECKS.put("QR.Priority", CheckParam.enum(PRIORITIES));
	CHECKS.put("QR.Forward.Query", CheckParam.bool());
	CHECKS.put("QR.Forward.Retrieve", CheckParam.bool());
	CHECKS.put("QR.TransferSyntax",
		   CheckParam.enum(NATIVE_TRANSFER_SYNTAX_NAMES));
	CHECKS.put("Forward.KeepOpen", CheckParam.bool());
	CHECKS.put("Forward.CallingTitle", CheckParam.bool());
	CHECKS.put("SCN", CheckParam.bool());
	CHECKS.put("SCN.Total", CheckParam.bool());
	CHECKS.put("Fileset", CheckParam.bool());
	CHECKS.put("Fileset.Separate", CheckParam.bool());
	CHECKS.put("Fileset.SplitSeries", CheckParam.bool());
	CHECKS.put("Fileset.TransferSyntax",
		   CheckParam.enum(MEDIA_TRANSFER_SYNTAX_NAMES));
	CHECKS.put("Fileset.Icon", CheckParam.bool());
	CHECKS.put("Fileset.Icon.MaxRows", CheckParam.range(32, 512));
	CHECKS.put("Fileset.Icon.MaxColumns", CheckParam.range(32, 512));
	CHECKS.put("Fileset.Icon.Jpeg", CheckParam.bool());
	CHECKS.put("Fileset.Icon.Jpeg.Quality", CheckParam.range(0, 100));
	CHECKS.put("Fileset.Icon.Jpeg.Multiframe", CheckParam.bool());
	CHECKS.put("Fileset.Icon.Color", CheckParam.enum(ICON_COLOR));
	CHECKS.put("Fileset.MaxSize[MB]", CheckParam.range(10, 100000));
	CHECKS.put("Website", CheckParam.bool());
	CHECKS.put("Website.Separate", CheckParam.bool());
	CHECKS.put("Website.Jpeg.MaxRows", CheckParam.range(32, 10000));
	CHECKS.put("Website.Jpeg.MaxColumns", CheckParam.range(32, 10000));
	CHECKS.put("Website.Jpeg.Multiframe", CheckParam.bool());
	CHECKS.put("Website.Jpeg.Quality", CheckParam.range(0, 100));
	CHECKS.put("Website.Jpeg.BurnInOverlays", CheckParam.bool());
	CHECKS.put("Verbose", CheckParam.enum(VERBOSE));
    }
}
