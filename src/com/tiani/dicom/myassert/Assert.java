/* Assert - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.myassert;

public class Assert
{
    public static void isTrue(String string, boolean bool) {
	if (!bool)
	    fail(string);
    }
    
    public static void isTrue(boolean bool) {
	isTrue(null, bool);
    }
    
    public static void equals(double d, double d_0_, double d_1_) {
	equals(null, d, d_0_, d_1_);
    }
    
    public static void equals(long l, long l_2_) {
	equals(null, l, l_2_);
    }
    
    public static void equals(Object object, Object object_3_) {
	equals(null, object, object_3_);
    }
    
    public static void equals(String string, double d, double d_4_,
			      double d_5_) {
	if (Math.abs(d - d_4_) > d_5_)
	    _failNotEquals(string, new Double(d), new Double(d_4_));
    }
    
    public static void equals(String string, long l, long l_6_) {
	equals(string, new Long(l), new Long(l_6_));
    }
    
    public static void equals(String string, Object object, Object object_7_) {
	if ((object != null || object_7_ != null)
	    && (object == null || !object.equals(object_7_)))
	    _failNotEquals(string, object, object_7_);
    }
    
    public static void notNull(Object object) {
	notNull(null, object);
    }
    
    public static void notNull(String string, Object object) {
	isTrue(string, object != null);
    }
    
    public static void isNull(Object object) {
	isNull(null, object);
    }
    
    public static void isNull(String string, Object object) {
	isTrue(string, object == null);
    }
    
    public static void same(Object object, Object object_8_) {
	same(null, object, object_8_);
    }
    
    public static void same(String string, Object object, Object object_9_) {
	if (object != object_9_)
	    _failNotSame(string, object, object_9_);
    }
    
    public static void fail() {
	fail(null);
    }
    
    public static void fail(String string) {
	throw new AssertionFailedError(string);
    }
    
    private static void _failNotEquals(String string, Object object,
				       Object object_10_) {
	String string_11_ = "";
	if (string != null)
	    string_11_ = string + " ";
	fail(string_11_ + "expected:<" + object + "> but was:<" + object_10_
	     + ">");
    }
    
    private static void _failNotSame(String string, Object object,
				     Object object_12_) {
	String string_13_ = "";
	if (string != null)
	    string_13_ = string + " ";
	fail(string_13_ + "expected same");
    }
}
