/* ModalityPPSCreateSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.risserver;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Vector;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefNCreateSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;

public class ModalityPPSCreateSCP extends DefNCreateSCP
{
    private static final int INVALID_ATTRIBUTE_VALUE = 262;
    private static final int PROCESSING_FAILURE = 272;
    private static final int DUPLICATE_SOP_INSTANCE = 273;
    private static final int MISSING_ATTRIBUTE = 288;
    private static final int MISSING_ATTRIBUTE_VALUE = 289;
    private static final int[] _REQ_VALUE
	= { 1210, 1206, 1198, 1201, 1202, 1205, 81 };
    private static final int[] _REQ_ATTRIB
	= { 1210, 1206, 1198, 1201, 1202, 1205, 81, 147, 148, 150, 152, 110,
	    1199, 1200, 1207, 1208, 96, 1203, 1204, 427, 1209, 1227 };
    private static final int[] _REQ_VALUE_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE
	= { 425 };
    private static final int[] _REQ_ATTRIB_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE
	= { 425, 107, 77, 589, 547, 582, 580 };
    private Repository _repository;
    private boolean _validate;
    
    public ModalityPPSCreateSCP(Repository repository) {
	_repository = repository;
    }
    
    protected int create(DimseExchange dimseexchange, String string,
			 String string_0_, DicomMessage dicommessage,
			 DicomMessage dicommessage_1_) {
	try {
	    DicomObject dicomobject = dicommessage.getDataset();
	    if (string_0_ == null)
		dicommessage_1_.set(13, string_0_ = UIDUtils.createUID());
	    dicomobject.setFileMetaInformation
		(new FileMetaInformation(string, string_0_));
	    File file = _repository.createMPPSFile(string_0_);
	    if (file.exists()) {
		Debug.out
		    .println("ERROR: Duplicate SOP instance - " + string_0_);
		return 273;
	    }
	    int i = check(dicomobject, dicommessage_1_);
	    switch (i) {
	    case 0: {
		FileOutputStream fileoutputstream = new FileOutputStream(file);
		try {
		    dicomobject.write(fileoutputstream, true);
		} finally {
		    fileoutputstream.close();
		}
		if (Debug.DEBUG > 0)
		    Debug.out
			.println("Create Modality PPS SOP instance - " + file);
	    }
		/* fall through */
	    default:
		return i;
	    }
	} catch (Exception exception) {
	    Debug.out.println("ERROR: Processing failure - " + exception);
	    return 272;
	}
    }
    
    private int check(DicomObject dicomobject, DicomMessage dicommessage) {
	Vector vector = checkMissingAttrib(dicomobject, _REQ_ATTRIB);
	Vector vector_2_ = checkMissingValue(dicomobject, _REQ_VALUE);
	int i = dicomobject.getSize(1210);
	switch (i) {
	case -1:
	    return 288;
	case 0:
	    return !vector.isEmpty() ? 288 : 289;
	default: {
	    Vector[] vectors = new Vector[i];
	    Vector[] vectors_3_ = new Vector[i];
	    for (int i_4_ = 0; i_4_ < i; i_4_++) {
		DicomObject dicomobject_5_
		    = (DicomObject) dicomobject.get(1210, i_4_);
		vectors[i_4_]
		    = (checkMissingAttrib
		       (dicomobject_5_,
			_REQ_ATTRIB_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE));
		vectors_3_[i_4_]
		    = (checkMissingValue
		       (dicomobject_5_,
			_REQ_VALUE_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE));
	    }
	    return (!vector.isEmpty() || isAnyNotEmpty(vectors) ? 288
		    : !vector_2_.isEmpty() || isAnyNotEmpty(vectors_3_) ? 289
		    : 0);
	}
	}
    }
    
    private static boolean isAnyNotEmpty(Vector[] vectors) {
	int i = vectors.length;
	for (int i_6_ = 0; i_6_ < i; i_6_++) {
	    if (!vectors[i_6_].isEmpty())
		return true;
	}
	return false;
    }
    
    private static Vector checkMissingValue(DicomObject dicomobject,
					    int[] is) {
	Vector vector = new Vector();
	for (int i = 0; i < is.length; i++) {
	    if (dicomobject.getSize(is[i]) <= 0) {
		vector.addElement(new Integer(is[i]));
		Debug.out.println("Missing attribute value: "
				  + new Tag(DDict.getGroup(is[i]),
					    DDict.getElement(is[i])));
	    }
	}
	return vector;
    }
    
    private static Vector checkMissingAttrib(DicomObject dicomobject,
					     int[] is) {
	Vector vector = new Vector();
	for (int i = 0; i < is.length; i++) {
	    if (dicomobject.getSize(is[i]) < 0) {
		vector.addElement(new Integer(is[i]));
		Debug.out.println("Missing attribute: "
				  + new Tag(DDict.getGroup(is[i]),
					    DDict.getElement(is[i])));
	    }
	}
	return vector;
    }
}
