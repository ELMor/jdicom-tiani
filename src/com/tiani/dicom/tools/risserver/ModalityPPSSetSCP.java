/* ModalityPPSSetSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.risserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.Vector;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.framework.DefNSetSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.util.Tag;

public class ModalityPPSSetSCP extends DefNSetSCP
{
    private static final String IN_PROGRESS = "IN PROGRESS";
    private static final int PROCESSING_FAILURE = 272;
    private static final int NO_SUCH_ATTRIBUTE = 261;
    private static final int NO_SUCH_OBJECT_INSTANCE = 274;
    private static final int MISSING_ATTRIBUTE = 288;
    private static final int MISSING_ATTRIBUTE_VALUE = 289;
    private static final int MAY_NO_LONGER_BE_UPDATED = 42768;
    private static final int[] _REQ_NOT_ALLOWED
	= { 1210, 1206, 1198, 1201, 1202, 147, 148, 150, 152, 110, 1199, 1200,
	    81, 427 };
    private static final int[] _FINAL_REQ_VALUE = { 1203, 1204, 1227 };
    private static final int[] _FINAL_REQ_VALUE_PERFORMED_SERIES_SEQUENCE
	= { 239, 426 };
    private static final int[] _FINAL_REQ_ATTRIB_PERFORMED_SERIES_SEQUENCE
	= { 100, 239, 102, 426, 97, 79 };
    private Repository _repository;
    private boolean _validate;
    
    public ModalityPPSSetSCP(Repository repository) {
	_repository = repository;
    }
    
    protected int set(DimseExchange dimseexchange, String string,
		      String string_0_, DicomMessage dicommessage,
		      DicomMessage dicommessage_1_) {
	try {
	    DicomObject dicomobject = dicommessage.getDataset();
	    File file = _repository.createMPPSFile(string_0_);
	    if (!file.exists()) {
		Debug.out
		    .println("ERROR: No Such object instance - " + string_0_);
		return 274;
	    }
	    DicomObject dicomobject_2_ = new DicomObject();
	    FileInputStream fileinputstream = new FileInputStream(file);
	    try {
		dicomobject_2_.read(fileinputstream, true);
	    } finally {
		fileinputstream.close();
	    }
	    if (!"IN PROGRESS".equals(dicomobject_2_.getS(1205))) {
		Debug.out.println
		    ("ERROR: Performed Procedure Step Object may no longer be updated");
		dicommessage_1_.set(12, new Integer(42768));
		return 272;
	    }
	    int i = set(dicomobject_2_, dicommessage, dicommessage_1_);
	    switch (i) {
	    case 0:
	    case 261:
		if (!"IN PROGRESS".equals(dicomobject_2_.getS(1205)))
		    i = checkFinal(dicomobject_2_, i);
		/* fall through */
	    default:
		switch (i) {
		case 0:
		case 261: {
		    FileOutputStream fileoutputstream
			= new FileOutputStream(file);
		    try {
			dicomobject_2_.write(fileoutputstream, true);
		    } finally {
			fileoutputstream.close();
		    }
		    if (Debug.DEBUG > 0)
			Debug.out.println("Set Modality PPS SOP instance - "
					  + file);
		}
		    /* fall through */
		default:
		    return i;
		}
	    }
	} catch (Exception exception) {
	    Debug.out.println("ERROR: Processing failure - " + exception);
	    return 272;
	}
    }
    
    private static int set
	(DicomObject dicomobject, DicomMessage dicommessage,
	 DicomMessage dicommessage_3_)
	throws DicomException {
	DicomObject dicomobject_4_ = dicommessage.getDataset();
	Vector vector = checkNotAllowAttrib(dicomobject_4_, _REQ_NOT_ALLOWED);
	Vector vector_5_ = new Vector();
	Enumeration enumeration = dicomobject_4_.enumerateVRs(false, false);
	while (enumeration.hasMoreElements()) {
	    TagValue tagvalue = (TagValue) enumeration.nextElement();
	    int i = tagvalue.getGroup();
	    int i_6_ = tagvalue.getElement();
	    if (dicomobject.getSize_ge(i, i_6_) >= 0) {
		dicomobject.set_ge(i, i_6_, null);
		int i_7_ = tagvalue.size();
		for (int i_8_ = 0; i_8_ < i_7_; i_8_++)
		    dicomobject.append_ge(i, i_6_, tagvalue.getValue(i_8_));
	    } else {
		Debug.out.println("WARNING: Try to set not-created attribute: "
				  + new Tag(i, i_6_));
		vector_5_.addElement(tagvalue);
	    }
	}
	return vector.isEmpty() && vector_5_.isEmpty() ? 0 : 261;
    }
    
    private static Vector checkNotAllowAttrib(DicomObject dicomobject,
					      int[] is) {
	Vector vector = new Vector();
	for (int i = 0; i < is.length; i++) {
	    if (dicomobject.getSize(is[i]) >= 0) {
		dicomobject.deleteItem(is[i]);
		vector.addElement(new Integer(is[i]));
		Debug.out.println("WARNING: Try to set not-allowed attribute: "
				  + new Tag(DDict.getGroup(is[i]),
					    DDict.getElement(is[i])));
	    }
	}
	return vector;
    }
    
    private static int checkFinal(DicomObject dicomobject, int i)
	throws DicomException {
	Vector vector = checkMissingValue(dicomobject, _FINAL_REQ_VALUE);
	switch (dicomobject.getSize(1227)) {
	case -1:
	    return 288;
	case 0:
	    return 289;
	default: {
	    DicomObject dicomobject_9_ = (DicomObject) dicomobject.get(1227);
	    Vector vector_10_
		= (checkMissingAttrib
		   (dicomobject_9_,
		    _FINAL_REQ_ATTRIB_PERFORMED_SERIES_SEQUENCE));
	    Vector vector_11_ = (checkMissingValue
				 (dicomobject_9_,
				  _FINAL_REQ_VALUE_PERFORMED_SERIES_SEQUENCE));
	    return (!vector_10_.isEmpty() ? 288
		    : !vector.isEmpty() || !vector_11_.isEmpty() ? 289 : i);
	}
	}
    }
    
    private static Vector checkMissingValue(DicomObject dicomobject,
					    int[] is) {
	Vector vector = new Vector();
	for (int i = 0; i < is.length; i++) {
	    if (dicomobject.getSize(is[i]) <= 0) {
		vector.addElement(new Integer(is[i]));
		Debug.out.println("Missing attribute value: "
				  + new Tag(DDict.getGroup(is[i]),
					    DDict.getElement(is[i])));
	    }
	}
	return vector;
    }
    
    private static Vector checkMissingAttrib(DicomObject dicomobject,
					     int[] is) {
	Vector vector = new Vector();
	for (int i = 0; i < is.length; i++) {
	    if (dicomobject.getSize(is[i]) < 0) {
		vector.addElement(new Integer(is[i]));
		Debug.out.println("Missing attribute: "
				  + new Tag(DDict.getGroup(is[i]),
					    DDict.getElement(is[i])));
	    }
	}
	return vector;
    }
}
