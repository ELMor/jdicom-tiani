/* Validate - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;

import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.tiani.dicom.iod.CRImageIOD;
import com.tiani.dicom.iod.CTImageIOD;
import com.tiani.dicom.iod.CompositeIOD;
import com.tiani.dicom.iod.DefCallbackUser;
import com.tiani.dicom.iod.GSPresentationStateIOD;
import com.tiani.dicom.iod.MRImageIOD;
import com.tiani.dicom.iod.NMImageIOD;
import com.tiani.dicom.iod.PETImageIOD;
import com.tiani.dicom.iod.SCImageIOD;
import com.tiani.dicom.iod.StandaloneOverlayIOD;
import com.tiani.dicom.iod.USImageIOD;
import com.tiani.dicom.iod.USMultiFrameImageIOD;
import com.tiani.dicom.iod.UserOption;
import com.tiani.dicom.iod.XAImageIOD;
import com.tiani.dicom.iod.XRFImageIOD;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JFileListDialog;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.ui.UserOptionTableModel;
import com.tiani.dicom.util.ExampleFileFilter;
import com.tiani.dicom.util.ExtDDict;

public class Validate extends JApplet
{
    private static final ExtDDict extDDict = new ExtDDict();
    private DefCallbackUser callbackUser = new DefCallbackUser();
    private String[] _IOD_LABEL
	= { "CR", "CT", "MR", "NM", "PET", "US", "US MF", "SC", "XA", "RF",
	    "St.OV", "St.CV", "St.LUT", "GSPS" };
    private UserOption[][] _IOD_USER_OPTIONS
	= { CRImageIOD.userOptions, CTImageIOD.userOptions,
	    MRImageIOD.userOptions, NMImageIOD.userOptions,
	    PETImageIOD.userOptions, USImageIOD.userOptions,
	    USMultiFrameImageIOD.userOptions, SCImageIOD.userOptions,
	    XAImageIOD.userOptions, XRFImageIOD.userOptions,
	    StandaloneOverlayIOD.userOptions, StandaloneOverlayIOD.userOptions,
	    StandaloneOverlayIOD.userOptions,
	    GSPresentationStateIOD.userOptions };
    private JFileChooser _fileChooser = null;
    private JFileListDialog _fileListDlg = null;
    private JFrame _frame = null;
    private final FileFilter _HTMLFileFilter
	= new ExampleFileFilter("html", "HTML");
    private File _curFile = null;
    private JButton _validateButton = new JButton("Validate");
    private JButton _validateMultiButton = new JButton("Validate *");
    private JButton _saveHtmlButton = new JButton("HTML");
    private JTextArea _logTextArea = new JTextArea();
    private PrintStream _log
	= new PrintStream(new DocumentOutputStream(_logTextArea.getDocument(),
						   50000),
			  true);
    private JTabbedPane tabbedPane = new JTabbedPane(2);
    private JSplitPane splitPane;
    private static final DicomObjectTableModel _NULL_MODEL
	= new DicomObjectTableModel();
    private JSizeColumnsToFitTable _table
	= new JSizeColumnsToFitTable(_NULL_MODEL);
    
    public static void main(String[] strings) {
	Validate validate = new Validate();
	validate._frame
	    = new AppletFrame("Validate v1.7.26", validate, 500, 500);
    }
    
    public void init() {
	_validateButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file = Validate.this.chooseFile("Validate File",
						     "Validate", 0, false);
		if (file != null)
		    Validate.this.validate(file, true);
	    }
	});
	_validateMultiButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File[] files = Validate.this.chooseFiles();
		if (files != null)
		    Validate.this.validate(files);
	    }
	});
	_saveHtmlButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (_curFile != null)
		    _fileChooser.setSelectedFile(new File(_curFile.getParent(),
							  (_curFile.getName()
							   + ".html")));
		File file = Validate.this.chooseFile("Save as HTML file",
						     "Save", 0, true);
		if (file != null)
		    Validate.this.saveHtml(file);
	    }
	});
	AppletContext appletcontext = this.getAppletContext();
	if (appletcontext instanceof AppletFrame)
	    appletcontext = null;
	Box box = Box.createHorizontalBox();
	box.add(_validateButton);
	box.add(_validateMultiButton);
	box.add(_saveHtmlButton);
	box.add(Box.createGlue());
	box.add(new JTianiButton(appletcontext));
	for (int i = 0; i < _IOD_LABEL.length; i++)
	    tabbedPane.add(_IOD_LABEL[i],
			   (new JScrollPane
			    (new JSizeColumnsToFitTable
			     (new UserOptionTableModel(_IOD_USER_OPTIONS[i],
						       callbackUser)))));
	tabbedPane.add("Attributs", new JScrollPane(_table));
	splitPane
	    = new JSplitPane(0, tabbedPane, new JAutoScrollPane(_logTextArea));
	Container container = this.getContentPane();
	container.add(box, "North");
	container.add(splitPane, "Center");
    }
    
    private File chooseFile(String string, String string_3_, int i,
			    boolean bool) {
	try {
	    if (_fileChooser == null) {
		_fileChooser = new JFileChooser(".");
		_fileChooser.addChoosableFileFilter(_HTMLFileFilter);
	    }
	    _fileChooser.setFileFilter(bool ? _HTMLFileFilter
				       : _fileChooser
					     .getAcceptAllFileFilter());
	    _fileChooser.setDialogTitle(string);
	    _fileChooser.setFileSelectionMode(i);
	    int i_4_ = _fileChooser.showDialog(_frame, string_3_);
	    File file = _fileChooser.getSelectedFile();
	    return i_4_ == 0 ? file : null;
	} catch (SecurityException securityexception) {
	    _log.println(securityexception);
	    showPolicyFile();
	    return null;
	}
    }
    
    private void showPolicyFile() {
	try {
	    URL url = new URL(this.getDocumentBase(),
			      this.getParameter("PolicyFile"));
	    this.getAppletContext().showDocument(url, "_blank");
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private File[] chooseFiles() {
	File file = chooseFile("Select Directory", "Select", 2, false);
	if (file == null)
	    return null;
	if (_fileListDlg == null)
	    _fileListDlg = new JFileListDialog(_frame, "Validate files");
	return _fileListDlg.getSelectedFiles(file.isDirectory() ? file
					     : new File(file.getParent()));
    }
    
    private void validate(File file, boolean bool) {
	try {
	    _log.println("Validate file " + file);
	    DicomObject dicomobject = new DicomObject();
	    TianiInputStream tianiinputstream
		= new TianiInputStream(new FileInputStream(file));
	    try {
		tianiinputstream.read(dicomobject);
	    } finally {
		tianiinputstream.close();
	    }
	    _curFile = file;
	    if (bool)
		_table.setModel(new DicomObjectTableModel(dicomobject));
	    String string = dicomobject.getS(62);
	    String string_5_ = UID.getUIDEntry(string).getName();
	    CompositeIOD compositeiod
		= CompositeIOD.getIOD(string, dicomobject);
	    if (compositeiod == null)
		_log.println("No IOD for  " + string_5_ + " available");
	    else {
		Vector vector = new Vector();
		if (!compositeiod.validate(dicomobject, vector,
					   callbackUser)) {
		    _log.println("ERROR detected for " + string_5_ + " :");
		    Enumeration enumeration = vector.elements();
		    while (enumeration.hasMoreElements())
			_log.println(enumeration.nextElement());
		} else
		    _log.println("No error detected for " + string_5_);
	    }
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private void validate(final File[] files) {
	new Thread(new Runnable() {
	    public void run() {
		for (int i = 0; i < files.length; i++)
		    Validate.this.validate(files[i], false);
	    }
	}).start();
    }
    
    private void saveHtml(File file) {
	try {
	    FileOutputStream fileoutputstream = new FileOutputStream(file);
	    PrintWriter printwriter = new PrintWriter(fileoutputstream);
	    try {
		_table.toHTML(printwriter, _curFile.getName());
	    } finally {
		printwriter.close();
	    }
	    _log.println("Save as HTML to " + file);
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
}
