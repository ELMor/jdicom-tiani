/* ModalitySCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.tools.modalityscu.JFilterPanel;
import com.tiani.dicom.tools.modalityscu.JTreePanel;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;

public class ModalitySCU extends JApplet implements IAssociationListener
{
    private DimseExchange _dimseExchange = null;
    private VerboseAssociation _as = null;
    private boolean _autoClose = false;
    private int _curQRMsgID = 0;
    private final IDimseRspListener _autoCloseSCU;
    private Tag[] NCREATE_ATTRIB;
    private static final Tag[] SET_PPS_INFORMATION_MODULE
	= { new Tag(8, 4146), new Tag(64, 592), new Tag(64, 593),
	    new Tag(64, 594), new Tag(64, 596), new Tag(64, 597),
	    new Tag(64, 640) };
    private static final Tag[] SET_IMAGE_ACQUISITION_RESULTS_MODULE
	= { new Tag(64, 608), new Tag(64, 832) };
    private static final Tag[] NSET_ATTRIB
	= IOD.accumulate(new Tag[][]
			 { SET_PPS_INFORMATION_MODULE,
			   SET_IMAGE_ACQUISITION_RESULTS_MODULE,
			   IOD.RADIATION_DOSE_MODULE,
			   IOD.BILLING_AND_MATERIAL_MANAGEMENT_CODE_MODULE });
    private final IDimseRspListener _queryFindSCU;
    private Properties _params;
    public static final String[] PRIORITIES = { "MEDIUM", "HIGH", "LOW" };
    public static final String[] PARAM_NAMES
	= { "Host", "Port", "CalledTitle", "CallingTitle", "Priority",
	    "Verbose" };
    private static String[] VERBOSE = { "0", "1", "2", "3", "4", "5" };
    private static Hashtable PARAM_CHECKS = new Hashtable();
    private JButton _openButton;
    private JButton _closeButton;
    private JButton _echoButton;
    private JButton _findButton;
    private JButton _createButton;
    private JButton _setButton;
    private JButton _cancelButton;
    private static final DicomObjectTableModel _NULL_MODEL;
    private JSizeColumnsToFitTable _recordTable;
    private JFilterPanel _filterPanel;
    private JTabbedPane tabbedPane;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;
    private JTreePanel _treePanel;
    
    private class NCreateRspListener implements IDimseRspListener
    {
	DicomObject _pps = null;
	
	public NCreateRspListener(DicomObject dicomobject) {
	    _pps = dicomobject;
	}
	
	public void handleRSP
	    (DimseExchange dimseexchange, int i, int i_0_,
	     DicomMessage dicommessage)
	    throws DicomException {
	    if (dicommessage.getSize(13) > 0) {
		_pps.getFileMetaInformation().set(30, dicommessage.get(13));
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
			((DicomObjectTableModel) _recordTable.getModel())
			    .fireTableDataChanged();
		    }
		});
	    }
	    if (_autoClose) {
		_autoClose = false;
		ModalitySCU.this.close();
	    }
	}
    }
    
    public static void main(String[] strings) {
	final String propSpec
	    = strings.length > 0 ? strings[0] : "ModalitySCU.properties";
	Properties properties = null;
	try {
	    properties = loadProperties(new FileInputStream(propSpec));
	} catch (Exception exception) {
	    /* empty */
	}
	final ModalitySCU instance = new ModalitySCU(properties);
	new AppletFrame("ModalitySCU v1.7.26", instance, 700, 500, new WindowAdapter() {
	    public void windowClosing(WindowEvent windowevent) {
		try {
		    instance.storeParams(propSpec);
		} catch (Exception exception) {
		    System.out.println(exception);
		}
		System.exit(0);
	    }
	});
    }
    
    public ModalitySCU() {
	_autoCloseSCU = new IDimseRspListener() {
	    public void handleRSP(DimseExchange dimseexchange, int i, int i_3_,
				  DicomMessage dicommessage) {
		if (_autoClose) {
		    _autoClose = false;
		    ModalitySCU.this.close();
		}
	    }
	};
	NCREATE_ATTRIB
	    = (IOD.accumulate
	       (new Tag[][]
		{ IOD.PPS_RELATIONSHIP_MODULE, IOD.PPS_INFORMATION_MODULE,
		  IOD.IMAGE_ACQUISITION_RESULTS_MODULE,
		  IOD.RADIATION_DOSE_MODULE,
		  IOD.BILLING_AND_MATERIAL_MANAGEMENT_CODE_MODULE }));
	_queryFindSCU = new IDimseRspListener() {
	    private Vector rspQueue = new Vector();
	    private Runnable addChild = new Runnable() {
		public void run() {
		    while (!rspQueue.isEmpty()) {
			_treePanel
			    .addWLItem((DicomObject) rspQueue.elementAt(0));
			rspQueue.removeElementAt(0);
		    }
		}
	    };
	    private Runnable finish = new Runnable() {
		public void run() {
		    _cancelButton.setEnabled(false);
		    if (_autoClose) {
			_autoClose = false;
			ModalitySCU.this.close();
		    }
		}
	    };
	    
	    public void handleRSP
		(DimseExchange dimseexchange, int i, int i_5_,
		 DicomMessage dicommessage)
		throws DicomException {
		if (Status.isPending(i_5_)) {
		    rspQueue.addElement(dicommessage.getDataset());
		    SwingUtilities.invokeLater(addChild);
		} else
		    SwingUtilities.invokeLater(finish);
	    }
	};
	_params = null;
	_openButton = new JButton("Open");
	_closeButton = new JButton("Close");
	_echoButton = new JButton("Echo");
	_findButton = new JButton("Query WL");
	_createButton = new JButton("Create PPS");
	_setButton = new JButton("Update PPS");
	_cancelButton = new JButton("Cancel");
	_recordTable = new JSizeColumnsToFitTable(_NULL_MODEL);
	_filterPanel = new JFilterPanel();
	tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 10000), true);
	_treePanel
	    = new JTreePanel(_createButton, _setButton, _recordTable, _log);
    }
    
    protected ModalitySCU(Properties properties) {
	_autoCloseSCU = new IDimseRspListener() {
	    public void handleRSP(DimseExchange dimseexchange, int i, int i_7_,
				  DicomMessage dicommessage) {
		if (_autoClose) {
		    _autoClose = false;
		    ModalitySCU.this.close();
		}
	    }
	};
	NCREATE_ATTRIB
	    = (IOD.accumulate
	       (new Tag[][]
		{ IOD.PPS_RELATIONSHIP_MODULE, IOD.PPS_INFORMATION_MODULE,
		  IOD.IMAGE_ACQUISITION_RESULTS_MODULE,
		  IOD.RADIATION_DOSE_MODULE,
		  IOD.BILLING_AND_MATERIAL_MANAGEMENT_CODE_MODULE }));
	_queryFindSCU = new IDimseRspListener() {
	    private Vector rspQueue = new Vector();
	    private Runnable addChild = new Runnable() {
		public void run() {
		    while (!rspQueue.isEmpty()) {
			_treePanel
			    .addWLItem((DicomObject) rspQueue.elementAt(0));
			rspQueue.removeElementAt(0);
		    }
		}
	    };
	    private Runnable finish = new Runnable() {
		public void run() {
		    _cancelButton.setEnabled(false);
		    if (_autoClose) {
			_autoClose = false;
			ModalitySCU.this.close();
		    }
		}
	    };
	    
	    public void handleRSP
		(DimseExchange dimseexchange, int i, int i_9_,
		 DicomMessage dicommessage)
		throws DicomException {
		if (Status.isPending(i_9_)) {
		    rspQueue.addElement(dicommessage.getDataset());
		    SwingUtilities.invokeLater(addChild);
		} else
		    SwingUtilities.invokeLater(finish);
	    }
	};
	_params = null;
	_openButton = new JButton("Open");
	_closeButton = new JButton("Close");
	_echoButton = new JButton("Echo");
	_findButton = new JButton("Query WL");
	_createButton = new JButton("Create PPS");
	_setButton = new JButton("Update PPS");
	_cancelButton = new JButton("Cancel");
	_recordTable = new JSizeColumnsToFitTable(_NULL_MODEL);
	_filterPanel = new JFilterPanel();
	tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 10000), true);
	_treePanel
	    = new JTreePanel(_createButton, _setButton, _recordTable, _log);
	_params = properties;
    }
    
    public void init() {
	_openButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		ModalitySCU.this.open();
	    }
	});
	_closeButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		ModalitySCU.this.close();
	    }
	});
	_echoButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		ModalitySCU.this.echo(ModalitySCU.this.isOpen());
	    }
	});
	_findButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		ModalitySCU.this.find(ModalitySCU.this.isOpen());
	    }
	});
	_createButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		ModalitySCU.this.create(ModalitySCU.this.isOpen());
	    }
	});
	_setButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		ModalitySCU.this.set(ModalitySCU.this.isOpen());
	    }
	});
	_cancelButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		ModalitySCU.this.cancel();
	    }
	});
	AppletContext appletcontext = this.getAppletContext();
	if (appletcontext instanceof AppletFrame)
	    appletcontext = null;
	Box box = Box.createHorizontalBox();
	box.add(_openButton);
	box.add(_closeButton);
	box.add(_echoButton);
	box.add(_findButton);
	box.add(_cancelButton);
	box.add(_createButton);
	box.add(_setButton);
	box.add(Box.createGlue());
	box.add(new JTianiButton(appletcontext));
	Debug.out = _log;
	if (appletcontext != null)
	    getAppletParams();
	if (_params == null || !verifyParams()) {
	    try {
		_params
		    = loadProperties(EditDicomObject.class.getResourceAsStream
				     ("ModalitySCU.properties"));
	    } catch (IOException ioexception) {
		Assert.fail("Failed to load ModalitySCU.properties ressource");
	    }
	}
	initTabbedPane();
	Container container = this.getContentPane();
	container.add(box, "North");
	container.add(tabbedPane, "Center");
	enableButtons(false);
    }
    
    private void initTabbedPane() {
	JPropertiesTable jpropertiestable
	    = new JPropertiesTable(PARAM_NAMES, _params, PARAM_CHECKS);
	tabbedPane.add("Props", new JScrollPane(jpropertiestable));
	tabbedPane.add("Filter", new JScrollPane(_filterPanel));
	JSplitPane jsplitpane
	    = new JSplitPane(1, _treePanel, new JScrollPane(_recordTable));
	tabbedPane.add("Result", jsplitpane);
	tabbedPane.add("Log", new JAutoScrollPane(_logTextArea));
    }
    
    static Properties loadProperties(InputStream inputstream)
	throws IOException {
	Properties properties = new Properties();
	try {
	    properties.load(inputstream);
	} finally {
	    inputstream.close();
	}
	return properties;
    }
    
    private void getAppletParams() {
	_params = new Properties();
	for (int i = 0; i < PARAM_NAMES.length; i++) {
	    String string;
	    if ((string = this.getParameter(PARAM_NAMES[i])) != null)
		_params.put(PARAM_NAMES[i], string);
	}
    }
    
    private void storeParams(String string) throws IOException {
	CheckParam.verify(_params, PARAM_CHECKS);
	FileOutputStream fileoutputstream = new FileOutputStream(string);
	try {
	    _params.store(fileoutputstream, "Properties for ModalitySCU");
	} finally {
	    fileoutputstream.close();
	}
    }
    
    private boolean verifyParams() {
	try {
	    CheckParam.verify(_params, PARAM_CHECKS);
	    return true;
	} catch (Exception exception) {
	    _log.println(exception);
	    return false;
	}
    }
    
    private boolean getBooleanParam(String string) {
	String string_17_ = _params.getProperty(string);
	return (string_17_ != null
		&& "true".compareTo(string_17_.toLowerCase()) == 0);
    }
    
    private int getIntParam(String string) {
	return Integer.parseInt(_params.getProperty(string));
    }
    
    private void enableButtons(boolean bool) {
	_openButton.setEnabled(!bool);
	_closeButton.setEnabled(bool);
	if (!bool)
	    _cancelButton.setEnabled(false);
    }
    
    public void associateRequestReceived(VerboseAssociation verboseassociation,
					 Request request) {
	/* empty */
    }
    
    public void associateResponseReceived
	(VerboseAssociation verboseassociation, Response response) {
	/* empty */
    }
    
    public void associateRequestSent(VerboseAssociation verboseassociation,
				     Request request) {
	/* empty */
    }
    
    public void associateResponseSent(VerboseAssociation verboseassociation,
				      Response response) {
	/* empty */
    }
    
    public void releaseRequestReceived(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseReceived
	(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseRequestSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void abortReceived(VerboseAssociation verboseassociation,
			      Abort abort) {
	/* empty */
    }
    
    public void abortSent(VerboseAssociation verboseassociation, int i,
			  int i_18_) {
	/* empty */
    }
    
    public void socketClosed(VerboseAssociation verboseassociation) {
	enableButtons(false);
    }
    
    private boolean isOpen() {
	return _as != null && _as.isOpen();
    }
    
    private void open() {
	tabbedPane.setSelectedIndex(3);
	if (verifyParams()) {
	    if (isOpen())
		close();
	    try {
		Request request = new Request();
		request.setCalledTitle(_params.getProperty("CalledTitle"));
		request.setCallingTitle(_params.getProperty("CallingTitle"));
		int[] is = { 8193 };
		request.addPresentationContext(4097, is);
		request.addPresentationContext(4141, is);
		request.addPresentationContext(4142, is);
		Socket socket = new Socket(_params.getProperty("Host"),
					   getIntParam("Port"));
		Debug.DEBUG = getIntParam("Verbose");
		Requestor requestor = new Requestor(socket, request);
		requestor.addAssociationListener(this);
		_as = requestor.openAssoc();
		if (_as != null) {
		    _dimseExchange
			= new DimseExchange(_as, null, false, false, 1);
		    Thread thread = new Thread(_dimseExchange);
		    thread.start();
		    enableButtons(true);
		}
	    } catch (SecurityException securityexception) {
		_log.println(securityexception);
		showPolicyFile();
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	}
    }
    
    private void showPolicyFile() {
	try {
	    URL url = new URL(this.getDocumentBase(),
			      this.getParameter("PolicyFile"));
	    this.getAppletContext().showDocument(url, "_blank");
	} catch (Exception exception) {
	    _log.println(exception.toString());
	}
    }
    
    private void close() {
	if (isOpen()) {
	    if (_cancelButton.isEnabled()) {
		_autoClose = true;
		cancel();
	    } else {
		try {
		    _dimseExchange.releaseAssoc();
		    _dimseExchange = null;
		    _as = null;
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	}
    }
    
    private void echo(boolean bool) {
	tabbedPane.setSelectedIndex(3);
	if (!bool)
	    open();
	if (isOpen()) {
	    try {
		_curQRMsgID = _as.nextMessageID();
		DicomMessage dicommessage
		    = new DicomMessage(48, _curQRMsgID, null);
		dicommessage.affectedSOPclassUID("1.2.840.10008.1.1");
		_dimseExchange.sendRQ(_curQRMsgID, 4097, dicommessage,
				      _autoCloseSCU);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	    if (!bool)
		_autoClose = true;
	}
    }
    
    private DicomObject getRecord() {
	DicomObjectTableModel dicomobjecttablemodel
	    = (DicomObjectTableModel) _recordTable.getModel();
	return dicomobjecttablemodel.getDicomObject();
    }
    
    private DicomObject extract(DicomObject dicomobject, Tag[] tags)
	throws DicomException {
	DicomObject dicomobject_19_ = new DicomObject();
	for (int i = 0; i < tags.length; i++) {
	    Tag tag = tags[i];
	    int i_20_ = tag.getGroup();
	    int i_21_ = tag.getElement();
	    int i_22_ = dicomobject.getSize_ge(i_20_, i_21_);
	    dicomobject_19_.set_ge(i_20_, i_21_,
				   dicomobject.get_ge(i_20_, i_21_));
	    for (int i_23_ = 1; i_23_ < i_22_; i_23_++)
		dicomobject_19_.append_ge(i_20_, i_21_,
					  dicomobject.get_ge(i_20_, i_21_,
							     i_23_));
	}
	return dicomobject_19_;
    }
    
    private void create(boolean bool) {
	tabbedPane.setSelectedIndex(3);
	if (!bool)
	    open();
	if (isOpen()) {
	    try {
		DicomObject dicomobject = getRecord();
		_curQRMsgID = _as.nextMessageID();
		DicomMessage dicommessage
		    = new DicomMessage(320, _curQRMsgID,
				       extract(dicomobject, NCREATE_ATTRIB));
		dicommessage.affectedSOP("1.2.840.10008.3.1.2.3.3",
					 dicomobject.getFileMetaInformation
					     ().getS(30));
		_dimseExchange.sendRQ(_curQRMsgID, 4142, dicommessage,
				      new NCreateRspListener(dicomobject));
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	    if (!bool)
		_autoClose = true;
	}
    }
    
    private void set(boolean bool) {
	tabbedPane.setSelectedIndex(3);
	if (!bool)
	    open();
	if (isOpen()) {
	    try {
		DicomObject dicomobject = getRecord();
		_curQRMsgID = _as.nextMessageID();
		DicomMessage dicommessage
		    = new DicomMessage(288, _curQRMsgID,
				       extract(dicomobject, NSET_ATTRIB));
		dicommessage.requestedSOP("1.2.840.10008.3.1.2.3.3",
					  dicomobject.getFileMetaInformation
					      ().getS(30));
		_dimseExchange.sendRQ(_curQRMsgID, 4142, dicommessage,
				      _autoCloseSCU);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	    if (!bool)
		_autoClose = true;
	}
    }
    
    private void find(boolean bool) {
	if (verifyParams()) {
	    if (!bool)
		open();
	    if (isOpen()) {
		try {
		    _curQRMsgID = _as.nextMessageID();
		    DicomMessage dicommessage
			= new DicomMessage(32, _curQRMsgID,
					   _filterPanel.getFilter());
		    dicommessage.affectedSOPclassUID("1.2.840.10008.5.1.4.31");
		    dicommessage.priority
			(indexOf(PRIORITIES, _params.getProperty("Priority")));
		    _dimseExchange.sendRQ(_curQRMsgID, 4141, dicommessage,
					  _queryFindSCU);
		    _cancelButton.setEnabled(true);
		    tabbedPane.setSelectedIndex(2);
		} catch (Exception exception) {
		    _log.println(exception);
		}
		if (!bool)
		    _autoClose = true;
	    }
	}
    }
    
    private void cancel() {
	tabbedPane.setSelectedIndex(3);
	if (isOpen()) {
	    try {
		_dimseExchange.sendCancelRQ(_curQRMsgID, 4141);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	}
    }
    
    public static int indexOf(String[] strings, String string) {
	int i = strings.length;
	while (i-- > 0) {
	    if (strings[i].equals(string))
		return i;
	}
	return i;
    }
    
    static {
	PARAM_CHECKS.put("Port", CheckParam.range(100, 65535));
	PARAM_CHECKS.put("Priority", CheckParam.enum(PRIORITIES));
	PARAM_CHECKS.put("Verbose", CheckParam.enum(VERBOSE));
	_NULL_MODEL = new DicomObjectTableModel();
    }
}
