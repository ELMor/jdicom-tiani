/* Decaps - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Decaps
{
    private static final String USAGE
	= "Usage: java -jar decaps.jar source|--stdin [dest|--stdout]\n            (to decaps one file)\n   or  java -jar decaps.jar source... directory\n            (to decaps several files)\n";
    private static final byte SH_INIT_SWAP = 1;
    private static final byte SH_INIT_NOSWAP = 2;
    private static final byte CH_INIT = 3;
    private static final byte CH_END = -2;
    private static final byte SH_END = -1;
    private static final byte SH_REP = 10;
    private static final byte SH_NIB_DIF = 12;
    private static final byte SH_CHAR_DIF = 14;
    private static final byte SH_INCOMP_12 = 16;
    private static final byte SH_INCOMP = 18;
    private static final byte CH_REP = 110;
    private static final byte CH_NIB_DIF = 112;
    private static final byte CH_INCOMP = 118;
    private final DataInputStream in;
    private OutputStream out;
    private boolean endOfDecompress = false;
    private byte lB = 0;
    private short lS = 0;
    private int headerSize = -1;
    private boolean compressed = false;
    
    private Decaps(String string, InputStream inputstream) throws IOException {
	in = new DataInputStream(inputstream);
	init(string);
    }
    
    private void init(String string) throws IOException {
	String string_0_ = in.readLine();
	if (!string_0_.startsWith("MAGIC"))
	    throw new IOException("Not an Encapsed File - " + string);
	while (!"ENDINFO".equals(string_0_ = in.readLine())) {
	    if (string_0_.startsWith("FILETYPE")
		&& !"IMAGE".equals(string_0_.substring(9)))
		throw new IOException("Not an Image File - " + string);
	    if (string_0_.startsWith("COMPRESSION"))
		compressed = !"NONE".equals(string_0_.substring(12));
	    if (string_0_.startsWith("HEADERSIZE")) {
		try {
		    headerSize = Integer.parseInt(string_0_.substring(11));
		} catch (IllegalArgumentException illegalargumentexception) {
		    throw new IOException("Header Format Error - " + string);
		}
	    }
	}
	if (headerSize == -1)
	    throw new IOException("Header Format Error" + string);
    }
    
    private void writeTo(OutputStream outputstream) throws IOException {
	if (compressed) {
	    while (headerSize-- > 0)
		outputstream.write(in.read());
	    out = outputstream;
	    decompress_init();
	    while (!endOfDecompress)
		decompress_more();
	} else {
	    int i;
	    while ((i = in.read()) != -1)
		outputstream.write(i);
	}
    }
    
    private void decompress_init() throws IOException {
	switch (in.readByte()) {
	case 3:
	    in.readInt();
	    out(in.readByte());
	    break;
	case 1:
	case 2:
	    in.readInt();
	    out(in.readShort());
	    break;
	}
    }
    
    private void decompress_more() throws IOException {
	if (!endOfDecompress) {
	    byte i = in.readByte();
	    switch (i) {
	    case -2:
		endOfDecompress = true;
		break;
	    case -1:
		endOfDecompress = true;
		break;
	    default: {
		int i_1_ = in.readByte() & 0xff;
	    switch_1_:
		switch (i) {
		case 118:
		    while (i_1_-- > 0)
			out(in.readByte());
		    break;
		case 18:
		    while (i_1_-- > 0)
			out(in.readShort());
		    break;
		case 16:
		    do {
			int i_2_ = in.readByte();
			int i_3_ = in.readByte();
			out((short) ((i_2_ << 4 & 0xf00) + (i_2_ << 4 & 0xf0)
				     + (i_3_ >> 4 & 0xf)));
			if (--i_1_ == 0)
			    break switch_1_;
			if (i_1_ < 0)
			    throw new IOException("length decoding mismatch");
			out((short) ((i_3_ << 8 & 0xf00)
				     + (in.readByte() & 0xff)));
			if (--i_1_ == 0)
			    break switch_1_;
		    } while (i_1_ >= 0);
		    throw new IOException("length decoding mismatch");
		case 14: {
		    int i_4_ = lS & 0xffff;
		    while (i_1_-- > 0) {
			i_4_ += (in.readByte() & 0xff) - 127;
			out((short) i_4_);
		    }
		    break;
		}
		case 112: {
		    int i_5_ = lB & 0xff;
		    do {
			int i_6_ = in.readByte();
			i_5_ += (i_6_ >> 4 & 0xf) - 7;
			out((byte) i_5_);
			if (--i_1_ == 0)
			    break;
			i_5_ += (i_6_ & 0xf) - 7;
			out((byte) i_5_);
		    } while (--i_1_ != 0);
		    break;
		}
		case 12: {
		    int i_7_ = lS & 0xffff;
		    do {
			int i_8_ = in.readByte();
			i_7_ += (i_8_ >> 4 & 0xf) - 7;
			out((short) i_7_);
			if (--i_1_ == 0)
			    break;
			i_7_ += (i_8_ & 0xf) - 7;
			out((short) i_7_);
		    } while (--i_1_ != 0);
		    break;
		}
		case 110:
		    while (i_1_-- > 0)
			out(lB);
		    break;
		case 10:
		    while (i_1_-- > 0)
			out(lS);
		    break;
		default:
		    throw new IOException
			      ("illegal function code while decompressing: "
			       + i);
		}
	    }
	    }
	}
    }
    
    private void out(byte i) throws IOException {
	out.write(lB = i);
    }
    
    private void out(short i) throws IOException {
	lS = i;
	out.write((byte) i);
	out.write((byte) (i >> 8));
    }
    
    private static void process(File file, File file_9_) {
	String string = "stdin";
	String string_10_ = "stdout";
	InputStream inputstream = System.in;
	java.io.FilterOutputStream filteroutputstream = System.out;
	try {
	    if (file != null) {
		string = "" + file;
		inputstream
		    = new BufferedInputStream(new FileInputStream(file));
	    }
	    if (file_9_ != null) {
		string_10_ = "" + file_9_;
		filteroutputstream
		    = new BufferedOutputStream(new FileOutputStream(file_9_));
	    }
	    new Decaps(string, inputstream).writeTo(filteroutputstream);
	    System.err.println("Decapsed " + string + " to " + string_10_);
	} catch (IOException ioexception) {
	    System.err
		.println("Failed to decaps " + string + " to " + string_10_);
	    System.err.println(ioexception);
	} finally {
	    if (inputstream != null) {
		try {
		    inputstream.close();
		} catch (IOException ioexception) {
		    /* empty */
		}
	    }
	    if (filteroutputstream != null) {
		try {
		    filteroutputstream.close();
		} catch (IOException ioexception) {
		    /* empty */
		}
	    }
	}
    }
    
    public static void main(String[] strings) {
	if (strings.length == 0) {
	    System.err.println
		("Usage: java -jar decaps.jar source|--stdin [dest|--stdout]\n            (to decaps one file)\n   or  java -jar decaps.jar source... directory\n            (to decaps several files)\n");
	    System.exit(1);
	}
	File file = "--stdin".equals(strings[0]) ? null : new File(strings[0]);
	File file_11_ = ("--stdout".equals(strings[strings.length - 1]) ? null
			 : new File(strings[strings.length - 1]));
	boolean bool = file != null && file.isDirectory();
	boolean bool_12_ = file_11_ != null && file_11_.isDirectory();
	if (!bool_12_) {
	    if (bool || strings.length > 2) {
		System.err.println
		    ("to process several files, the last parameter must be a directory");
		System.err.println
		    ("Usage: java -jar decaps.jar source|--stdin [dest|--stdout]\n            (to decaps one file)\n   or  java -jar decaps.jar source... directory\n            (to decaps several files)\n");
		System.exit(1);
	    }
	    process(file, file_11_);
	} else {
	    for (int i = 0; i < strings.length - 1; i++) {
		file = new File(strings[i]);
		if (file.isDirectory())
		    processDir(file, file_11_);
		else
		    process(new File(strings[i]),
			    new File(file_11_,
				     (strings[i].substring
				      (strings[i]
					   .lastIndexOf(File.separatorChar)
				       + 1))));
	    }
	}
    }
    
    private static void processDir(File file, File file_13_) {
	String[] strings = file.list();
	for (int i = 0; i < strings.length; i++) {
	    File file_14_ = new File(file, strings[i]);
	    File file_15_ = new File(file_13_, strings[i]);
	    if (file_14_.isDirectory()) {
		file_15_.mkdir();
		processDir(file_14_, file_15_);
	    } else
		process(file_14_, file_15_);
	}
    }
}
