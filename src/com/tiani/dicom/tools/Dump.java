/* Dump - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class Dump
{
    private final DataInputStream in;
    
    public static void main(String[] strings) throws Exception {
	if (strings.length != 1) {
	    System.err.println("Usage: java -jar dcmdump.jar <file>");
	    System.exit(1);
	}
	DataInputStream datainputstream
	    = (new DataInputStream
	       (new BufferedInputStream(new FileInputStream(strings[0]))));
	try {
	    new Dump(datainputstream).dump(System.out);
	} finally {
	    try {
		datainputstream.close();
	    } catch (IOException ioexception) {
		/* empty */
	    }
	}
    }
    
    public Dump(DataInputStream datainputstream) {
	in = datainputstream;
    }
    
    public void dump(OutputStream outputstream)
	throws IOException, DicomException {
	byte[] is = new byte[5];
	in.mark(5);
	in.readFully(is);
	if (is[0] == 77 && is[1] == 65 && is[2] == 71 && is[3] == 73
	    && is[4] == 67)
	    dumpEncapsHeader(outputstream);
	else
	    in.reset();
	DicomObject dicomobject = new DicomObject();
	dicomobject.read(in, false);
	dicomobject.dumpVRs(outputstream, true);
	if (dicomobject.getPixelDataLength() > 0) {
	    String string = ("Pixel Data Length: "
			     + dicomobject.getPixelDataLength() + "\n");
	    outputstream.write(string.getBytes());
	}
    }
    
    private void dumpEncapsHeader(OutputStream outputstream)
	throws IOException {
	DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
	dataoutputstream.writeBytes("MAGIC");
	String string;
	do {
	    string = in.readLine();
	    dataoutputstream.writeBytes(string);
	    dataoutputstream.writeBytes("\n");
	} while (!"ENDINFO".equals(string));
    }
}
