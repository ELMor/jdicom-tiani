/* JFilterPanel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.modalityscu;
import java.util.Enumeration;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;

public final class JFilterPanel extends JTabbedPane
{
    private DicomObject[] _filter;
    private static final String[] _TAB_LABEL
	= { "SPS", "PROCEDURE", "SERVICE REQUEST", "PATIENT", "VISIT" };
    private static final int[] _SPS_SQ_INIT_KEYS
	= { 81, 574, 575, 576, 579, 580, 582, 581 };
    private static final int[] _PROC_INIT_KEYS = { 425, 107, 589, 547, 548 };
    private static final int[] _SERVICE_INIT_KEYS = { 77, 540, 88 };
    private static final int[] _PAT_INIT_KEYS
	= { 147, 148, 150, 152, 609, 571, 174, 165, 166, 159, 568 };
    private static final int[] _VISIT_INIT_KEYS = { 554, 569, 110 };
    private static final int[][] _FILTER_INIT_KEYS
	= { _SPS_SQ_INIT_KEYS, _PROC_INIT_KEYS, _SERVICE_INIT_KEYS,
	    _PAT_INIT_KEYS, _VISIT_INIT_KEYS };
    private static final Tag[] _SPS_TAGS
	= IOD.accumulate(new Tag[][] { IOD.SPECIFIC_CHARACTER_SET,
				       IOD.SCHEDULED_PROCEDURE_STEP_MODULE });
    private static final Tag[] _PROC_TAGS = IOD.REQUESTED_PROCEDURE_MODULE;
    private static final Tag[] _SERVICE_TAGS
	= IOD.IMAGING_SERVICE_REQUEST_MODULE;
    private static final Tag[] _PAT_TAGS
	= IOD.accumulate(new Tag[][] { IOD.PATIENT_RELATIONSHIP_MODULE,
				       IOD.PATIENT_IDENTIFICATION_MODULE,
				       IOD.PATIENT_DEMOGRAPHIC_MODULE,
				       IOD.PATIENT_MEDICAL_MODULE });
    private static final Tag[] _VISIT_TAGS
	= IOD.accumulate(new Tag[][] { IOD.VISIT_RELATIONSHIP_MODULE,
				       IOD.VISIT_IDENTIFICATION_MODULE,
				       IOD.VISIT_STATUS_MODULE,
				       IOD.VISIT_ADMISSION_MODULE });
    private static final Tag[][] _FILTER_TAGS
	= { _SPS_TAGS, _PROC_TAGS, _SERVICE_TAGS, _PAT_TAGS, _VISIT_TAGS };
    
    public JFilterPanel() {
	_filter = new DicomObject[_TAB_LABEL.length];
	try {
	    for (int i = 0; i < _TAB_LABEL.length; i++) {
		_filter[i] = createFilter(i);
		DicomObjectTableModel dicomobjecttablemodel
		    = new DicomObjectTableModel(_filter[i], false, true, true);
		dicomobjecttablemodel.addTags(_FILTER_TAGS[i]);
		this.add(_TAB_LABEL[i],
			 new JScrollPane(new JSizeColumnsToFitTable
					 (dicomobjecttablemodel)));
	    }
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.getMessage());
	}
    }
    
    public DicomObject getFilter() throws DicomException {
	DicomObject dicomobject = new DicomObject();
	for (int i = 0; i < _TAB_LABEL.length; i++)
	    copyDicomObject(_filter[i], dicomobject);
	return dicomobject;
    }
    
    private static void copyDicomObject
	(DicomObject dicomobject, DicomObject dicomobject_0_)
	throws DicomException {
	Enumeration enumeration = dicomobject.enumerateVRs(false, false);
	while (enumeration.hasMoreElements())
	    copyTagValue((TagValue) enumeration.nextElement(), dicomobject_0_);
    }
    
    private static void copyTagValue
	(TagValue tagvalue, DicomObject dicomobject) throws DicomException {
	int i = tagvalue.getGroup();
	int i_1_ = tagvalue.getElement();
	dicomobject.deleteItem_ge(i, i_1_);
	int i_2_ = tagvalue.size();
	if (i_2_ > 0) {
	    for (int i_3_ = 0; i_3_ < i_2_; i_3_++)
		dicomobject.append_ge(i, i_1_, tagvalue.getValue(i_3_));
	} else
	    dicomobject.set_ge(i, i_1_, null);
    }
    
    private static DicomObject createFilter(int i) throws DicomException {
	DicomObject dicomobject = new DicomObject();
	int[] is = _FILTER_INIT_KEYS[i];
	for (int i_4_ = 0; i_4_ < is.length; i_4_++)
	    dicomobject.set(is[i_4_], null);
	if (i != 0)
	    return dicomobject;
	DicomObject dicomobject_5_ = new DicomObject();
	dicomobject_5_.set(587, dicomobject);
	return dicomobject_5_;
    }
}
