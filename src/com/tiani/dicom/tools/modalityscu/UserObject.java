/* UserObject - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.modalityscu;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

class UserObject
{
    public static final int[] WL_KEY = { 587, 582 };
    public static final int[][] WL_LABELS
	= { { 147 }, { 77 }, { 589 }, { 587, 582 } };
    public static final int[][] PPS_LABELS
	= { { 147 }, { 1210, 77 }, { 1210, 589 }, { 1206 } };
    private DicomObject _dataset;
    private int[][] _labels;
    
    public UserObject(DicomObject dicomobject, int[][] is)
	throws DicomException {
	_dataset = dicomobject;
	_labels = is;
    }
    
    public DicomObject getDataset() {
	return _dataset;
    }
    
    public void setDataset(DicomObject dicomobject) {
	_dataset = dicomobject;
    }
    
    public String toString() {
	StringBuffer stringbuffer = new StringBuffer();
	stringbuffer.append(getS(_dataset, _labels[0]));
	stringbuffer.append('|');
	stringbuffer.append(getS(_dataset, _labels[1]));
	stringbuffer.append('|');
	stringbuffer.append(getS(_dataset, _labels[2]));
	stringbuffer.append('|');
	stringbuffer.append(getS(_dataset, _labels[3]));
	return stringbuffer.toString();
    }
    
    public boolean equalsPatient(UserObject userobject_0_) {
	if (_dataset.getSize(148) > 0)
	    return _dataset.get(148).equals(userobject_0_._dataset.get(148));
	if (userobject_0_._dataset.getSize(148) > 0)
	    return false;
	if (_dataset.getSize(147) > 0)
	    return _dataset.get(147).equals(userobject_0_._dataset.get(147));
	return userobject_0_._dataset.getSize(148) <= 0;
    }
    
    public boolean equalsWLitem(UserObject userobject_1_)
	throws DicomException {
	Object object = get(_dataset, WL_KEY);
	return (object != null
		&& object.equals(get(userobject_1_._dataset, WL_KEY)));
    }
    
    private Object get(DicomObject dicomobject, int[] is)
	throws DicomException {
	Object object = dicomobject;
	for (int i = 0; object != null && i < is.length; i++)
	    object = ((DicomObject) object).get(is[i]);
	return object;
    }
    
    private String getS(DicomObject dicomobject, int[] is) {
	try {
	    return get(dicomobject, is).toString();
	} catch (Exception exception) {
	    return "";
	}
    }
}
