/* RawToDicom - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.util.UIDUtils;

public final class RawToDicom
{
    public static void main(String[] strings) {
	if (strings.length <= 4)
	    System.out.println
		("Usage: java com.tiani.tool.RawToDicom <dicomHeader> <inFile> <outFile> <LE|BE>\n                                      [(gggg,eeee)=val ..]\n  or   java com.tiani.tool.RawToDicom <dicomHeader> <inDir> <outDir> <LE|BE>\n                                      [(gggg,eeee)=val ..]\n\nExample:\n  java com.tiani.tool.RawToDicom img0.dcm img1.raw img1.dcm LE\n                     \"(0020,000D)=1.22.333\" \"(0020,000E)=1.22.333.4444\"\n  Supplements Raw Data img1.raw in little Endian Format with attributes taken\n  from DICOM file img0.dcm and explicit specified Study+Series Instance UIDs\n  to new DICOM file img1.dcm.\n");
	else {
	    try {
		DicomObject dicomobject = new DicomObject();
		TianiInputStream tianiinputstream
		    = new TianiInputStream(new FileInputStream(strings[0]));
		try {
		    tianiinputstream.read(dicomobject, false);
		} finally {
		    tianiinputstream.close();
		}
		boolean bool = strings[3].equalsIgnoreCase("BE");
		File file = new File(strings[1]);
		boolean bool_0_ = file.isDirectory();
		File file_1_ = new File(strings[2]);
		if (!bool_0_)
		    transfer(file, file_1_, dicomobject, UIDUtils.createUID(),
			     bool, strings);
		else {
		    if (!file_1_.isDirectory())
			throw new IOException("Error accesing directory "
					      + file_1_);
		    String[] strings_2_ = file.list();
		    for (int i = 0; i < strings_2_.length; i++)
			transfer(new File(file, strings_2_[i]),
				 new File(file_1_, strings_2_[i]), dicomobject,
				 UIDUtils.createUID(), bool, strings);
		}
	    } catch (Exception exception) {
		System.out.println(exception);
	    }
	}
    }
    
    private static void transfer
	(File file, File file_3_, DicomObject dicomobject, String string,
	 boolean bool, String[] strings)
	throws IOException, DicomException {
	byte[] is = new byte[(int) file.length()];
	FileInputStream fileinputstream = new FileInputStream(file);
	try {
	    fileinputstream.read(is);
	} finally {
	    fileinputstream.close();
	}
	if (bool)
	    swapBytes(is);
	dicomobject.set(63, string);
	dicomobject.set(1184, is);
	for (int i = 4; i < strings.length; i++) {
	    String string_4_ = strings[i];
	    dicomobject.set_ge(Integer.parseInt(string_4_.substring(1, 5), 16),
			       Integer.parseInt(string_4_.substring(6, 10),
						16),
			       string_4_.substring(12));
	}
	FileOutputStream fileoutputstream = new FileOutputStream(file_3_);
	try {
	    dicomobject.write(fileoutputstream,
			      dicomobject.getFileMetaInformation() != null);
	} finally {
	    fileoutputstream.close();
	}
	System.out.println("Create " + file_3_);
    }
    
    private static void swapBytes(byte[] is) {
	int i = is.length - 1;
	for (int i_5_ = 1; i_5_ < i; i_5_++) {
	    byte i_6_ = is[i_5_];
	    is[i_5_] = is[++i_5_];
	    is[i_5_] = i_6_;
	}
    }
}
