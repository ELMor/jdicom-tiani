/* ModifyDicomObject - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.legacy.TianiInputStream;

public final class ModifyDicomObject
{
    private static class MyTagValue extends TagValue
    {
	public MyTagValue(String string) {
	    StringTokenizer stringtokenizer
		= new StringTokenizer(string, ", \t\r\n\014");
	    group = Integer.parseInt(stringtokenizer.nextToken(), 16);
	    element = Integer.parseInt(stringtokenizer.nextToken(), 16);
	    while (stringtokenizer.hasMoreTokens())
		val.addElement(stringtokenizer.nextToken("\\"));
	}
    }
    
    public static void main(String[] strings) {
	if (strings.length != 3) {
	    System.out.println
		("Usage: ModifyDicomObject <inFile> <outFile> <attribFile>");
	    System.out.println
		("or     ModifyDicomObject <inDir> <outDir> <attribFile>");
	} else {
	    try {
		Vector vector = readAttribsFrom(strings[2]);
		File file = new File(strings[0]);
		boolean bool = file.isDirectory();
		File file_0_ = new File(strings[1]);
		if (!bool)
		    modify(file, file_0_, vector);
		else {
		    if (!file_0_.isDirectory())
			throw new IOException("Error accesing directory "
					      + file_0_);
		    String[] strings_1_ = file.list();
		    for (int i = 0; i < strings_1_.length; i++) {
			try {
			    modify(new File(file, strings_1_[i]),
				   new File(file_0_, strings_1_[i]), vector);
			} catch (Exception exception) {
			    System.out.println(exception);
			}
		    }
		}
	    } catch (Exception exception) {
		System.out.println(exception);
	    }
	}
    }
    
    private static Vector readAttribsFrom(String string) throws IOException {
	Vector vector = new Vector();
	BufferedReader bufferedreader
	    = (new BufferedReader
	       (new InputStreamReader(new FileInputStream(string))));
	try {
	    String string_2_;
	    while ((string_2_ = bufferedreader.readLine()) != null) {
		string_2_ = string_2_.trim();
		if (string_2_.length() > 0 && string_2_.charAt(0) != '#')
		    vector.addElement(new MyTagValue(string_2_));
	    }
	} finally {
	    bufferedreader.close();
	}
	return vector;
    }
    
    private static void modify(File file, File file_3_, Vector vector)
	throws IOException, DicomException {
	DicomObject dicomobject = new DicomObject();
	TianiInputStream tianiinputstream
	    = new TianiInputStream(new FileInputStream(file));
	try {
	    tianiinputstream.read(dicomobject, true);
	} finally {
	    tianiinputstream.close();
	}
	Enumeration enumeration = vector.elements();
	while (enumeration.hasMoreElements())
	    modify(dicomobject, (TagValue) enumeration.nextElement());
	FileOutputStream fileoutputstream = new FileOutputStream(file_3_);
	try {
	    dicomobject.write(fileoutputstream,
			      dicomobject.getFileMetaInformation() != null);
	} finally {
	    fileoutputstream.close();
	}
	System.out.print('.');
    }
    
    private static void modify(DicomObject dicomobject, TagValue tagvalue)
	throws DicomException {
	dicomobject.set_ge(tagvalue.getGroup(), tagvalue.getElement(),
			   tagvalue.getValue());
	for (int i = 1; i < tagvalue.size(); i++)
	    dicomobject.set_ge(tagvalue.getGroup(), tagvalue.getElement(),
			       tagvalue.getValue(i), i);
    }
}
