/* AETPermission - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public class AETPermission extends AET
{
    public boolean permit_store = false;
    public boolean permit_wl = false;
    public boolean permit_find = false;
    public boolean permit_move = false;
    
    public AETPermission(String string, String string_0_, int i) {
	super(string, string_0_, i);
    }
    
    public void setStore(boolean bool) {
	permit_store = bool;
    }
    
    public boolean isStore() {
	return permit_store;
    }
    
    public void setWL(boolean bool) {
	permit_wl = bool;
    }
    
    public boolean isWL() {
	return permit_wl;
    }
    
    public void setFind(boolean bool) {
	permit_find = bool;
    }
    
    public boolean isFind() {
	return permit_find;
    }
    
    public void setMove(boolean bool) {
	permit_move = bool;
    }
    
    public boolean isMove() {
	return permit_move;
    }
    
    public String toString() {
	return (this.getClass().getName() + "[title=" + title + ",host=" + host
		+ ",port=" + port + ",permit store=" + permit_store
		+ ",permit wl=" + permit_wl + ",permit find=" + permit_find
		+ ", permit_move=" + permit_move + "]");
    }
}
