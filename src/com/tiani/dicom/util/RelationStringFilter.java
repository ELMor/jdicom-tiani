/* RelationStringFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public class RelationStringFilter implements IStringFilter
{
    public static final int EQ = 0;
    public static final int GE = 1;
    public static final int GT = 2;
    public static final int LE = 3;
    public static final int LT = 4;
    public static final int NE = 5;
    private String _val;
    private int _rel;
    
    public RelationStringFilter(String string, int i) {
	_val = string;
	_rel = i;
    }
    
    public boolean accept(String string) {
	int i = string.compareTo(_val);
	switch (_rel) {
	case 0:
	    return i == 0;
	case 5:
	    return i != 0;
	case 4:
	    return i < 0;
	case 3:
	    return i <= 0;
	case 2:
	    return i > 0;
	case 1:
	    return i >= 0;
	default:
	    return false;
	}
    }
}
