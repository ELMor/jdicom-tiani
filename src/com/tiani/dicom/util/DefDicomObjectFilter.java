/* DefDicomObjectFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Enumeration;

import com.archimed.dicom.DDate;
import com.archimed.dicom.DDateRange;
import com.archimed.dicom.DDateTime;
import com.archimed.dicom.DDateTimeRange;
import com.archimed.dicom.DDict;
import com.archimed.dicom.DTime;
import com.archimed.dicom.DTimeRange;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;

public class DefDicomObjectFilter implements IDicomObjectFilter
{
    private int _dname;
    private IStringFilter _strFilter;
    
    public DefDicomObjectFilter(int i, IStringFilter istringfilter) {
	_dname = i;
	_strFilter = istringfilter;
    }
    
    public boolean accept(DicomObject dicomobject) throws DicomException {
	int i = dicomobject.getSize(_dname);
	if (i <= 0)
	    return true;
	for (int i_0_ = 0; i_0_ < i; i_0_++) {
	    if (_strFilter.accept(dicomobject.getS(_dname)))
		return true;
	}
	return false;
    }
    
    public static IDicomObjectFilter getSingleValueMatching(int i,
							    String string) {
	return new DefDicomObjectFilter(i,
					new RelationStringFilter(string, 0));
    }
    
    public static IDicomObjectFilter getWildCardMatching(int i,
							 String string) {
	if (string.indexOf('*') == -1 && string.indexOf('?') == -1)
	    return getSingleValueMatching(i, string);
	return new DefDicomObjectFilter(i, new WildcardStringFilter(string));
    }
    
    public static IDicomObjectFilter getDateRangeMatching
	(int i, DDateRange ddaterange) {
	DDate ddate = ddaterange.getDate1();
	DDate ddate_1_ = ddaterange.getDate2();
	if (ddate != null && ddate_1_ != null)
	    return (new DefDicomObjectFilter
		    (i, (new AndStringFilter
			 (new IStringFilter[]
			  { new RelationStringFilter(ddate.toDICOMString(), 1),
			    new RelationStringFilter(ddate_1_.toDICOMString(),
						     3) }))));
	if (ddate != null)
	    return (new DefDicomObjectFilter
		    (i, new RelationStringFilter(ddate.toDICOMString(), 1)));
	return (new DefDicomObjectFilter
		(i, new RelationStringFilter(ddate_1_.toDICOMString(), 3)));
    }
    
    public static IDicomObjectFilter getDateTimeRangeMatching
	(int i, DDateTimeRange ddatetimerange) {
	DDateTime ddatetime = ddatetimerange.getDateTime1();
	DDateTime ddatetime_2_ = ddatetimerange.getDateTime2();
	if (ddatetime != null && ddatetime_2_ != null)
	    return (new DefDicomObjectFilter
		    (i,
		     (new AndStringFilter
		      (new IStringFilter[]
		       { new RelationStringFilter(ddatetime.toDICOMString(),
						  1),
			 new RelationStringFilter(ddatetime_2_.toDICOMString(),
						  3) }))));
	if (ddatetime != null)
	    return (new DefDicomObjectFilter
		    (i,
		     new RelationStringFilter(ddatetime.toDICOMString(), 1)));
	return (new DefDicomObjectFilter
		(i,
		 new RelationStringFilter(ddatetime_2_.toDICOMString(), 3)));
    }
    
    public static IDicomObjectFilter getTimeRangeMatching
	(int i, DTimeRange dtimerange) {
	DTime dtime = dtimerange.getTime1();
	DTime dtime_3_ = dtimerange.getTime2();
	if (dtime != null && dtime_3_ != null)
	    return (new DefDicomObjectFilter
		    (i, (new AndStringFilter
			 (new IStringFilter[]
			  { new RelationStringFilter(dtime.toDICOMString(), 1),
			    new RelationStringFilter(dtime_3_.toDICOMString(),
						     3) }))));
	if (dtime != null)
	    return (new DefDicomObjectFilter
		    (i, new RelationStringFilter(dtime.toDICOMString(), 1)));
	return (new DefDicomObjectFilter
		(i, new RelationStringFilter(dtime_3_.toDICOMString(), 3)));
    }
    
    public static DicomObject createReturn
	(DicomObject dicomobject, DicomObject dicomobject_4_)
	throws DicomException {
	return createReturn(dicomobject, new DicomObject[] { dicomobject_4_ });
    }
    
    public static DicomObject createReturn
	(DicomObject dicomobject, DicomObject[] dicomobjects)
	throws DicomException {
	DicomObject dicomobject_5_ = new DicomObject();
	Enumeration enumeration = dicomobject.enumerateVRs(false, false);
	while (enumeration.hasMoreElements()) {
	    TagValue tagvalue = (TagValue) enumeration.nextElement();
	    int i = tagvalue.getGroup();
	    int i_6_ = tagvalue.getElement();
	    dicomobject_5_.set_ge(i, i_6_, null);
	    DicomObject dicomobject_7_
		= (tagvalue.size() > 0 && DDict.getTypeCode(i, i_6_) == 10
		   ? (DicomObject) tagvalue.getValue(0) : null);
	    for (int i_8_ = 0; i_8_ < dicomobjects.length; i_8_++) {
		DicomObject dicomobject_9_ = dicomobjects[i_8_];
		int i_10_ = dicomobject_9_.getSize_ge(i, i_6_);
		for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
		    Object object = dicomobject_9_.get_ge(i, i_6_, i_11_);
		    dicomobject_5_.append_ge
			(i, i_6_,
			 (dicomobject_7_ != null
			  ? (Object) createReturn(dicomobject_7_,
						  (DicomObject) object)
			  : object));
		}
		if (dicomobject_9_.getSize(57) == 1)
		    dicomobject_5_.set(57, dicomobject_9_.get(57));
	    }
	}
	return dicomobject_5_;
    }
}
