/* ByteRampFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public final class ByteRampFactory
{
    private static final int CACHE_CAPACITY = 4;
    private static byte[][] _cache = new byte[4][];
    private static int _writePos = 0;
    
    public static void init(byte[] is, int i, int i_0_) {
	int i_1_ = i_0_ - i;
	int i_2_ = is.length - 1;
	if (i_2_ == 0)
	    is[0] = (byte) ((i + i_0_) / 2);
	else if (i_1_ > 0) {
	    for (int i_3_ = 0; i_3_ <= i_2_; i_3_++)
		is[i_3_] = (byte) (i + (i_1_ * i_3_ + (i_3_ >> 1)) / i_2_);
	} else {
	    for (int i_4_ = 0; i_4_ <= i_2_; i_4_++)
		is[i_4_] = (byte) (i + (i_1_ * i_4_ - (i_4_ >> 1)) / i_2_);
	}
    }
    
    public static byte[] create(int i, int i_5_, int i_6_) {
	byte[] is = lookupCache(i, i_5_, i_6_);
	if (is != null)
	    return is;
	is = new byte[i];
	init(is, i_5_, i_6_);
	updateCache(is);
	return is;
    }
    
    public static void setCacheCapacity(int i) {
	if (i != _cache.length) {
	    byte[][] is = new byte[i][];
	    System.arraycopy(_cache, 0, is, 0, Math.min(_cache.length, i));
	    _cache = is;
	    _writePos = Math.min(_writePos, i - 1);
	}
    }
    
    private static void updateCache(byte[] is) {
	_cache[_writePos++] = is;
	_writePos %= _cache.length;
    }
    
    private static byte[] lookupCache(int i, int i_7_, int i_8_) {
	for (int i_9_ = 0; i_9_ < _cache.length; i_9_++) {
	    byte[] is = _cache[i_9_];
	    if (is != null && is.length == i && (is[0] & 0xff) == i_7_
		&& (is[i - 1] & 0xff) == i_8_) {
		_cache[i_9_] = _cache[_writePos];
		updateCache(is);
		return is;
	    }
	}
	return null;
    }
}
