/* SeqDicomObjectFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class SeqDicomObjectFilter implements IDicomObjectFilter
{
    private int _dname;
    private IDicomObjectFilter _seqFilter;
    
    public SeqDicomObjectFilter(int i, IDicomObjectFilter idicomobjectfilter) {
	_dname = i;
	_seqFilter = idicomobjectfilter;
    }
    
    public boolean accept(DicomObject dicomobject) throws DicomException {
	int i = dicomobject.getSize(_dname);
	for (int i_0_ = 0; i_0_ < i; i_0_++) {
	    if (_seqFilter.accept((DicomObject) dicomobject.get(_dname)))
		return true;
	}
	return false;
    }
}
