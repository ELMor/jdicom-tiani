/* IOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Vector;

public class IOD
{
    public static final Tag[] FILE_META_INFO
	= { new Tag(2, 1), new Tag(2, 2), new Tag(2, 3), new Tag(2, 16),
	    new Tag(2, 18), new Tag(2, 19), new Tag(2, 22), new Tag(2, 256),
	    new Tag(2, 258) };
    private static final Tag[] REFERENCED_SOP_SEQUENCE
	= { new Tag(8, 4432), new Tag(8, 4437) };
    private static final Tag[] BASE_CODE_SEQUENCE
	= { new Tag(8, 256), new Tag(8, 258), new Tag(8, 259),
	    new Tag(8, 260) };
    public static final Tag[] PATIENT_RELATIONSHIP_MODULE
	= { new Tag(8, 4368, REFERENCED_SOP_SEQUENCE),
	    new Tag(8, 4389, REFERENCED_SOP_SEQUENCE),
	    new Tag(56, 4, REFERENCED_SOP_SEQUENCE) };
    public static final Tag[] PATIENT_IDENTIFICATION_MODULE
	= { new Tag(16, 16), new Tag(16, 32), new Tag(16, 33),
	    new Tag(16, 4096), new Tag(16, 4097), new Tag(16, 4101),
	    new Tag(16, 4192), new Tag(16, 4240) };
    public static final Tag[] PATIENT_DEMOGRAPHIC_MODULE
	= { new Tag(16, 4112), new Tag(16, 48), new Tag(16, 50),
	    new Tag(16, 64), new Tag(16, 80, BASE_CODE_SEQUENCE),
	    new Tag(16, 4128), new Tag(16, 4144), new Tag(16, 4160),
	    new Tag(16, 4224), new Tag(16, 4225), new Tag(16, 8528),
	    new Tag(16, 8530), new Tag(16, 8532), new Tag(16, 8544),
	    new Tag(16, 8576), new Tag(16, 8688), new Tag(16, 16384),
	    new Tag(64, 12289) };
    public static final Tag[] PATIENT_MEDICAL_MODULE
	= { new Tag(16, 8192), new Tag(16, 8464), new Tag(16, 8608),
	    new Tag(16, 8624), new Tag(16, 8640), new Tag(16, 8656),
	    new Tag(56, 80), new Tag(56, 1280) };
    public static final Tag[] VISIT_RELATIONSHIP_MODULE
	= { new Tag(8, 4368, REFERENCED_SOP_SEQUENCE),
	    new Tag(8, 4384, REFERENCED_SOP_SEQUENCE) };
    public static final Tag[] VISIT_IDENTIFICATION_MODULE
	= { new Tag(8, 128), new Tag(8, 129),
	    new Tag(8, 130, BASE_CODE_SEQUENCE), new Tag(56, 16),
	    new Tag(56, 17) };
    public static final Tag[] VISIT_STATUS_MODULE
	= { new Tag(56, 8), new Tag(56, 768), new Tag(56, 1024),
	    new Tag(56, 16384) };
    public static final Tag[] VISIT_ADMISSION_MODULE
	= { new Tag(8, 144), new Tag(8, 146), new Tag(8, 148),
	    new Tag(8, 4224), new Tag(8, 4228, BASE_CODE_SEQUENCE),
	    new Tag(56, 22), new Tag(56, 32), new Tag(56, 33) };
    public static final Tag[] SCHEDULED_PROCEDURE_STEP_SEQUENCE
	= { new Tag(8, 96), new Tag(50, 4208), new Tag(64, 1), new Tag(64, 2),
	    new Tag(64, 3), new Tag(64, 4), new Tag(64, 5), new Tag(64, 6),
	    new Tag(64, 7), new Tag(64, 8, BASE_CODE_SEQUENCE), new Tag(64, 9),
	    new Tag(64, 16), new Tag(64, 17), new Tag(64, 18), new Tag(64, 32),
	    new Tag(64, 1024) };
    public static final Tag[] SCHEDULED_PROCEDURE_STEP_MODULE
	= { new Tag(64, 256, SCHEDULED_PROCEDURE_STEP_SEQUENCE) };
    public static final Tag[] REQUESTED_PROCEDURE_MODULE
	= { new Tag(8, 4368, REFERENCED_SOP_SEQUENCE), new Tag(32, 13),
	    new Tag(32, 13), new Tag(50, 10), new Tag(50, 4192),
	    new Tag(50, 4196, BASE_CODE_SEQUENCE), new Tag(64, 4097),
	    new Tag(64, 4098), new Tag(64, 4099), new Tag(64, 4100),
	    new Tag(64, 4101), new Tag(64, 4104), new Tag(64, 4105),
	    new Tag(64, 4112), new Tag(64, 5120) };
    public static final Tag[] IMAGING_SERVICE_REQUEST_MODULE
	= { new Tag(8, 80), new Tag(8, 144), new Tag(50, 4146),
	    new Tag(50, 4147), new Tag(64, 8193), new Tag(64, 8196),
	    new Tag(64, 8197), new Tag(64, 8200), new Tag(64, 8201),
	    new Tag(64, 8208), new Tag(64, 8214), new Tag(64, 8215),
	    new Tag(64, 9216) };
    public static final Tag[] SCHEDULED_STEP_ATTRIBUTE_SEQUENCE
	= { new Tag(8, 80), new Tag(8, 4368, REFERENCED_SOP_SEQUENCE),
	    new Tag(32, 13), new Tag(50, 4192), new Tag(64, 7),
	    new Tag(64, 8, BASE_CODE_SEQUENCE), new Tag(64, 9),
	    new Tag(64, 4097), new Tag(64, 8214), new Tag(64, 8215) };
    public static final Tag[] PPS_RELATIONSHIP_MODULE
	= { new Tag(8, 4384, REFERENCED_SOP_SEQUENCE), new Tag(16, 16),
	    new Tag(16, 32), new Tag(16, 48), new Tag(16, 64),
	    new Tag(64, 624, SCHEDULED_STEP_ATTRIBUTE_SEQUENCE) };
    public static final Tag[] PPS_INFORMATION_MODULE
	= { new Tag(8, 4146, BASE_CODE_SEQUENCE), new Tag(64, 577),
	    new Tag(64, 578), new Tag(64, 579), new Tag(64, 580),
	    new Tag(64, 581), new Tag(64, 592), new Tag(64, 593),
	    new Tag(64, 594), new Tag(64, 595), new Tag(64, 596),
	    new Tag(64, 597), new Tag(64, 640) };
    public static final Tag[] PERFORMED_SERIES_SEQUENCE
	= { new Tag(8, 84), new Tag(8, 4158), new Tag(8, 4176),
	    new Tag(8, 4208), new Tag(8, 4416, REFERENCED_SOP_SEQUENCE),
	    new Tag(24, 4144), new Tag(32, 14),
	    new Tag(64, 544, REFERENCED_SOP_SEQUENCE) };
    public static final Tag[] IMAGE_ACQUISITION_RESULTS_MODULE
	= { new Tag(8, 96), new Tag(32, 16),
	    new Tag(64, 608, BASE_CODE_SEQUENCE),
	    new Tag(64, 832, PERFORMED_SERIES_SEQUENCE) };
    public static final Tag[] RADIATION_DOSE_MODULE
	= { new Tag(8, 8745, BASE_CODE_SEQUENCE), new Tag(24, 4368),
	    new Tag(24, 4446), new Tag(64, 768), new Tag(64, 769),
	    new Tag(64, 770), new Tag(64, 771), new Tag(64, 774),
	    new Tag(64, 784) };
    public static final Tag[] FILM_CONSUMPTION_SEQUENCE
	= { new Tag(8192, 48), new Tag(8208, 80), new Tag(8448, 368) };
    public static final Tag[] QUANTITY_SEQUENCE_SEQUENCE
	= { new Tag(64, 660), new Tag(64, 661, BASE_CODE_SEQUENCE) };
    public static final Tag[] BILLING_SUPPLIES_AND_DEVICES_SEQUENCE
	= { new Tag(64, 659, QUANTITY_SEQUENCE_SEQUENCE),
	    new Tag(64, 662, BASE_CODE_SEQUENCE) };
    public static final Tag[] BILLING_AND_MATERIAL_MANAGEMENT_CODE_MODULE
	= { new Tag(64, 800, BASE_CODE_SEQUENCE),
	    new Tag(64, 801, FILM_CONSUMPTION_SEQUENCE),
	    new Tag(64, 804, BILLING_SUPPLIES_AND_DEVICES_SEQUENCE) };
    public static final Tag[] REFERENCED_IMAGE_SEQUENCE
	= { new Tag(8, 4432), new Tag(8, 4437), new Tag(8, 4448) };
    public static final Tag[] ENHANCED_CODE_SEQUENCE
	= { new Tag(8, 256), new Tag(8, 258), new Tag(8, 259), new Tag(8, 260),
	    new Tag(8, 261), new Tag(8, 262), new Tag(8, 263), new Tag(8, 267),
	    new Tag(8, 268), new Tag(8, 269), new Tag(8, 271) };
    public static final Tag[] PATIENT_MODULE
	= { new Tag(8, 4384, REFERENCED_SOP_SEQUENCE), new Tag(16, 16),
	    new Tag(16, 32), new Tag(16, 48), new Tag(16, 50), new Tag(16, 64),
	    new Tag(16, 4096), new Tag(16, 4097), new Tag(16, 8544),
	    new Tag(16, 16384) };
    public static final Tag[] SPECIMEN_SEQUENCE
	= { new Tag(64, 1361), new Tag(64, 1434, BASE_CODE_SEQUENCE),
	    new Tag(64, 1786) };
    public static final Tag[] SPECIMEN_IDENTIFICATION_MODULE
	= { new Tag(64, 1290), new Tag(64, 1360, SPECIMEN_SEQUENCE) };
    public static final Tag[] GENERAL_STUDY_MODULE
	= { new Tag(8, 32), new Tag(8, 48), new Tag(8, 80), new Tag(8, 144),
	    new Tag(8, 4144), new Tag(8, 4146, BASE_CODE_SEQUENCE),
	    new Tag(8, 4168), new Tag(8, 4192),
	    new Tag(8, 4368, REFERENCED_SOP_SEQUENCE), new Tag(32, 13),
	    new Tag(32, 16) };
    public static final Tag[] PATIENT_STUDY_MODULE
	= { new Tag(8, 4224), new Tag(16, 4112), new Tag(16, 4128),
	    new Tag(16, 4144), new Tag(16, 8576), new Tag(16, 8624) };
    public static final Tag[] REQUEST_ATTRIBUTES_SEQUENCE
	= { new Tag(64, 4097), new Tag(64, 9), new Tag(64, 7),
	    new Tag(64, 8, BASE_CODE_SEQUENCE) };
    public static final Tag[] FRAME_OF_REFERENCE_MODULE
	= { new Tag(32, 82), new Tag(32, 4160) };
    public static final Tag[] IMAGE_PLANE_MODULE
	= { new Tag(24, 80), new Tag(32, 50), new Tag(32, 55),
	    new Tag(32, 4161), new Tag(40, 48) };
    public static final Tag[] CONTRAST_BOLUS_ADMINISTRATION_ROUTE_SEQUENCE
	= { new Tag(8, 256), new Tag(8, 258), new Tag(8, 259), new Tag(8, 260),
	    new Tag(24, 20, BASE_CODE_SEQUENCE) };
    public static final Tag[] CONTRAST_BOLUS_MODULE
	= { new Tag(24, 16), new Tag(24, 18, BASE_CODE_SEQUENCE),
	    new Tag(24, 20, CONTRAST_BOLUS_ADMINISTRATION_ROUTE_SEQUENCE),
	    new Tag(24, 4160), new Tag(24, 4161), new Tag(24, 4162),
	    new Tag(24, 4163), new Tag(24, 4164) };
    public static final Tag[] GENERAL_SERIES_MODULE
	= { new Tag(8, 96), new Tag(8, 33), new Tag(8, 49), new Tag(8, 4158),
	    new Tag(8, 4176), new Tag(8, 4208),
	    new Tag(8, 4369, REFERENCED_SOP_SEQUENCE), new Tag(24, 21),
	    new Tag(24, 4144), new Tag(24, 20736), new Tag(32, 14),
	    new Tag(32, 17), new Tag(32, 96), new Tag(40, 264),
	    new Tag(40, 265), new Tag(64, 580), new Tag(64, 581),
	    new Tag(64, 596), new Tag(64, 608, BASE_CODE_SEQUENCE),
	    new Tag(64, 629, REQUEST_ATTRIBUTES_SEQUENCE) };
    public static final Tag[] GENERAL_EQUIPMENT_MODULE
	= { new Tag(8, 112), new Tag(8, 128), new Tag(8, 129),
	    new Tag(8, 4112), new Tag(8, 4160), new Tag(8, 4240),
	    new Tag(24, 4096), new Tag(24, 4128), new Tag(24, 4176),
	    new Tag(24, 4608), new Tag(24, 4609), new Tag(40, 288) };
    public static final Tag[] MASK_SUBTRACTION_SEQUENCE
	= { new Tag(40, 24833), new Tag(40, 24834), new Tag(40, 24848),
	    new Tag(40, 24850), new Tag(40, 24852), new Tag(40, 24864),
	    new Tag(40, 24976) };
    public static final Tag[] MASK_MODULE
	= { new Tag(40, 4240), new Tag(40, 24832, MASK_SUBTRACTION_SEQUENCE) };
    public static final Tag[] DISPLAY_SHUTTER_MODULE
	= { new Tag(24, 5632), new Tag(24, 5634), new Tag(24, 5636),
	    new Tag(24, 5638), new Tag(24, 5640), new Tag(24, 5648),
	    new Tag(24, 5650), new Tag(24, 5664), new Tag(24, 5666) };
    public static final Tag[] IMAGE_PIXEL_MODULE
	= { new Tag(40, 2), new Tag(40, 4), new Tag(40, 6), new Tag(40, 16),
	    new Tag(40, 17), new Tag(40, 52), new Tag(40, 256),
	    new Tag(40, 258), new Tag(40, 257), new Tag(40, 259),
	    new Tag(40, 262), new Tag(40, 263), new Tag(40, 4353),
	    new Tag(40, 4354), new Tag(40, 4355), new Tag(40, 4609),
	    new Tag(40, 4610), new Tag(40, 4611), new Tag(32736, 16) };
    public static final Tag[] MULTIFRAME_MODULE
	= { new Tag(40, 8), new Tag(40, 9) };
    public static final Tag[] GENERAL_IMAGE_MODULE
	= { new Tag(8, 8), new Tag(8, 34), new Tag(8, 35), new Tag(8, 50),
	    new Tag(8, 51), new Tag(8, 4416, REFERENCED_IMAGE_SEQUENCE),
	    new Tag(8, 8465), new Tag(8, 8466, REFERENCED_IMAGE_SEQUENCE),
	    new Tag(32, 18), new Tag(32, 19), new Tag(32, 32),
	    new Tag(32, 4098), new Tag(32, 16384), new Tag(40, 768),
	    new Tag(40, 769), new Tag(40, 8464), new Tag(40, 8466),
	    new Tag(136, 512, IMAGE_PIXEL_MODULE) };
    public static final Tag[] CT_IMAGE_MODULE
	= { new Tag(24, 34), new Tag(24, 96), new Tag(24, 144),
	    new Tag(24, 4352), new Tag(24, 4368), new Tag(24, 4369),
	    new Tag(24, 4384), new Tag(24, 4400), new Tag(24, 4416),
	    new Tag(24, 4432), new Tag(24, 4433), new Tag(24, 4434),
	    new Tag(24, 4448), new Tag(24, 4464), new Tag(24, 4496),
	    new Tag(24, 4624), new Tag(32, 18), new Tag(40, 4178),
	    new Tag(40, 4179) };
    public static final Tag[] MR_IMAGE_MODULE
	= { new Tag(24, 32), new Tag(24, 33), new Tag(24, 34), new Tag(24, 35),
	    new Tag(24, 36), new Tag(24, 37), new Tag(24, 128),
	    new Tag(24, 129), new Tag(24, 131), new Tag(24, 132),
	    new Tag(24, 133), new Tag(24, 134), new Tag(24, 135),
	    new Tag(24, 136), new Tag(24, 137), new Tag(24, 145),
	    new Tag(24, 146), new Tag(24, 147), new Tag(24, 148),
	    new Tag(24, 149), new Tag(24, 4192), new Tag(24, 4194),
	    new Tag(24, 4224), new Tag(24, 4225), new Tag(24, 4226),
	    new Tag(24, 4227), new Tag(24, 4228), new Tag(24, 4229),
	    new Tag(24, 4230), new Tag(24, 4232), new Tag(24, 4240),
	    new Tag(24, 4244), new Tag(24, 4352), new Tag(24, 4688),
	    new Tag(24, 4689), new Tag(24, 4880), new Tag(24, 4882),
	    new Tag(24, 4884), new Tag(24, 4885), new Tag(24, 4886),
	    new Tag(24, 4888), new Tag(32, 256), new Tag(32, 261),
	    new Tag(32, 272) };
    public static final Tag[] PATIENT_ORIENTATION_CODE_SEQUENCE
	= { new Tag(8, 256), new Tag(8, 258), new Tag(8, 259), new Tag(8, 260),
	    new Tag(84, 1042, BASE_CODE_SEQUENCE) };
    public static final Tag[] NM_PET_PATIENT_ORIENTATION_MODULE
	= { new Tag(84, 1040, PATIENT_ORIENTATION_CODE_SEQUENCE),
	    new Tag(84, 1044, BASE_CODE_SEQUENCE) };
    public static final Tag[] NM_IMAGE_PIXEL_MODULE
	= { new Tag(40, 2), new Tag(40, 4), new Tag(40, 48), new Tag(40, 256),
	    new Tag(40, 257), new Tag(40, 258) };
    public static final Tag[] NM_MULTIFRAME_MODULE
	= { new Tag(40, 9), new Tag(84, 16), new Tag(84, 17), new Tag(84, 32),
	    new Tag(84, 33), new Tag(84, 48), new Tag(84, 49), new Tag(84, 80),
	    new Tag(84, 81), new Tag(84, 96), new Tag(84, 97),
	    new Tag(84, 112), new Tag(84, 113), new Tag(84, 128),
	    new Tag(84, 129), new Tag(84, 144), new Tag(84, 256) };
    public static final Tag[] ANATOMIC_REGION_SEQUENCE
	= { new Tag(8, 256), new Tag(8, 258), new Tag(8, 259), new Tag(8, 260),
	    new Tag(8, 8736, BASE_CODE_SEQUENCE) };
    public static final Tag[] PRIMARY_ANATOMIC_STRUCTURE_SEQUENCE
	= { new Tag(8, 256), new Tag(8, 258), new Tag(8, 259), new Tag(8, 260),
	    new Tag(8, 8752, BASE_CODE_SEQUENCE) };
    public static final Tag[] NM_IMAGE_MODULE
	= { new Tag(8, 8), new Tag(8, 4400, REFERENCED_SOP_SEQUENCE),
	    new Tag(8, 4421, REFERENCED_SOP_SEQUENCE),
	    new Tag(8, 8728, ANATOMIC_REGION_SEQUENCE),
	    new Tag(8, 8744, PRIMARY_ANATOMIC_STRUCTURE_SEQUENCE),
	    new Tag(24, 112), new Tag(24, 113), new Tag(24, 4193),
	    new Tag(24, 4400), new Tag(24, 4401), new Tag(24, 4674),
	    new Tag(24, 4675), new Tag(24, 4864), new Tag(24, 4865),
	    new Tag(24, 4866), new Tag(24, 20512), new Tag(40, 81),
	    new Tag(40, 8464), new Tag(84, 1024) };
    public static final Tag[] ENERGY_WINDOW_RANGE_SEQUENCE
	= { new Tag(84, 20), new Tag(84, 21) };
    public static final Tag[] ENERGY_WINDOW_INFORMATION_SEQUENCE
	= { new Tag(84, 19, ENERGY_WINDOW_RANGE_SEQUENCE), new Tag(84, 24) };
    public static final Tag[] CALIBRATION_DATA_SEQUENCE
	= { new Tag(24, 4165), new Tag(84, 23), new Tag(84, 776) };
    public static final Tag[] RADIOPHARMACEUTICAL_INFORMATION_SEQUENCE
	= { new Tag(24, 49), new Tag(24, 4208), new Tag(24, 4209),
	    new Tag(24, 4210), new Tag(24, 4211), new Tag(24, 4212),
	    new Tag(84, 768, BASE_CODE_SEQUENCE),
	    new Tag(84, 770, BASE_CODE_SEQUENCE),
	    new Tag(84, 772, BASE_CODE_SEQUENCE),
	    new Tag(84, 774, CALIBRATION_DATA_SEQUENCE) };
    public static final Tag[] INTERVENTION_DRUG_INFORMATION_SEQUENCE
	= { new Tag(24, 38), new Tag(24, 39), new Tag(24, 40),
	    new Tag(24, 41, BASE_CODE_SEQUENCE), new Tag(24, 52),
	    new Tag(24, 53), new Tag(84, 770, BASE_CODE_SEQUENCE) };
    public static final Tag[] NM_ISOTOPE_MODULE
	= { new Tag(24, 38, INTERVENTION_DRUG_INFORMATION_SEQUENCE),
	    new Tag(84, 18, ENERGY_WINDOW_INFORMATION_SEQUENCE),
	    new Tag(84, 22, RADIOPHARMACEUTICAL_INFORMATION_SEQUENCE) };
    public static final Tag[] VIEW_CODE_SEQUENCE
	= { new Tag(8, 256), new Tag(8, 258), new Tag(8, 259), new Tag(8, 260),
	    new Tag(84, 546, BASE_CODE_SEQUENCE) };
    public static final Tag[] DETECTOR_INFORMATION_INFORMATION_SEQUENCE
	= { new Tag(24, 4368), new Tag(24, 4384), new Tag(24, 4418),
	    new Tag(24, 4421), new Tag(24, 4423), new Tag(24, 4425),
	    new Tag(24, 4480), new Tag(24, 4481), new Tag(24, 4482),
	    new Tag(24, 4483), new Tag(24, 4484), new Tag(32, 50),
	    new Tag(32, 55), new Tag(40, 49), new Tag(40, 50),
	    new Tag(84, 512), new Tag(84, 544, VIEW_CODE_SEQUENCE) };
    public static final Tag[] NM_DETECTOR_MODULE
	= { new Tag(84, 34, DETECTOR_INFORMATION_INFORMATION_SEQUENCE) };
    public static final Tag[] ROTATION_INFORMATION_SEQUENCE
	= { new Tag(24, 4368), new Tag(24, 4400), new Tag(24, 4401),
	    new Tag(24, 4416), new Tag(24, 4418), new Tag(24, 4419),
	    new Tag(24, 4420), new Tag(24, 4674), new Tag(84, 83),
	    new Tag(84, 512) };
    public static final Tag[] NM_TOMO_ACQUISITION_MODULE
	= { new Tag(84, 82, ROTATION_INFORMATION_SEQUENCE), new Tag(84, 514) };
    public static final Tag[] TIME_SLOT_INFORMATION_SEQUENCE
	= { new Tag(84, 115) };
    public static final Tag[] DATA_INFORMATION_SEQUENCE
	= { new Tag(24, 4194), new Tag(24, 4195), new Tag(24, 4225),
	    new Tag(24, 4226), new Tag(24, 4227), new Tag(24, 4228),
	    new Tag(84, 114, TIME_SLOT_INFORMATION_SEQUENCE) };
    public static final Tag[] GATED_INFORMATION_SEQUENCE
	= { new Tag(24, 4192), new Tag(24, 4196),
	    new Tag(84, 99, DATA_INFORMATION_SEQUENCE) };
    public static final Tag[] NM_MULTIGATED_ACQUISITION_MODULE
	= { new Tag(24, 4224), new Tag(24, 4229), new Tag(24, 4230),
	    new Tag(24, 4232), new Tag(84, 98, GATED_INFORMATION_SEQUENCE) };
    public static final Tag[] PHASE_INFORMATION_INFORMATION_SEQUENCE
	= { new Tag(24, 4674), new Tag(84, 51), new Tag(84, 54),
	    new Tag(84, 56), new Tag(84, 528), new Tag(84, 529) };
    public static final Tag[] NM_PHASE_MODULE
	= { new Tag(84, 50, PHASE_INFORMATION_INFORMATION_SEQUENCE) };
    public static final Tag[] NM_RECONSTRUCTION_MODULE
	= { new Tag(24, 80), new Tag(24, 136), new Tag(24, 4352),
	    new Tag(24, 4624), new Tag(32, 4161) };
    public static final Tag[] OVERLAY_PLANE_MODULE
	= { new Tag(24576, 16), new Tag(24576, 17), new Tag(24576, 34),
	    new Tag(24576, 64), new Tag(24576, 69), new Tag(24576, 80),
	    new Tag(24576, 256), new Tag(24576, 258), new Tag(24576, 4865),
	    new Tag(24576, 4866), new Tag(24576, 4867), new Tag(24576, 5376),
	    new Tag(24576, 12288) };
    public static final Tag[] MULTIFRAME_OVERLAY_MODULE
	= { new Tag(24576, 21), new Tag(24576, 81) };
    public static final Tag[] REFERENCED_OVERLAY_SEQUENCE
	= { new Tag(8, 4432), new Tag(8, 4437), new Tag(20480, 9744) };
    public static final Tag[] CURVE_MODULE
	= { new Tag(20480, 5), new Tag(20480, 16), new Tag(20480, 32),
	    new Tag(20480, 34), new Tag(20480, 48), new Tag(20480, 64),
	    new Tag(20480, 259), new Tag(20480, 260), new Tag(20480, 261),
	    new Tag(20480, 262), new Tag(20480, 272), new Tag(20480, 274),
	    new Tag(20480, 276), new Tag(20480, 9472),
	    new Tag(20480, 9728, REFERENCED_OVERLAY_SEQUENCE),
	    new Tag(20480, 12288) };
    public static final Tag[] OVERLAY_CURVE_ACTIVATION_MODULE
	= { new Tag(20480, 4097), new Tag(24576, 4097) };
    public static final Tag[] MODALITY_LUT_SEQUENCE
	= { new Tag(40, 12290), new Tag(40, 12291), new Tag(40, 12292),
	    new Tag(40, 12294) };
    public static final Tag[] MODALITY_LUT_MODULE
	= { new Tag(40, 4178), new Tag(40, 4179), new Tag(40, 4180),
	    new Tag(40, 12288, MODALITY_LUT_SEQUENCE) };
    public static final Tag[] LUT_SEQUENCE
	= { new Tag(40, 12290), new Tag(40, 12291), new Tag(40, 12294) };
    public static final Tag[] VOI_LUT_MODULE
	= { new Tag(40, 4176), new Tag(40, 4177), new Tag(40, 4181),
	    new Tag(40, 12304, LUT_SEQUENCE) };
    public static final Tag[] SOFTCOPY_PRESENTATION_LUT_MODULE
	= { new Tag(8272, 16, LUT_SEQUENCE), new Tag(8272, 32) };
    public static final Tag[] SOFTCOPY_VOI_LUT_SEQUENCE
	= { new Tag(8, 4416, REFERENCED_IMAGE_SEQUENCE), new Tag(40, 4176),
	    new Tag(40, 4177), new Tag(40, 4181),
	    new Tag(40, 12304, LUT_SEQUENCE) };
    public static final Tag[] SOFTCOPY_VOI_LUT_MODULE
	= { new Tag(40, 12560, SOFTCOPY_VOI_LUT_SEQUENCE) };
    public static final Tag[] COMMON_SOP_MODULE
	= { new Tag(8, 5), new Tag(8, 18), new Tag(8, 19), new Tag(8, 20),
	    new Tag(8, 22), new Tag(8, 24), new Tag(32, 19) };
    public static final Tag[] PATIENT_SUMMARY_MODULE
	= { new Tag(16, 16), new Tag(16, 32) };
    public static final Tag[] SC_REFERENCED_IMAGE_SEQUENCE
	= { new Tag(8, 84), new Tag(8, 4432), new Tag(8, 4437),
	    new Tag(136, 304), new Tag(136, 320) };
    public static final Tag[] SC_REFERENCED_SERIES_SEQUENCE
	= { new Tag(8, 84), new Tag(8, 4416, SC_REFERENCED_IMAGE_SEQUENCE),
	    new Tag(32, 14), new Tag(136, 304), new Tag(136, 320) };
    public static final Tag[] STUDY_CONTENT_MODULE
	= { new Tag(8, 4373, SC_REFERENCED_SERIES_SEQUENCE), new Tag(32, 13),
	    new Tag(32, 16) };
    public static final Tag[] BM_DISPLAY_SHUTTER_MODULE
	= { new Tag(24, 5632), new Tag(24, 5666), new Tag(24, 5667) };
    public static final Tag[] DISPLAYED_AREA_SELECTION_SEQUENCE
	= { new Tag(8, 4416, REFERENCED_IMAGE_SEQUENCE), new Tag(112, 82),
	    new Tag(112, 83), new Tag(112, 256), new Tag(112, 257),
	    new Tag(112, 258), new Tag(112, 259) };
    public static final Tag[] DISPLAYED_AREA_MODULE
	= { new Tag(112, 90, DISPLAYED_AREA_SELECTION_SEQUENCE) };
    public static final Tag[] TEXT_OBJECT_SEQUENCE
	= { new Tag(112, 3), new Tag(112, 4), new Tag(112, 6),
	    new Tag(112, 16), new Tag(112, 17), new Tag(112, 18),
	    new Tag(112, 20), new Tag(112, 21) };
    public static final Tag[] GRAPHIC_OBJECT_SEQUENCE
	= { new Tag(112, 5), new Tag(112, 32), new Tag(112, 33),
	    new Tag(112, 34), new Tag(112, 36) };
    public static final Tag[] GRAPHIC_ANNOTATION_SEQUENCE
	= { new Tag(8, 4416, REFERENCED_IMAGE_SEQUENCE), new Tag(112, 2),
	    new Tag(112, 8, TEXT_OBJECT_SEQUENCE),
	    new Tag(112, 9, GRAPHIC_OBJECT_SEQUENCE) };
    public static final Tag[] GRAPHIC_ANNOTATION_MODULE
	= { new Tag(112, 1, GRAPHIC_ANNOTATION_SEQUENCE) };
    public static final Tag[] SPATIAL_TRANSFORMATION_MODULE
	= { new Tag(112, 65), new Tag(112, 66) };
    public static final Tag[] GRAPHIC_LAYER_SEQUENCE
	= { new Tag(112, 2), new Tag(112, 98), new Tag(112, 102),
	    new Tag(112, 103), new Tag(112, 104) };
    public static final Tag[] GRAPHIC_LAYER_MODULE
	= { new Tag(112, 96, GRAPHIC_LAYER_SEQUENCE) };
    public static final Tag[] PS_REFERENCED_SERIES_SEQUENCE
	= { new Tag(8, 84), new Tag(8, 4416, REFERENCED_IMAGE_SEQUENCE),
	    new Tag(32, 14), new Tag(136, 304), new Tag(136, 320) };
    public static final Tag[] MASK_SUBRACTION_SEQUENCE
	= { new Tag(40, 24833), new Tag(40, 24850) };
    public static final Tag[] PRESENTATION_STATE_MODULE
	= { new Tag(8, 4373, PS_REFERENCED_SERIES_SEQUENCE), new Tag(24, 5666),
	    new Tag(32, 19), new Tag(40, 4240),
	    new Tag(40, 24832, MASK_SUBRACTION_SEQUENCE), new Tag(112, 128),
	    new Tag(112, 129), new Tag(112, 130), new Tag(112, 131),
	    new Tag(112, 132) };
    public static final Tag[] SR_DOCUMENT_SERIES_MODULE
	= { new Tag(8, 96), new Tag(8, 4369, REFERENCED_SOP_SEQUENCE),
	    new Tag(32, 14), new Tag(32, 17) };
    public static final Tag[] SR_REFERENCED_SERIES_SEQUENCE
	= { new Tag(8, 84), new Tag(8, 4505, REFERENCED_SOP_SEQUENCE),
	    new Tag(32, 14), new Tag(136, 304), new Tag(136, 320) };
    public static final Tag[] SOP_INSTANCE_REFERENCE_MACRO
	= { new Tag(8, 4373, SR_REFERENCED_SERIES_SEQUENCE), new Tag(32, 13) };
    public static final Tag[] REFERENCED_REQUEST_SEQUENCE
	= { new Tag(8, 80), new Tag(8, 4368, REFERENCED_SOP_SEQUENCE),
	    new Tag(32, 13), new Tag(50, 4192),
	    new Tag(50, 4196, ENHANCED_CODE_SEQUENCE), new Tag(64, 8214),
	    new Tag(64, 8215), new Tag(64, 4097) };
    public static final Tag[] VERIFYING_OBSERVER_SEQUENCE
	= { new Tag(64, 40999), new Tag(64, 41008), new Tag(64, 41077),
	    new Tag(64, 41096, ENHANCED_CODE_SEQUENCE) };
    public static final Tag[] SR_DOCUMENT_GENERAL_MODULE
	= { new Tag(8, 35), new Tag(8, 51), new Tag(32, 19),
	    new Tag(64, 41075, VERIFYING_OBSERVER_SEQUENCE),
	    new Tag(64, 41824, SOP_INSTANCE_REFERENCE_MACRO),
	    new Tag(64, 41840, REFERENCED_REQUEST_SEQUENCE),
	    new Tag(64, 41842, ENHANCED_CODE_SEQUENCE),
	    new Tag(64, 41845, SOP_INSTANCE_REFERENCE_MACRO),
	    new Tag(64, 41861, SOP_INSTANCE_REFERENCE_MACRO),
	    new Tag(64, 42129), new Tag(64, 42130), new Tag(64, 42131),
	    new Tag(64, 42277, SOP_INSTANCE_REFERENCE_MACRO) };
    public static final Tag[] TEMPLATE_IDENTIFICATION_MACRO
	= { new Tag(8, 261), new Tag(64, 56064), new Tag(64, 56070),
	    new Tag(64, 56071), new Tag(64, 56075), new Tag(64, 56076),
	    new Tag(64, 56077) };
    public static final Tag[] MEASURED_VALUE_SEQUENCE
	= { new Tag(64, 2282, ENHANCED_CODE_SEQUENCE), new Tag(64, 41738) };
    public static final Tag[] SR_REFERENCED_SOP_SEQUENCE
	= { new Tag(8, 4432), new Tag(8, 4437), new Tag(8, 4448),
	    new Tag(64, 41136) };
    public static final Tag[] SR_CONTENT_SEQUENCE = new Tag[23];
    public static final Tag[] SR_DOCUMENT_CONTENT_MODULE;
    public static final Tag[][] COMPUTED_TOMOGRAPHY_IMAGE;
    public static final Tag[][] MAGNETIC_RESONANCE_IMAGE;
    public static final Tag[][] NUCLEAR_MEDICINE_IMAGE;
    public static final Tag[][] BASIC_STUDY_DESCRIPTOR;
    public static final Tag[][] GRAYSCALE_SOFTCOPY_PRESENTATION_STATE;
    public static final Tag[][] STRUCTURED_REPORT_DOCUMENT;
    public static final Tag[][] MODALITY_PERFORMED_PROCEDURE_STEP;
    public static final Tag[] SPECIFIC_CHARACTER_SET;
    public static final Tag[][] MODALITY_WORKLIST_ITEM;
    
    public static Tag[] accumulate(Tag[][] tags) {
	Vector vector = new Vector();
	for (int i = 0; i < tags.length; i++) {
	    for (int i_0_ = 0; i_0_ < tags[i].length; i_0_++)
		_addTag(vector, tags[i][i_0_]);
	}
	Tag[] tags_1_ = new Tag[vector.size()];
	vector.copyInto(tags_1_);
	return tags_1_;
    }
    
    private static void _addTag(Vector vector, Tag tag) {
	int i = vector.size();
	for (int i_2_ = 0; i_2_ < i; i_2_++) {
	    int i_3_ = tag.compareTo((Tag) vector.elementAt(i_2_));
	    if (i_3_ <= 0) {
		if (i_3_ < 0)
		    vector.insertElementAt(tag, i_2_);
		return;
	    }
	}
	vector.addElement(tag);
    }
    
    static {
	SR_CONTENT_SEQUENCE[0] = new Tag(8, 4505, SR_REFERENCED_SOP_SEQUENCE);
	SR_CONTENT_SEQUENCE[1] = new Tag(64, 40976);
	SR_CONTENT_SEQUENCE[2] = new Tag(64, 41010);
	SR_CONTENT_SEQUENCE[3] = new Tag(64, 41024);
	SR_CONTENT_SEQUENCE[4] = new Tag(64, 41027, ENHANCED_CODE_SEQUENCE);
	SR_CONTENT_SEQUENCE[5] = new Tag(64, 41040);
	SR_CONTENT_SEQUENCE[6] = new Tag(64, 41248);
	SR_CONTENT_SEQUENCE[7] = new Tag(64, 41249);
	SR_CONTENT_SEQUENCE[8] = new Tag(64, 41250);
	SR_CONTENT_SEQUENCE[9] = new Tag(64, 41251);
	SR_CONTENT_SEQUENCE[10] = new Tag(64, 41252);
	SR_CONTENT_SEQUENCE[11] = new Tag(64, 41264);
	SR_CONTENT_SEQUENCE[12] = new Tag(64, 41266);
	SR_CONTENT_SEQUENCE[13] = new Tag(64, 41272);
	SR_CONTENT_SEQUENCE[14] = new Tag(64, 41274);
	SR_CONTENT_SEQUENCE[15] = new Tag(64, 41312);
	SR_CONTENT_SEQUENCE[16] = new Tag(64, 41320, ENHANCED_CODE_SEQUENCE);
	SR_CONTENT_SEQUENCE[17] = new Tag(64, 41728, MEASURED_VALUE_SEQUENCE);
	SR_CONTENT_SEQUENCE[18]
	    = new Tag(64, 42244, TEMPLATE_IDENTIFICATION_MACRO);
	SR_CONTENT_SEQUENCE[19] = new Tag(64, 42800, SR_CONTENT_SEQUENCE);
	SR_CONTENT_SEQUENCE[20] = new Tag(64, 56179);
	SR_CONTENT_SEQUENCE[21] = new Tag(112, 34);
	SR_CONTENT_SEQUENCE[22] = new Tag(112, 35);
	SR_DOCUMENT_CONTENT_MODULE
	    = new Tag[] { new Tag(64, 41010), new Tag(64, 41024),
			  new Tag(64, 41027, ENHANCED_CODE_SEQUENCE),
			  new Tag(64, 41040),
			  new Tag(64, 42244, TEMPLATE_IDENTIFICATION_MACRO),
			  new Tag(64, 42800, SR_CONTENT_SEQUENCE) };
	COMPUTED_TOMOGRAPHY_IMAGE
	    = new Tag[][] { PATIENT_MODULE, GENERAL_STUDY_MODULE,
			    PATIENT_STUDY_MODULE, FRAME_OF_REFERENCE_MODULE,
			    GENERAL_SERIES_MODULE, GENERAL_EQUIPMENT_MODULE,
			    GENERAL_IMAGE_MODULE, IMAGE_PLANE_MODULE,
			    IMAGE_PIXEL_MODULE, CONTRAST_BOLUS_MODULE,
			    CT_IMAGE_MODULE, OVERLAY_PLANE_MODULE,
			    VOI_LUT_MODULE, COMMON_SOP_MODULE };
	MAGNETIC_RESONANCE_IMAGE
	    = new Tag[][] { PATIENT_MODULE, GENERAL_STUDY_MODULE,
			    PATIENT_STUDY_MODULE, FRAME_OF_REFERENCE_MODULE,
			    GENERAL_SERIES_MODULE, GENERAL_EQUIPMENT_MODULE,
			    GENERAL_IMAGE_MODULE, IMAGE_PLANE_MODULE,
			    IMAGE_PIXEL_MODULE, CONTRAST_BOLUS_MODULE,
			    MR_IMAGE_MODULE, OVERLAY_PLANE_MODULE,
			    VOI_LUT_MODULE, COMMON_SOP_MODULE };
	NUCLEAR_MEDICINE_IMAGE
	    = (new Tag[][]
	       { PATIENT_MODULE, GENERAL_STUDY_MODULE, PATIENT_STUDY_MODULE,
		 GENERAL_SERIES_MODULE, NM_PET_PATIENT_ORIENTATION_MODULE,
		 FRAME_OF_REFERENCE_MODULE, GENERAL_EQUIPMENT_MODULE,
		 GENERAL_IMAGE_MODULE, IMAGE_PIXEL_MODULE,
		 NM_IMAGE_PIXEL_MODULE, MULTIFRAME_MODULE,
		 NM_MULTIFRAME_MODULE, NM_IMAGE_MODULE, NM_ISOTOPE_MODULE,
		 NM_DETECTOR_MODULE, NM_TOMO_ACQUISITION_MODULE,
		 NM_MULTIGATED_ACQUISITION_MODULE, NM_PHASE_MODULE,
		 NM_RECONSTRUCTION_MODULE, OVERLAY_PLANE_MODULE,
		 MULTIFRAME_OVERLAY_MODULE, CURVE_MODULE, VOI_LUT_MODULE,
		 COMMON_SOP_MODULE });
	BASIC_STUDY_DESCRIPTOR
	    = new Tag[][] { PATIENT_SUMMARY_MODULE, STUDY_CONTENT_MODULE,
			    COMMON_SOP_MODULE };
	GRAYSCALE_SOFTCOPY_PRESENTATION_STATE
	    = (new Tag[][]
	       { PATIENT_MODULE, GENERAL_STUDY_MODULE, PATIENT_STUDY_MODULE,
		 GENERAL_SERIES_MODULE, GENERAL_EQUIPMENT_MODULE, MASK_MODULE,
		 DISPLAY_SHUTTER_MODULE, BM_DISPLAY_SHUTTER_MODULE,
		 OVERLAY_PLANE_MODULE, DISPLAYED_AREA_MODULE,
		 OVERLAY_CURVE_ACTIVATION_MODULE, GRAPHIC_ANNOTATION_MODULE,
		 SPATIAL_TRANSFORMATION_MODULE, GRAPHIC_LAYER_MODULE,
		 MODALITY_LUT_MODULE, SOFTCOPY_VOI_LUT_MODULE,
		 SOFTCOPY_PRESENTATION_LUT_MODULE, PRESENTATION_STATE_MODULE,
		 COMMON_SOP_MODULE });
	STRUCTURED_REPORT_DOCUMENT
	    = new Tag[][] { PATIENT_MODULE, SPECIMEN_IDENTIFICATION_MODULE,
			    GENERAL_STUDY_MODULE, PATIENT_STUDY_MODULE,
			    SR_DOCUMENT_SERIES_MODULE,
			    GENERAL_EQUIPMENT_MODULE,
			    SR_DOCUMENT_GENERAL_MODULE,
			    SR_DOCUMENT_CONTENT_MODULE, COMMON_SOP_MODULE };
	MODALITY_PERFORMED_PROCEDURE_STEP
	    = new Tag[][] { PPS_RELATIONSHIP_MODULE, PPS_INFORMATION_MODULE,
			    IMAGE_ACQUISITION_RESULTS_MODULE,
			    RADIATION_DOSE_MODULE,
			    BILLING_AND_MATERIAL_MANAGEMENT_CODE_MODULE,
			    COMMON_SOP_MODULE };
	SPECIFIC_CHARACTER_SET = new Tag[] { new Tag(8, 5) };
	MODALITY_WORKLIST_ITEM
	    = (new Tag[][]
	       { SPECIFIC_CHARACTER_SET, PATIENT_RELATIONSHIP_MODULE,
		 PATIENT_IDENTIFICATION_MODULE, PATIENT_DEMOGRAPHIC_MODULE,
		 PATIENT_MEDICAL_MODULE, VISIT_RELATIONSHIP_MODULE,
		 VISIT_IDENTIFICATION_MODULE, VISIT_STATUS_MODULE,
		 VISIT_ADMISSION_MODULE, SCHEDULED_PROCEDURE_STEP_MODULE,
		 REQUESTED_PROCEDURE_MODULE, IMAGING_SERVICE_REQUEST_MODULE });
    }
}
