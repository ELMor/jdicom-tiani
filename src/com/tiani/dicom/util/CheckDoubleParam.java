/* CheckDoubleParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

class CheckDoubleParam extends CheckParam
{
    private double _min;
    private double _max;
    
    public CheckDoubleParam(double d, double d_0_, int i) {
	super(i);
	if (d > d_0_)
	    throw new IllegalArgumentException("not a valid intervall");
	_min = d;
	_max = d_0_;
    }
    
    public void check(String string) throws IllegalArgumentException {
	super.check(string);
	if (string != null && string.length() != 0) {
	    double d = Double.parseDouble(string);
	    if (d < _min || d > _max)
		throw new IllegalArgumentException("outside [" + _min + ','
						   + _max + ']');
	}
    }
}
