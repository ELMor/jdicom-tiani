/* URLParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.net.MalformedURLException;
import java.net.URL;

class URLParam extends CheckParam
{
    public URLParam(int i) {
	super(i);
    }
    
    public void check(String string) {
	super.check(string);
	if (string != null && string.length() != 0) {
	    try {
		new URL(string);
	    } catch (MalformedURLException malformedurlexception) {
		throw new IllegalArgumentException(malformedurlexception
						       .toString());
	    }
	}
    }
}
