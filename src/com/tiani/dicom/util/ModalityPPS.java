/* ModalityPPS - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class ModalityPPS
{
    public static final String MPPS_UID = "1.2.840.10008.3.1.2.3.3";
    public static final String IN_PROGRESS = "IN PROGRESS";
    public static final String DISCONTINUED = "DISCONTINUED";
    public static final String COMPLETED = "COMPLETED";
    public static final int[] NCREATE_REQ_VALUE
	= { 1210, 1206, 1198, 1201, 1202, 1205, 81 };
    public static final int[] NCREATE_REQ_ATTRIB
	= { 1210, 1206, 1198, 1201, 1202, 1205, 81, 147, 148, 150, 152, 110,
	    1199, 1200, 1207, 1208, 96, 1203, 1204, 427, 1209, 1227 };
    public static final int[] NCREATE_SSA_SQ_REQ_VALUE = { 425 };
    public static final int[] NCREATE_SSA_SQ_REQ_ATTRIB
	= { 425, 107, 77, 589, 547, 582, 580, 581 };
    public static final int[] NSET_NOT_ALLOWED
	= { 1210, 1206, 1198, 1201, 1202, 147, 148, 150, 152, 110, 1199, 1200,
	    81, 427 };
    public static final int[] NSET_FINAL_REQ_VALUE = { 1203, 1204, 1227 };
    public static final int[] NSET_FINAL_PS_SQ_REQ_VALUE = { 239, 426 };
    public static final int[] NSET_FINAL_PS_REQ_ATTRIB
	= { 100, 239, 102, 426, 97, 79 };
    
    public static void initCreateMPPS(DicomObject dicomobject, String string,
				      String string_0_, String string_1_,
				      String string_2_, String string_3_,
				      String string_4_) throws DicomException {
	dicomobject.set(1210, createSSAttributes(string));
	initCreateMPPS2(dicomobject, string_0_, string_1_, string_2_,
			string_3_, string_4_);
    }
    
    public static void initCreateMPPS
	(DicomObject dicomobject, DicomObject[] dicomobjects, String string,
	 String string_5_, String string_6_, String string_7_,
	 String string_8_)
	throws DicomException {
	for (int i = 0; i < dicomobjects.length; i++)
	    dicomobject.append(1210, createSSAttributes(dicomobjects[i]));
	initCreateMPPS2(dicomobject, string, string_5_, string_6_, string_7_,
			string_8_);
    }
    
    private static void initCreateMPPS2
	(DicomObject dicomobject, String string, String string_9_,
	 String string_10_, String string_11_, String string_12_)
	throws DicomException {
	dicomobject.set(1206, string);
	dicomobject.set(1198, string_9_);
	dicomobject.set(1201, string_10_);
	dicomobject.set(1202, string_11_);
	dicomobject.set(1205, "IN PROGRESS");
	dicomobject.set(81, string_12_);
	dicomobject.set(147, null);
	dicomobject.set(148, null);
	dicomobject.set(150, null);
	dicomobject.set(152, null);
	dicomobject.set(110, null);
	dicomobject.set(1199, null);
	dicomobject.set(1200, null);
	dicomobject.set(1207, null);
	dicomobject.set(1208, null);
	dicomobject.set(96, null);
	dicomobject.set(1203, null);
	dicomobject.set(1204, null);
	dicomobject.set(427, null);
	dicomobject.set(1209, null);
	dicomobject.set(1227, null);
    }
    
    public static DicomObject createCreateMPPS
	(DicomObject[] dicomobjects, String string, String string_13_,
	 String string_14_, String string_15_, String string_16_)
	throws DicomException {
	DicomObject dicomobject = new DicomObject();
	initCreateMPPS(dicomobject, dicomobjects, string, string_13_,
		       string_14_, string_15_, string_16_);
	return dicomobject;
    }
    
    public static DicomObject createCreateMPPS
	(String string, String string_17_, String string_18_,
	 String string_19_, String string_20_, String string_21_)
	throws DicomException {
	DicomObject dicomobject = new DicomObject();
	initCreateMPPS(dicomobject, string, string_17_, string_18_, string_19_,
		       string_20_, string_21_);
	return dicomobject;
    }
    
    public static void initSSAttributes
	(DicomObject dicomobject, String string) throws DicomException {
	dicomobject.set(425, string);
	dicomobject.set(107, null);
	dicomobject.set(77, null);
	dicomobject.set(589, null);
	dicomobject.set(547, null);
	dicomobject.set(582, null);
	dicomobject.set(580, null);
	dicomobject.set(581, null);
    }
    
    public static void initSSAttributes
	(DicomObject dicomobject, DicomObject dicomobject_22_)
	throws DicomException {
	dicomobject.set(425, dicomobject_22_.get(425));
	dicomobject.set(107, null);
	if (dicomobject_22_.getSize(107) > 0) {
	    DicomObject dicomobject_23_
		= (DicomObject) dicomobject_22_.get(107);
	    DicomObject dicomobject_24_ = new DicomObject();
	    dicomobject_24_.set(115, dicomobject_23_.get(115));
	    dicomobject_24_.set(116, dicomobject_23_.get(116));
	    dicomobject.set(107, dicomobject_24_);
	}
	dicomobject.set(77, dicomobject_22_.get(77));
	dicomobject.set(589, dicomobject_22_.get(589));
	dicomobject.set(547, dicomobject_22_.get(547));
	if (dicomobject_22_.getSize(1419) > 0)
	    dicomobject.set(1419, dicomobject_22_.get(1419));
	if (dicomobject_22_.getSize(1420) > 0)
	    dicomobject.set(1420, dicomobject_22_.get(1420));
	dicomobject.set(582, null);
	dicomobject.set(580, null);
	dicomobject.set(581, null);
	if (dicomobject_22_.getSize(587) > 0) {
	    DicomObject dicomobject_25_
		= (DicomObject) dicomobject_22_.get(587);
	    dicomobject.set(582, dicomobject_25_.get(582));
	    dicomobject.set(580, dicomobject_25_.get(580));
	    int i = dicomobject_25_.getSize(581);
	    for (int i_26_ = 0; i_26_ < i; i_26_++) {
		DicomObject dicomobject_27_
		    = (DicomObject) dicomobject_25_.get(581, i_26_);
		DicomObject dicomobject_28_ = new DicomObject();
		dicomobject_28_.set(91, dicomobject_27_.get(91));
		dicomobject_28_.set(92, dicomobject_27_.get(92));
		if (dicomobject_27_.getSize(1337) > 0)
		    dicomobject_28_.set(1337, dicomobject_27_.get(1337));
		if (dicomobject_27_.getSize(93) > 0)
		    dicomobject_28_.set(93, dicomobject_27_.get(93));
		dicomobject.append(581, dicomobject_28_);
	    }
	}
    }
    
    public static DicomObject createSSAttributes(String string)
	throws DicomException {
	DicomObject dicomobject = new DicomObject();
	initSSAttributes(dicomobject, string);
	return dicomobject;
    }
    
    public static DicomObject createSSAttributes(DicomObject dicomobject)
	throws DicomException {
	DicomObject dicomobject_29_ = new DicomObject();
	initSSAttributes(dicomobject_29_, dicomobject);
	return dicomobject_29_;
    }
}
