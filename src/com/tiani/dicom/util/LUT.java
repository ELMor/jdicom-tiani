/* LUT - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.Debug;

public abstract class LUT
{
    static final int MUST_MULTIPLY = 2147483647;
    
    public static class Short extends LUT
    {
	int offset;
	int maxY;
	short y0;
	short yN;
	final short[] data;
	
	public Short(int i, short[] is, int i_0_) {
	    offset = i;
	    maxY = i_0_;
	    data = is;
	    y0 = is[0];
	    yN = is[is.length - 1];
	}
	
	public String toString() {
	    return ("ShortLUT: offset=" + offset + ", length=" + data.length
		    + ", maxY=" + maxY + dumpdata());
	}
	
	private String dumpdata() {
	    if (Debug.DEBUG < 3)
		return "";
	    int i = Math.min(256, data.length);
	    StringBuffer stringbuffer = new StringBuffer();
	    stringbuffer.append(", data=");
	    for (int i_1_ = 0; i_1_ < i; i_1_++) {
		stringbuffer.append(data[i_1_] & 0xffff);
		stringbuffer.append('\\');
	    }
	    if (i < data.length)
		stringbuffer.append("..");
	    return stringbuffer.toString();
	}
	
	public Short(int i, short[] is) {
	    this(i, is, 16);
	}
	
	public Short(int i, byte[] is, int i_2_) {
	    this(i, LUT.copyByteToShort(is, null), i_2_);
	}
	
	public Short(int i, int i_3_, int i_4_, int i_5_) {
	    this(i_3_, ShortRampFactory.create(i, i_4_, i_5_), i_5_);
	}
	
	public Short(int i, int i_6_, int i_7_, int i_8_, boolean bool) {
	    this(i_6_, ShortRampFactory.create(i, bool ? i_8_ : i_7_,
					       bool ? i_7_ : i_8_), i_8_);
	}
	
	public final int offset() {
	    return offset;
	}
	
	public final void setOffset(int i) {
	    offset = i;
	}
	
	public final int maxY() {
	    return maxY;
	}
	
	public final void setMaxY(int i) {
	    maxY = i;
	}
	
	public final int length() {
	    return data.length;
	}
	
	public final byte lookupByte(int i) {
	    return (byte) lookupShort(i);
	}
	
	public final short lookupShort(int i) {
	    return (i -= offset) <= 0 ? y0 : i >= data.length ? yN : data[i];
	}
	
	public final int lookupInt(int i) {
	    return lookupShort(i) & 0xffff;
	}
	
	public final void reverse() {
	    if (offset != 0)
		throw new IllegalArgumentException
			  ("Cannot reverse LUT with offset " + offset);
	    int i = 0;
	    int i_9_ = data.length;
	    while (i < i_9_) {
		short i_10_ = data[i];
		data[i++] = data[--i_9_];
		data[i_9_] = i_10_;
	    }
	    y0 = data[0];
	    yN = data[data.length - 1];
	}
	
	public final void inverse() {
	    for (int i = 0; i < data.length; i++)
		data[i] = (short) (maxY - (data[i] & 0xffff));
	    y0 = data[0];
	    yN = data[data.length - 1];
	}
	
	public final LUT.Byte1 rescaleToByte(int i) {
	    byte[] is = new byte[data.length];
	    int i_11_ = LUT.rShift(maxY, i);
	    if (i_11_ != 2147483647) {
		for (int i_12_ = 0; i_12_ < data.length; i_12_++)
		    is[i_12_] = (byte) ((data[i_12_] & 0xffff) >> i_11_);
	    } else {
		for (int i_13_ = 0; i_13_ < data.length; i_13_++)
		    is[i_13_] = (byte) ((data[i_13_] & 0xffff) * i / maxY);
	    }
	    return new LUT.Byte1(offset, is, i);
	}
	
	public final LUT.Short rescaleToShort(int i) {
	    int i_14_ = LUT.rShift(maxY, i);
	    if (i_14_ == 0)
		return this;
	    short[] is = new short[data.length];
	    if (i_14_ != 2147483647) {
		if (i_14_ > 0) {
		    for (int i_15_ = 0; i_15_ < data.length; i_15_++)
			is[i_15_] = (short) ((data[i_15_] & 0xffff) >> i_14_);
		} else {
		    int i_16_ = -i_14_;
		    for (int i_17_ = 0; i_17_ < data.length; i_17_++)
			is[i_17_] = (short) ((data[i_17_] & 0xffff) << i_16_);
		}
	    } else {
		for (int i_18_ = 0; i_18_ < data.length; i_18_++)
		    is[i_18_] = (short) ((data[i_18_] & 0xffff) * i / maxY);
	    }
	    return new LUT.Short(offset, is, i);
	}
	
	public final LUT rescaleLength(int i) {
	    if (offset != 0)
		throw new IllegalArgumentException
			  ("Cannot rescale length of LUT with offset "
			   + offset);
	    short[] is = new short[i];
	    if (data.length % i == 0) {
		int i_19_ = data.length / i;
		int i_20_ = 0;
		int i_21_ = 0;
		while (i_21_ < i) {
		    is[i_21_] = data[i_20_];
		    i_21_++;
		    i_20_ += i_19_;
		}
	    } else {
		int i_22_ = data.length - 1;
		float f = (float) data.length / (float) i;
		for (int i_23_ = 0; i_23_ < i; i_23_++) {
		    float f_25_;
		    int i_24_ = (int) (f_25_ = (float) i_23_ * f);
		    if (i_24_ == i_22_)
			is[i_23_] = data[i_24_];
		    else {
			float f_26_ = f_25_ - (float) i_24_;
			is[i_23_]
			    = (short) (int) (((float) (data[i_24_] & 0xffff)
					      * (1.0F - f_26_))
					     + (float) (data[i_24_ + 1]
							& 0xffff) * f_26_);
		    }
		}
	    }
	    return new LUT.Short(0, is, maxY);
	}
    }
    
    public static class Byte1 extends LUT
    {
	int offset;
	int maxY;
	byte y0;
	byte yN;
	final byte[] data;
	
	public Byte1(int i, byte[] is, int i_27_) {
	    offset = i;
	    maxY = i_27_;
	    data = is;
	    y0 = is[0];
	    yN = is[is.length - 1];
	}
	
	public Byte1(int i, byte[] is) {
	    this(i, is, 8);
	}
	
	public Byte1(int i, int i_28_, int i_29_, int i_30_) {
	    this(i_28_, ByteRampFactory.create(i, i_29_, i_30_), i_30_);
	}
	
	public Byte1(int i, int i_31_, int i_32_, int i_33_, boolean bool) {
	    this(i_31_, ByteRampFactory.create(i, bool ? i_33_ : i_32_,
					       bool ? i_32_ : i_33_), i_33_);
	}
	
	public final int offset() {
	    return offset;
	}
	
	public final void setOffset(int i) {
	    offset = i;
	}
	
	public final int maxY() {
	    return maxY;
	}
	
	public final void setMaxY(int i) {
	    maxY = i;
	}
	
	public String toString() {
	    return ("ByteLUT: offset=" + offset + ", length=" + data.length
		    + ", maxY=" + maxY + dumpdata());
	}
	
	private String dumpdata() {
	    if (Debug.DEBUG < 3)
		return "";
	    int i = Math.min(256, data.length);
	    StringBuffer stringbuffer = new StringBuffer();
	    stringbuffer.append(", data=");
	    for (int i_34_ = 0; i_34_ < i; i_34_++) {
		stringbuffer.append(data[i_34_] & 0xff);
		stringbuffer.append('\\');
	    }
	    if (i < data.length)
		stringbuffer.append("..");
	    return stringbuffer.toString();
	}
	
	public final int length() {
	    return data.length;
	}
	
	public final byte lookupByte(int i) {
	    return (i -= offset) <= 0 ? y0 : i >= data.length ? yN : data[i];
	}
	
	public final short lookupShort(int i) {
	    return (short) lookupInt(i);
	}
	
	public final int lookupInt(int i) {
	    return lookupByte(i) & 0xff;
	}
	
	public final void reverse() {
	    if (offset != 0)
		throw new IllegalArgumentException
			  ("Cannot reverse LUT with offset " + offset);
	    int i = 0;
	    int i_35_ = data.length;
	    while (i < i_35_) {
		byte i_36_ = data[i];
		data[i++] = data[--i_35_];
		data[i_35_] = i_36_;
	    }
	    y0 = data[0];
	    yN = data[data.length - 1];
	}
	
	public final void inverse() {
	    for (int i = 0; i < data.length; i++)
		data[i] = (byte) (maxY - (data[i] & 0xff));
	    y0 = data[0];
	    yN = data[data.length - 1];
	}
	
	public final LUT.Byte1 rescaleToByte(int i) {
	    int i_37_ = LUT.rShift(maxY, i);
	    if (i_37_ == 0)
		return this;
	    byte[] is = new byte[data.length];
	    if (i_37_ != 2147483647) {
		for (int i_38_ = 0; i_38_ < data.length; i_38_++)
		    is[i_38_] = (byte) ((data[i_38_] & 0xff) >> i_37_);
	    } else {
		for (int i_39_ = 0; i_39_ < data.length; i_39_++)
		    is[i_39_] = (byte) ((data[i_39_] & 0xff) * i / maxY);
	    }
	    return new LUT.Byte1(offset, is, i);
	}
	
	public final LUT.Short rescaleToShort(int i) {
	    short[] is = new short[data.length];
	    int i_40_ = LUT.rShift(maxY, i);
	    if (i_40_ != 2147483647) {
		for (int i_41_ = 0; i_41_ < data.length; i_41_++)
		    is[i_41_] = (short) ((data[i_41_] & 0xff) >> i_40_);
	    } else {
		for (int i_42_ = 0; i_42_ < data.length; i_42_++)
		    is[i_42_] = (short) ((data[i_42_] & 0xff) * i / maxY);
	    }
	    return new LUT.Short(offset, is, i);
	}
	
	public final LUT rescaleLength(int i) {
	    if (offset != 0)
		throw new IllegalArgumentException
			  ("Cannot rescale length of LUT with offset "
			   + offset);
	    byte[] is = new byte[i];
	    if (data.length % i == 0) {
		int i_43_ = data.length / i;
		int i_44_ = 0;
		int i_45_ = 0;
		while (i_45_ < i) {
		    is[i_45_] = data[i_44_];
		    i_45_++;
		    i_44_ += i_43_;
		}
	    } else {
		int i_46_ = data.length - 1;
		float f = (float) data.length / (float) i;
		for (int i_47_ = 0; i_47_ < i; i_47_++) {
		    float f_49_;
		    int i_48_ = (int) (f_49_ = (float) i_47_ * f);
		    if (i_48_ == i_46_)
			is[i_47_] = data[i_48_];
		    else {
			float f_50_ = f_49_ - (float) i_48_;
			is[i_47_] = (byte) (int) (((float) (data[i_48_] & 0xff)
						   * (1.0F - f_50_))
						  + (float) (data[i_48_ + 1]
							     & 0xff) * f_50_);
		    }
		}
	    }
	    return new LUT.Byte1(0, is, maxY);
	}
    }
    
    static final int rShift(int i, int i_51_) {
	int i_52_ = i_51_ - i;
	if (i_52_ == 0)
	    return 0;
	int i_53_ = (i_52_ > 0 ? i : i_51_) + 1;
	int i_54_ = (i_52_ > 0 ? i_51_ : i) + 1;
	int i_55_ = 0;
	for (/**/; (i_54_ & 0x1) == 0; i_54_ >>= 1) {
	    if (i_54_ == i_53_)
		return i_52_ > 0 ? -i_55_ : i_55_;
	    i_55_++;
	}
	return 2147483647;
    }
    
    public abstract int offset();
    
    public abstract void setOffset(int i);
    
    public abstract int maxY();
    
    public abstract void setMaxY(int i);
    
    public abstract int length();
    
    public abstract byte lookupByte(int i);
    
    public abstract short lookupShort(int i);
    
    public abstract int lookupInt(int i);
    
    public abstract void inverse();
    
    public abstract void reverse();
    
    public abstract Byte1 rescaleToByte(int i);
    
    public abstract Short rescaleToShort(int i);
    
    public abstract LUT rescaleLength(int i);
    
    public static int lookupChain(int i, LUT[] luts) {
	int i_56_ = luts.length;
	int i_57_ = i;
	for (int i_58_ = 0; i_58_ < i_56_; i_58_++) {
	    LUT lut;
	    if ((lut = luts[i_58_]) != null)
		i_57_ = luts[i_58_].lookupInt(i_57_);
	}
	return i_57_;
    }
    
    public static LUT[] trimLUTs(LUT[] luts) {
	int i = 0;
	int i_59_;
	for (i_59_ = luts.length - 1; i <= i_59_; i++) {
	    if (luts[i] != null)
		break;
	}
	for (/**/; i <= i_59_ && luts[i_59_] == null; i_59_--) {
	    /* empty */
	}
	int i_60_ = i_59_ - i + 1;
	if (i_60_ == luts.length)
	    return luts;
	LUT[] luts_61_ = new LUT[i_60_];
	System.arraycopy(luts, i, luts_61_, 0, i_60_);
	return luts_61_;
    }
    
    public static LUT join(LUT[] luts) {
	luts = trimLUTs(luts);
	switch (luts.length) {
	case 0:
	    return null;
	case 1:
	    return luts[0];
	default: {
	    LUT lut = luts[0];
	    LUT lut_62_ = luts[luts.length - 1];
	    if (lut_62_ instanceof Byte1) {
		byte[] is = new byte[lut.length()];
		int i = lut.offset();
		int i_63_ = 0;
		while (i_63_ < is.length) {
		    is[i_63_] = (byte) lookupChain(i, luts);
		    i_63_++;
		    i++;
		}
		return new Byte1(lut.offset(), is, lut_62_.maxY());
	    }
	    short[] is = new short[lut.length()];
	    int i = lut.offset();
	    int i_64_ = 0;
	    while (i_64_ < is.length) {
		is[i_64_] = (short) lookupChain(i, luts);
		i_64_++;
		i++;
	    }
	    return new Short(lut.offset(), is, lut_62_.maxY());
	}
	}
    }
    
    public static LUT createLUT(int i, int i_65_, int i_66_, int i_67_) {
	return (i_67_ > 255 ? (LUT) new Short(i, i_65_, 0, i_67_)
		: new Byte1(i, i_65_, 0, i_67_));
    }
    
    public static LUT createLUT(int i, int i_68_, int i_69_, int i_70_,
				boolean bool) {
	return (i_70_ > 255 ? (LUT) new Short(i, i_68_, 0, i_70_, bool)
		: new Byte1(i, i_68_, 0, i_70_, bool));
    }
    
    public static short[] copyByteToShort(byte[] is, short[] is_71_) {
	if (is_71_ == null)
	    is_71_ = new short[is.length / 2];
	int i = 0;
	int i_72_ = 0;
	while (i_72_ < is_71_.length) {
	    is_71_[i_72_] = (short) (is[i] & 0xff | is[i + 1] << 8);
	    i_72_++;
	    i++;
	    i++;
	}
	return is_71_;
    }
}
