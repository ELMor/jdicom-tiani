/* AETable - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

public class AETable extends Properties implements IAETable
{
    public AETable() {
	/* empty */
    }
    
    public AETable(Properties properties) {
	super(properties);
    }
    
    public AET lookup(String string) {
	try {
	    String string_0_ = getHost(string);
	    if (string_0_ != null)
		return new AET(string, string_0_, getPort(string));
	} catch (Exception exception) {
	    /* empty */
	}
	return null;
    }
    
    public final String getHost(String string) {
	return this.getProperty(string + ".host");
    }
    
    public final int getPort(String string) {
	return Integer.parseInt(this.getProperty(string + ".port"));
    }
    
    public void add(AET aet) {
	this.put(aet.title + ".host", aet.host);
	this.put(aet.title + ".port", "" + aet.port);
    }
    
    public AET removeAET(String string) {
	AET aet = lookup(string);
	if (string != null) {
	    this.remove(string + ".host");
	    this.remove(string + ".port");
	}
	return aet;
    }
    
    public String[] list() {
	Vector vector = new Vector();
	Enumeration enumeration = this.keys();
	while (enumeration.hasMoreElements()) {
	    String string = (String) enumeration.nextElement();
	    if (string.endsWith(".host")) {
		try {
		    String string_1_;
		    getPort(string_1_ = string.substring(0,
							 string.length() - 5));
		    vector.addElement(string_1_);
		} catch (Exception exception) {
		    /* empty */
		}
	    }
	}
	String[] strings = new String[vector.size()];
	vector.copyInto(strings);
	return strings;
    }
}
