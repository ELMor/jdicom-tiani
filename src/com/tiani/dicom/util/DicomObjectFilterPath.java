/* DicomObjectFilterPath - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Vector;

public class DicomObjectFilterPath extends Vector
{
    public IDicomObjectFilter first() {
	return (IDicomObjectFilter) this.firstElement();
    }
    
    public IDicomObjectFilter last() {
	return (IDicomObjectFilter) this.lastElement();
    }
    
    public IDicomObjectFilter getAt(int i) {
	return (IDicomObjectFilter) this.elementAt(i);
    }
    
    public DicomObjectFilterPath remaining() {
	DicomObjectFilterPath dicomobjectfilterpath_0_
	    = (DicomObjectFilterPath) this.clone();
	dicomobjectfilterpath_0_.removeElementAt(0);
	return dicomobjectfilterpath_0_;
    }
}
