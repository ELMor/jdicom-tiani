/* ByteArrayPool - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Iterator;
import java.util.LinkedList;

import com.archimed.dicom.Debug;

public class ByteArrayPool
{
    private static int defaultMaxSize = 1000000;
    private static ByteArrayPool instance = null;
    private final int maxSize;
    private int curSize;
    private final LinkedList pool = new LinkedList();
    
    public static ByteArrayPool getInstance() {
	if (instance == null) {
	    synchronized (ByteArrayPool.class) {
		if (instance == null)
		    instance = new ByteArrayPool();
	    }
	}
	return instance;
    }
    
    public static void setDefaultMaxSize(int i) {
	defaultMaxSize = i;
    }
    
    public static int getDefaultMaxSize() {
	return defaultMaxSize;
    }
    
    public ByteArrayPool() {
	this(defaultMaxSize);
    }
    
    public ByteArrayPool(int i) {
	maxSize = i;
	curSize = 0;
    }
    
    public byte[] getByteArray(int i) {
	byte[] is = remove(i);
	if (Debug.DEBUG > 1)
	    Debug.out.println(is == null
			      ? "jdicom: ByteArrayPool -> new byte[" + i + "]"
			      : ("jdicom: ByteArrayPool -> reuse byte[" + i
				 + "]"));
	return is != null ? is : new byte[i];
    }
    
    public void release(byte[] is) {
	if (is.length < maxSize)
	    add(is);
    }
    
    private synchronized byte[] remove(int i) {
	Iterator iterator = pool.iterator();
	while (iterator.hasNext()) {
	    byte[] is;
	    if ((is = (byte[]) iterator.next()).length == i) {
		iterator.remove();
		curSize -= i;
		return is;
	    }
	}
	return null;
    }
    
    private synchronized void add(byte[] is) {
	pool.addLast(is);
	for (curSize += is.length; curSize > maxSize;
	     curSize -= ((byte[]) pool.removeFirst()).length) {
	    /* empty */
	}
    }
}
