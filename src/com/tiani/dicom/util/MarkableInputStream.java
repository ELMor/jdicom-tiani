/* MarkableInputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MarkableInputStream extends FilterInputStream
{
    private byte[] _buffer = null;
    private int _pos = 0;
    private int _len = 0;
    
    public MarkableInputStream(InputStream inputstream) {
	super(inputstream);
    }
    
    public final int available() throws IOException {
	return _len - _pos + in.available();
    }
    
    public final void mark(int i) {
	if (in.markSupported())
	    in.mark(i);
	else {
	    int i_0_ = _len - _pos;
	    byte[] is = new byte[Math.max(i, i_0_)];
	    if (_buffer != null && i_0_ > 0)
		System.arraycopy(_buffer, _pos, is, 0, i_0_);
	    _pos = 0;
	    _len = i_0_;
	    _buffer = is;
	}
    }
    
    public final boolean markSupported() {
	return true;
    }
    
    public final int read() throws IOException {
	if (_buffer == null)
	    return in.read();
	if (_pos < _len)
	    return _buffer[_pos++];
	if (_len < _buffer.length) {
	    if (in.read(_buffer, _pos, 1) == -1)
		return -1;
	    _len++;
	    return _buffer[_pos++];
	}
	_buffer = null;
	_pos = 0;
	_len = 0;
	return in.read();
    }
    
    public final int read(byte[] is, int i, int i_1_) throws IOException {
	if (_buffer == null)
	    return in.read(is, i, i_1_);
	int i_2_ = _len - _pos;
	int i_3_ = i_1_ - i_2_;
	if (i_3_ > 0) {
	    if (i_3_ > _buffer.length - _len) {
		System.arraycopy(_buffer, _pos, is, i, i_2_);
		_buffer = null;
		_pos = 0;
		_len = 0;
		int i_4_;
		if ((i_4_ = in.read(is, i + i_2_, i_1_ - i_2_)) == -1)
		    return -1;
		return i_2_ + i_4_;
	    }
	    int i_5_;
	    if ((i_5_ = in.read(_buffer, _len, i_3_)) == -1)
		return -1;
	    _len += i_5_;
	}
	int i_6_ = Math.min(i_1_, _len - _pos);
	System.arraycopy(_buffer, _pos, is, i, i_6_);
	_pos += i_6_;
	return i_6_;
    }
    
    public final int read(byte[] is) throws IOException {
	return read(is, 0, is.length);
    }
    
    public final void reset() throws IOException {
	if (in.markSupported())
	    in.reset();
	else {
	    if (_buffer == null)
		throw new IOException("Resetting to invalid mark");
	    _pos = 0;
	}
    }
    
    public final long skip(long l) throws IOException {
	if (_buffer == null)
	    return in.skip(l);
	int i = _len - _pos;
	if (l < (long) i) {
	    _pos += l;
	    return l;
	}
	_buffer = null;
	_pos = 0;
	_len = 0;
	return (long) i + in.skip(l - (long) i);
    }
}
