/* OrStringFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public class OrStringFilter implements IStringFilter
{
    private IStringFilter[] filter;
    
    public OrStringFilter(IStringFilter[] istringfilters) {
	filter = istringfilters;
    }
    
    public boolean accept(String string) {
	for (int i = 0; i < filter.length; i++) {
	    if (filter[i].accept(string))
		return true;
	}
	return false;
    }
}
