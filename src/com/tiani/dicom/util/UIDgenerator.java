/* UIDgenerator - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Random;

import com.tiani.dicom.myassert.Assert;

public final class UIDgenerator
{
    public static String prefix = "1.2.40.0.13.0.";
    public static String defaultSuffix = ".0";
    public static String studySuffix = ".1";
    public static String seriesSuffix = ".2";
    public static Hashtable suffixForClassUID = new Hashtable();
    private static final Random _random = new Random();
    private static final byte[] _byte1 = new byte[1];
    private static final SimpleDateFormat _formater
	= new SimpleDateFormat("yyyyMMddHHmmssSSS");
    
    public static String createStudyInstanceUID() {
	return createUID(studySuffix);
    }
    
    public static String createSeriesInstanceUID() {
	return createUID(seriesSuffix);
    }
    
    public static String createSOPInstanceUIDFor(String string) {
	String string_0_ = (String) suffixForClassUID.get(string);
	return createUID(string_0_ != null ? string_0_ : defaultSuffix);
    }
    
    public static String createUID(String string) {
	return (prefix + createID(64 - prefix.length() - string.length())
		+ string);
    }
    
    public static String createID(int i) {
	Assert.isTrue(i > 0);
	_random.nextBytes(_byte1);
	String string = _formater.format(new Date()) + (_byte1[0] + 128);
	int i_1_ = string.length() - i;
	if (i_1_ <= 0)
	    return string;
	return "1" + string.substring(i_1_ + 1);
    }
}
