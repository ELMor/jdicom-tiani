/* Tag - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.DDict;

public final class Tag
{
    static ExtDDict extDDict = new ExtDDict();
    private int _gr;
    private int _el;
    private Tag[] _sqTags = null;
    private int _typeCode;
    private String _tagPrompt;
    private String _typePrompt;
    private String _descPrompt;
    
    public Tag(int i, int i_0_) {
	_gr = i;
	_el = i_0_;
	_tagPrompt = '(' + _toHexString(_gr) + ',' + _toHexString(_el) + ')';
	_typeCode = DDict.getTypeCode(i, i_0_);
	_typePrompt = vrPrompt(_typeCode);
	_descPrompt = ((_gr & 0x1) != 0 ? "Proprietary Tag"
		       : DDict.getDescription(DDict.lookupDDict(_gr, _el)));
    }
    
    public Tag(int i, int i_1_, Tag[] tags) {
	this(i, i_1_);
	_sqTags = tags;
    }
    
    public int getGroup() {
	return _gr;
    }
    
    public int getElement() {
	return _el;
    }
    
    public Tag[] getSQTags() {
	return _sqTags;
    }
    
    public boolean equals(Object object) {
	if (this == object)
	    return true;
	if (object == null || object.getClass() != Tag.class)
	    return false;
	Tag tag_2_ = (Tag) object;
	return _gr == tag_2_._gr && _el == tag_2_._el;
    }
    
    public int hashCode() {
	return (_gr << 16) + _el;
    }
    
    public String toString() {
	return _tagPrompt;
    }
    
    public int compareTo(Tag tag_3_) {
	if (this == tag_3_)
	    return 0;
	return hashCode() - tag_3_.hashCode();
    }
    
    public int getTypeCode() {
	return _typeCode;
    }
    
    public boolean isTypeSQ() {
	return _typeCode == 10;
    }
    
    public String getTypePrompt() {
	return _typePrompt;
    }
    
    public String getDescription() {
	return _descPrompt;
    }
    
    private static String _toHexString(int i) {
	StringBuffer stringbuffer
	    = new StringBuffer(Integer.toHexString(i).toUpperCase());
	while (stringbuffer.length() < 4)
	    stringbuffer.insert(0, '0');
	return stringbuffer.toString();
    }
    
    public static String vrPrompt(int i) {
	switch (i) {
	case 4:
	    return "AE";
	case 17:
	    return "AS";
	case 5:
	    return "AT";
	case 9:
	    return "CS";
	case 11:
	    return "DA";
	case 16:
	    return "DS";
	case 28:
	    return "DT";
	case 20:
	    return "FD";
	case 26:
	    return "FL";
	case 15:
	    return "IS";
	case 6:
	    return "LO";
	case 18:
	    return "LT";
	case 8:
	    return "OB";
	case 24:
	    return "OW";
	case 14:
	    return "PN";
	case 7:
	    return "SH";
	case 19:
	    return "SL";
	case 10:
	    return "SQ";
	case 23:
	    return "SS";
	case 13:
	    return "ST";
	case 12:
	    return "TM";
	case 2:
	    return "UI";
	case 0:
	    return "UN";
	case 1:
	    return "UL";
	case 3:
	    return "US";
	case 27:
	    return "UT";
	case 25:
	    return "NONE";
	case 21:
	    return "US|SS";
	case 22:
	    return "OW|OB";
	default:
	    return null;
	}
    }
}
