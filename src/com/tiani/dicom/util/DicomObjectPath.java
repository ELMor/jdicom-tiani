/* DicomObjectPath - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Enumeration;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;

public class DicomObjectPath extends Vector
{
    public DicomObjectPath() {
	/* empty */
    }
    
    public DicomObjectPath(DicomObject dicomobject) {
	super(1);
	this.addElement(dicomobject);
    }
    
    public DicomObjectPath(DicomObject dicomobject,
			   DicomObjectPath dicomobjectpath_0_) {
	super(dicomobjectpath_0_ != null ? dicomobjectpath_0_.size() + 1 : 1);
	this.addElement(dicomobject);
	if (dicomobjectpath_0_ != null) {
	    Enumeration enumeration = dicomobjectpath_0_.elements();
	    while (enumeration.hasMoreElements())
		this.addElement(enumeration.nextElement());
	}
    }
    
    public void add(DicomObject dicomobject) {
	this.addElement(dicomobject);
    }
    
    public DicomObject first() {
	return (DicomObject) this.firstElement();
    }
    
    public DicomObject last() {
	return (DicomObject) this.lastElement();
    }
    
    public DicomObject getAt(int i) {
	return (DicomObject) this.elementAt(i);
    }
    
    public DicomObjectPath remaining() {
	DicomObjectPath dicomobjectpath_1_ = (DicomObjectPath) this.clone();
	dicomobjectpath_1_.removeElementAt(0);
	return dicomobjectpath_1_;
    }
    
    public DicomObject getComposite(boolean bool) throws DicomException {
	DicomObject dicomobject = new DicomObject();
	Enumeration enumeration = this.elements();
	while (enumeration.hasMoreElements())
	    _addDicomObject((DicomObject) enumeration.nextElement(),
			    dicomobject, bool);
	return dicomobject;
    }
    
    private static void _addDicomObject
	(DicomObject dicomobject, DicomObject dicomobject_2_, boolean bool)
	throws DicomException {
	Enumeration enumeration = dicomobject.enumerateVRs(false, false);
	while (enumeration.hasMoreElements())
	    _addTagValue((TagValue) enumeration.nextElement(), dicomobject_2_,
			 bool);
    }
    
    private static void _addTagValue
	(TagValue tagvalue, DicomObject dicomobject, boolean bool)
	throws DicomException {
	int i = tagvalue.getGroup();
	int i_3_ = tagvalue.getElement();
	if ((i & 0x1) != 0)
	    Debug.out.println("jdicom: [WARN] skipping private attribute ("
			      + Integer.toHexString(i) + ","
			      + Integer.toHexString(i_3_) + ")");
	else {
	    if (!bool)
		dicomobject.deleteItem_ge(i, i_3_);
	    int i_4_ = tagvalue.size();
	    for (int i_5_ = 0; i_5_ < i_4_; i_5_++)
		dicomobject.append_ge(i, i_3_, tagvalue.getValue(i_5_));
	}
    }
}
