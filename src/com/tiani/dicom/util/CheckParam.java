/* CheckParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public class CheckParam
{
    private static final String[] TRUE_FALSE = { "true", "false" };
    private static final CheckParam _BOOL = new CheckEnumParam(TRUE_FALSE, 1);
    protected final int _type;
    
    public CheckParam(int i) {
	_type = i;
    }
    
    public void check(String string) throws IllegalArgumentException {
	if (string == null) {
	    if (_type <= 2)
		throw new IllegalArgumentException("missing parameter");
	} else if (string.length() == 0 && _type == 1)
	    throw new IllegalArgumentException("missing value");
    }
    
    public String[] getTags() {
	return null;
    }
    
    public boolean isMulti() {
	return false;
    }
    
    public static CheckParam bool() {
	return _BOOL;
    }
    
    public static CheckParam range(int i, int i_0_) {
	return new CheckIntParam(i, i_0_, 1);
    }
    
    public static CheckParam range(int i, int i_1_, int i_2_) {
	return new CheckIntParam(i, i_1_, i_2_);
    }
    
    public static CheckParam range(double d, double d_3_) {
	return new CheckDoubleParam(d, d_3_, 1);
    }
    
    public static CheckParam range(double d, double d_4_, int i) {
	return new CheckDoubleParam(d, d_4_, i);
    }
    
    public static CheckParam enum(String[] strings) {
	return new CheckEnumParam(strings, 1);
    }
    
    public static CheckParam multiEnum(String[] strings) {
	return new CheckMultiEnumParam(strings);
    }
    
    public static CheckParam enum(String[] strings, int i) {
	return new CheckEnumParam(strings, i);
    }
    
    public static CheckParam defined(String[] strings) {
	return new DefinedParam(strings, 1);
    }
    
    public static CheckParam defined(String[] strings, int i) {
	return new DefinedParam(strings, i);
    }
    
    public static CheckParam url() {
	return new URLParam(1);
    }
    
    public static CheckParam url(int i) {
	return new URLParam(i);
    }
    
    public static void verify(Properties properties, Hashtable hashtable)
	throws IllegalArgumentException {
	Enumeration enumeration = hashtable.keys();
	while (enumeration.hasMoreElements()) {
	    String string = (String) enumeration.nextElement();
	    CheckParam checkparam = (CheckParam) hashtable.get(string);
	    try {
		checkparam.check(properties.getProperty(string));
	    } catch (IllegalArgumentException illegalargumentexception) {
		throw new IllegalArgumentException(string + ": "
						   + illegalargumentexception);
	    }
	}
    }
}
