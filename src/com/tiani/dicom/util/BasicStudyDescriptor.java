/* BasicStudyDescriptor - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Hashtable;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class BasicStudyDescriptor extends DicomObject
{
    private Hashtable _refSeries = new Hashtable();
    private Hashtable _refImages = new Hashtable();
    private String _studyInstanceUID = null;
    
    public BasicStudyDescriptor(String string) throws DicomException {
	this.set(62, "1.2.840.10008.1.9");
	this.set(63, string);
    }
    
    private void _init(DicomObject dicomobject) throws DicomException {
	this.set(147, dicomobject.get(147));
	this.set(148, dicomobject.get(148));
	_studyInstanceUID = dicomobject.getS(425);
	this.set(425, _studyInstanceUID);
	this.set(427, dicomobject.get(427));
    }
    
    public boolean add(DicomObject dicomobject) throws DicomException {
	return _add(dicomobject, null, null);
    }
    
    public boolean addRetrSeries
	(DicomObject dicomobject, DicomObject dicomobject_0_)
	throws DicomException {
	return _add(dicomobject, dicomobject_0_, null);
    }
    
    public boolean addRetrImage
	(DicomObject dicomobject, DicomObject dicomobject_1_)
	throws DicomException {
	return _add(dicomobject, null, dicomobject_1_);
    }
    
    private boolean _add(DicomObject dicomobject, DicomObject dicomobject_2_,
			 DicomObject dicomobject_3_) throws DicomException {
	String string = dicomobject.getS(63);
	if (_refImages.containsKey(string))
	    return false;
	if (_studyInstanceUID == null)
	    _init(dicomobject);
	else if (!_studyInstanceUID.equals(dicomobject.getS(425)))
	    throw new DicomException
		      ("BasicStudyDescriptor.add: image belongs to different study");
	String string_4_ = dicomobject.getS(426);
	DicomObject dicomobject_5_
	    = _getRefSeries(dicomobject.getS(426), dicomobject.getS(1207),
			    dicomobject.getS(97), dicomobject_2_);
	DicomObject dicomobject_6_ = new DicomObject();
	if (dicomobject_3_ != null)
	    dicomobject_6_.addGroups(dicomobject_3_);
	dicomobject_6_.set(115, dicomobject.getS(62));
	dicomobject_6_.set(116, string);
	dicomobject_5_.append(113, dicomobject_6_);
	_refImages.put(string, dicomobject_6_);
	return true;
    }
    
    private DicomObject _getRefSeries
	(String string, String string_7_, String string_8_,
	 DicomObject dicomobject)
	throws DicomException {
	DicomObject dicomobject_9_ = (DicomObject) _refSeries.get(string);
	if (dicomobject_9_ == null) {
	    dicomobject_9_ = new DicomObject();
	    if (dicomobject != null)
		dicomobject_9_.addGroups(dicomobject);
	    dicomobject_9_.set(426, string);
	    if (string_8_ != null)
		dicomobject_9_.set(97, string_8_);
	    if (string_7_ != null)
		dicomobject_9_.set(1207, string_7_);
	    this.append(109, dicomobject_9_);
	    _refSeries.put(string, dicomobject_9_);
	}
	return dicomobject_9_;
    }
    
    private DicomObject _getRefSeries
	(String string, DicomObject dicomobject) throws DicomException {
	DicomObject dicomobject_10_ = (DicomObject) _refSeries.get(string);
	if (dicomobject_10_ == null) {
	    dicomobject_10_ = new DicomObject();
	    if (dicomobject != null)
		dicomobject_10_.addGroups(dicomobject);
	    dicomobject_10_.set(426, string);
	    this.append(109, dicomobject_10_);
	    _refSeries.put(string, dicomobject_10_);
	}
	return dicomobject_10_;
    }
    
    public static DicomObject getRetrieveInfo(String string, String string_11_,
					      String string_12_) {
	try {
	    DicomObject dicomobject = new DicomObject();
	    dicomobject.set(79, string);
	    dicomobject.set(697, string_11_);
	    dicomobject.set(698, string_12_);
	    return dicomobject;
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.toString());
	}
    }
    
    public static DicomObject getRetrieveInfo(String string,
					      String string_13_) {
	try {
	    DicomObject dicomobject = new DicomObject();
	    dicomobject.set(697, string);
	    dicomobject.set(698, string_13_);
	    return dicomobject;
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.toString());
	}
    }
    
    public static DicomObject getRetrieveInfo(String string) {
	try {
	    DicomObject dicomobject = new DicomObject();
	    dicomobject.set(79, string);
	    return dicomobject;
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.toString());
	}
    }
}
