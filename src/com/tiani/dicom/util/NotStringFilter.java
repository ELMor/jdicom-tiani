/* NotStringFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public class NotStringFilter implements IStringFilter
{
    private IStringFilter filter;
    
    public NotStringFilter(IStringFilter istringfilter) {
	filter = istringfilter;
    }
    
    public boolean accept(String string) {
	return !filter.accept(string);
    }
}
