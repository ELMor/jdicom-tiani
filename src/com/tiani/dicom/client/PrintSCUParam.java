/* PrintSCUParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.client;
import java.util.Hashtable;
import java.util.Properties;

import com.archimed.dicom.Debug;
import com.tiani.dicom.util.CheckParam;

final class PrintSCUParam
{
    static final int LUT_SESSION_LEVEL = 0;
    static final int LUT_FILMBOX_LEVEL = 1;
    static final int LUT_IMAGEBOX_LEVEL = 2;
    private final int _port;
    private final int maxPduSize;
    private final boolean grouplens;
    private final boolean _verification;
    private final boolean _basicGrayscalePrintManagement;
    private final boolean _basicColorPrintManagement;
    private final boolean _basicAnnotationBox;
    private final boolean _basicPrintImageOverlayBox;
    private final boolean _presentationLUT;
    private final boolean _printJob;
    private final boolean _printerConfigurationRetrieval;
    private final boolean sendAspectRatioAlways;
    private final int _requestedZoom;
    private final int _burnInInfo;
    private final int bitDepth;
    private final int inflateBitsAlloc;
    private final int lutShape;
    private final double lutGamma;
    private final int lutLevel;
    private final boolean lutApplyBySCU;
    private final boolean lutScaleToFitBitDepth;
    private final boolean minMaxWindowing;
    private final int _verbose;
    private final Properties _prop;
    public static final String[] KEYS
	= { "Host", "Port", "CalledTitle", "CallingTitle", "MaxPduSize",
	    "Grouplens", "SOP.Verification",
	    "SOP.BasicGrayscalePrintManagement",
	    "SOP.BasicColorPrintManagement", "SOP.BasicAnnotationBox",
	    "SOP.BasicPrintImageOverlayBox", "SOP.PresentationLUT",
	    "SOP.PrintJob", "SOP.PrinterConfigurationRetrieval",
	    "Session.NumberOfCopies", "Session.PrintPriority",
	    "Session.MediumType", "Session.FilmDestination",
	    "Session.FilmSessionLabel", "Session.MemoryAllocation",
	    "Session.OwnerID", "FilmBox.ImageDisplayFormat",
	    "FilmBox.FilmOrientation", "FilmBox.FilmSizeID",
	    "FilmBox.RequestedResolutionID",
	    "FilmBox.AnnotationDisplayFormatID", "FilmBox.MagnificationType",
	    "FilmBox.SmoothingType", "FilmBox.BorderDensity",
	    "FilmBox.EmptyImageDensity", "FilmBox.MinDensity",
	    "FilmBox.MaxDensity", "FilmBox.Trim",
	    "FilmBox.ConfigurationInformation", "FilmBox.Illumination",
	    "FilmBox.ReflectedAmbientLight", "ImageBox.Polarity",
	    "ImageBox.MagnificationType", "ImageBox.SmoothingType",
	    "ImageBox.MinDensity", "ImageBox.MaxDensity",
	    "ImageBox.ConfigurationInformation",
	    "ImageBox.RequestedDecimateCropBehavior",
	    "ImageBox.RequestedImageSize", "LUT.Shape", "LUT.Gamma",
	    "LUT.Level", "LUT.ScaleToFitBitDepth", "LUT.ApplyBySCU",
	    "User.SendAspectRatio", "User.RequestedZoom", "User.BurnInInfo",
	    "User.BurnInInfo.Properties", "User.BitDepth",
	    "User.InflateBitsAlloc", "User.MinMaxWindowing", "Verbose",
	    "DumpCmdsetIntoDir", "DumpDatasetIntoDir" };
    public static Hashtable CHECKS = new Hashtable();
    private static final String[] PRINT_PRIORITY
	= { "", "HIGH", "MED", "LOW" };
    private static final String[] MEDIUM_TYPE
	= { "", "PAPER", "CLEAR FILM", "BLUE FILM" };
    private static final String[] FILM_DESTINATION
	= { "", "MAGAZINE", "PROCESSOR", "BIN_1", "BIN_2", "BIN_3", "BIN_4",
	    "BIN_5", "BIN_6", "BIN_7", "BIN_8" };
    private static final String[] IMAGE_DISPLAY_FORMAT
	= { "STANDARD\\1,1", "STANDARD\\2,3", "ROW\\2", "COL\\2", "SLIDE",
	    "SUPERSLIDE", "CUSTOM\\1" };
    private static final String[] FILM_ORIENTATION
	= { "", "PORTRAIT", "LANDSCAPE" };
    private static final String[] FILM_SIZE_ID
	= { "", "8INX10IN", "10INX12IN", "10INX14IN", "11INX14IN", "14INX14IN",
	    "14INX17IN", "24CMX24CM", "24CMX30CM" };
    private static final String[] MAGNIFICATION_TYPE
	= { "", "REPLICATE", "BILINEAR", "CUBIC", "NONE" };
    private static final String[] DENSITY = { "", "BLACK", "WHITE" };
    private static final String[] YES_NO = { "", "YES", "NO" };
    private static final String[] REQUESTED_RESOLUTION_ID
	= { "", "STANDARD", "HIGH" };
    private static final String[] POLARITY = { "", "NORMAL", "REVERSE" };
    private static final String[] REQUESTED_DECIMATE_CROP_BEHAVIOR
	= { "", "DECIMATE", "CROP", "FAIL" };
    private static final String[] SEND_ASPECTRATIO = { "Always", "IfNot1/1" };
    private static final String[] BURNIN_INFO
	= { "No", "IfNoOverlays", "Always" };
    static final int LUT_FILE = 0;
    static final int LUT_GAMMA = 1;
    static final int LUT_IDENTITY = 2;
    static final int LUT_LIN_OD = 3;
    static final int LUT_INVERSE = 4;
    private static final String[] LUT_SHAPE
	= { "<file>", "<gamma>", "IDENTITY", "LIN OD", "INVERSE" };
    private static final String[] LUT_LEVEL
	= { "FilmSession", "FilmBox", "ImageBox" };
    private static final String[] INFLATE_BIT_DEPTH
	= { "Always", "IfNonLinear", "No" };
    private static final String[] VERBOSE = { "0", "1", "2", "3", "4", "5" };
    
    public PrintSCUParam(Properties properties)
	throws IllegalArgumentException {
	_prop = (Properties) properties.clone();
	CheckParam.verify(_prop, CHECKS);
	_port = Integer.parseInt(_prop.getProperty("Port"));
	maxPduSize = Integer.parseInt(_prop.getProperty("MaxPduSize"));
	grouplens = parseBoolean(_prop.getProperty("Grouplens"));
	_verification = parseBoolean(_prop.getProperty("SOP.Verification"));
	_basicGrayscalePrintManagement
	    = (parseBoolean
	       (_prop.getProperty("SOP.BasicGrayscalePrintManagement")));
	_basicColorPrintManagement
	    = parseBoolean(_prop.getProperty("SOP.BasicColorPrintManagement"));
	_basicAnnotationBox
	    = parseBoolean(_prop.getProperty("SOP.BasicAnnotationBox"));
	_basicPrintImageOverlayBox
	    = parseBoolean(_prop.getProperty("SOP.BasicPrintImageOverlayBox"));
	_presentationLUT
	    = parseBoolean(_prop.getProperty("SOP.PresentationLUT"));
	_printJob = parseBoolean(_prop.getProperty("SOP.PrintJob"));
	_printerConfigurationRetrieval
	    = (parseBoolean
	       (_prop.getProperty("SOP.PrinterConfigurationRetrieval")));
	lutShape = indexOf(LUT_SHAPE, _prop.getProperty("LUT.Shape"));
	lutGamma = Double.parseDouble(_prop.getProperty("LUT.Gamma"));
	lutLevel = indexOf(LUT_LEVEL, _prop.getProperty("LUT.Level"));
	lutApplyBySCU = parseBoolean(_prop.getProperty("LUT.ApplyBySCU"));
	lutScaleToFitBitDepth
	    = parseBoolean(_prop.getProperty("LUT.ScaleToFitBitDepth"));
	String string = _prop.getProperty("User.RequestedZoom");
	_requestedZoom = (string != null && string.length() > 0
			  ? Integer.parseInt(string) : 0);
	sendAspectRatioAlways
	    = indexOf(SEND_ASPECTRATIO,
		      _prop.getProperty("User.SendAspectRatio")) == 0;
	_burnInInfo
	    = indexOf(BURNIN_INFO, _prop.getProperty("User.BurnInInfo"));
	bitDepth = Integer.parseInt(_prop.getProperty("User.BitDepth"));
	inflateBitsAlloc = indexOf(INFLATE_BIT_DEPTH,
				   _prop.getProperty("User.InflateBitsAlloc"));
	minMaxWindowing
	    = parseBoolean(_prop.getProperty("User.MinMaxWindowing"));
	_verbose = Integer.parseInt(_prop.getProperty("Verbose"));
	String string_0_ = _prop.getProperty("DumpCmdsetIntoDir");
	Debug.dumpCmdsetIntoDir
	    = string_0_ != null && string_0_.length() > 0 ? string_0_ : null;
	string_0_ = _prop.getProperty("DumpDatasetIntoDir");
	Debug.dumpDatasetIntoDir
	    = string_0_ != null && string_0_.length() > 0 ? string_0_ : null;
    }
    
    public String toString() {
	StringBuffer stringbuffer = new StringBuffer();
	stringbuffer.append("Param:\n");
	for (int i = 0; i < KEYS.length; i++)
	    stringbuffer.append(KEYS[i]).append('=').append
		(_prop.getProperty(KEYS[i])).append('\n');
	return stringbuffer.toString();
    }
    
    public String getHost() {
	return _prop.getProperty("Host");
    }
    
    public String getCallingTitle() {
	return _prop.getProperty("CallingTitle");
    }
    
    public String getCalledTitle() {
	return _prop.getProperty("CalledTitle");
    }
    
    public String getLUTShape() {
	return _prop.getProperty("LUT.Shape");
    }
    
    public int getMaxPduSize() {
	return maxPduSize;
    }
    
    public int getRequestedZoom() {
	return _requestedZoom;
    }
    
    public String getBurnInInfoProperties() {
	return _prop.getProperty("User.BurnInInfo.Properties");
    }
    
    public int getPort() {
	return _port;
    }
    
    public boolean isGrouplens() {
	return grouplens;
    }
    
    public boolean isVerification() {
	return _verification;
    }
    
    public boolean isBasicGrayscalePrintManagement() {
	return _basicGrayscalePrintManagement;
    }
    
    public boolean isBasicColorPrintManagement() {
	return _basicColorPrintManagement;
    }
    
    public boolean isBasicAnnotationBox() {
	return _basicAnnotationBox;
    }
    
    public boolean isBasicPrintImageOverlayBox() {
	return _basicPrintImageOverlayBox;
    }
    
    public boolean isPresentationLUT() {
	return _presentationLUT;
    }
    
    public boolean isPrintJob() {
	return _printJob;
    }
    
    public boolean isPrinterConfigurationRetrieval() {
	return _printerConfigurationRetrieval;
    }
    
    public int[] getAbstractSyntaxes() {
	int[] is = new int[8];
	int i = 0;
	if (_verification)
	    is[i++] = 4097;
	if (_basicGrayscalePrintManagement)
	    is[i++] = 12292;
	if (_basicColorPrintManagement)
	    is[i++] = 12294;
	if (_basicAnnotationBox)
	    is[i++] = 4114;
	if (_basicPrintImageOverlayBox)
	    is[i++] = 4160;
	if (_presentationLUT)
	    is[i++] = 4145;
	if (_printJob)
	    is[i++] = 4113;
	if (_printerConfigurationRetrieval)
	    is[i++] = 4175;
	int[] is_1_ = new int[i];
	System.arraycopy(is, 0, is_1_, 0, i);
	return is_1_;
    }
    
    public boolean isLutFile() {
	return lutShape == 0;
    }
    
    public boolean isLutGamma() {
	return lutShape == 1;
    }
    
    public boolean isLutShape() {
	return lutShape == 2 || lutShape == 3 || lutShape == 4;
    }
    
    public double getLutGamma() {
	return lutGamma;
    }
    
    public boolean isLutApplyBySCU() {
	return lutApplyBySCU;
    }
    
    public boolean isLutScaleToFitBitDepth() {
	return lutScaleToFitBitDepth;
    }
    
    public boolean isSendAspectRatioAlways() {
	return sendAspectRatioAlways;
    }
    
    public int getBurnInInfo() {
	return _burnInInfo;
    }
    
    public int getLutLevel() {
	return lutLevel;
    }
    
    public int getBitDepth() {
	return bitDepth;
    }
    
    public int getInflateBitsAlloc() {
	return inflateBitsAlloc;
    }
    
    public boolean isMinMaxWindowing() {
	return minMaxWindowing;
    }
    
    public int getVerbose() {
	return _verbose;
    }
    
    public Properties getProperties() {
	return _prop;
    }
    
    private static boolean parseBoolean(String string) {
	return string != null && "true".compareTo(string.toLowerCase()) == 0;
    }
    
    private int indexOf(String[] strings, String string) {
	int i = strings.length;
	while (i-- > 0 && !strings[i].equals(string)) {
	    /* empty */
	}
	return i;
    }
    
    static {
	CHECKS.put("Port", CheckParam.range(100, 65535));
	CHECKS.put("MaxPduSize", CheckParam.range(0, 65535));
	CHECKS.put("Grouplens", CheckParam.bool());
	CHECKS.put("SOP.Verification", CheckParam.bool());
	CHECKS.put("SOP.BasicGrayscalePrintManagement", CheckParam.bool());
	CHECKS.put("SOP.BasicColorPrintManagement", CheckParam.bool());
	CHECKS.put("SOP.BasicAnnotationBox", CheckParam.bool());
	CHECKS.put("SOP.BasicPrintImageOverlayBox", CheckParam.bool());
	CHECKS.put("SOP.PresentationLUT", CheckParam.bool());
	CHECKS.put("SOP.PrintJob", CheckParam.bool());
	CHECKS.put("SOP.PrinterConfigurationRetrieval", CheckParam.bool());
	CHECKS.put("Session.PrintPriority",
		   CheckParam.defined(PRINT_PRIORITY, 3));
	CHECKS.put("Session.MediumType", CheckParam.defined(MEDIUM_TYPE, 3));
	CHECKS.put("Session.FilmDestination",
		   CheckParam.defined(FILM_DESTINATION, 3));
	CHECKS.put("FilmBox.ImageDisplayFormat",
		   CheckParam.defined(IMAGE_DISPLAY_FORMAT, 1));
	CHECKS.put("FilmBox.FilmOrientation",
		   CheckParam.enum(FILM_ORIENTATION, 3));
	CHECKS.put("FilmBox.FilmSizeID", CheckParam.defined(FILM_SIZE_ID, 3));
	CHECKS.put("FilmBox.MagnificationType",
		   CheckParam.defined(MAGNIFICATION_TYPE, 3));
	CHECKS.put("FilmBox.BorderDensity", CheckParam.defined(DENSITY, 3));
	CHECKS.put("FilmBox.EmptyImageDensity",
		   CheckParam.defined(DENSITY, 3));
	CHECKS.put("FilmBox.Trim", CheckParam.enum(YES_NO, 3));
	CHECKS.put("FilmBox.RequestedResolutionID",
		   CheckParam.defined(REQUESTED_RESOLUTION_ID, 3));
	CHECKS.put("ImageBox.Polarity", CheckParam.enum(POLARITY, 3));
	CHECKS.put("ImageBox.MagnificationType",
		   CheckParam.defined(MAGNIFICATION_TYPE, 3));
	CHECKS.put("ImageBox.RequestedDecimateCropBehavior",
		   CheckParam.enum(REQUESTED_DECIMATE_CROP_BEHAVIOR, 3));
	CHECKS.put("User.SendAspectRatio", CheckParam.enum(SEND_ASPECTRATIO));
	CHECKS.put("User.RequestedZoom", CheckParam.range(10, 400, 3));
	CHECKS.put("User.BurnInInfo", CheckParam.enum(BURNIN_INFO));
	CHECKS.put("User.BitDepth", CheckParam.range(8, 16));
	CHECKS.put("User.InflateBitsAlloc",
		   CheckParam.enum(INFLATE_BIT_DEPTH));
	CHECKS.put("User.MinMaxWindowing", CheckParam.bool());
	CHECKS.put("LUT.Shape", CheckParam.enum(LUT_SHAPE));
	CHECKS.put("LUT.Gamma", CheckParam.range(0.1, 10.0));
	CHECKS.put("LUT.Level", CheckParam.enum(LUT_LEVEL));
	CHECKS.put("LUT.ApplyBySCU", CheckParam.bool());
	CHECKS.put("LUT.ScaleToFitBitDepth", CheckParam.bool());
	CHECKS.put("Verbose", CheckParam.enum(VERBOSE));
    }
}
