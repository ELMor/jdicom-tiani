// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   PrintSCU.java

package com.tiani.dicom.client;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.overlay.AttribOverlayFactory;
import com.tiani.dicom.overlay.BurnInOverlay;
import com.tiani.dicom.overlay.Overlay;
import com.tiani.dicom.print.PrintManagementSCU;
import com.tiani.dicom.print.PrintManagementUtils;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.EnumPMI;
import com.tiani.dicom.util.UIDUtils;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.AbstractButton;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.tiani.dicom.client:
//            PrintSCUParam

public class PrintSCU extends JApplet
{
    private abstract class MyActionListener
        implements ActionListener, Runnable
    {

        public void actionPerformed(ActionEvent actionevent)
        {
            try
            {
                param = new PrintSCUParam(properties);
                Debug.DEBUG = param.getVerbose();
                if(preprocess())
                {
                    tabbedPane.setSelectedIndex(1);
                    (new Thread(this)).start();
                }
            }
            catch(Exception exception)
            {
                if(Debug.DEBUG > 1)
                    exception.printStackTrace(log);
                else
                    log.println(exception);
            }
        }

        boolean preprocess()
            throws Exception
        {
            return true;
        }

        public void run()
        {
            try
            {
                execute();
            }
            catch(Exception exception)
            {
                log.println(exception);
            }
            enableButtons();
        }

        abstract void execute()
            throws Exception;

        private MyActionListener()
        {
        }

    }


    private JLabel assocLabel;
    private JButton connectButton;
    private JButton echoButton;
    private JButton releaseButton;
    private JToggleButton colorButton;
    private JLabel printerLabel;
    private JButton getPrinterButton;
    private JLabel configurationLabel;
    private JButton getConfigurationButton;
    private JLabel printJobLabel;
    private JButton getPrintJobButton;
    private JLabel sessionLabel;
    private JButton crSessionButton;
    private JButton setSessionButton;
    private JButton printSessionButton;
    private JButton delSessionButton;
    private JLabel filmBoxLabel;
    private JButton crFilmBoxButton;
    private JButton setFilmBoxButton;
    private JButton printFilmBoxButton;
    private JButton delFilmBoxButton;
    private JLabel imageBoxLabel;
    private JButton setImageBoxButton;
    private JComboBox setImageBoxComboBox;
    private JLabel annotationLabel;
    private JButton setAnnotationButton;
    private JComboBox setAnnotationComboBox;
    private JLabel overlayLabel;
    private JButton crOverlayButton;
    private JButton setOverlayButton;
    private JButton delOverlayButton;
    private JLabel lutLabel;
    private JButton crLUTButton;
    private JButton delLUTButton;
    private JPanel controlPanel;
    private JTabbedPane tabbedPane;
    private JTextArea logTextArea;
    private Document logDoc;
    private PrintStream log;
    private JFileChooser fileChooser;
    private Properties properties;
    private PrintSCUParam param;
    private String curPrintJobUid;
    private String curLutUid;
    private String annotationText;
    private DicomObject image;
    private final DicomObject lutAttribs;
    private final PrintManagementSCU scu;
    private final DicomObject IDENTITY_PLUT;
    private final ActionListener onConnect;
    private final ActionListener onRelease;
    private final ActionListener onEcho;
    private final ActionListener onGetPrinter;
    private final ActionListener onGetConfiguration;
    private final ActionListener onGetPrintJob;
    private final ActionListener onCreateSession;
    private final ActionListener onSetSession;
    private final ActionListener onPrintSession;
    private final ActionListener onDeleteSession;
    private final ActionListener onCreateFilmBox;
    private final ActionListener onSetFilmBox;
    private final ActionListener onPrintFilmBox;
    private final ActionListener onDeleteFilmBox;
    private final ActionListener onSetImageBox;
    private final ActionListener onSetAnnotation;
    private final ActionListener onCreateLUT;
    private final ActionListener onDeleteLUT;
    private final ActionListener onCreateOverlay;
    private final ActionListener onSetOverlay;
    private final ActionListener onDeleteOverlay;

    private int prepareOverlays(DicomObject dicomobject)
        throws DicomException
    {
        int i = dicomobject.getI(466);
        int j = dicomobject.getI(467);
        int k = Overlay.listOverlayGroups(dicomobject).length;
        if(param.getBurnInInfo() > (k <= 0 ? 0 : 1))
        {
            int l = Overlay.getFreeOverlayGroup(dicomobject);
            if(l == -1)
            {
                log.println("No free overlay group 60xx to burn in attribute info");
            } else
            {
                if(param.getVerbose() > 0)
                    log.println("Create info overlay");
                createOverlayFactory().createOverlay(i, j & -8, dicomobject, l);
                k++;
            }
        }
        return k;
    }

    private AttribOverlayFactory createOverlayFactory()
    {
        Properties properties1 = null;
        String s = param.getBurnInInfoProperties();
        if(s.length() > 0)
            try
            {
                FileInputStream fileinputstream = new FileInputStream(s);
                try
                {
                    properties1 = new Properties();
                    properties1.load(fileinputstream);
                }
                finally
                {
                    fileinputstream.close();
                }
            }
            catch(Exception exception)
            {
                Debug.out.println(exception);
                properties1 = null;
            }
        return new AttribOverlayFactory(properties1);
    }

    private void enableButtons()
    {
        try
        {
            connectButton.setEnabled(!scu.isConnected());
            echoButton.setEnabled(scu.isEnabled(4097));
            releaseButton.setEnabled(scu.isConnected());
            boolean flag = scu.isEnabled(12294) || scu.isEnabled(12292);
            if(flag)
            {
                boolean flag1 = colorButton.isSelected();
                if(!scu.isEnabled(flag1 ? 12294 : 12292))
                    colorButton.setSelected(!flag1);
            }
            colorButton.setEnabled(flag && !scu.isFilmSession() && scu.isEnabled(12294) && scu.isEnabled(12292));
            getPrinterButton.setEnabled(flag);
            getConfigurationButton.setEnabled(scu.isEnabled(4175));
            getPrintJobButton.setEnabled(curPrintJobUid != null && scu.isEnabled(4113));
            crSessionButton.setEnabled(flag);
            setSessionButton.setEnabled(scu.isFilmSession());
            printSessionButton.setEnabled(scu.isFilmSession());
            delSessionButton.setEnabled(scu.isFilmSession());
            crFilmBoxButton.setEnabled(scu.isFilmSession());
            setFilmBoxButton.setEnabled(scu.isFilmBox());
            printFilmBoxButton.setEnabled(scu.isFilmBox());
            delFilmBoxButton.setEnabled(scu.isFilmBox());
            setImageBoxButton.setEnabled(scu.isFilmBox());
            int i;
            if((i = scu.isFilmBox() ? scu.countImageBoxes() : 0) != setImageBoxComboBox.getItemCount())
                setImageBoxComboBox.setModel(createComboBoxModel(i));
            boolean flag2 = scu.isFilmBox() && scu.isEnabled(4114);
            setAnnotationButton.setEnabled(flag2);
            if((i = !flag2 || !scu.isFilmBox() ? 0 : scu.countAnnotationBoxes()) != setAnnotationComboBox.getItemCount())
                setAnnotationComboBox.setModel(createComboBoxModel(i));
            crLUTButton.setEnabled(scu.isConnected());
            delLUTButton.setEnabled(scu.isConnected() && curLutUid != null);
            crOverlayButton.setEnabled(false);
            setOverlayButton.setEnabled(false);
            delOverlayButton.setEnabled(false);
        }
        catch(Exception exception)
        {
            log.println(exception);
        }
    }

    private ComboBoxModel createComboBoxModel(int i)
    {
        Integer ainteger[] = new Integer[i];
        for(int j = 0; j < i; j++)
            ainteger[j] = new Integer(j + 1);

        return new DefaultComboBoxModel(ainteger);
    }

    private void initControlPanel()
    {
        controlPanel.add(assocLabel);
        controlPanel.add(printerLabel);
        controlPanel.add(sessionLabel);
        controlPanel.add(filmBoxLabel);
        controlPanel.add(imageBoxLabel);
        controlPanel.add(annotationLabel);
        controlPanel.add(new JTianiButton(isApplet() ? getAppletContext() : null));
        controlPanel.add(connectButton);
        controlPanel.add(getPrinterButton);
        controlPanel.add(crSessionButton);
        controlPanel.add(crFilmBoxButton);
        controlPanel.add(setImageBoxButton);
        controlPanel.add(setAnnotationButton);
        controlPanel.add(overlayLabel);
        controlPanel.add(echoButton);
        controlPanel.add(lutLabel);
        controlPanel.add(setSessionButton);
        controlPanel.add(setFilmBoxButton);
        controlPanel.add(setImageBoxComboBox);
        controlPanel.add(setAnnotationComboBox);
        controlPanel.add(crOverlayButton);
        controlPanel.add(releaseButton);
        controlPanel.add(crLUTButton);
        controlPanel.add(printSessionButton);
        controlPanel.add(printFilmBoxButton);
        controlPanel.add(printJobLabel);
        controlPanel.add(configurationLabel);
        controlPanel.add(setOverlayButton);
        controlPanel.add(colorButton);
        controlPanel.add(delLUTButton);
        controlPanel.add(delSessionButton);
        controlPanel.add(delFilmBoxButton);
        controlPanel.add(getPrintJobButton);
        controlPanel.add(getConfigurationButton);
        controlPanel.add(delOverlayButton);
    }

    private void initActionListeners()
    {
        connectButton.addActionListener(onConnect);
        echoButton.addActionListener(onEcho);
        releaseButton.addActionListener(onRelease);
        getPrinterButton.addActionListener(onGetPrinter);
        getConfigurationButton.addActionListener(onGetConfiguration);
        getPrintJobButton.addActionListener(onGetPrintJob);
        crSessionButton.addActionListener(onCreateSession);
        setSessionButton.addActionListener(onSetSession);
        printSessionButton.addActionListener(onPrintSession);
        delSessionButton.addActionListener(onDeleteSession);
        crFilmBoxButton.addActionListener(onCreateFilmBox);
        setFilmBoxButton.addActionListener(onSetFilmBox);
        printFilmBoxButton.addActionListener(onPrintFilmBox);
        delFilmBoxButton.addActionListener(onDeleteFilmBox);
        setImageBoxButton.addActionListener(onSetImageBox);
        setAnnotationButton.addActionListener(onSetAnnotation);
        crLUTButton.addActionListener(onCreateLUT);
        delLUTButton.addActionListener(onDeleteLUT);
        crOverlayButton.addActionListener(onCreateOverlay);
        setOverlayButton.addActionListener(onSetOverlay);
        delOverlayButton.addActionListener(onDeleteOverlay);
    }

    private boolean isApplet()
    {
        return !(getAppletContext() instanceof AppletFrame);
    }

    public static void main(String args[])
    {
        try
        {
            final String propFile = args.length <= 0 ? "PrintSCU.properties" : args[0];
            final PrintSCU applet = new PrintSCU(loadProperties(propFile));
            new AppletFrame("PrintSCU v1.7.30", applet, 700, 500, new WindowAdapter() {

                public void windowClosing(WindowEvent windowevent)
                {
                    try
                    {
                        PrintSCU.storeProperties(propFile, applet.properties);
                    }
                    catch(Exception exception)
                    {
                        System.out.println(exception);
                    }
                    System.exit(0);
                }

            });
        }
        catch(Throwable throwable)
        {
            System.out.println(throwable);
        }
    }

    public PrintSCU()
    {
        assocLabel = new JLabel("Association", 0);
        connectButton = new JButton("Connect");
        echoButton = new JButton("Echo");
        releaseButton = new JButton("Release");
        colorButton = new JToggleButton("Color");
        printerLabel = new JLabel("Printer", 0);
        getPrinterButton = new JButton("Get");
        configurationLabel = new JLabel("Configuration", 0);
        getConfigurationButton = new JButton("Get");
        printJobLabel = new JLabel("PrintJob", 0);
        getPrintJobButton = new JButton("Get");
        sessionLabel = new JLabel("Session", 0);
        crSessionButton = new JButton("Create");
        setSessionButton = new JButton("Set");
        printSessionButton = new JButton("Print");
        delSessionButton = new JButton("Delete");
        filmBoxLabel = new JLabel("FilmBox", 0);
        crFilmBoxButton = new JButton("Create");
        setFilmBoxButton = new JButton("Set");
        printFilmBoxButton = new JButton("Print");
        delFilmBoxButton = new JButton("Delete");
        imageBoxLabel = new JLabel("ImageBox", 0);
        setImageBoxButton = new JButton("Set");
        setImageBoxComboBox = new JComboBox();
        annotationLabel = new JLabel("Annotation", 0);
        setAnnotationButton = new JButton("Set");
        setAnnotationComboBox = new JComboBox();
        overlayLabel = new JLabel("Overlay", 0);
        crOverlayButton = new JButton("Create");
        setOverlayButton = new JButton("Set");
        delOverlayButton = new JButton("Delete");
        lutLabel = new JLabel("Pres.LUT", 0);
        crLUTButton = new JButton("Create");
        delLUTButton = new JButton("Delete");
        controlPanel = new JPanel(new GridLayout(0, 7));
        tabbedPane = new JTabbedPane(2);
        logTextArea = new JTextArea();
        logDoc = logTextArea.getDocument();
        log = new PrintStream(new DocumentOutputStream(logDoc, 20000), true);
        fileChooser = null;
        properties = null;
        param = null;
        curPrintJobUid = null;
        curLutUid = null;
        annotationText = null;
        image = null;
        lutAttribs = new DicomObject();
        scu = new PrintManagementSCU();
        IDENTITY_PLUT = new DicomObject();
        onConnect = new MyActionListener() {

            void execute()
                throws Exception
            {
                try
                {
                    scu.setMaxPduSize(param.getMaxPduSize());
                    scu.setGrouplens(param.isGrouplens());
                    scu.connect(param.getHost(), param.getPort(), param.getCalledTitle(), param.getCallingTitle(), param.getAbstractSyntaxes());
                }
                catch(SecurityException securityexception)
                {
                    log.println(securityexception.getMessage());
                    showPolicyFile();
                }
            }

        };
        onRelease = new MyActionListener() {

            void execute()
                throws Exception
            {
                scu.release();
                curLutUid = null;
            }

        };
        onEcho = new MyActionListener() {

            void execute()
                throws Exception
            {
                scu.echo();
            }

        };
        onGetPrinter = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomMessage dicommessage = colorButton.isSelected() ? scu.getColorPrinterStatus() : scu.getGrayscalePrinterStatus();
                DicomObject dicomobject = dicommessage.getDataset();
                if(dicomobject != null)
                    dicomobject.dumpVRs(log);
            }

        };
        onGetConfiguration = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomMessage dicommessage = scu.getPrinterConfiguration();
                DicomObject dicomobject = dicommessage.getDataset();
                if(dicomobject != null)
                    dicomobject.dumpVRs(log);
            }

        };
        onGetPrintJob = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomMessage dicommessage = scu.getPrintJobStatus(curPrintJobUid);
                DicomObject dicomobject = dicommessage.getDataset();
                if(dicomobject != null)
                    dicomobject.dumpVRs(log);
            }

        };
        onCreateSession = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomObject dicomobject = new DicomObject();
                PrintManagementUtils.setFilmSessionCreateAttribs(dicomobject, param.getProperties());
                if(curLutUid != null && !param.isLutApplyBySCU() && param.getLutLevel() == 0)
                    PrintManagementUtils.addReferencedPresentationLUT(dicomobject, curLutUid);
                DicomMessage dicommessage = colorButton.isSelected() ? scu.createColorFilmSession(dicomobject) : scu.createGrayscaleFilmSession(dicomobject);
            }

        };
        onSetSession = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomObject dicomobject = new DicomObject();
                PrintManagementUtils.setFilmSessionSetAttribs(dicomobject, param.getProperties());
                if(curLutUid != null && !param.isLutApplyBySCU() && param.getLutLevel() == 0)
                    PrintManagementUtils.addReferencedPresentationLUT(dicomobject, curLutUid);
                scu.setFilmSession(dicomobject);
            }

        };
        onPrintSession = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomMessage dicommessage = scu.printFilmSession();
                DicomObject dicomobject;
                DicomObject dicomobject1;
                if((dicomobject = dicommessage.getDataset()) != null && (dicomobject1 = (DicomObject)dicomobject.get(758)) != null)
                    curPrintJobUid = (String)dicomobject1.get(116);
                else
                    curPrintJobUid = null;
            }

        };
        onDeleteSession = new MyActionListener() {

            void execute()
                throws Exception
            {
                scu.deleteFilmSession();
                curLutUid = null;
            }

        };
        onCreateFilmBox = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomObject dicomobject = new DicomObject();
                PrintManagementUtils.setFilmBoxCreateAttribs(dicomobject, param.getProperties());
                if(curLutUid != null && !param.isLutApplyBySCU() && param.getLutLevel() == 1)
                    PrintManagementUtils.addReferencedPresentationLUT(dicomobject, curLutUid);
                scu.createFilmBox(dicomobject);
            }

        };
        onSetFilmBox = new MyActionListener() {

            void execute()
                throws Exception
            {
                DicomObject dicomobject = new DicomObject();
                PrintManagementUtils.setFilmBoxSetAttribs(dicomobject, param.getProperties());
                if(curLutUid != null && !param.isLutApplyBySCU() && param.getLutLevel() == 1)
                    PrintManagementUtils.addReferencedPresentationLUT(dicomobject, curLutUid);
                scu.setFilmBox(dicomobject);
            }

        };
        onPrintFilmBox = new MyActionListener() {

            void execute()
                throws Exception
            {
                scu.printFilmBox();
            }

        };
        onDeleteFilmBox = new MyActionListener() {

            void execute()
                throws Exception
            {
                scu.deleteFilmBox();
            }

        };
        onSetImageBox = new MyActionListener() {

            boolean preprocess()
                throws Exception
            {
                File file = chooseFile("select image file");
                if(file == null)
                    return false;
                TianiInputStream tianiinputstream = new TianiInputStream(new FileInputStream(file));
                image = new DicomObject();
                try
                {
                    tianiinputstream.read(image, true);
                }
                finally
                {
                    tianiinputstream.close();
                }
                if(Debug.DEBUG > 1)
                    log.println("Load from " + file + '[' + tianiinputstream.getFormatName() + '-' + UID.getUIDEntry(tianiinputstream.getTransferSyntaxId(image)).getName() + ']');
                String s = (String)image.get(462);
                if(colorButton.isSelected())
                {
                    if(EnumPMI.isGrayscale(s))
                    {
                        PrintManagementUtils.preformatGrayscale(image, null, 8, 2, param.isMinMaxWindowing());
                        PrintManagementUtils.grayscaleToColor(image);
                        if(Debug.DEBUG > 1)
                            log.println("Convert grayscale pixel attributs to color pixel attributs");
                    } else
                    {
                        PrintManagementUtils.preformatColor(image);
                    }
                } else
                {
                    if(!EnumPMI.isGrayscale(s))
                    {
                        PrintManagementUtils.preformatColor(image);
                        PrintManagementUtils.colorToGrayscale(image);
                        if(Debug.DEBUG > 1)
                            log.println("Convert color pixel attributs to grayscale pixel attributs");
                    }
                    PrintManagementUtils.preformatGrayscale(image, curLutUid == null || !param.isLutApplyBySCU() ? null : lutAttribs, param.getBitDepth(), param.getInflateBitsAlloc(), param.isMinMaxWindowing());
                }
                if(prepareOverlays(image) > 0)
                {
                    if(Debug.DEBUG > 1)
                        log.println("Burn in overlays");
                    BurnInOverlay.burnInAll(image);
                }
                return true;
            }

            void execute()
                throws Exception
            {
                int i = setImageBoxComboBox.getSelectedIndex() + 1;
                PrintManagementUtils.setPixelAspectRatio(image, param.isSendAspectRatioAlways());
                DicomObject dicomobject = new DicomObject();
                PrintManagementUtils.setImageBoxSetAttribs(dicomobject, param.getProperties());
                if(param.getRequestedZoom() > 0)
                    dicomobject.set(731, PrintManagementUtils.getRequestedImageSize(image, (float)param.getRequestedZoom() / 100F));
                if(curLutUid != null && !param.isLutApplyBySCU() && param.getLutLevel() == 2)
                    PrintManagementUtils.addReferencedPresentationLUT(dicomobject, curLutUid);
                DicomMessage dicommessage = colorButton.isSelected() ? scu.setColorImageBox(i, image, dicomobject) : scu.setGrayscaleImageBox(i, image, dicomobject);
                image = null;
            }

        };
        onSetAnnotation = new MyActionListener() {

            boolean preprocess()
                throws Exception
            {
                annotationText = JOptionPane.showInputDialog(null, "Annotation Text");
                return annotationText != null;
            }

            void execute()
                throws Exception
            {
                int i = setAnnotationComboBox.getSelectedIndex() + 1;
                scu.setAnnotationBox(i, annotationText);
            }

        };
        onCreateLUT = new MyActionListener() {

            boolean preprocess()
                throws Exception
            {
                lutAttribs.clear();
                if(param.isLutShape())
                {
                    lutAttribs.set(1240, param.getLUTShape());
                    return true;
                }
                if(param.isLutGamma())
                {
                    int i = param.getBitDepth();
                    DicomObject dicomobject = PrintManagementUtils.createGammaLUTSequenceItem(1 << i, i, param.getLutGamma());
                    lutAttribs.set(1239, dicomobject);
                    return true;
                }
                File file = chooseFile("select presentation lut file");
                if(file == null)
                    return false;
                FileInputStream fileinputstream = new FileInputStream(file);
                try
                {
                    lutAttribs.read(fileinputstream);
                }
                finally
                {
                    fileinputstream.close();
                }
                if(lutAttribs.getSize(1239) != 1)
                    throw new DicomException("Missing Presentation LUT Sequence in " + file);
                if(param.isLutScaleToFitBitDepth())
                    PrintManagementUtils.rescaleLUTtoFitBitDepth((DicomObject)lutAttribs.get(1239), param.getBitDepth());
                return true;
            }

            void execute()
                throws Exception
            {
                curLutUid = UIDUtils.createUID();
                if(scu.isEnabled(4145))
                    scu.createPresentationLUT(curLutUid, param.isLutApplyBySCU() ? IDENTITY_PLUT : lutAttribs);
            }

        };
        onDeleteLUT = new MyActionListener() {

            void execute()
                throws Exception
            {
                if(scu.isEnabled(4145))
                    scu.deletePresentationLUT(curLutUid);
                curLutUid = null;
            }

        };
        onCreateOverlay = new MyActionListener() {

            void execute()
                throws Exception
            {
            }

        };
        onSetOverlay = new MyActionListener() {

            void execute()
                throws Exception
            {
            }

        };
        onDeleteOverlay = new MyActionListener() {

            void execute()
                throws Exception
            {
            }

        };
    }

    protected PrintSCU(Properties properties1)
    {
this();        properties = properties1;
    }

    private void initProperties()
    {
        if(isApplet())
            getAppletParams();
        if(properties == null || !verifyProperties())
            try
            {
                properties = loadProperties((com.tiani.dicom.client.PrintSCU.class).getResourceAsStream("PrintSCU.properties"));
            }
            catch(IOException ioexception)
            {
                throw new RuntimeException("Failed to load PrintSCU.properties ressource");
            }
    }

    private void initTabbedPane()
    {
        JPropertiesTable jpropertiestable = new JPropertiesTable(PrintSCUParam.KEYS, properties, PrintSCUParam.CHECKS);
        tabbedPane.add("Props", new JScrollPane(jpropertiestable));
        tabbedPane.add("Log", new JAutoScrollPane(logTextArea));
    }

    public void init()
    {
        Debug.out = log;
        initProperties();
        initTabbedPane();
        initControlPanel();
        initActionListeners();
        Container container = getContentPane();
        container.add(controlPanel, "North");
        container.add(tabbedPane, "Center");
        enableButtons();
        try
        {
            IDENTITY_PLUT.set(1240, "IDENTITY");
        }
        catch(DicomException dicomexception)
        {
            dicomexception.printStackTrace(log);
        }
    }

    private File chooseFile(String s)
    {
        try
        {
            if(fileChooser == null)
            {
                fileChooser = new JFileChooser(".");
                fileChooser.setFileSelectionMode(0);
            }
            fileChooser.setDialogTitle(s);
            int i = fileChooser.showOpenDialog(null);
            File file = fileChooser.getSelectedFile();
            return i != 0 ? null : file;
        }
        catch(SecurityException securityexception)
        {
            log.println(securityexception.getMessage());
        }
        showPolicyFile();
        return null;
    }

    private void showPolicyFile()
    {
        try
        {
            URL url = new URL(getDocumentBase(), getParameter("PolicyFile"));
            getAppletContext().showDocument(url, "_blank");
        }
        catch(Exception exception)
        {
            log.println(exception.getMessage());
        }
    }

    private static Properties loadProperties(String s)
    {
        try
        {
            return loadProperties(((InputStream) (new FileInputStream(s))));
        }
        catch(Exception exception)
        {
            System.out.println(exception);
        }
        return null;
    }

    private static Properties loadProperties(InputStream inputstream)
        throws IOException
    {
        Properties properties1 = new Properties();
        try
        {
            properties1.load(inputstream);
        }
        finally
        {
            inputstream.close();
        }
        return properties1;
    }

    private boolean verifyProperties()
    {
        try
        {
            CheckParam.verify(properties, PrintSCUParam.CHECKS);
        }
        catch(IllegalArgumentException illegalargumentexception)
        {
            log.println(illegalargumentexception);
            return false;
        }
        return true;
    }

    private static void storeProperties(String s, Properties properties1)
        throws IOException
    {
        CheckParam.verify(properties1, PrintSCUParam.CHECKS);
        FileOutputStream fileoutputstream = new FileOutputStream(s);
        try
        {
            properties1.store(fileoutputstream, "Properties for PrintSCU");
        }
        finally
        {
            fileoutputstream.close();
        }
    }

    private void getAppletParams()
    {
        properties = new Properties();
        for(int i = 0; i < PrintSCUParam.KEYS.length; i++)
        {
            String s;
            if((s = getParameter(PrintSCUParam.KEYS[i])) != null)
                properties.put(PrintSCUParam.KEYS[i], s);
        }

    }
























}
