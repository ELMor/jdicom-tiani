/* StorageSCUParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.client;
import java.util.Hashtable;
import java.util.Properties;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.service.IconFactory;
import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.UIDUtils;

final class StorageSCUParam
{
    private final Properties props;
    private final int port;
    private final int storageCmtPort;
    private final boolean storageCmtMultiThreadTCP;
    private final boolean storageCmtMultiThreadAssoc;
    private final boolean storageCmtCheckAET;
    private final boolean storageCmtAutoRelease;
    private final boolean storageCmtAbort;
    private final int maxInvoke;
    private final int assocTimeout;
    private final int releaseTimeout;
    private final int maxPduSize;
    private final int priority;
    private final boolean splitMultiFrame;
    private final boolean skipPrivate;
    private final boolean anonymize;
    private final boolean dicomize;
    private final boolean resample;
    private final int maxRows;
    private final int maxColumns;
    private final int verbose;
    private final int tsid;
    private final String uidPrefix;
    private final Request request;
    private final IconFactory iconFactory;
    public static final String[] KEYS
	= { "Host", "Port", "CalledTitle", "CallingTitle", "StorageCmt.Port",
	    "StorageCmt.MultiThreadTCP", "StorageCmt.MultiThreadAssoc",
	    "StorageCmt.CheckAET", "StorageCmt.AutoRelease",
	    "StorageCmt.Abort", "AssocTimeout[ms]", "ReleaseTimeout[ms]",
	    "MaxInvoke", "MaxPduSize", "Priority", "TransferSyntax",
	    "SkipPrivate", "SplitMultiFrame", "Anonymize", "Anonymize.NewName",
	    "Anonymize.NewID", "Dicomize", "Dicomize.UIDprefix",
	    "Dicomize.StudyInstanceUID", "Dicomize.SeriesInstanceUID",
	    "Dicomize.SOPInstanceUID", "Dicomize.SOPClassUID",
	    "Dicomize.Photometric", "Resample", "Resample.MaxRows",
	    "Resample.MaxColumns", "Verbose", "DumpCmdsetIntoDir",
	    "DumpDatasetIntoDir" };
    private static final int[] DEFAULT_TS_IDS = { 8193 };
    private static final int[] JPEG_TS_IDS = { 8196 };
    private static final String[] PRIORITIES = { "MEDIUM", "HIGH", "LOW" };
    private static final String[] SOP_CLASS_UIDS
	= { "1.2.840.10008.5.1.4.1.1.1", "1.2.840.10008.5.1.4.1.1.2",
	    "1.2.840.10008.5.1.4.1.1.4", "1.2.840.10008.5.1.4.1.1.7",
	    "1.2.840.10008.5.1.4.1.1.6.1", "1.2.840.10008.5.1.4.1.1.6" };
    private static final String[] PHOTOMETRIC
	= { "MONOCHROME1", "MONOCHROME2", "RGB" };
    private static final String[] IMAGE_TRANSFERSYNTAX
	= { "ImplicitVRLittleEndian", "ExplicitVRLittleEndian",
	    "ExplicitVRBigEndian", "JPEGLossless", "JPEGBaseline" };
    private static final int[] TRANSFERSYNTAX_IDS
	= { 8193, 8194, 8195, 8197, 8196 };
    private static final String[] VERBOSE = { "0", "1", "2", "3", "4", "5" };
    public static Hashtable CHECKS = new Hashtable();
    
    public StorageSCUParam(Properties properties)
	throws IllegalArgumentException {
	props = (Properties) properties.clone();
	CheckParam.verify(props, CHECKS);
	port = Integer.parseInt(props.getProperty("Port"));
	storageCmtPort
	    = Integer.parseInt(props.getProperty("StorageCmt.Port"));
	storageCmtMultiThreadTCP
	    = parseBoolean(props.getProperty("StorageCmt.MultiThreadTCP"));
	storageCmtMultiThreadAssoc
	    = parseBoolean(props.getProperty("StorageCmt.MultiThreadAssoc"));
	storageCmtCheckAET
	    = parseBoolean(props.getProperty("StorageCmt.CheckAET"));
	storageCmtAutoRelease
	    = parseBoolean(props.getProperty("StorageCmt.AutoRelease"));
	storageCmtAbort = parseBoolean(props.getProperty("StorageCmt.Abort"));
	assocTimeout = Integer.parseInt(props.getProperty("AssocTimeout[ms]"));
	releaseTimeout
	    = Integer.parseInt(props.getProperty("ReleaseTimeout[ms]"));
	maxPduSize = Integer.parseInt(props.getProperty("MaxPduSize"));
	maxInvoke = Integer.parseInt(props.getProperty("MaxInvoke"));
	priority = indexOf(PRIORITIES, props.getProperty("Priority"));
	skipPrivate = parseBoolean(props.getProperty("SkipPrivate"));
	splitMultiFrame = parseBoolean(props.getProperty("SplitMultiFrame"));
	anonymize = parseBoolean(props.getProperty("Anonymize"));
	dicomize = parseBoolean(props.getProperty("Dicomize"));
	resample = parseBoolean(props.getProperty("Resample"));
	maxRows = Integer.parseInt(props.getProperty("Resample.MaxRows"));
	maxColumns
	    = Integer.parseInt(props.getProperty("Resample.MaxColumns"));
	verbose = Integer.parseInt(props.getProperty("Verbose"));
	String string = props.getProperty("Dicomize.UIDprefix");
	uidPrefix = string != null && string.length() > 0 ? string : null;
	tsid
	    = TRANSFERSYNTAX_IDS[indexOf(IMAGE_TRANSFERSYNTAX,
					 props.getProperty("TransferSyntax"))];
	try {
	    request = createRequest(props.getProperty("CallingTitle"),
				    props.getProperty("CalledTitle"),
				    maxPduSize, maxInvoke, tsid);
	} catch (IllegalValueException illegalvalueexception) {
	    throw new RuntimeException(illegalvalueexception.getMessage());
	}
	iconFactory = tsid == 8196 ? createIconFactory(resample, maxRows,
						       maxColumns) : null;
	string = props.getProperty("DumpCmdsetIntoDir");
	Debug.dumpCmdsetIntoDir
	    = string != null && string.length() > 0 ? string : null;
	string = props.getProperty("DumpDatasetIntoDir");
	Debug.dumpDatasetIntoDir
	    = string != null && string.length() > 0 ? string : null;
    }
    
    private static IconFactory createIconFactory(boolean bool, int i,
						 int i_0_) {
	IconFactory iconfactory
	    = bool ? new IconFactory(i, i_0_) : new IconFactory();
	iconfactory.setJpeg(true);
	iconfactory.setJpegMultiframe(true);
	return iconfactory;
    }
    
    private static Request createRequest
	(String string, String string_1_, int i, int i_2_, int i_3_)
	throws IllegalValueException {
	int[] is = { i_3_, 8193 };
	int[] is_4_ = DEFAULT_TS_IDS;
	switch (i_3_) {
	case 8193:
	    is = DEFAULT_TS_IDS;
	    break;
	case 8194:
	case 8195:
	    is_4_ = is;
	    break;
	}
	Request request = new Request();
	request.setCalledTitle(string_1_);
	request.setCallingTitle(string);
	request.setMaxPduSize(i);
	if (i_2_ != 1) {
	    request.setMaxOperationsInvoked(i_2_);
	    request.setMaxOperationsPerformed(1);
	}
	request.addPresentationContext(4097, is_4_);
	request.addPresentationContext(4100, is_4_);
	request.addPresentationContext(4165, is_4_);
	request.addPresentationContext(4166, is_4_);
	request.addPresentationContext(4167, is_4_);
	request.addPresentationContext(4168, is_4_);
	request.addPresentationContext(4194, is_4_);
	request.addPresentationContext(4196, is_4_);
	request.addPresentationContext(4195, is_4_);
	if (i_3_ == 8196)
	    request.addPresentationContext(4123, JPEG_TS_IDS);
	else {
	    request.addPresentationContext(4118, is);
	    request.addPresentationContext(4119, is);
	    request.addPresentationContext(4120, is);
	    request.addPresentationContext(4121, is);
	    request.addPresentationContext(4122, is);
	    request.addPresentationContext(4123, is);
	    request.addPresentationContext(4128, is);
	    request.addPresentationContext(4129, is);
	    request.addPresentationContext(4130, is);
	    request.addPresentationContext(4131, is);
	    request.addPresentationContext(4148, is);
	    request.addPresentationContext(4149, is);
	    request.addPresentationContext(4151, is);
	    request.addPresentationContext(4153, is);
	    request.addPresentationContext(4157, is);
	    request.addPresentationContext(4158, is);
	    request.addPresentationContext(4159, is);
	    request.addPresentationContext(4161, is);
	    request.addPresentationContext(4162, is);
	    request.addPresentationContext(4163, is);
	    request.addPresentationContext(4164, is);
	    request.addPresentationContext(4176, is);
	    request.addPresentationContext(4177, is);
	    request.addPresentationContext(4178, is);
	    request.addPresentationContext(4179, is);
	    request.addPresentationContext(4180, is);
	    request.addPresentationContext(4181, is);
	}
	request.addPresentationContext(4124, is_4_);
	request.addPresentationContext(4125, is_4_);
	request.addPresentationContext(4126, is_4_);
	request.addPresentationContext(4127, is_4_);
	request.addPresentationContext(4152, is_4_);
	request.addPresentationContext(4154, is_4_);
	request.addPresentationContext(4155, is_4_);
	request.addPresentationContext(4156, is_4_);
	request.addPresentationContext(4182, is_4_);
	request.addPresentationContext(4183, is_4_);
	request.addPresentationContext(4184, is_4_);
	request.addPresentationContext(4147, is_4_);
	request.addPresentationContext(4099, is_4_);
	request.addPresentationContext(4197, is_4_);
	return request;
    }
    
    public String toString() {
	StringBuffer stringbuffer = new StringBuffer();
	stringbuffer.append("Param:\n");
	for (int i = 0; i < KEYS.length; i++)
	    stringbuffer.append(KEYS[i]).append('=').append
		(props.getProperty(KEYS[i])).append('\n');
	return stringbuffer.toString();
    }
    
    public String getHost() {
	return props.getProperty("Host");
    }
    
    public int getPort() {
	return port;
    }
    
    public int getStorageCmtPort() {
	return storageCmtPort;
    }
    
    public String getCallingTitle() {
	return props.getProperty("CallingTitle");
    }
    
    public String getCalledTitle() {
	return props.getProperty("CalledTitle");
    }
    
    public int getReleaseTimeout() {
	return releaseTimeout;
    }
    
    public int getAssocTimeout() {
	return assocTimeout;
    }
    
    public int getMaxPduSize() {
	return maxPduSize;
    }
    
    public int getMaxInvoke() {
	return maxInvoke;
    }
    
    public Request getRequest() {
	return request;
    }
    
    public int getTSID() {
	return tsid;
    }
    
    public int getPriority() {
	return priority;
    }
    
    public boolean isSkipPrivate() {
	return skipPrivate;
    }
    
    public boolean isSplitMultiFrame() {
	return splitMultiFrame;
    }
    
    public boolean isAnonymize() {
	return anonymize;
    }
    
    public boolean isStorageCmtMultiThreadTCP() {
	return storageCmtMultiThreadTCP;
    }
    
    public boolean isStorageCmtMultiThreadAssoc() {
	return storageCmtMultiThreadAssoc;
    }
    
    public boolean isStorageCmtCheckAET() {
	return storageCmtCheckAET;
    }
    
    public boolean isStorageCmtAutoRelease() {
	return storageCmtAutoRelease;
    }
    
    public boolean isStorageCmtAbort() {
	return storageCmtAbort;
    }
    
    public String getAnonymizeNewName() {
	return props.getProperty("Anonymize.NewName");
    }
    
    public String getAnonymizeNewID() {
	return props.getProperty("Anonymize.NewID");
    }
    
    public boolean isDicomize() {
	return dicomize;
    }
    
    public boolean isResample() {
	return resample;
    }
    
    public boolean isStudyInstanceUID() {
	return props.contains("Dicomize.StudyInstanceUID");
    }
    
    public boolean isSeriesInstanceUID() {
	return props.contains("Dicomize.SeriesInstanceUID");
    }
    
    public boolean isSOPInstanceUID() {
	return props.contains("Dicomize.SOPInstanceUID");
    }
    
    public String getStudyInstanceUID() {
	return getInstanceUID("Dicomize.StudyInstanceUID");
    }
    
    public String getSeriesInstanceUID() {
	return getInstanceUID("Dicomize.SeriesInstanceUID");
    }
    
    public String getSOPInstanceUID() {
	return getInstanceUID("Dicomize.SOPInstanceUID");
    }
    
    public String getInstanceUID(String string) {
	String string_5_ = props.getProperty(string);
	return (string_5_ != null && string_5_.length() > 0 ? string_5_
		: UIDUtils.createUID(uidPrefix));
    }
    
    public String getSOPClassUID() {
	return props.getProperty("Dicomize.SOPClassUID");
    }
    
    public String getPhotometric() {
	return props.getProperty("Dicomize.Photometric");
    }
    
    public int getMaxRows() {
	return maxRows;
    }
    
    public int getMaxColumns() {
	return maxColumns;
    }
    
    public int getVerbose() {
	return verbose;
    }
    
    public IconFactory getIconFactory() {
	return iconFactory;
    }
    
    public Properties getProperties() {
	return props;
    }
    
    private static boolean parseBoolean(String string) {
	return string != null && "true".compareTo(string.toLowerCase()) == 0;
    }
    
    private int indexOf(String[] strings, String string) {
	int i = strings.length;
	while (i-- > 0 && !strings[i].equals(string)) {
	    /* empty */
	}
	return i;
    }
    
    static {
	CHECKS.put("Port", CheckParam.range(100, 65535));
	CHECKS.put("StorageCmt.Port", CheckParam.range(100, 65535));
	CHECKS.put("StorageCmt.MultiThreadTCP", CheckParam.bool());
	CHECKS.put("StorageCmt.MultiThreadAssoc", CheckParam.bool());
	CHECKS.put("StorageCmt.CheckAET", CheckParam.bool());
	CHECKS.put("StorageCmt.AutoRelease", CheckParam.bool());
	CHECKS.put("StorageCmt.Abort", CheckParam.bool());
	CHECKS.put("AssocTimeout[ms]", CheckParam.range(0, 65535));
	CHECKS.put("ReleaseTimeout[ms]", CheckParam.range(0, 65535));
	CHECKS.put("MaxPduSize", CheckParam.range(0, 65535));
	CHECKS.put("MaxInvoke", CheckParam.range(0, 65535));
	CHECKS.put("Priority", CheckParam.enum(PRIORITIES));
	CHECKS.put("TransferSyntax", CheckParam.enum(IMAGE_TRANSFERSYNTAX));
	CHECKS.put("SkipPrivate", CheckParam.bool());
	CHECKS.put("SplitMultiFrame", CheckParam.bool());
	CHECKS.put("Anonymize", CheckParam.bool());
	CHECKS.put("Dicomize", CheckParam.bool());
	CHECKS.put("Dicomize.SOPClassUID", CheckParam.enum(SOP_CLASS_UIDS));
	CHECKS.put("Dicomize.Photometric", CheckParam.enum(PHOTOMETRIC));
	CHECKS.put("Resample", CheckParam.bool());
	CHECKS.put("Resample.MaxRows", CheckParam.range(32, 65535));
	CHECKS.put("Resample.MaxColumns", CheckParam.range(32, 65535));
	CHECKS.put("Verbose", CheckParam.enum(VERBOSE));
    }
}
