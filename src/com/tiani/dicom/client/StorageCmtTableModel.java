/* StorageCmtTableModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.client;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.Status;

class StorageCmtTableModel extends AbstractTableModel
{
    private static final String SUCCESS = "Success";
    private static final String NOT_YET = "Not yet";
    private static final String[] HEADER
	= { "Commitment", "File sent", "PatientID", "PatientName", "StudyID",
	    "StudyDate", "Md", "S#", "I#", "RetrieveAET", "FileSetID",
	    "FileSetUID" };
    private static final int STATUS = 0;
    private static final int FILE_PATH = 1;
    private static final int DNAME_OFFSET = 2;
    private static final int RETRIEVE_AET = 9;
    private static final int FILESET_ID = 10;
    private static final int FILESET_UID = 11;
    private static final int[] DNAMES = { 148, 147, 427, 64, 81, 428, 430 };
    private final PrintStream log;
    private final Vector data = new Vector();
    private final IDimseListener cmtResultListener = new IDimseListener() {
	public void notify(DicomMessage dicommessage) {
	    DicomObject dicomobject = dicommessage.getDataset();
	    if (dicomobject == null)
		log.println
		    ("Error: Missing Event Info in received N-EVENT-REPORT RQ");
	    else {
		int i = dicomobject.getSize(121);
		for (int i_0_ = 0; i_0_ < i; i_0_++)
		    StorageCmtTableModel.this.setSuccess(((DicomObject)
							  dicomobject
							      .get(121, i_0_)),
							 dicomobject);
		int i_1_ = dicomobject.getSize(120);
		for (int i_2_ = 0; i_2_ < i_1_; i_2_++)
		    StorageCmtTableModel.this
			.setFailed((DicomObject) dicomobject.get(120, i_2_));
	    }
	}
    };
    
    private class StorageListener implements IDimseRspListener
    {
	private RowData rowData;
	
	public StorageListener(RowData rowdata) {
	    rowData = rowdata;
	}
	
	public void handleRSP
	    (DimseExchange dimseexchange, int i, int i_3_,
	     DicomMessage dicommessage)
	    throws IOException, DicomException, IllegalValueException,
		   UnknownUIDException {
	    if (Status.getStatusEntry(i_3_).getType() != 5)
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
			StorageListener.this.insert();
		    }
		});
	}
	
	private void insert() {
	    int i = data.indexOf(rowData);
	    if (i == -1) {
		i = data.size();
		data.addElement(rowData);
		StorageCmtTableModel.this.fireTableRowsInserted(i, i);
	    }
	}
    }
    
    private class RowData
    {
	final String uid;
	final String[] attribs
	    = new String[StorageCmtTableModel.HEADER.length];
	final DicomObject sopReference = new DicomObject();
	
	RowData(DicomObject dicomobject, String string) throws DicomException {
	    uid = (String) dicomobject.get(63);
	    attribs[0] = "Not yet";
	    attribs[1] = string;
	    attribs[9] = "";
	    attribs[10] = "";
	    attribs[11] = "";
	    for (int i = 0; i < StorageCmtTableModel.DNAMES.length; i++)
		attribs[i + 2]
		    = dicomobject.getS(StorageCmtTableModel.DNAMES[i]);
	    sopReference.set(115, dicomobject.get(62));
	    sopReference.set(116, uid);
	}
	
	public int hashCode() {
	    return uid.hashCode();
	}
	
	public boolean equals(Object object) {
	    return (object instanceof RowData
		    && uid.equals(((RowData) object).uid));
	}
	
	void setFailed(DicomObject dicomobject) {
	    attribs[0] = "Unrecognized failure reason";
	    try {
		switch (dicomobject.getI(119)) {
		case 272:
		    attribs[0] = "Processing failure";
		    break;
		case 274:
		    attribs[0] = "No such object instance";
		    break;
		case 531:
		    attribs[0] = "Resource limitation";
		    break;
		case 290:
		    attribs[0] = "Referenced SOP Class not supported";
		    break;
		case 281:
		    attribs[0] = "Class / Instance conflict";
		    break;
		case 305:
		    attribs[0] = "Duplicate transaction UID";
		    break;
		}
	    } catch (DicomException dicomexception) {
		log.println(dicomexception.getMessage());
	    }
	    attribs[9] = "";
	    attribs[10] = "";
	    attribs[11] = "";
	}
	
	void setSuccess(DicomObject dicomobject, DicomObject dicomobject_5_) {
	    attribs[0] = "Success";
	    attribs[9] = StorageCmtTableModel.this.getString(79, dicomobject,
							     dicomobject_5_);
	    attribs[10] = StorageCmtTableModel.this.getString(697, dicomobject,
							      dicomobject_5_);
	    attribs[11] = StorageCmtTableModel.this.getString(698, dicomobject,
							      dicomobject_5_);
	}
    }
    
    private void setSuccess(DicomObject dicomobject,
			    DicomObject dicomobject_6_) {
	try {
	    int i = rowIndex(dicomobject);
	    if (i == -1)
		log.println
		    ("Error: Did not request commitment of SOP instance - "
		     + dicomobject.getS(116));
	    else {
		((RowData) data.elementAt(i)).setSuccess(dicomobject,
							 dicomobject_6_);
		this.fireTableRowsUpdated(i, i);
	    }
	} catch (DicomException dicomexception) {
	    log.println(dicomexception.getMessage());
	}
    }
    
    private void setFailed(DicomObject dicomobject) {
	try {
	    int i = rowIndex(dicomobject);
	    if (i == -1)
		log.println
		    ("Error: Did not request commitment of SOP instance - "
		     + dicomobject.getS(116));
	    else {
		((RowData) data.elementAt(i)).setFailed(dicomobject);
		this.fireTableRowsUpdated(i, i);
	    }
	} catch (DicomException dicomexception) {
	    log.println(dicomexception.getMessage());
	}
    }
    
    private int rowIndex(DicomObject dicomobject) {
	String string = (String) dicomobject.get(116);
	int i = data.size();
	for (int i_7_ = 0; i_7_ < i; i_7_++) {
	    RowData rowdata = (RowData) data.elementAt(i_7_);
	    if (string.equals(rowdata.uid))
		return i_7_;
	}
	return -1;
    }
    
    private String getString(int i, DicomObject dicomobject,
			     DicomObject dicomobject_8_) {
	try {
	    String string;
	    if ((string = (String) dicomobject.get(i)) != null
		|| (string = (String) dicomobject_8_.get(i)) != null)
		return string;
	    return "";
	} catch (Exception exception) {
	    log.println(exception.getMessage());
	    return "";
	}
    }
    
    public StorageCmtTableModel(PrintStream printstream) {
	log = printstream;
    }
    
    public IDimseRspListener getStorageListener
	(DicomObject dicomobject, String string) throws DicomException {
	return new StorageListener(new RowData(dicomobject, string));
    }
    
    public IDimseListener getCmtResultListener() {
	return cmtResultListener;
    }
    
    public String getColumnName(int i) {
	return HEADER[i];
    }
    
    public int getColumnCount() {
	return HEADER.length;
    }
    
    public int getRowCount() {
	return data.size();
    }
    
    public Object getValueAt(int i, int i_9_) {
	return ((RowData) data.elementAt(i)).attribs[i_9_];
    }
    
    public DicomObject getSopReferenceAt(int i) {
	return ((RowData) data.elementAt(i)).sopReference;
    }
    
    public boolean isCommited(int i) {
	return ((RowData) data.elementAt(i)).attribs[0] == "Success";
    }
}
