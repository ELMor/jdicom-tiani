/* RmiCfgClient - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.client;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.util.Properties;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.Document;

import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.ui.PropertiesTableModel;
import com.tiani.dicom.util.ExampleFileFilter;
import com.tiani.rmicfg.IServer;
import com.tiani.rmicfg.IServerFactory;

public class RmiCfgClient extends JApplet
{
    private JFileChooser _fileChooser;
    private JTextField _url = new JTextField("rmi://localhost/ServerFactory");
    private JButton _qrySrv = new JButton("?");
    private JButton _delSrv = new JButton("-");
    private JButton _newSrv = new JButton("+");
    private JComboBox _classnames = new JComboBox();
    private JButton _qryProp = new JButton("?");
    private JButton _apply = new JButton("=");
    private JButton _start = new JButton(">");
    private JButton _stop = new JButton("||");
    private JCheckBox _abort = new JCheckBox("abort");
    private JCheckBox _join = new JCheckBox("join");
    private JButton _load = new JButton("L");
    private JButton _save = new JButton("S");
    private JTextArea _logTextArea = new JTextArea();
    private Document _logDoc = _logTextArea.getDocument();
    private PrintStream _log
	= new PrintStream(new DocumentOutputStream(_logDoc, 10000), true);
    private IServerFactory _factory = null;
    private Vector _rows = new Vector();
    private Properties _properties = null;
    public static final FileFilter FILE_FILTER
	= new ExampleFileFilter("properties", "Properties");
    private static final PropertiesTableModel EMPTY_PROP_TAB_MODEL
	= new PropertiesTableModel();
    private static final String[] _HEADERS = { "Name", "Type", "run" };
    private final AbstractTableModel _srvTabModel = new AbstractTableModel() {
	public String getColumnName(int i) {
	    return RmiCfgClient._HEADERS[i];
	}
	
	public int getColumnCount() {
	    return 3;
	}
	
	public int getRowCount() {
	    return _rows.size();
	}
	
	public Class getColumnClass(int i) {
	    return i < 2 ? String.class : Boolean.class;
	}
	
	public Object getValueAt(int i, int i_0_) {
	    Row row = (Row) _rows.elementAt(i);
	    switch (i_0_) {
	    case 0:
		return row._srvName;
	    case 1:
		return row._srvType;
	    case 2:
		return new Boolean(row._srvRun);
	    default:
		return null;
	    }
	}
    };
    private JTable _srvTab = new JTable(_srvTabModel);
    private JTable _propTab = new JTable(EMPTY_PROP_TAB_MODEL);
    
    private static class Row
    {
	IServer _srv;
	String _srvName;
	String _srvType;
	boolean _srvRun;
	
	private Row() {
	    /* empty */
	}
    }
    
    public static void main(String[] strings) {
	RmiCfgClient rmicfgclient = new RmiCfgClient();
	new AppletFrame("RMI Configuration & Control Client 1.0", rmicfgclient,
			500, 400);
    }
    
    public void init() {
	_url.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.queryServer();
	    }
	});
	_qrySrv.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.queryServer();
	    }
	});
	_delSrv.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.delServer();
	    }
	});
	_newSrv.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.newServer();
	    }
	});
	_qryProp.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.queryProperties();
	    }
	});
	_apply.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.apply();
	    }
	});
	_start.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.srvStart();
	    }
	});
	_stop.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.srvStop();
	    }
	});
	_load.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.loadProp();
	    }
	});
	_save.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		RmiCfgClient.this.saveProp();
	    }
	});
	ListSelectionModel listselectionmodel = _srvTab.getSelectionModel();
	listselectionmodel.setSelectionMode(0);
	listselectionmodel
	    .addListSelectionListener(new ListSelectionListener() {
	    public void valueChanged(ListSelectionEvent listselectionevent) {
		if (!listselectionevent.getValueIsAdjusting()) {
		    RmiCfgClient.this.enableButtons();
		    RmiCfgClient.this.queryProperties();
		}
	    }
	});
	JPanel jpanel = new JPanel(new GridBagLayout());
	GridBagConstraints gridbagconstraints = new GridBagConstraints();
	jpanel.add(new JLabel("factory: "), gridbagconstraints);
	gridbagconstraints.fill = 2;
	gridbagconstraints.weightx = 100.0;
	jpanel.add(_url, gridbagconstraints);
	gridbagconstraints.fill = 0;
	gridbagconstraints.weightx = 0.0;
	jpanel.add(new JTianiButton(isApplet() ? this.getAppletContext()
				    : null),
		   gridbagconstraints);
	JSplitPane jsplitpane
	    = new JSplitPane(1, createFactoryPane(), createServerPane());
	jsplitpane.setPreferredSize(new Dimension(300, 250));
	JSplitPane jsplitpane_12_
	    = new JSplitPane(0, jsplitpane, new JAutoScrollPane(_logTextArea));
	jsplitpane_12_.setOneTouchExpandable(true);
	Container container = this.getContentPane();
	container.add(jpanel, "North");
	container.add(jsplitpane_12_, "Center");
	String string = this.getParameter("AutoLoad");
	if (string != null && string.length() > 0) {
	    _url.setText(string);
	    queryServer();
	}
	enableButtons();
	if (System.getSecurityManager() == null)
	    System.setSecurityManager(new RMISecurityManager());
    }
    
    private JPanel createFactoryPane() {
	JPanel jpanel = new JPanel(new BorderLayout());
	Box box = Box.createHorizontalBox();
	box.add(_qrySrv);
	box.add(_newSrv);
	box.add(_delSrv);
	jpanel.add(box, "North");
	jpanel.add(new JScrollPane(_srvTab), "Center");
	return jpanel;
    }
    
    private JPanel createServerPane() {
	JPanel jpanel = new JPanel(new BorderLayout());
	Box box = Box.createHorizontalBox();
	box.add(_qryProp);
	box.add(_apply);
	box.add(_start);
	box.add(_stop);
	box.add(_abort);
	box.add(_join);
	box.add(Box.createHorizontalGlue());
	box.add(_load);
	box.add(_save);
	jpanel.add(box, "North");
	jpanel.add(new JScrollPane(_propTab), "Center");
	return jpanel;
    }
    
    private boolean isApplet() {
	return this.getDocumentBase() != null;
    }
    
    private void queryServer() {
	String string = _url.getText();
	_log.println("Query Server Factory - " + string);
	try {
	    _factory = (IServerFactory) Naming.lookup(string);
	    String[] strings = _factory.listServerNames();
	    _rows.clear();
	    for (int i = 0; i < strings.length; i++) {
		Row row = new Row();
		row._srvName = strings[i];
		row._srv = _factory.getServer(strings[i]);
		row._srvType = row._srv.getType();
		row._srvRun = row._srv.isRunning();
		_rows.addElement(row);
	    }
	    _srvTabModel.fireTableDataChanged();
	    _classnames
		.setModel(new DefaultComboBoxModel(_factory.listClassNames()));
	} catch (SecurityException securityexception) {
	    _log.println(securityexception);
	    showPolicyFile();
	} catch (Exception exception) {
	    _log.println(exception);
	}
	enableButtons();
    }
    
    private void newServer() {
	if (_factory != null) {
	    String string = JOptionPane.showInputDialog(this, _classnames,
							"New Server", 3);
	    if (string != null && string.length() > 0) {
		try {
		    String string_13_ = (String) _classnames.getSelectedItem();
		    _log.println("Create Server - " + string + ":"
				 + string_13_);
		    _factory.createServer(string_13_, string);
		} catch (Exception exception) {
		    _log.println(exception);
		}
		queryServer();
		selectServer(string);
	    }
	    enableButtons();
	}
    }
    
    private void selectServer(String string) {
	int i = _rows.size();
	for (int i_14_ = 0; i_14_ < i; i_14_++) {
	    Row row = (Row) _rows.elementAt(i_14_);
	    if (string.equals(row._srvName)) {
		_srvTab.setRowSelectionInterval(i_14_, i_14_);
		break;
	    }
	}
    }
    
    private void delServer() {
	int i = _srvTab.getSelectedRow();
	if (i != -1) {
	    try {
		Row row = (Row) _rows.elementAt(i);
		_log.println("Delete Server - " + row._srvName);
		_factory.removeServer(row._srvName);
		_rows.remove(i);
		_srvTabModel.fireTableRowsDeleted(i, i);
		_propTab.setModel(EMPTY_PROP_TAB_MODEL);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	    enableButtons();
	}
    }
    
    private void queryProperties() {
	int i = _srvTab.getSelectedRow();
	if (i == -1)
	    _propTab.setModel(EMPTY_PROP_TAB_MODEL);
	else {
	    try {
		Row row = (Row) _rows.elementAt(i);
		_log.println("Query Properties from Server - " + row._srvName);
		String[] strings = row._srv.getPropertyNames();
		_properties = row._srv.getProperties();
		_propTab.setModel(new PropertiesTableModel(strings,
							   _properties));
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	}
    }
    
    private void apply() {
	int i = _srvTab.getSelectedRow();
	if (i != -1) {
	    try {
		Row row = (Row) _rows.elementAt(i);
		_log.println("Set Properties of Server - " + row._srvName);
		row._srv.setProperties(_properties);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	}
    }
    
    private void srvStart() {
	int i = _srvTab.getSelectedRow();
	if (i != -1) {
	    try {
		Row row = (Row) _rows.elementAt(i);
		_log.println("Start Server - " + row._srvName);
		row._srv.start(_properties);
		row._srvRun = row._srv.isRunning();
		_srvTabModel.fireTableCellUpdated(i, 2);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	    enableButtons();
	}
    }
    
    private void srvStop() {
	int i = _srvTab.getSelectedRow();
	if (i != -1) {
	    try {
		Row row = (Row) _rows.elementAt(i);
		_log.println("Stop Server - " + row._srvName);
		row._srv.stop(_abort.isSelected(), _join.isSelected());
		row._srvRun = row._srv.isRunning();
		_srvTabModel.fireTableCellUpdated(i, 2);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	    enableButtons();
	}
    }
    
    private void loadProp() {
	int i = _srvTab.getSelectedRow();
	if (i != -1) {
	    Row row = (Row) _rows.elementAt(i);
	    File file
		= chooseFile(row._srvName + ".properties",
			     "Load Properties for " + row._srvName, "Load");
	    if (file != null) {
		try {
		    _log.println("Load Properties - " + file);
		    FileInputStream fileinputstream
			= new FileInputStream(file);
		    try {
			_properties.clear();
			_properties.load(fileinputstream);
			((AbstractTableModel) _propTab.getModel())
			    .fireTableDataChanged();
		    } finally {
			fileinputstream.close();
		    }
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	}
    }
    
    private void saveProp() {
	int i = _srvTab.getSelectedRow();
	if (i != -1) {
	    Row row = (Row) _rows.elementAt(i);
	    File file
		= chooseFile(row._srvName + ".properties",
			     "Save Properties of " + row._srvName, "Save");
	    if (file != null) {
		try {
		    _log.println("Save Properties - " + file);
		    FileOutputStream fileoutputstream
			= new FileOutputStream(file);
		    try {
			_properties.store(fileoutputstream,
					  "Properties for " + row._srvType);
		    } finally {
			fileoutputstream.close();
		    }
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	}
    }
    
    private File chooseFile(String string, String string_15_,
			    String string_16_) {
	try {
	    if (_fileChooser == null) {
		_fileChooser = new JFileChooser(".");
		_fileChooser.setFileSelectionMode(0);
		_fileChooser.setFileFilter(FILE_FILTER);
	    }
	    _fileChooser.setDialogTitle(string_15_);
	    _fileChooser.setSelectedFile(new File(string));
	    int i = _fileChooser.showDialog(JOptionPane
						.getFrameForComponent(this),
					    string_16_);
	    File file = _fileChooser.getSelectedFile();
	    return i == 0 ? file : null;
	} catch (SecurityException securityexception) {
	    _log.println(securityexception);
	    showPolicyFile();
	    return null;
	}
    }
    
    private void showPolicyFile() {
	try {
	    URL url = new URL(this.getDocumentBase(),
			      this.getParameter("PolicyFile"));
	    this.getAppletContext().showDocument(url, "_blank");
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private void enableButtons() {
	int i = _srvTab.getSelectedRow();
	Row row = i != -1 ? (Row) _rows.elementAt(i) : null;
	_newSrv.setEnabled(_factory != null);
	_delSrv.setEnabled(row != null && !row._srvRun);
	_qryProp.setEnabled(row != null);
	_apply.setEnabled(row != null);
	_start.setEnabled(row != null && !row._srvRun);
	_stop.setEnabled(row != null && row._srvRun);
	_abort.setEnabled(row != null && row._srvRun);
	_join.setEnabled(row != null && row._srvRun);
	_load.setEnabled(row != null);
	_save.setEnabled(row != null);
    }
}
