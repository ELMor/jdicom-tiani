/* GSPresentationStateIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public class GSPresentationStateIOD
{
    static final Attribute[] seriesModule
	= { new Attribute(81, 1, null, new IfEqual(81, "PR")) };
    static final UserOption cDisplayedAreaNotApplyToAll
	= new UserOption("C:Displayed Area not apply to all");
    static final String[] presentationSizeModes
	= { "SCALE TO FIT", "TRUE SIZE", "MAGNIFY" };
    static final Attribute[] displayedAeraSelectionSequence
	= { new Attribute(113, 1, cDisplayedAreaNotApplyToAll, null,
			  CommonImage.referencedMFImageSequence),
	    new Attribute(1411, 1, null, null),
	    new Attribute(1412, 1, null, null),
	    new Attribute(1415, 1, null,
			  new IfEqual(1415, presentationSizeModes)),
	    new Attribute(1416, 1, new IfEqual(1415, "TRUE SIZE"), null),
	    new Attribute(1417, 1, new IfNot(new IfPresent(1416)), null),
	    new Attribute(1418, 1, new IfEqual(1415, "MAGNIFY"), null) };
    static final Attribute[] displayedAeraModule
	= { new Attribute(1413, 1, null, null,
			  displayedAeraSelectionSequence) };
    static final UserOption cGraphicAnnotationModule
	= new UserOption("C:Graphic Annotations");
    static final String[] YN = { "Y", "N" };
    static final String[] PIXEL_OR_DISPLAY = { "PIXEL", "DISPLAY" };
    static final String[] LEFT_RIGHT_CENTER = { "LEFT", "RIGHT", "CENTER" };
    static final String[] GRAPHIC_TYPES
	= { "POINT", "POLYLINE", "INTERPOLATED", "CIRCLE", "ELLIPSE" };
    static final String[] CLOSED_GRAPHIC_TYPES = { "CIRCLE", "ELLIPSE" };
    static final UserOption cGraphAnnotNotApplyToAll
	= new UserOption("C:Graphic Annotion not apply to all");
    static final ICondition isBoundingBox = new IfPresent(1389);
    static final ICondition noBoundingBox = new IfNot(isBoundingBox);
    static final ICondition isAnchorPoint = new IfPresent(1391);
    static final ICondition noAnchorPoint = new IfNot(isAnchorPoint);
    static final Attribute[] textObjectSequenceModule
	= { new Attribute(1383, 1, isBoundingBox,
			  new IfEqual(1383, PIXEL_OR_DISPLAY)),
	    new Attribute(1384, 1, isAnchorPoint,
			  new IfEqual(1384, PIXEL_OR_DISPLAY)),
	    new Attribute(1386, 1, null, null),
	    new Attribute(1389, 1, noAnchorPoint, null),
	    new Attribute(1390, 1, noAnchorPoint, null),
	    new Attribute(1409, 1, isBoundingBox,
			  new IfEqual(1409, LEFT_RIGHT_CENTER)),
	    new Attribute(1391, 1, noBoundingBox, null),
	    new Attribute(1392, 1, isAnchorPoint, new IfEqual(1392, YN)) };
    static final Attribute[] graphicObjectSequenceModule
	= { new Attribute(1385, 1, null, new IfEqual(1385, PIXEL_OR_DISPLAY)),
	    new Attribute(1393, 1, null, new IfEqualInt(1393, 2)),
	    new Attribute(1394, 1, null, null),
	    new Attribute(1395, 1, null, null),
	    new Attribute(1396, 1, null, new IfEqual(1396, GRAPHIC_TYPES)),
	    new Attribute(1397, 1, new IfEqual(1396, CLOSED_GRAPHIC_TYPES),
			  new IfEqual(1397, YN)) };
    static final Attribute[] graphicAnnotationSequenceModule
	= { new Attribute(113, 1, cGraphAnnotNotApplyToAll, null,
			  CommonImage.referencedMFImageSequence),
	    new Attribute(1382, 1, null, null),
	    new Attribute(1387, 1, new IfNot(new IfPresent(1388)), null,
			  textObjectSequenceModule),
	    new Attribute(1388, 1, new IfNot(new IfPresent(1387)), null,
			  graphicObjectSequenceModule) };
    static final Attribute[] graphicAnnotationModule
	= { new Attribute(1381, 1, null, null,
			  graphicAnnotationSequenceModule) };
    static final int[] IMAGE_ROTATIONS = { 0, 90, 180, 270 };
    static final UserOption cSpatialTransformation
	= new UserOption("C:Spatial Transformation applied");
    static final Attribute[] spatialTransformationModule
	= { new Attribute(1410, 1, null,
			  new IfEqualInt(1410, IMAGE_ROTATIONS)),
	    new Attribute(1398, 1, null, new IfEqual(1398, YN)) };
    static final Attribute[] graphicLayerSequenceModule
	= { new Attribute(1382, 1, null, null),
	    new Attribute(1400, 1, null, null),
	    new Attribute(1401, 3, null, null),
	    new Attribute(1414, 3, null, null),
	    new Attribute(1402, 3, null, null) };
    static final Attribute[] graphicLayerModule
	= { new Attribute(1399, 1, null, null, graphicLayerSequenceModule) };
    static final Attribute[] referencedSeriesSequence
	= { new Attribute(426, 1, null, null),
	    new Attribute(79, 3, null, null),
	    new Attribute(697, 3, null, null),
	    new Attribute(698, 3, null, null),
	    new Attribute(113, 1, null, null,
			  CommonImage.referencedMFImageSequence) };
    static final Attribute[] presentationStateModule
	= { new Attribute(430, 1, null, null),
	    new Attribute(1403, 1, null, null),
	    new Attribute(1404, 2, null, null),
	    new Attribute(1405, 1, null, null),
	    new Attribute(1406, 1, null, null),
	    new Attribute(1407, 2, null, null),
	    new Attribute(109, 1, null, null, referencedSeriesSequence),
	    new Attribute(1377, 1, new IfPresent(352), null) };
    static final UserOption cMaskSubtraction
	= new UserOption("C:Apply Mask Subtraction");
    static final Attribute[] maskSubtractionSequence
	= { new Attribute(518, 1, null,
			  new IfEqual(518, new String[] { "AVG_SUB", "TID" })),
	    new Attribute(520, 1, new IfEqual(518, "AVG_SUB"), null),
	    new Attribute(521, 1, new IfSizeInRange(520, 1, 2147483647), null),
	    new Attribute(522, 3, null, null),
	    new Attribute(523, 2, new IfEqual(518, "TID"), null),
	    new Attribute(524, 3, null, null) };
    static final Attribute[] maskModule
	= { new Attribute(517, 1, null, null, maskSubtractionSequence),
	    new Attribute(493, 1, null, new IfEqual(493, "SUB")) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem(seriesModule, null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(maskModule, cMaskSubtraction),
	    new CompositeIOD.ModuleTableItem(CommonImage.displayShutterModule,
					     CommonImage.cDisplayShutter),
	    new CompositeIOD.ModuleTableItem(CommonImage.bitmapDisplayShutter,
					     (CommonImage
					      .cBitmapDisplayShutter)),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.cOverlayPlane),
	    new CompositeIOD.ModuleTableItem(displayedAeraModule, null),
	    new CompositeIOD.ModuleTableItem(graphicAnnotationModule,
					     cGraphicAnnotationModule),
	    new CompositeIOD.ModuleTableItem(graphicLayerModule,
					     cGraphicAnnotationModule),
	    new CompositeIOD.ModuleTableItem(spatialTransformationModule,
					     cSpatialTransformation),
	    new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule,
					     LUTModules.cModalityLUT),
	    new CompositeIOD.ModuleTableItem(LUTModules.softcopyVOILUTModule,
					     LUTModules.cSoftcopyVOILUT),
	    new CompositeIOD.ModuleTableItem(LUTModules.presentationLUTModule,
					     null),
	    new CompositeIOD.ModuleTableItem(presentationStateModule, null),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, cMaskSubtraction,
	    CommonImage.cDisplayShutter, CommonImage.cBitmapDisplayShutter,
	    OverlayModules.cOverlayPlane, CommonImage.cRefMultiframeImage,
	    cSpatialTransformation, cDisplayedAreaNotApplyToAll,
	    cGraphicAnnotationModule, cGraphAnnotNotApplyToAll,
	    LUTModules.cModalityLUT, LUTModules.cSoftcopyVOILUT,
	    LUTModules.cVOILUTNotApplyToAll,
	    GeneralModules.cSpecificCharacterSet };
    
    private GSPresentationStateIOD() {
	/* empty */
    }
}
