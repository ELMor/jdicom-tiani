/* NMImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class NMImageIOD
{
    static final UserOption cPatientOrientationModifier
	= new UserOption("C:Patient Orientation Modifier");
    public static final Attribute[] patientOrientationCodeSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(667, 2, cPatientOrientationModifier,
			  new IfSizeInRange(667, 0, 1),
			  CommonImage.codeSequence) };
    static final Attribute[] patientOrientationModule
	= { new Attribute(666, 2, null, new IfSizeInRange(666, 0, 1),
			  patientOrientationCodeSequence),
	    new Attribute(668, 2, null, new IfSizeInRange(668, 0, 1),
			  CommonImage.codeSequence) };
    static final Attribute[] imagePixelModule
	= { new Attribute(461, 1, null, CommonImage.is1SamplePerPixel),
	    new Attribute(462, 1, null,
			  CommonImage.isMonochrome2OrPaletteColor),
	    new Attribute(475, 1, null, CommonImage.is8or16BitsAllocated),
	    new Attribute(476, 1, null,
			  CommonImage.isBitsStoredEqualsBitsAllocated),
	    new Attribute(477, 1, null,
			  CommonImage.isHighBitEqualsBitsStoredLess1),
	    new Attribute(470, 2, null, null) };
    static final ICondition frameIncrementPointerValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    String string = dicomobject.getS(58, 2);
	    if (string == null)
		return true;
	    if (string.equals("STATIC") || string.equals("WHOLE BODY"))
		return (dicomobject.getI(465, 0) == 5505040
			&& dicomobject.getI(465, 1) == 5505056);
	    if (string.equals("DYNAMIC"))
		return (dicomobject.getI(465, 0) == 5505040
			&& dicomobject.getI(465, 1) == 5505056
			&& dicomobject.getI(465, 2) == 5505072
			&& dicomobject.getI(465, 3) == 5505280);
	    if (string.equals("GATED"))
		return (dicomobject.getI(465, 0) == 5505040
			&& dicomobject.getI(465, 1) == 5505056
			&& dicomobject.getI(465, 2) == 5505120
			&& dicomobject.getI(465, 3) == 5505136);
	    if (string.equals("TOMO"))
		return (dicomobject.getI(465, 0) == 5505040
			&& dicomobject.getI(465, 1) == 5505056
			&& dicomobject.getI(465, 2) == 5505104
			&& dicomobject.getI(465, 3) == 5505168);
	    if (string.equals("GATED TOMO"))
		return (dicomobject.getI(465, 0) == 5505040
			&& dicomobject.getI(465, 1) == 5505056
			&& dicomobject.getI(465, 2) == 5505104
			&& dicomobject.getI(465, 3) == 5505120
			&& dicomobject.getI(465, 4) == 5505136
			&& dicomobject.getI(465, 5) == 5505168);
	    if (string.equals("RECON TOMO"))
		return dicomobject.getI(465, 0) == 5505152;
	    if (string.equals("RECON GATED TOMO"))
		return (dicomobject.getI(465, 0) == 5505120
			&& dicomobject.getI(465, 1) == 5505136
			&& dicomobject.getI(465, 2) == 5505152);
	    return true;
	}
    };
    static final ICondition ifEnergyWindowVector
	= new IfAnyEqualInt(465, 5505040);
    static final ICondition ifDetectorVector = new IfAnyEqualInt(465, 5505056);
    static final ICondition ifPhaseVector = new IfAnyEqualInt(465, 5505072);
    static final ICondition ifRotationVector = new IfAnyEqualInt(465, 5505104);
    static final ICondition ifRRIntervalVector
	= new IfAnyEqualInt(465, 5505120);
    static final ICondition ifTimeSlotVector = new IfAnyEqualInt(465, 5505136);
    static final ICondition ifSliceVector = new IfAnyEqualInt(465, 5505152);
    static final ICondition ifAngularViewVector
	= new IfAnyEqualInt(465, 5505168);
    static final ICondition ifTimeSliceVector
	= new IfAnyEqualInt(465, 5505280);
    static final Attribute[] multiFrameModule
	= { new Attribute(465, 1, null, null),
	    new Attribute(619, 1, ifEnergyWindowVector, null),
	    new Attribute(620, 1, null, null),
	    new Attribute(628, 1, ifDetectorVector, null),
	    new Attribute(629, 1, null, null),
	    new Attribute(631, 1, ifPhaseVector, null),
	    new Attribute(632, 1, ifPhaseVector, null),
	    new Attribute(637, 1, ifRotationVector, null),
	    new Attribute(638, 1,
			  new IfEqual(58, 2,
				      new String[] { "TOMO", "GATED TOMO",
						     "RECON TOMO",
						     "RECON GATED TOMO" }),
			  null),
	    new Attribute(641, 1, ifRRIntervalVector, null),
	    new Attribute(642, 1, ifRRIntervalVector, null),
	    new Attribute(645, 1, ifTimeSlotVector, null),
	    new Attribute(646, 1, ifTimeSlotVector, null),
	    new Attribute(649, 1, ifSliceVector, null),
	    new Attribute(650, 1, ifSliceVector, null),
	    new Attribute(651, 1, ifAngularViewVector, null),
	    new Attribute(652, 1, ifTimeSliceVector, null) };
    static final ICondition ifWholeBodyImage
	= new IfEqual(58, 2, "WHOLE BODY");
    public static final Attribute[] anatomicRegionSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(139, 3, null, new IfSizeInRange(139, 1, 2147483647),
			  CommonImage.codeSequence) };
    public static final Attribute[] primaryAnatomicStructureSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(141, 3, null, new IfSizeInRange(141, 1, 2147483647),
			  CommonImage.codeSequence) };
    static final ICondition enumImageType
	= new IfMultiEqual(58, (new String[][]
				{ { "ORIGINAL", "DERIVED" }, { "PRIMARY" },
				  { "STATIC", "DYNAMIC", "GATED", "WHOLE BODY",
				    "TOMO", "GATED TOMO", "RECON TOMO",
				    "RECON GATED TOMO" },
				  { "EMISSION", "TRANSMISSION" } }));
    static final Attribute[] imageModule
	= { new Attribute(58, 1, null, enumImageType),
	    new Attribute(665, 3, null, null),
	    new Attribute(504, 1, CommonImage.cLossyImageCompression,
			  CommonImage.enumLossyImageCompression),
	    new Attribute(207, 2, null, null),
	    new Attribute(208, 3, null, null),
	    new Attribute(282, 3, null, null),
	    new Attribute(283, 3, null, null),
	    new Attribute(320, 1,
			  new IfEqual(58, 2,
				      new String[] { "STATIC", "WHOLE BODY" }),
			  null),
	    new Attribute(321, 3, null, null),
	    new Attribute(371, 3, null, null),
	    new Attribute(474, 3, null, null),
	    new Attribute(328, 3, null,
			  new IfEqual(328, new String[] { "1PS", "2PS", "PCN",
							  "MSP" })),
	    new Attribute(327, 2, ifWholeBodyImage, null),
	    new Attribute(329, 2, ifWholeBodyImage, null),
	    new Attribute(112, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(114, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(252, 3, null, null),
	    new Attribute(138, 3, null, new IfSizeEqual(138, 1),
			  anatomicRegionSequence),
	    new Attribute(140, 3, null, new IfSizeInRange(140, 1, 2147483647),
			  primaryAnatomicStructureSequence) };
    static final Attribute[] energyWindowRangeSequence
	= { new Attribute(623, 3, null, null),
	    new Attribute(624, 3, null, null) };
    static final Attribute[] energyWindowInformationSequence
	= { new Attribute(627, 3, null, null),
	    new Attribute(622, 3, null, null, energyWindowRangeSequence) };
    static final Attribute[] calibrationDataSequence
	= { new Attribute(664, 1, null, null),
	    new Attribute(245, 3, null, null),
	    new Attribute(626, 3, null, null) };
    static final Attribute[] radiopharmaceuticalInformationSequence
	= { new Attribute(660, 2, null, new IfSizeEqual(660, 1),
			  CommonImage.codeSequence),
	    new Attribute(258, 3, null, null),
	    new Attribute(661, 3, null, new IfSizeEqual(661, 1),
			  CommonImage.codeSequence),
	    new Attribute(259, 3, null, null),
	    new Attribute(260, 3, null, null),
	    new Attribute(261, 3, null, null),
	    new Attribute(262, 3, null, null),
	    new Attribute(663, 3, null, null, calibrationDataSequence),
	    new Attribute(195, 3, null, null),
	    new Attribute(662, 3, null, new IfSizeEqual(662, 1),
			  CommonImage.codeSequence) };
    static final Attribute[] interventionDrugInformationSequence
	= { new Attribute(198, 3, null, null),
	    new Attribute(192, 3, null, new IfSizeEqual(192, 1),
			  CommonImage.codeSequence),
	    new Attribute(661, 3, null, new IfSizeEqual(661, 1),
			  CommonImage.codeSequence),
	    new Attribute(199, 3, null, null),
	    new Attribute(190, 3, null, null),
	    new Attribute(191, 3, null, null) };
    static final ICondition energyWindowInformationSequenceSizeFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(620);
	    if (i == 2147483647)
		return true;
	    return i == dicomobject.getSize(621);
	}
    };
    static final Attribute[] isotopeModule
	= { new Attribute(621, 2, null,
			  energyWindowInformationSequenceSizeFilter,
			  energyWindowInformationSequence),
	    new Attribute(625, 2, null, new IfSizeInRange(625, 1, 2147483647),
			  radiopharmaceuticalInformationSequence),
	    new Attribute(189, 3, null, null,
			  interventionDrugInformationSequence) };
    static final UserOption cViewAngulationModifier
	= new UserOption("C:View Angulation Modifier");
    static final Attribute[] viewCodeSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(659, 2, cViewAngulationModifier,
			  new IfSizeEqual(659, 1), CommonImage.codeSequence) };
    static final Attribute[] detectorInformationSequence
	= { new Attribute(311, 3, null, null),
	    new Attribute(312, 3, null, null),
	    new Attribute(296, 3, null, null),
	    new Attribute(297, 3, null, null),
	    new Attribute(313, 2, null, null),
	    new Attribute(314, 3, null, null),
	    new Attribute(315, 3, null, null),
	    new Attribute(472, 3, null, null),
	    new Attribute(471, 3, null, null),
	    new Attribute(294, 3, null, null),
	    new Attribute(280, 3, null, null),
	    new Attribute(277, 2,
			  new IfAnd(new ICondition[]
				    { new IfEqual(58, 3, "TRANSMISSION"),
				      new IfNot(new IfEqual(58, 2, "TOMO")) }),
			  null),
	    new Attribute(654, 3, null, null),
	    new Attribute(291, 3, null, null),
	    new Attribute(441, 2, null, null),
	    new Attribute(440, 2, null, null),
	    new Attribute(658, 3, null, new IfSizeEqual(658, 1),
			  viewCodeSequence) };
    static final ICondition detectorInformationSequenceSizeFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(629);
	    if (i == 2147483647)
		return true;
	    return i == dicomobject.getSize(630);
	}
    };
    static final Attribute[] detectorModule
	= { new Attribute(630, 2, detectorInformationSequenceSizeFilter, null,
			  detectorInformationSequence) };
    static final Attribute[] rotationInformationSequence
	= { new Attribute(654, 1, null, null),
	    new Attribute(293, 1, null, null),
	    new Attribute(289, 1, null,
			  new IfEqual(289, new String[] { "CW", "CC" })),
	    new Attribute(292, 1, null, null),
	    new Attribute(320, 1, null, null),
	    new Attribute(291, 3, null, null),
	    new Attribute(277, 2, new IfEqual(58, 3, "TRANSMISSION"), null),
	    new Attribute(640, 1, null, null),
	    new Attribute(283, 3, null, null),
	    new Attribute(282, 3, null, null) };
    static final ICondition rotationInformationSequenceSizeFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(638);
	    if (i == 2147483647)
		return true;
	    return i == dicomobject.getSize(639);
	}
    };
    static final Attribute[] tomoAcquisitionModule
	= { new Attribute(639, 2, null, rotationInformationSequenceSizeFilter,
			  rotationInformationSequence),
	    new Attribute(655, 3, null,
			  new IfEqual(655,
				      new String[] { "STEP AND SHOOT",
						     "CONTINUOUS",
						     "ACQ DURING STEP" })) };
    static final Attribute[] timeSlotInformationSequence
	= { new Attribute(648, 3, null, null) };
    static final Attribute[] dataInformationSequence
	= { new Attribute(254, 3, null, null),
	    new Attribute(253, 3, null, null),
	    new Attribute(267, 3, null, null),
	    new Attribute(268, 3, null, null),
	    new Attribute(269, 3, null, null),
	    new Attribute(270, 3, null, null),
	    new Attribute(647, 2, null, null, timeSlotInformationSequence) };
    static final Attribute[] gatedInformationSequence
	= { new Attribute(251, 3, null, null),
	    new Attribute(255, 3, null, null),
	    new Attribute(644, 2, null, null, dataInformationSequence) };
    static final ICondition gatedInformationSequenceSizeFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(642);
	    if (i == 2147483647)
		return true;
	    return i == dicomobject.getSize(643);
	}
    };
    static final Attribute[] multiGatedAcquisitionModule
	= { new Attribute(266, 3, null,
			  new IfEqual(266, new String[] { "Y", "N" })),
	    new Attribute(271, 3, null, null),
	    new Attribute(272, 3, null, null),
	    new Attribute(273, 3, null, null),
	    new Attribute(643, 2, ifRRIntervalVector,
			  gatedInformationSequenceSizeFilter,
			  gatedInformationSequence) };
    static final Attribute[] phaseInformationSequence
	= { new Attribute(635, 1, null, null),
	    new Attribute(320, 1, null, null),
	    new Attribute(636, 1, null, null),
	    new Attribute(634, 1, null, null),
	    new Attribute(656, 3, null, null),
	    new Attribute(657, 1, new IfPresent(656), null) };
    static final ICondition phaseInformationSequenceSizeFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(632);
	    if (i == 2147483647)
		return true;
	    return i == dicomobject.getSize(633);
	}
    };
    static final Attribute[] phaseModule
	= { new Attribute(633, 2, null, phaseInformationSequenceSizeFilter,
			  phaseInformationSequence) };
    static final Attribute[] reconstructionModule
	= { new Attribute(221, 2, null, null),
	    new Attribute(276, 3, null, null),
	    new Attribute(319, 3, null, null),
	    new Attribute(205, 2, null, null),
	    new Attribute(451, 3, null, null) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem(patientOrientationModule, null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .frameOfReferenceModule),
					     CommonImage.uFrameOfReference),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(imagePixelModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.multiFrameModule,
					     null),
	    new CompositeIOD.ModuleTableItem(multiFrameModule, null),
	    new CompositeIOD.ModuleTableItem(imageModule, null),
	    new CompositeIOD.ModuleTableItem(isotopeModule, null),
	    new CompositeIOD.ModuleTableItem(detectorModule, null),
	    (new CompositeIOD.ModuleTableItem
	     (tomoAcquisitionModule,
	      new IfEqual(58, 2,
			  new String[] { "TOMO", "GATED TOMO", "RECON TOMO",
					 "RECON GATED TOMO" }))),
	    (new CompositeIOD.ModuleTableItem
	     (multiGatedAcquisitionModule,
	      new IfEqual(58, 2, new String[] { "GATED", "GATED TOMO",
						"RECON GATED TOMO" }))),
	    new CompositeIOD.ModuleTableItem(phaseModule,
					     new IfEqual(58, 2, "DYNAMIC")),
	    (new CompositeIOD.ModuleTableItem
	     (reconstructionModule,
	      new IfEqual(58, 2,
			  new String[] { "RECON TOMO", "RECON GATED TOMO" }))),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem((OverlayModules
					      .multiFrameOverlayModule),
					     (OverlayModules
					      .uMultiFrameOverlay)),
	    new CompositeIOD.ModuleTableItem(CurveModules.curveModule,
					     CurveModules.uCurve),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, cPatientOrientationModifier,
	    CommonImage.uFrameOfReference,
	    CommonImage.cImagesTemporallyRelated,
	    CommonImage.cLossyImageCompression,
	    CommonImage.cPixelAspectRatioNot11, cViewAngulationModifier,
	    OverlayModules.uOverlayPlane, OverlayModules.uMultiFrameOverlay,
	    CurveModules.uCurve, LUTModules.uVOILUT,
	    GeneralModules.cSpecificCharacterSet };
    
    private NMImageIOD() {
	/* empty */
    }
}
