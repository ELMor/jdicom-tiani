/* DefCallbackUser - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import java.util.Hashtable;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class DefCallbackUser extends Hashtable implements ICallbackUser
{
    static final ICallbackUser yes = new ICallbackUser() {
	public boolean isTrue(String string, DicomObject dicomobject)
	    throws DicomException {
	    return true;
	}
    };
    static final ICallbackUser no = new ICallbackUser() {
	public boolean isTrue(String string, DicomObject dicomobject)
	    throws DicomException {
	    return false;
	}
    };
    
    public boolean isTrue(String string, DicomObject dicomobject)
	throws DicomException {
	ICallbackUser icallbackuser = (ICallbackUser) this.get(string);
	return (icallbackuser != null
		&& icallbackuser.isTrue(string, dicomobject));
    }
    
    public void put(String string, boolean bool) {
	super.put(string, bool ? yes : no);
    }
    
    public boolean isTrue(String string) {
	return this.get(string) == yes;
    }
}
