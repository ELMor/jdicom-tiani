/* AttributeContext - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public class AttributeContext
{
    private AttributeContext parent;
    private int dname;
    private int index;
    
    public AttributeContext(AttributeContext attributecontext_0_, int i,
			    int i_1_) {
	parent = attributecontext_0_;
	dname = i;
	index = i_1_;
    }
    
    public AttributeContext parent() {
	return parent;
    }
    
    public int dname() {
	return dname;
    }
    
    public int index() {
	return index;
    }
    
    public String toString() {
	StringBuffer stringbuffer = new StringBuffer();
	if (parent != null)
	    stringbuffer.append(parent.toString());
	stringbuffer.append(Attribute.getTagDescription(dname));
	stringbuffer.append('[');
	stringbuffer.append(index);
	stringbuffer.append("]>");
	return stringbuffer.toString();
    }
}
