/* IfAnyEqualInt - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfAnyEqualInt implements ICondition
{
    private int dname;
    private int[] values;
    
    public IfAnyEqualInt(int i, int[] is) {
	dname = i;
	values = is;
    }
    
    public IfAnyEqualInt(int i, int i_0_) {
	this(i, new int[] { i_0_ });
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	int i = dicomobject.getSize(dname);
	for (int i_1_ = 0; i_1_ < i; i_1_++) {
	    int i_2_ = dicomobject.getI(dname, i_1_);
	    for (int i_3_ = 0; i_3_ < values.length; i_3_++) {
		if (i_2_ == values[i_3_])
		    return true;
	    }
	}
	return false;
    }
}
