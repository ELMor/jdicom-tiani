/* UserOption - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class UserOption implements ICondition
{
    private String _option;
    
    public UserOption(String string) {
	_option = string;
    }
    
    public String option() {
	return _option;
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	return (icallbackuser != null
		? icallbackuser.isTrue(_option, dicomobject) : false);
    }
}
