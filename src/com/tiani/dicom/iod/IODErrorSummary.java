/* IODErrorSummary - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import java.util.Enumeration;
import java.util.Vector;

public class IODErrorSummary
{
    private int[][] count = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
    private Vector offendingElements = new Vector();
    
    public IODErrorSummary(Vector vector) {
	Enumeration enumeration = vector.elements();
	while (enumeration.hasMoreElements()) {
	    IODError ioderror = (IODError) enumeration.nextElement();
	    count[ioderror.kind()][ioderror.type() - 1]++;
	    offendingElements.addElement(ioderror.getAT());
	}
    }
    
    public Enumeration offendingElements() {
	return offendingElements.elements();
    }
    
    public Integer[] getOffendingElements() {
	Integer[] integers = new Integer[offendingElements.size()];
	offendingElements.copyInto(integers);
	return integers;
    }
    
    public final int missingAttrib(int i) {
	return count[0][i - 1];
    }
    
    public final int missingAttrib() {
	return count[0][0] + count[0][1] + count[0][2];
    }
    
    public final int emptyData(int i) {
	return count[1][i];
    }
    
    public final int emptyData() {
	return count[1][0] + count[1][1] + count[1][2];
    }
    
    public final int invalidData(int i) {
	return count[2][i - 1];
    }
    
    public final int invalidData() {
	return count[2][0] + count[2][1] + count[2][2];
    }
    
    public final int type(int i) {
	return missingAttrib(i) + emptyData(i) + invalidData(i);
    }
}
