/* XRFImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class XRFImageIOD extends XRayImageIOD
{
    static final UserOption uXRFPositioner
	= new UserOption("U:XRF Positioner");
    static final Attribute[] positionerModule
	= { new Attribute(278, 3, null, null),
	    new Attribute(277, 3, null, null),
	    new Attribute(279, 3, null, null),
	    new Attribute(1196, 3, null, null) };
    static final Attribute[] tomoAcquisitionModule
	= { new Attribute(342, 1, null, null),
	    new Attribute(343, 3, null, null),
	    new Attribute(344, 3, null, null),
	    new Attribute(1266, 3, null, null),
	    new Attribute(1267, 3, null, null),
	    new Attribute(1268, 3, null, null) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule,
					     CommonImage.cContrastBolusUsed),
	    new CompositeIOD.ModuleTableItem(CommonImage.cineModule,
					     CommonImage.cMultiFrameCineImage),
	    new CompositeIOD.ModuleTableItem(CommonImage.multiFrameModule,
					     CommonImage.cMultiFrameCineImage),
	    new CompositeIOD.ModuleTableItem(CommonImage.framePointersModule,
					     CommonImage.uFramePointers),
	    new CompositeIOD.ModuleTableItem(CommonImage.maskModule,
					     (CommonImage
					      .cImageMayBeSubtracted)),
	    new CompositeIOD.ModuleTableItem(CommonImage.displayShutterModule,
					     CommonImage.uDisplayShutter),
	    new CompositeIOD.ModuleTableItem(CommonImage.deviceModule,
					     CommonImage.uDevice),
	    new CompositeIOD.ModuleTableItem(CommonImage.therapyModule,
					     CommonImage.uTherapy),
	    new CompositeIOD.ModuleTableItem(XRayImageIOD.imageModule, null),
	    new CompositeIOD.ModuleTableItem(XRayImageIOD.acquisitionModule,
					     null),
	    new CompositeIOD.ModuleTableItem(XRayImageIOD.collimatorModule,
					     XRayImageIOD.uXRayCollimator),
	    new CompositeIOD.ModuleTableItem(XRayImageIOD.tableModule,
					     XRayImageIOD.uXRayTable),
	    new CompositeIOD.ModuleTableItem(positionerModule, uXRFPositioner),
	    new CompositeIOD.ModuleTableItem(tomoAcquisitionModule,
					     new IfAnyEqual(185, "TOMO")),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem((OverlayModules
					      .multiFrameOverlayModule),
					     (OverlayModules
					      .cMultiFrameOverlay)),
	    new CompositeIOD.ModuleTableItem(CurveModules.curveModule,
					     CurveModules.uCurve),
	    (new CompositeIOD.ModuleTableItem
	     (LUTModules.modalityLUTModule,
	      new IfOr(new ICondition[]
		       { new IfEqual(486, "LOG"),
			 new IfAnd(new ICondition[]
				   { new IfEqual(486, "DISP"),
				     LUTModules.uModalityLUT }) }))),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated,
	    CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed,
	    CommonImage.cMultiFrameImage, CommonImage.cMultiFrameCineImage,
	    CommonImage.uFramePointers, CommonImage.cImageMayBeSubtracted,
	    XRayImageIOD.uXRayCollimator, CommonImage.uDisplayShutter,
	    CommonImage.uTherapy, CommonImage.uDevice, XRayImageIOD.uXRayTable,
	    uXRFPositioner, OverlayModules.uOverlayPlane,
	    OverlayModules.cMultiFrameOverlay, CurveModules.uCurve,
	    LUTModules.uModalityLUT, LUTModules.uVOILUT,
	    GeneralModules.cSpecificCharacterSet };
    
    private XRFImageIOD() {
	/* empty */
    }
}
