/* BasicStudyDescriptorIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class BasicStudyDescriptorIOD
{
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientSummaryModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.studyContentModule,
					     null),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.cSeriesRetrieveAET, CommonImage.cSeriesFilesetUID,
	    CommonImage.cImageRetrieveAET, CommonImage.cImageFilesetUID };
    
    private BasicStudyDescriptorIOD() {
	/* empty */
    }
}
