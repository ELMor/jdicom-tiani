/* IfSizeEqual - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfSizeEqual implements ICondition
{
    private int dname;
    private int size;
    
    public IfSizeEqual(int i, int i_0_) {
	dname = i;
	size = i_0_;
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	return dicomobject.getSize(dname) == size;
    }
}
