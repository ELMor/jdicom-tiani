/* IfEqualInt - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfEqualInt implements ICondition
{
    private int dname;
    private int index;
    private int[] values;
    
    public IfEqualInt(int i, int i_0_, int[] is) {
	dname = i;
	index = i_0_;
	values = is;
    }
    
    public IfEqualInt(int i, int[] is) {
	this(i, 0, is);
    }
    
    public IfEqualInt(int i, int i_1_, int i_2_) {
	this(i, i_1_, new int[] { i_2_ });
    }
    
    public IfEqualInt(int i, int i_3_) {
	this(i, 0, i_3_);
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	int i = dicomobject.getI(dname, index);
	for (int i_4_ = 0; i_4_ < values.length; i_4_++) {
	    if (i == values[i_4_])
		return true;
	}
	return false;
    }
}
