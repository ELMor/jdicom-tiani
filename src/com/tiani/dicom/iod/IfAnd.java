/* IfAnd - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfAnd implements ICondition
{
    private ICondition[] args;
    
    public IfAnd(ICondition[] iconditions) {
	args = iconditions;
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	for (int i = 0; i < args.length; i++) {
	    if (!args[i].isTrue(dicomobject, icallbackuser))
		return false;
	}
	return true;
    }
}
