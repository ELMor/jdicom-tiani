/* CompositeIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class CompositeIOD
{
    private Hashtable table = new Hashtable();
    public static final CompositeIOD crImage
	= new CompositeIOD(CRImageIOD.moduleTable);
    public static final CompositeIOD ctImage
	= new CompositeIOD(CTImageIOD.moduleTable);
    public static final CompositeIOD mrImage
	= new CompositeIOD(MRImageIOD.moduleTable);
    public static final CompositeIOD nmImage
	= new CompositeIOD(NMImageIOD.moduleTable);
    public static final CompositeIOD usImage
	= new CompositeIOD(USImageIOD.moduleTable);
    public static final CompositeIOD usMultiFrameImage
	= new CompositeIOD(USMultiFrameImageIOD.moduleTable);
    public static final CompositeIOD usCurve
	= new CompositeIOD(USImageIOD.moduleTableCurve);
    public static final CompositeIOD usMultiFrameCurve
	= new CompositeIOD(USMultiFrameImageIOD.moduleTableCurve);
    public static final CompositeIOD scImage
	= new CompositeIOD(SCImageIOD.moduleTable);
    public static final CompositeIOD standaloneOverlay
	= new CompositeIOD(StandaloneOverlayIOD.moduleTable);
    public static final CompositeIOD standaloneCurve
	= new CompositeIOD(StandaloneCurveIOD.moduleTable);
    public static final CompositeIOD basicStudyDescriptor
	= new CompositeIOD(BasicStudyDescriptorIOD.moduleTable);
    public static final CompositeIOD standaloneModalityLUT
	= new CompositeIOD(StandaloneModalityLUTIOD.moduleTable);
    public static final CompositeIOD standaloneVOILUT
	= new CompositeIOD(StandaloneVOILUTIOD.moduleTable);
    public static final CompositeIOD xaImage
	= new CompositeIOD(XAImageIOD.moduleTable);
    public static final CompositeIOD xrfImage
	= new CompositeIOD(XRFImageIOD.moduleTable);
    public static final CompositeIOD petImage
	= new CompositeIOD(PETImageIOD.moduleTable);
    public static final CompositeIOD standalonePETCurve
	= new CompositeIOD(StandalonePETCurveIOD.moduleTable);
    public static final CompositeIOD gsPresentationState
	= new CompositeIOD(GSPresentationStateIOD.moduleTable);
    
    static final class ModuleTableItem
    {
	private Attribute[] module;
	private ICondition cond;
	
	public ModuleTableItem(Attribute[] attributes, ICondition icondition) {
	    module = attributes;
	    cond = icondition;
	}
	
	public final void copyTo(Hashtable hashtable) {
	    for (int i = 0; i < module.length; i++) {
		Attribute attribute = module[i];
		hashtable.put(new Integer(attribute.dname()),
			      new AttributeTableItem(attribute, cond));
	    }
	}
    }
    
    static final class AttributeTableItem
    {
	private Attribute attrib;
	private ICondition cond;
	
	public AttributeTableItem(Attribute attribute, ICondition icondition) {
	    attrib = attribute;
	    cond = icondition;
	}
	
	public boolean validate
	    (DicomObject dicomobject, Vector vector,
	     ICallbackUser icallbackuser)
	    throws DicomException {
	    if (cond != null && !cond.isTrue(dicomobject, icallbackuser))
		return true;
	    return attrib.validate(dicomobject, vector, icallbackuser, null);
	}
    }
    
    public static CompositeIOD getIOD
	(String string, DicomObject dicomobject) throws DicomException {
	if (string != null && string.length() != 0) {
	    if (string.equals("1.2.840.10008.5.1.4.1.1.1"))
		return crImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.2"))
		return ctImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.4"))
		return mrImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.20"))
		return nmImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.128"))
		return petImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.7"))
		return scImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.9"))
		return standaloneCurve;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.10"))
		return standaloneModalityLUT;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.8"))
		return standaloneOverlay;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.9"))
		return basicStudyDescriptor;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.11"))
		return standaloneVOILUT;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.129"))
		return standalonePETCurve;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.6.1")) {
		if (dicomobject.getSize(1184) != -1)
		    return usImage;
		return usCurve;
	    }
	    if (string.equals("1.2.840.10008.5.1.4.1.1.4.1")) {
		if (dicomobject.getSize(1184) != -1)
		    return usMultiFrameImage;
		return usMultiFrameCurve;
	    }
	    if (string.equals("1.2.840.10008.5.1.4.1.1.12.1"))
		return xaImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.12.2"))
		return xrfImage;
	    if (string.equals("1.2.840.10008.5.1.4.1.1.11.1"))
		return gsPresentationState;
	}
	return null;
    }
    
    public CompositeIOD(ModuleTableItem[] moduletableitems) {
	for (int i = 0; i < moduletableitems.length; i++)
	    moduletableitems[i].copyTo(table);
    }
    
    public boolean validate
	(DicomObject dicomobject, Vector vector, ICallbackUser icallbackuser)
	throws DicomException {
	boolean bool = true;
	Enumeration enumeration = table.elements();
	while (enumeration.hasMoreElements()) {
	    AttributeTableItem attributetableitem
		= (AttributeTableItem) enumeration.nextElement();
	    if (!attributetableitem.validate(dicomobject, vector,
					     icallbackuser))
		bool = false;
	}
	return bool;
    }
}
