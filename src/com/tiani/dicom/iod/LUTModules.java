/* LUTModules - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class LUTModules
{
    static final UserOption cModalityLUT
	= new UserOption("C:Apply Modality LUT");
    static final UserOption uModalityLUT = new UserOption("U:Modality LUT");
    static final Attribute[] modalityLUTSequence
	= { new Attribute(506, 1, null, null),
	    new Attribute(507, 3, null, null),
	    new Attribute(508, 1, null, null),
	    new Attribute(509, 1, null, null) };
    static final ICondition ifRescaleInterceptPresent = new IfPresent(489);
    static final Attribute[] modalityLUTModule
	= { new Attribute(505, 3, null, null, modalityLUTSequence),
	    new Attribute(489, 1, new IfNot(new IfPresent(505)), null),
	    new Attribute(490, 1, ifRescaleInterceptPresent, null),
	    new Attribute(491, 1, ifRescaleInterceptPresent, null) };
    static final UserOption uVOILUT = new UserOption("U:VOI LUT");
    static final Attribute[] LUTSequence
	= { new Attribute(506, 1, null, null),
	    new Attribute(507, 3, null, null),
	    new Attribute(509, 1, null, null) };
    static final ICondition isWindowWidthVM = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    return dicomobject.getSize(488) == dicomobject.getSize(487);
	}
    };
    static final Attribute[] VOILUTModule
	= { new Attribute(510, 3, null, null, LUTSequence),
	    new Attribute(487, 3, null, null),
	    new Attribute(488, 1, new IfPresent(487), isWindowWidthVM),
	    new Attribute(492, 3, null, null) };
    static final Attribute[] LUTIdentificationModule
	= { new Attribute(439, 2, null, null),
	    new Attribute(113, 3, null, null,
			  CommonImage.referencedSOPSequence) };
    static final String[] SHAPE_VALUES = { "IDENTITY", "INVERSE" };
    static final Attribute[] presentationLUTModule
	= { new Attribute(1239, 1, new IfNot(new IfPresent(1240)),
			  new IfSizeEqual(1239, 1), LUTSequence),
	    new Attribute(1240, 1, new IfNot(new IfPresent(1239)),
			  new IfEqual(1240, SHAPE_VALUES)) };
    static final UserOption cSoftcopyVOILUT
	= new UserOption("C:Apply VOI LUT");
    static final UserOption cVOILUTNotApplyToAll
	= new UserOption("C:VOI LUT not apply to all");
    static final Attribute[] softcopyVOILUTSequenceModule
	= { new Attribute(113, 1, cVOILUTNotApplyToAll, null,
			  CommonImage.referencedMFImageSequence),
	    new Attribute(510, 1, new IfNot(new IfPresent(487)),
			  new IfSizeEqual(510, 1), LUTSequence),
	    new Attribute(487, 3, null, null),
	    new Attribute(488, 1, new IfPresent(487), isWindowWidthVM),
	    new Attribute(492, 3, null, null) };
    static final Attribute[] softcopyVOILUTModule
	= { new Attribute(1408, 1, null, null, softcopyVOILUTSequenceModule) };
    
    private LUTModules() {
	/* empty */
    }
}
