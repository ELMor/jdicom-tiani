/* CurveModules - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

final class CurveModules
{
    static final Attribute[] curveIdentificationModule
	= { new Attribute(438, 2, null, null),
	    new Attribute(69, 3, null, null), new Attribute(75, 3, null, null),
	    new Attribute(113, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(112, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(114, 3, null, null,
			  CommonImage.referencedSOPSequence) };
    static final UserOption uCurve = new UserOption("U:Curve");
    static final Attribute[] curveModule = new Attribute[0];
    static final UserOption uAudio = new UserOption("U:Audio");
    static final Attribute[] audioModule = new Attribute[0];
    
    private CurveModules() {
	/* empty */
    }
}
