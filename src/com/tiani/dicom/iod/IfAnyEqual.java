/* IfAnyEqual - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfAnyEqual implements ICondition
{
    private int dname;
    private Object[] values;
    
    public IfAnyEqual(int i, Object[] objects) {
	dname = i;
	values = objects;
    }
    
    public IfAnyEqual(int i, Object object) {
	this(i, new Object[] { object });
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	int i = dicomobject.getSize(dname);
	for (int i_0_ = 0; i_0_ < i; i_0_++) {
	    Object object = dicomobject.get(dname, i_0_);
	    for (int i_1_ = 0; i_1_ < values.length; i_1_++) {
		if (object.equals(values[i_1_]))
		    return true;
	    }
	}
	return false;
    }
}
