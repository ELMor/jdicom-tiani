/* CommonImage - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class CommonImage
{
    public static final Attribute[] referencedSOPSequence
	= { new Attribute(115, 1, null, null),
	    new Attribute(116, 1, null, null) };
    public static final Attribute[] codeSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null) };
    static final UserOption cRefMultiframeImage
	= new UserOption("C:Ref Multiframe Image");
    public static final Attribute[] referencedMFImageSequence
	= { new Attribute(115, 1, null, null),
	    new Attribute(116, 1, null, null),
	    new Attribute(117, 1, cRefMultiframeImage, null) };
    static final ICondition enumPatientSex
	= new IfEqual(152, new String[] { "M", "F", "O" });
    static final Attribute[] patientModule
	= { new Attribute(147, 2, null, null),
	    new Attribute(148, 2, null, null),
	    new Attribute(150, 2, null, null),
	    new Attribute(152, 2, null, enumPatientSex),
	    new Attribute(110, 3, null, new IfSizeEqual(110, 1),
			  referencedSOPSequence),
	    new Attribute(151, 3, null, null),
	    new Attribute(154, 3, null, null),
	    new Attribute(155, 3, null, null),
	    new Attribute(170, 3, null, null),
	    new Attribute(177, 3, null, null) };
    static final Attribute[] generalStudyModule
	= { new Attribute(425, 1, null, null),
	    new Attribute(64, 2, null, null), new Attribute(70, 2, null, null),
	    new Attribute(88, 2, null, null),
	    new Attribute(427, 2, null, null),
	    new Attribute(77, 2, null, null), new Attribute(95, 3, null, null),
	    new Attribute(99, 3, null, null),
	    new Attribute(101, 3, null, null),
	    new Attribute(107, 3, null, new IfSizeInRange(107, 0, 1),
			  referencedSOPSequence) };
    static final UserOption uPatientStudy = new UserOption("U:Patient Study");
    static final Attribute[] patientStudyModule
	= { new Attribute(103, 3, null, null),
	    new Attribute(157, 3, null, null),
	    new Attribute(158, 3, null, null),
	    new Attribute(159, 3, null, null),
	    new Attribute(171, 3, null, null),
	    new Attribute(173, 3, null, null) };
    static final ICondition requirePatientPositionModality
	= new IfEqual(81, new String[] { "CT", "MR" });
    static final ICondition enumLaterality
	= new IfEqual(443, new String[] { "L", "R" });
    static final ICondition requireLateralityBodyPart
	= new IfEqual(182,
		      new String[] { "CLAVICLE", "BREAST", "HIP", "SHOULDER",
				     "ELBOW", "KNEE", "ANKLE", "HAND", "FOOT",
				     "EXTREMITY", "HEAD", "LEG", "ARM" });
    static final Attribute[] requestAttributesSequence
	= { new Attribute(589, 1, null, null),
	    new Attribute(582, 1, null, null),
	    new Attribute(580, 3, null, null),
	    new Attribute(581, 3, null, new IfSizeInRange(581, 1, 2147483647),
			  codeSequence) };
    static final Attribute[] generalSeriesModule
	= { new Attribute(81, 1, null, null),
	    new Attribute(426, 1, null, null),
	    new Attribute(428, 2, null, null),
	    new Attribute(443, 2, requireLateralityBodyPart, enumLaterality),
	    new Attribute(65, 3, null, null), new Attribute(71, 3, null, null),
	    new Attribute(100, 3, null, null),
	    new Attribute(239, 3, null, null),
	    new Attribute(97, 3, null, null),
	    new Attribute(102, 3, null, null),
	    new Attribute(108, 3, null, new IfSizeInRange(108, 0, 1),
			  referencedSOPSequence),
	    new Attribute(182, 3, null, null),
	    new Attribute(380, 2, requirePatientPositionModality, null),
	    new Attribute(481, 3, null, null),
	    new Attribute(482, 3, null, null),
	    new Attribute(1211, 3, null,
			  new IfSizeInRange(1211, 1, 2147483647),
			  requestAttributesSequence),
	    new Attribute(1206, 3, null, null),
	    new Attribute(1201, 3, null, null),
	    new Attribute(1202, 3, null, null),
	    new Attribute(1207, 3, null, null) };
    static final UserOption uFrameOfReference
	= new UserOption("U:Frame Of Reference");
    static final Attribute[] frameOfReferenceModule
	= { new Attribute(442, 1, null, null),
	    new Attribute(450, 2, null, null) };
    static final UserOption uGeneralEquipment
	= new UserOption("U:General Equipment");
    static final Attribute[] generalEquipmentModule
	= { new Attribute(84, 2, null, null), new Attribute(85, 3, null, null),
	    new Attribute(86, 3, null, null), new Attribute(94, 3, null, null),
	    new Attribute(98, 3, null, null),
	    new Attribute(105, 3, null, null),
	    new Attribute(228, 3, null, null),
	    new Attribute(236, 3, null, null),
	    new Attribute(250, 3, null, null),
	    new Attribute(317, 3, null, null),
	    new Attribute(318, 3, null, null),
	    new Attribute(485, 3, null, null) };
    static final UserOption cImagesTemporallyRelated
	= new UserOption("C:Images Temporally Related");
    static final UserOption cLossyImageCompression
	= new UserOption("C:Lossy Image Compression");
    static final ICondition enumImageType
	= new IfMultiEqual(58, new String[][] { { "ORIGINAL", "DERIVED" },
						{ "PRIMARY", "SECONDARY" } });
    static final ICondition enumLossyImageCompression
	= new IfEqual(504, new String[] { "00", "01" });
    static final Attribute[] generalImageModule
	= { new Attribute(430, 2, null, null),
	    new Attribute(436, 2,
			  new IfNot(new IfOr(new ICondition[]
					     { new IfPresent(441),
					       new IfPresent(666) })),
			  null),
	    new Attribute(67, 2, cImagesTemporallyRelated, null),
	    new Attribute(73, 2, cImagesTemporallyRelated, null),
	    new Attribute(58, 3, null, enumImageType),
	    new Attribute(429, 3, null, null),
	    new Attribute(66, 3, null, null), new Attribute(72, 3, null, null),
	    new Attribute(113, 3, null, null, referencedSOPSequence),
	    new Attribute(122, 3, null, null),
	    new Attribute(123, 3, null, null, referencedSOPSequence),
	    new Attribute(448, 3, null, null),
	    new Attribute(459, 3, null, null),
	    new Attribute(504, 3, null, enumLossyImageCompression) };
    static final Attribute[] imagePlaneModule
	= { new Attribute(470, 1, null, null),
	    new Attribute(441, 1, null, null),
	    new Attribute(440, 1, null, null),
	    new Attribute(205, 2, null, null),
	    new Attribute(451, 3, null, null) };
    static final ICondition isMonochrome2 = new IfEqual(462, "MONOCHROME2");
    static final ICondition isMonochrome2OrPaletteColor
	= new IfEqual(462, new String[] { "MONOCHROME2", "PALETTE COLOR" });
    static final ICondition isMonochrome
	= new IfEqual(462, new String[] { "MONOCHROME1", "MONOCHROME2" });
    static final ICondition enumPI1sample
	= new IfEqual(462, new String[] { "MONOCHROME1", "MONOCHROME2",
					  "PALETTE COLOR" });
    static final ICondition enumPI3sample
	= new IfEqual(462, new String[] { "RGB", "HSV", "YBR_FULL",
					  "YBR_FULL_422", "YBR_PARTIAL_422" });
    static final ICondition enumPI4sample
	= new IfEqual(462, new String[] { "ARGB", "CMYK" });
    static final ICondition requirePaletteColorLUT
	= new IfEqual(462, new String[] { "PALETTE COLOR", "ARGB" });
    static final ICondition is1SamplePerPixel = new IfEqualInt(461, 1);
    static final ICondition samplesPerPixelValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    boolean bool = false;
	    int i;
	    if (CommonImage.enumPI1sample.isTrue(dicomobject, icallbackuser))
		i = 1;
	    else if (CommonImage.enumPI3sample.isTrue(dicomobject,
						      icallbackuser))
		i = 3;
	    else if (CommonImage.enumPI4sample.isTrue(dicomobject,
						      icallbackuser))
		i = 4;
	    else
		return true;
	    return dicomobject.getI(461) == i;
	}
    };
    static final ICondition is16BitsAllocated = new IfEqualInt(475, 16);
    static final ICondition is8or16BitsAllocated
	= new IfEqualInt(475, new int[] { 8, 16 });
    static final ICondition bitsStoredValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(475);
	    int i_0_ = dicomobject.getI(476);
	    return i_0_ > 0 && i_0_ <= i;
	}
    };
    static final ICondition isBitsStoredEqualsBitsAllocated = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(475);
	    int i_1_ = dicomobject.getI(476);
	    return i == 2147483647 || i_1_ == i;
	}
    };
    static final ICondition highBitValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(475);
	    int i_2_ = dicomobject.getI(476);
	    int i_3_ = dicomobject.getI(477);
	    return i_2_ == 2147483647 || i_3_ >= i_2_ - 1 && i_3_ <= i - 1;
	}
    };
    static final ICondition isHighBitEqualsBitsStoredLess1 = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    int i = dicomobject.getI(476);
	    int i_4_ = dicomobject.getI(477);
	    return i == 2147483647 || i_4_ + 1 == i;
	}
    };
    static final ICondition isPixelRepresentation0 = new IfEqualInt(478, 0);
    static final UserOption cPixelAspectRatioNot11
	= new UserOption("C:Pixel Aspect Ratio not 1:1");
    static final Attribute[] imagePixelModule
	= { new Attribute(461, 1, null, samplesPerPixelValueFilter),
	    new Attribute(462, 1, null, null),
	    new Attribute(466, 1, null, null),
	    new Attribute(467, 1, null, null),
	    new Attribute(475, 1, null, null),
	    new Attribute(476, 1, null, bitsStoredValueFilter),
	    new Attribute(477, 1, null, highBitValueFilter),
	    new Attribute(478, 1, null,
			  new IfEqualInt(478, new int[] { 0, 1 })),
	    new Attribute(1184, 1, null, null),
	    new Attribute(463, 1, new IfNot(new IfEqualInt(461, 1)),
			  new IfEqualInt(463, new int[] { 0, 1 })),
	    new Attribute(473, 1, cPixelAspectRatioNot11, null),
	    new Attribute(479, 3, null, null),
	    new Attribute(480, 3, null, null),
	    new Attribute(494, 1, requirePaletteColorLUT, null),
	    new Attribute(495, 1, requirePaletteColorLUT, null),
	    new Attribute(496, 1, requirePaletteColorLUT, null),
	    new Attribute(498, 1, requirePaletteColorLUT, null),
	    new Attribute(499, 1, requirePaletteColorLUT, null),
	    new Attribute(500, 1, requirePaletteColorLUT, null) };
    static final UserOption cContrastBolusUsed
	= new UserOption("C:Contrast/Bolus Used");
    static final Attribute[] contrastBolusAdministrationRouteSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(193, 3, null, null, codeSequence) };
    static final Attribute[] contrastBolusModule
	= { new Attribute(179, 2, null, null),
	    new Attribute(180, 3, null, new IfSizeEqual(180, 1), codeSequence),
	    new Attribute(240, 3, null, null),
	    new Attribute(181, 3, null, new IfSizeEqual(181, 1),
			  contrastBolusAdministrationRouteSequence),
	    new Attribute(241, 3, null, null),
	    new Attribute(242, 3, null, null),
	    new Attribute(243, 3, null, null),
	    new Attribute(244, 3, null, null),
	    new Attribute(246, 3, null, null),
	    new Attribute(247, 3, null, null),
	    new Attribute(248, 3, null, null),
	    new Attribute(249, 3, null, null) };
    static final UserOption cMultiFrameCineImage
	= new UserOption("C:Multi-Frame Cine Image");
    static final Attribute[] cineModule
	= { new Attribute(322, 3, null, null),
	    new Attribute(254, 1, new IfEqualInt(465, 1577059), null),
	    new Attribute(256, 1, new IfEqualInt(465, 1577061), null),
	    new Attribute(132, 3, null, null),
	    new Attribute(133, 3, null, null),
	    new Attribute(134, 3, null, null),
	    new Attribute(204, 3, null, null),
	    new Attribute(257, 3, null, null),
	    new Attribute(209, 3, null, null),
	    new Attribute(320, 3, null, null) };
    static final UserOption cMultiFrameImage
	= new UserOption("C:Multi-Frame Image");
    static final Attribute[] multiFrameModule
	= { new Attribute(464, 1, null, null),
	    new Attribute(465, 1, null, null) };
    static final UserOption uFramePointers
	= new UserOption("U:Frame Pointers");
    static final Attribute[] framePointersModule
	= { new Attribute(512, 3, null, null),
	    new Attribute(513, 3, null, null),
	    new Attribute(514, 3, null, null) };
    static final UserOption cImageMayBeSubtracted
	= new UserOption("C:Image May Be Subtracted");
    static final Attribute[] maskSubtractionSequence
	= { new Attribute(518, 1, null, null),
	    new Attribute(519, 3, null, null),
	    new Attribute(520, 1, new IfEqual(518, "AVG_SUB"), null),
	    new Attribute(521, 3, null, null),
	    new Attribute(522, 3, null, null),
	    new Attribute(523, 2, new IfEqual(518, "TID"), null),
	    new Attribute(524, 3, null, null) };
    static final Attribute[] maskModule
	= { new Attribute(517, 1, null, null, maskSubtractionSequence),
	    new Attribute(493, 2, null, null) };
    static final UserOption uDisplayShutter
	= new UserOption("U:Display Shutter");
    static final UserOption cDisplayShutter
	= new UserOption("C:Display Shutter");
    static final ICondition ifShutterRectangular
	= new IfAnyEqual(352, "RECTANGULAR");
    static final ICondition ifShutterCircular
	= new IfAnyEqual(352, "CIRCULAR");
    static final ICondition ifShutterPolygonal
	= new IfAnyEqual(352, "POLYGONAL");
    static final Attribute[] displayShutterModule
	= { new Attribute(352, 1, null,
			  new IfAllEqual(352, new String[] { "RECTANGULAR",
							     "CIRCULAR",
							     "POLYGONAL" })),
	    new Attribute(353, 1, ifShutterRectangular, null),
	    new Attribute(354, 1, ifShutterRectangular, null),
	    new Attribute(355, 1, ifShutterRectangular, null),
	    new Attribute(356, 1, ifShutterRectangular, null),
	    new Attribute(357, 1, ifShutterCircular, null),
	    new Attribute(358, 1, ifShutterCircular, null),
	    new Attribute(359, 1, ifShutterPolygonal, null),
	    new Attribute(1377, 3, null, new IfInRange(1377, 0, 65535)) };
    static final UserOption uDevice = new UserOption("U:Device");
    static final Attribute[] deviceSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(612, 3, null, null),
	    new Attribute(613, 3, null, null),
	    new Attribute(614, 2, new IfPresent(613), null),
	    new Attribute(615, 3, null, null),
	    new Attribute(616, 3, null, null),
	    new Attribute(617, 3, null, null) };
    static final Attribute[] deviceModule
	= { new Attribute(611, 3, null, null, deviceSequence) };
    static final UserOption uTherapy = new UserOption("U:Therapy");
    static final Attribute[] interventionalTherapySequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(202, 2, null,
			  new IfEqual(202,
				      new String[] { "PRE", "INTERMEDIATE",
						     "POST", "NONE" })),
	    new Attribute(192, 3, null, null, codeSequence),
	    new Attribute(199, 3, null, null),
	    new Attribute(190, 3, null, null),
	    new Attribute(661, 3, null, new IfSizeEqual(661, 1), codeSequence),
	    new Attribute(203, 3, null, null) };
    static final Attribute[] therapyModule
	= { new Attribute(200, 3, null, null, interventionalTherapySequence) };
    static final UserOption cBitmapDisplayShutter
	= new UserOption("C:Bitmap Display Shutter");
    static final Attribute[] bitmapDisplayShutter
	= { new Attribute(352, 1, null, new IfEqual(352, "BITMAP")),
	    new Attribute(1378, 1, null, null),
	    new Attribute(1377, 3, null, new IfInRange(1377, 0, 65535)) };
    static final Attribute[] patientSummaryModule
	= { new Attribute(147, 2, null, null),
	    new Attribute(148, 2, null, null) };
    static final UserOption cSeriesRetrieveAET
	= new UserOption("C:Series Retrieve AET");
    static final UserOption cSeriesFilesetUID
	= new UserOption("C:Series Fileset (U)ID");
    static final UserOption cImageRetrieveAET
	= new UserOption("C:Image Retrieve AET");
    static final UserOption cImageFilesetUID
	= new UserOption("C:Image Fileset (U)ID");
    static final Attribute[] referencedImageSequence
	= { new Attribute(115, 2, null, null),
	    new Attribute(116, 2, null, null),
	    new Attribute(79, 2, cImageRetrieveAET, null),
	    new Attribute(697, 2, cImageFilesetUID, null),
	    new Attribute(698, 2, cImageFilesetUID, null) };
    static final Attribute[] referencedSeriesSequence
	= { new Attribute(426, 1, null, null),
	    new Attribute(79, 2, cSeriesRetrieveAET, null),
	    new Attribute(697, 2, cSeriesFilesetUID, null),
	    new Attribute(698, 2, cSeriesFilesetUID, null),
	    new Attribute(113, 1, null, null, referencedImageSequence) };
    static final Attribute[] studyContentModule
	= { new Attribute(427, 2, null, null),
	    new Attribute(425, 1, null, null),
	    new Attribute(109, 1, null, null, referencedSeriesSequence) };
    static final Attribute[] paletteColorLUTModule
	= { new Attribute(473, 1, cPixelAspectRatioNot11, null),
	    new Attribute(479, 3, null, null),
	    new Attribute(480, 3, null, null),
	    new Attribute(494, 1, requirePaletteColorLUT, null),
	    new Attribute(495, 1, requirePaletteColorLUT, null),
	    new Attribute(496, 1, requirePaletteColorLUT, null),
	    new Attribute(497, 3, null, null),
	    new Attribute(498, 1, requirePaletteColorLUT, null),
	    new Attribute(499, 1, requirePaletteColorLUT, null),
	    new Attribute(500, 1, requirePaletteColorLUT, null) };
    
    private CommonImage() {
	/* empty */
    }
}
