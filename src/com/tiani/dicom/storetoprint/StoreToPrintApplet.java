/* StoreToPrintApplet - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.storetoprint;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.text.Document;

import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;

public class StoreToPrintApplet extends JApplet
{
    private Properties _params = null;
    private StoreToPrint _server = null;
    private JButton _applyButton;
    private JButton _startButton;
    private JButton _stopButton;
    private JTabbedPane _tabbedPane;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;
    
    public StoreToPrintApplet() {
	_applyButton = new JButton("Apply");
	_startButton = new JButton("Start");
	_stopButton = new JButton("Stop");
	_tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 30000), true);
	_params = null;
    }
    
    StoreToPrintApplet(Properties properties) {
	_applyButton = new JButton("Apply");
	_startButton = new JButton("Start");
	_stopButton = new JButton("Stop");
	_tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 30000), true);
	_params = properties;
    }
    
    private void initTabbedPane() {
	JPropertiesTable jpropertiestable
	    = new JPropertiesTable(Param.KEYS, _params, Param.CHECKS);
	_tabbedPane.add("Props", new JScrollPane(jpropertiestable));
	_tabbedPane.add("Log", new JAutoScrollPane(_logTextArea));
    }
    
    public void init() {
	StoreToPrint.setLog(_log);
	if (isApplet())
	    _params = getAppletParam();
	if (_params != null) {
	    try {
		CheckParam.verify(_params, Param.CHECKS);
	    } catch (Exception exception) {
		_log.println(exception);
		_params = null;
	    }
	}
	_server = new StoreToPrint(_params);
	try {
	    _params = _server.getProperties();
	} catch (Exception exception) {
	    _log.println(exception);
	}
	_applyButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    _server.setProperties(_params);
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	});
	_startButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    _tabbedPane.setSelectedIndex(1);
		    _server.start(_params);
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	});
	_stopButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    _tabbedPane.setSelectedIndex(1);
		    _server.stop(true, false);
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	});
	Box box = Box.createHorizontalBox();
	box.add(_applyButton);
	box.add(_startButton);
	box.add(_stopButton);
	box.add(Box.createGlue());
	box.add(new JTianiButton(isApplet() ? this.getAppletContext() : null));
	initTabbedPane();
	Container container = this.getContentPane();
	container.add(box, "North");
	container.add(_tabbedPane, "Center");
    }
    
    private boolean isApplet() {
	return this.getDocumentBase() != null;
    }
    
    Properties getParams() {
	return _params;
    }
    
    private Properties getAppletParam() {
	Properties properties = new Properties();
	for (int i = 0; i < Param.KEYS.length; i++)
	    properties.put(Param.KEYS[i], this.getParameter(Param.KEYS[i]));
	return properties;
    }
}
