/* CStoreSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.storetoprint;
import com.tiani.dicom.framework.DefCStoreSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;

final class CStoreSCP extends DefCStoreSCP
{
    private PrintSCURelay _relay;
    
    public CStoreSCP(PrintSCURelay printscurelay) {
	_relay = printscurelay;
    }
    
    protected int store(DimseExchange dimseexchange, String string,
			String string_0_, DicomMessage dicommessage,
			DicomMessage dicommessage_1_) {
	return _relay.store(dimseexchange, string, string_0_, dicommessage,
			    dicommessage_1_);
    }
}
