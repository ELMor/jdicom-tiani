/* PrintMgtSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.storetoprint;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Properties;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.overlay.AttribOverlayFactory;
import com.tiani.dicom.overlay.BurnInOverlay;
import com.tiani.dicom.overlay.Overlay;
import com.tiani.dicom.print.PrintManagementSCU;
import com.tiani.dicom.print.PrintManagementUtils;
import com.tiani.dicom.util.EnumPMI;

final class PrintMgtSCU extends PrintManagementSCU
{
    private static final int UNKNOWN = 0;
    private static final int NORMAL = 1;
    private static final int WARNING = 2;
    private static final int FAILURE = 3;
    private final Param param;
    private final DicomObject filmSessionAttribs = new DicomObject();
    private final DicomObject filmBoxAttribs = new DicomObject();
    private final DicomObject imageBoxAttribs = new DicomObject();
    private int imagePos = 0;
    private int printerStatus = 0;
    private String printerStatusInfo = "";
    
    private static int toPrinterStatusConst(String string) {
	if ("NORMAL".equals(string))
	    return 1;
	if ("WARNING".equals(string))
	    return 2;
	if ("FAILURE".equals(string))
	    return 3;
	return 0;
    }
    
    private void checkPrinterStatus(int i) throws StoreToPrintException {
	if (printerStatus == 0) {
	    Object object = null;
	    DicomObject dicomobject = null;
	    int i_0_ = -1;
	    try {
		DicomMessage dicommessage = this.getPrinterStatus(i);
		dicomobject = dicommessage.getDataset();
		i_0_ = dicommessage.getI(9);
	    } catch (Exception exception) {
		Debug.out.println(exception);
	    }
	    if (dicomobject == null)
		throw new StoreToPrintException
			  (("Retrieve of printer status failed! Error status: "
			    + Integer.toHexString(i_0_)),
			   42755);
	    printerStatus
		= toPrinterStatusConst((String) dicomobject.get(760));
	    printerStatusInfo = (String) dicomobject.get(761);
	}
	switch (printerStatus) {
	case 1:
	    break;
	case 2:
	    if (param.isWarningAsFailure())
		throw new StoreToPrintException(printerStatusInfo, 42756);
	    break;
	default:
	    throw new StoreToPrintException(printerStatusInfo, 42755);
	}
    }
    
    public PrintMgtSCU(Param param) throws DicomException {
	this.param = param;
	PrintManagementUtils.setFilmSessionCreateAttribs(filmSessionAttribs,
							 param
							     .getProperties());
	PrintManagementUtils.setFilmBoxCreateAttribs(filmBoxAttribs,
						     param.getProperties());
	PrintManagementUtils.setImageBoxSetAttribs(imageBoxAttribs,
						   param.getProperties());
	this.addPrinterStatusListener(new IDimseListener() {
	    public void notify(DicomMessage dicommessage) {
		try {
		    printerStatus = dicommessage.getI(15);
		    DicomObject dicomobject = dicommessage.getDataset();
		    printerStatusInfo
			= dicomobject != null ? dicomobject.getS(761) : "";
		} catch (DicomException dicomexception) {
		    Debug.out.println(dicomexception);
		}
	    }
	});
    }
    
    public int store(String string, String string_2_,
		     DicomMessage dicommessage, DicomMessage dicommessage_3_) {
	DicomObject dicomobject = dicommessage.getDataset();
	try {
	    if (!this.isConnected())
		throw new StoreToPrintException
			  ("Association to printer aborted!", 42752);
	    if (isMonochrome(dicomobject)) {
		if (!this.isEnabled(12292))
		    throw new StoreToPrintException
			      ("No BasicGrayscalePrintManagement SOP negotiated!",
			       42753);
		checkPrinterStatus(12292);
		if (this.isColorFilmSession())
		    printAndDeleteFilmSession();
		if (!this.isFilmSession()) {
		    DicomMessage dicommessage_4_
			= this.createGrayscaleFilmSession(filmSessionAttribs);
		    if (!this.isFilmSession())
			throw new StoreToPrintException
				  (("Creation of Basic Film Session failed! Error status: "
				    + Integer.toHexString(dicommessage_4_
							      .getI(9))),
				   42758);
		}
		if (!this.isFilmBox()) {
		    DicomMessage dicommessage_5_
			= this.createFilmBox(filmBoxAttribs);
		    imagePos = 0;
		    if (!this.isFilmBox())
			throw new StoreToPrintException
				  (("Creation of Film Box failed! Error status: "
				    + Integer.toHexString(dicommessage_5_
							      .getI(9))),
				   42759);
		}
		this.setGrayscaleImageBox(++imagePos,
					  prepareImageBox(dicomobject),
					  imageBoxAttribs);
	    } else {
		if (!this.isEnabled(12294))
		    throw new StoreToPrintException
			      ("No BasicColorPrintManagement SOP negotiated!",
			       42754);
		checkPrinterStatus(12294);
		if (!this.isGrayscaleFilmSession())
		    printAndDeleteFilmSession();
		if (!this.isFilmSession()) {
		    DicomMessage dicommessage_6_
			= this.createColorFilmSession(filmSessionAttribs);
		    if (!this.isFilmSession())
			throw new StoreToPrintException
				  (("Creation of Basic Film Session failed! Error status: "
				    + Integer.toHexString(dicommessage_6_
							      .getI(9))),
				   42758);
		}
		if (!this.isFilmBox()) {
		    DicomMessage dicommessage_7_
			= this.createFilmBox(filmBoxAttribs);
		    imagePos = 0;
		    if (!this.isFilmBox())
			throw new StoreToPrintException
				  (("Creation of Film Box failed! Error status: "
				    + Integer.toHexString(dicommessage_7_
							      .getI(9))),
				   42759);
		}
		this.setColorImageBox(++imagePos, prepareImageBox(dicomobject),
				      imageBoxAttribs);
	    }
	    if (imagePos >= this.countImageBoxes()) {
		this.printFilmBox();
		this.deleteFilmBox();
	    }
	    return 0;
	} catch (StoreToPrintException storetoprintexception) {
	    storetoprintexception.printStackTrace(Debug.out);
	    try {
		dicommessage_3_.set(11, storetoprintexception.getMessage());
	    } catch (DicomException dicomexception) {
		/* empty */
	    }
	    return storetoprintexception.getStatus();
	} catch (Exception exception) {
	    exception.printStackTrace(Debug.out);
	    try {
		dicommessage_3_.set(11, exception.getMessage());
	    } catch (DicomException dicomexception) {
		/* empty */
	    }
	    return 43007;
	}
    }
    
    public void close() throws IOException, IllegalValueException,
			       InterruptedException, DicomException {
	if (this.isConnected()) {
	    if (this.isFilmSession())
		printAndDeleteFilmSession();
	    this.release();
	}
    }
    
    public Response open(Request request)
	throws IOException, IllegalValueException, UnknownUIDException,
	       UnknownHostException, DicomException {
	return this.connect(param.getPrintHost(), param.getPrintPort(),
			    request.getCalledTitle(),
			    request.getCallingTitle(),
			    param.getAbstractSyntaxes());
    }
    
    private static boolean isMonochrome(DicomObject dicomobject)
	throws DicomException {
	String string = dicomobject.getS(462);
	if (string == null || string.length() == 0)
	    throw new DicomException("Missing Photometric Intepretation");
	return string.startsWith("MONOCHROME");
    }
    
    private void printAndDeleteFilmSession()
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (this.isFilmBox()) {
	    this.printFilmBox();
	    this.deleteFilmBox();
	}
	this.deleteFilmSession();
    }
    
    private DicomObject prepareImageBox(DicomObject dicomobject)
	throws DicomException, IllegalValueException {
	String string = (String) dicomobject.get(462);
	if (EnumPMI.isGrayscale(string))
	    PrintManagementUtils.preformatGrayscale(dicomobject, null,
						    param.getBitDepth(),
						    param
							.getInflateBitsAlloc(),
						    param.isMinMaxWindowing());
	if (prepareOverlays(dicomobject) > 0) {
	    if (param.getVerbose() > 0)
		Debug.out.println("Burn in overlays");
	    BurnInOverlay.burnInAll(dicomobject);
	}
	PrintManagementUtils
	    .setPixelAspectRatio(dicomobject, param.isSendAspectRatioAlways());
	return PrintManagementUtils.getPixelModule(dicomobject);
    }
    
    private int prepareOverlays(DicomObject dicomobject)
	throws DicomException {
	int i = dicomobject.getI(466);
	int i_8_ = dicomobject.getI(467);
	int i_9_ = Overlay.listOverlayGroups(dicomobject).length;
	if (param.getBurnInInfo() > (i_9_ > 0 ? 1 : 0)) {
	    int i_10_ = Overlay.getFreeOverlayGroup(dicomobject);
	    if (i_10_ == -1)
		Debug.out.println
		    ("No free overlay group 60xx to burn in attribute info");
	    else {
		if (param.getVerbose() > 0)
		    Debug.out.println("Create info overlay");
		createOverlayFactory().createOverlay(i, i_8_ & ~0x7,
						     dicomobject, i_10_);
		i_9_++;
	    }
	}
	return i_9_;
    }
    
    private AttribOverlayFactory createOverlayFactory() {
	Properties properties = null;
	String string = param.getBurnInInfoProperties();
	if (string.length() > 0) {
	    try {
		FileInputStream fileinputstream = new FileInputStream(string);
		try {
		    properties = new Properties();
		    properties.load(fileinputstream);
		} finally {
		    fileinputstream.close();
		}
	    } catch (Exception exception) {
		Debug.out.println(exception);
		properties = null;
	    }
	}
	return new AttribOverlayFactory(properties);
    }
}
