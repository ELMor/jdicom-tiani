/* k - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.IOException;
import java.io.OutputStream;

class k extends OffsetOutputStream
{
    int a = 8193;
    boolean b = true;
    int c = 0;
    byte[] d;
    byte[] e;
    byte[] f;
    
    k(OutputStream outputstream) {
	super(outputstream);
	d = new byte[2];
	e = new byte[4];
	f = new byte[8];
    }
    
    k(OutputStream outputstream, int i) {
	super(outputstream, i);
	d = new byte[2];
	e = new byte[4];
	f = new byte[8];
    }
    
    void a(int i) {
	a = i;
    }
    
    int a() {
	return a;
    }
    
    void a(boolean bool) {
	b = bool;
    }
    
    boolean b() {
	return b;
    }
    
    int b(int i) throws IOException {
	if (a == 8195) {
	    d[1] = (byte) i;
	    d[0] = (byte) (i >>> 8);
	} else {
	    d[0] = (byte) i;
	    d[1] = (byte) (i >>> 8);
	}
	this.write(d);
	return 2;
    }
    
    int c(int i) throws IOException {
	if (a == 8195) {
	    e[3] = (byte) i;
	    e[2] = (byte) (i >>> 8);
	    e[1] = (byte) (i >>> 16);
	    e[0] = (byte) (i >>> 24);
	} else {
	    e[0] = (byte) i;
	    e[1] = (byte) (i >>> 8);
	    e[2] = (byte) (i >>> 16);
	    e[3] = (byte) (i >>> 24);
	}
	this.write(e);
	return 4;
    }
    
    int a(long l) throws IOException {
	if (a == 8195) {
	    f[7] = (byte) (int) l;
	    f[6] = (byte) (int) (l >>> 8);
	    f[5] = (byte) (int) (l >>> 16);
	    f[4] = (byte) (int) (l >>> 24);
	    f[3] = (byte) (int) (l >>> 32);
	    f[2] = (byte) (int) (l >>> 40);
	    f[1] = (byte) (int) (l >>> 48);
	    f[0] = (byte) (int) (l >>> 56);
	} else {
	    f[0] = (byte) (int) l;
	    f[1] = (byte) (int) (l >>> 8);
	    f[2] = (byte) (int) (l >>> 16);
	    f[3] = (byte) (int) (l >>> 24);
	    f[4] = (byte) (int) (l >>> 32);
	    f[5] = (byte) (int) (l >>> 40);
	    f[6] = (byte) (int) (l >>> 48);
	    f[7] = (byte) (int) (l >>> 56);
	}
	this.write(f);
	return 8;
    }
    
    int a(String string) throws IOException {
	this.write(string.getBytes());
	return string.length();
    }
}
