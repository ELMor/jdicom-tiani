/* DicomObject - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

public class DicomObject extends GroupList implements Cloneable
{
    public static int dumpLineLen = 72;
    DicomObject a = null;
    long b = 0L;
    int c = 0;
    int d = 0;
    
    public Object clone() {
	DicomObject dicomobject_0_ = new DicomObject();
	Enumeration enumeration = this.enumerateVRs(false, false);
	while (enumeration.hasMoreElements()) {
	    TagValue tagvalue = (TagValue) enumeration.nextElement();
	    int i = tagvalue.getGroup();
	    int i_1_ = tagvalue.getElement();
	    int i_2_ = tagvalue.size();
	    try {
		if (i_2_ == 0)
		    dicomobject_0_.set_ge(i, i_1_, null);
		else {
		    for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
			Object object = tagvalue.getValue(i_3_);
			if (object instanceof byte[]) {
			    byte[] is = (byte[]) object;
			    object = new byte[is.length];
			    System.arraycopy(is, 0, object, 0, is.length);
			} else if (object instanceof DicomObject) {
			    DicomObject dicomobject_4_ = (DicomObject) object;
			    object = dicomobject_4_.clone();
			}
			dicomobject_0_.append_ge(i, i_1_, object);
		    }
		}
	    } catch (DicomException dicomexception) {
		Debug.out.println("Error cloning attribute ("
				  + Integer.toHexString(i) + ","
				  + Integer.toHexString(i_1_) + ": "
				  + dicomexception);
	    }
	}
	return dicomobject_0_;
    }
    
    public void setVRs(DicomObject dicomobject_5_) throws DicomException {
	Enumeration enumeration = dicomobject_5_.enumerateVRs(false, false);
	while (enumeration.hasMoreElements()) {
	    TagValue tagvalue = (TagValue) enumeration.nextElement();
	    int i = tagvalue.size();
	    if (i >= 0) {
		int i_6_ = tagvalue.getGroup();
		int i_7_ = tagvalue.getElement();
		set_ge(i_6_, i_7_, null);
		for (int i_8_ = 0; i_8_ < i; i_8_++)
		    set_ge(i_6_, i_7_, tagvalue.getValue(i_8_), i_8_);
	    }
	}
    }
    
    public void appendVRs(DicomObject dicomobject_9_) throws DicomException {
	Enumeration enumeration = dicomobject_9_.enumerateVRs(false, false);
	while (enumeration.hasMoreElements()) {
	    TagValue tagvalue = (TagValue) enumeration.nextElement();
	    int i = tagvalue.size();
	    if (i > 0) {
		int i_10_ = tagvalue.getGroup();
		int i_11_ = tagvalue.getElement();
		int i_12_ = Math.max(0, getSize_ge(i_10_, i_11_));
		for (int i_13_ = 0; i_13_ < i; i_13_++)
		    set_ge(i_10_, i_11_, tagvalue.getValue(i_13_),
			   i_12_ + i_13_);
	    }
	}
    }
    
    private void a(int i, int i_14_, int i_15_, Object object, int i_16_)
	throws DicomException {
	Object object_17_ = null;
	boolean bool = true;
	VR vr = this.a(i, i_14_);
	if (vr == null) {
	    if (Debug.DEBUG >= 5) {
		if (object != null)
		    Debug.printMessage(("DicomObject.addValue <new>, index: "
					+ i_16_ + ", type: "
					+ object.getClass().getName()),
				       i, i_14_);
		else
		    Debug.printMessage(("DicomObject.addValue <new>, index: "
					+ i_16_ + ", type: null"),
				       i, i_14_);
	    }
	    vr = new VR(i, i_14_, i_15_);
	    bool = false;
	} else if (Debug.DEBUG >= 5) {
	    if (object != null)
		Debug.printMessage(("DicomObject.addValue <old>, index: "
				    + i_16_ + ", type: "
				    + object.getClass().getName()),
				   i, i_14_);
	    else
		Debug.printMessage(("DicomObject.addValue <old>, index: "
				    + i_16_ + ", type: null"),
				   i, i_14_);
	}
	if (i_16_ > vr.val.size())
	    throw new DicomException("Index (" + i_16_
				     + ") exceeds bounds of Data Element");
	try {
	    if (object == null)
		object_17_ = null;
	    else if (object instanceof String)
		object_17_ = e.a((String) object, vr.dcm_type);
	    else if (object instanceof byte[])
		object_17_ = e.a((byte[]) object, vr.dcm_type);
	    else if (object instanceof Integer)
		object_17_ = e.a((Integer) object, vr.dcm_type);
	    else if (object instanceof Float)
		object_17_ = e.a((Float) object, vr.dcm_type);
	    else if (object instanceof Double)
		object_17_ = e.a((Double) object, vr.dcm_type);
	    else if (object instanceof Person)
		object_17_ = e.a((Person) object, vr.dcm_type);
	    else if (object instanceof DDate)
		object_17_ = e.a((DDate) object, vr.dcm_type);
	    else if (object instanceof DDateRange)
		object_17_ = e.a((DDateRange) object, vr.dcm_type);
	    else if (object instanceof DTime)
		object_17_ = e.a((DTime) object, vr.dcm_type);
	    else if (object instanceof DTimeRange)
		object_17_ = e.a((DTimeRange) object, vr.dcm_type);
	    else if (object instanceof DDateTime)
		object_17_ = e.a((DDateTime) object, vr.dcm_type);
	    else if (object instanceof DDateTimeRange)
		object_17_ = e.a((DDateTimeRange) object, vr.dcm_type);
	    else if (object instanceof Short)
		object_17_ = e.a((Short) object, vr.dcm_type);
	    else if (object instanceof Long)
		object_17_ = e.a((Long) object, vr.dcm_type);
	    else if (object instanceof int[])
		object_17_ = e.a((int[]) object, vr.dcm_type);
	    else if (object instanceof DicomObject)
		object_17_ = e.a((DicomObject) object, vr.dcm_type);
	    else
		throw new DicomException("Unsupported type given: "
					 + object.getClass().getName());
	} catch (DicomException dicomexception) {
	    throw new DicomException(i, i_14_, dicomexception.getMessage());
	}
	if (!bool) {
	    if (object_17_ == null) {
		vr.val = new Vector();
		vr.empty = true;
	    } else {
		vr.val.addElement(object_17_);
		vr.empty = false;
	    }
	    this.a(vr);
	} else if (object_17_ != null) {
	    if (i_16_ < vr.val.size())
		vr.val.setElementAt(object_17_, i_16_);
	    else
		vr.val.addElement(object_17_);
	    if (vr.empty)
		vr.empty = false;
	}
    }
    
    public final int getPixelDataLength() {
	return d;
    }
    
    public void set(int i, Object object) throws DicomException {
	int i_18_ = DDict.getGroup(i);
	int i_19_ = DDict.getElement(i);
	set_ge(i_18_, i_19_, object);
    }
    
    public void set_ge(int i, int i_20_, Object object) throws DicomException {
	deleteItem_ge(i, i_20_);
	set_ge(i, i_20_, object, 0);
    }
    
    public void append(int i, Object object) throws DicomException {
	int i_21_ = DDict.getGroup(i);
	int i_22_ = DDict.getElement(i);
	append_ge(i_21_, i_22_, object);
    }
    
    public void append_ge(int i, int i_23_, Object object)
	throws DicomException {
	if (object != null) {
	    int i_24_ = getSize_ge(i, i_23_);
	    if (i_24_ == -1)
		i_24_ = 0;
	    set_ge(i, i_23_, object, i_24_);
	}
    }
    
    public void set(int i, Object object, int i_25_) throws DicomException {
	int i_26_ = DDict.getGroup(i);
	int i_27_ = DDict.getElement(i);
	set_ge(i_26_, i_27_, object, i_25_);
    }
    
    public void set_ge(int i, int i_28_, Object object, int i_29_)
	throws DicomException {
	int i_30_ = DDict.getTypeCode(i, i_28_);
	a(i, i_28_, i_30_, object, i_29_);
    }
    
    public int getSize(int i) {
	int i_31_ = DDict.getGroup(i);
	int i_32_ = DDict.getElement(i);
	return getSize_ge(i_31_, i_32_);
    }
    
    public int getSize_ge(int i, int i_33_) {
	VR vr = this.a(i, i_33_);
	if (vr == null)
	    return -1;
	return vr.val.size();
    }
    
    public long calculateOffset(int i, int i_34_, int i_35_, boolean bool,
				boolean bool_36_) {
	return new Offsets(this).calculateOffset(i, i_34_, i_35_, bool,
						 bool_36_, true);
    }
    
    public long getOffset(int i, int i_37_) {
	VR vr = this.a(DDict.getGroup(i), DDict.getElement(i));
	if (vr == null)
	    return 0L;
	if (i_37_ >= vr.val.size())
	    return 0L;
	if (vr.dcm_type != 10)
	    return 0L;
	return ((DicomObject) vr.val.elementAt(i_37_)).b;
    }
    
    public Object get(int i) {
	return get(i, 0);
    }
    
    public Object get_ge(int i, int i_38_) {
	return get_ge(i, i_38_, 0);
    }
    
    public Object get(int i, int i_39_) {
	int i_40_ = DDict.getGroup(i);
	int i_41_ = DDict.getElement(i);
	return get_ge(i_40_, i_41_, i_39_);
    }
    
    public Object get_ge(int i, int i_42_, int i_43_) {
	VR vr = this.a(i, i_42_);
	if (vr == null || vr.empty)
	    return null;
	if (i_43_ >= vr.val.size())
	    return null;
	return vr.val.elementAt(i_43_);
    }
    
    public String getS(int i) throws DicomException {
	return getS(i, 0);
    }
    
    public String getS_ge(int i, int i_44_) throws DicomException {
	return getS_ge(i, i_44_, 0);
    }
    
    public String getS(int i, int i_45_) throws DicomException {
	int i_46_ = DDict.getGroup(i);
	int i_47_ = DDict.getElement(i);
	return getS_ge(i_46_, i_47_, i_45_);
    }
    
    public String getS_ge(int i, int i_48_, int i_49_) throws DicomException {
	Object object = get_ge(i, i_48_, i_49_);
	if (object != null) {
	    int i_50_ = this.a(i, i_48_).dcm_type;
	    return e.a(object, i_50_);
	}
	return null;
    }
    
    public int getI(int i) throws DicomException {
	return getI(i, 0);
    }
    
    public int getI_ge(int i, int i_51_) throws DicomException {
	return getI_ge(i, i_51_, 0);
    }
    
    public int getI(int i, int i_52_) throws DicomException {
	int i_53_ = DDict.getGroup(i);
	int i_54_ = DDict.getElement(i);
	return getI_ge(i_53_, i_54_, i_52_);
    }
    
    public int getI_ge(int i, int i_55_, int i_56_) throws DicomException {
	Object object = get_ge(i, i_55_, i_56_);
	if (object != null) {
	    int i_57_ = this.a(i, i_55_).dcm_type;
	    return e.b(object, i_57_);
	}
	return 2147483647;
    }
    
    public Vector deleteItem(int i) {
	int i_58_ = DDict.getGroup(i);
	int i_59_ = DDict.getElement(i);
	return deleteItem_ge(i_58_, i_59_);
    }
    
    public Vector deleteItem_ge(int i, int i_60_) {
	VR vr = this.b(i, i_60_);
	if (vr == null)
	    return null;
	return vr.val;
    }
    
    public Object deleteItem(int i, int i_61_) {
	int i_62_ = DDict.getGroup(i);
	int i_63_ = DDict.getElement(i);
	return deleteItem_ge(i_62_, i_63_, i_61_);
    }
    
    public Object deleteItem_ge(int i, int i_64_, int i_65_) {
	VR vr = this.a(i, i_64_);
	if (i_65_ >= vr.val.size())
	    return null;
	Object object = vr.val.elementAt(i_65_);
	vr.val.removeElementAt(i_65_);
	return object;
    }
    
    public void read(InputStream inputstream)
	throws IOException, DicomException {
	read(inputstream, true);
    }
    
    public void read(InputStream inputstream, boolean bool)
	throws IOException, DicomException {
	InputStream inputstream_66_;
	if (inputstream.markSupported())
	    inputstream_66_ = inputstream;
	else
	    inputstream_66_ = new BufferedInputStream(inputstream);
	inputstream_66_.mark(256);
	boolean bool_67_;
	try {
	    bool_67_ = b(inputstream_66_);
	} catch (Exception exception) {
	    bool_67_ = false;
	}
	inputstream_66_.reset();
	if (bool_67_)
	    a(inputstream_66_, bool);
	else
	    a(0L, inputstream_66_, bool);
    }
    
    long a(InputStream inputstream) throws IOException, DicomException {
	if (Debug.DEBUG > 3)
	    Debug.a("DicomObject.readHeader.");
	byte[] is = new byte[128];
	byte[] is_68_ = new byte[4];
	f var_f = new f(inputstream);
	var_f.c(8194);
	var_f.readFully(is);
	var_f.readFully(is_68_);
	if (is_68_[0] != 68 || is_68_[1] != 73 || is_68_[2] != 67
	    || is_68_[3] != 77)
	    throw new DicomException("Not a valid Dicom-file");
	a = new DicomObject();
	VR vr = new VR();
	vr.readVRHeader(var_f);
	vr.readVRData(var_f);
	long l = ((Long) vr.val.elementAt(0)).longValue() + var_f.getOffset();
	while (var_f.getOffset() < l) {
	    vr = new VR();
	    vr.readVRHeader(var_f);
	    vr.readVRData(var_f);
	    a.a(vr);
	}
	return var_f.getOffset();
    }
    
    void a(InputStream inputstream, boolean bool)
	throws IOException, DicomException {
	long l = a(inputstream);
	a(l, inputstream, bool);
    }
    
    void a(long l, InputStream inputstream, boolean bool)
	throws IOException, DicomException {
	a(l, inputstream, 8193, bool);
    }
    
    public void read(InputStream inputstream, int i, boolean bool)
	throws IOException, DicomException {
	a(0L, inputstream, i, bool);
    }
    
    void a(long l, InputStream inputstream, int i, boolean bool)
	throws IOException, DicomException {
	if (Debug.DEBUG > 3)
	    Debug.a("DicomObject.readDICOMStream.");
	if (a != null) {
	    try {
		i = UID.getUIDEntry(a.getS(31).trim()).getConstant();
	    } catch (UnknownUIDException unknownuidexception) {
		i = 8192;
	    }
	}
	f var_f = new f(inputstream, (int) l);
	var_f.c(i);
	a(var_f, bool);
    }
    
    void a(f var_f, boolean bool) throws IOException, DicomException {
	for (;;) {
	    VR vr = new VR();
	    try {
		vr.readVRHeader(var_f);
		boolean bool_69_ = vr.group == 32736 && vr.element == 16;
		if (bool_69_) {
		    d = vr.dataLen;
		    if (!bool)
			break;
		}
		vr.readVRData(var_f);
		if (vr.element != 0)
		    this.a(vr);
		if (bool_69_)
		    break;
	    } catch (EOFException eofexception) {
		break;
	    }
	}
    }
    
    boolean b(InputStream inputstream) throws IOException, DicomException {
	byte[] is = new byte[128];
	byte[] is_70_ = new byte[4];
	DataInputStream datainputstream = new DataInputStream(inputstream);
	datainputstream.readFully(is);
	datainputstream.readFully(is_70_);
	if (new String(is_70_).equals("DICM"))
	    return true;
	return false;
    }
    
    public void write(OutputStream outputstream, boolean bool)
	throws DicomException, IOException {
	if (bool) {
	    if (a == null)
		a(8193);
	    a(outputstream);
	}
	int i;
	try {
	    i = UID.getUIDEntry(a.getS(31)).getConstant();
	} catch (Exception exception) {
	    i = 8193;
	}
	i var_i = new i(outputstream);
	var_i.a(false);
	var_i.a(i);
	if (Debug.DEBUG > 3)
	    Debug.a("DicomObject.writeTags. transfer syntax: " + i);
	a(var_i, true);
    }
    
    void a(OutputStream outputstream) throws IOException, DicomException {
	if (Debug.DEBUG > 3)
	    Debug.a("DicomObject.writeHeader.");
	byte[] is = new byte[128];
	i var_i = new i(outputstream);
	var_i.a(8194);
	for (int i = 0; i < 128; i++)
	    is[i] = (byte) 0;
	var_i.write(is);
	var_i.write(68);
	var_i.write(73);
	var_i.write(67);
	var_i.write(77);
	a.a(var_i, true);
    }
    
    public void write(OutputStream outputstream, boolean bool, int i,
		      boolean bool_71_) throws DicomException, IOException {
	if (bool) {
	    if (a == null)
		a(i);
	    a(outputstream);
	}
	i var_i = new i(outputstream);
	var_i.a(bool_71_);
	var_i.a(i);
	if (Debug.DEBUG > 3)
	    Debug.a("DicomObject.writeTags. transfer syntax: " + i);
	a(var_i, true);
    }
    
    public void write(OutputStream outputstream, boolean bool, int i,
		      boolean bool_72_,
		      boolean bool_73_) throws DicomException, IOException {
	if (bool) {
	    if (a == null)
		a(i);
	    a(outputstream);
	}
	i var_i = new i(outputstream);
	var_i.a(bool_72_);
	var_i.a(i);
	if (Debug.DEBUG > 3)
	    Debug.a("DicomObject.writeTags. transfer syntax: " + i);
	a(var_i, bool_73_);
    }
    
    void a(i var_i, boolean bool) throws IOException, DicomException {
	Enumeration enumeration = this.a(bool, var_i.a(), var_i.b(), true);
	while (enumeration.hasMoreElements()) {
	    VR vr = (VR) enumeration.nextElement();
	    vr.writeVRHeader(var_i);
	    vr.writeVRData(var_i);
	}
    }
    
    void a(int i) throws DicomException {
	if (a == null)
	    b(i);
	else {
	    try {
		a.set(31, UID.getUIDEntry(i).getValue(), 0);
	    } catch (IllegalValueException illegalvalueexception) {
		illegalvalueexception.printStackTrace();
	    }
	}
    }
    
    private void b(int i) throws DicomException {
	if (Debug.DEBUG > 3)
	    Debug.a
		("DicomObject.makeDICOMFileMetaInformation. transfer syntax: "
		 + i);
	if (a == null)
	    a = new DicomObject();
	if (getSize(62) != 1 || getSize(63) != 1)
	    throw new DicomException
		      ("This DicomObject can't be written to a Dicom File: Unable to find SOP Common Info");
	byte[] is = { 0, 1 };
	a.set(28, is, 0);
	a.set(29, get(62), 0);
	a.set(30, get(63), 0);
	String string = a.getS(31);
	if (string != null) {
	    boolean bool = false;
	    int i_74_;
	    try {
		i_74_ = UID.getUIDEntry(string).getConstant();
	    } catch (UnknownUIDException unknownuidexception) {
		throw new DicomException
			  ("Cannot change unknown transfersyntax into new one");
	    }
	    if (i_74_ != 8193 && i_74_ != 8194 && i_74_ != 8195)
		throw new DicomException
			  ("Cannot change encapsulated transfersyntax, use Compression.decompress().");
	}
	try {
	    a.set(31, UID.getUIDEntry(i).getValue(), 0);
	} catch (IllegalValueException illegalvalueexception) {
	    illegalvalueexception.printStackTrace();
	}
	if (a.getSize(32) == 0)
	    a.set(32, "1.2.826.0.1.3680043.2.60.0.1.888");
	if (a.getSize(33) == 0)
	    a.set(33, "SoftLink JDT 1.0");
    }
    
    public DicomObject getFileMetaInformation() {
	return a;
    }
    
    public DicomObject setFileMetaInformation(DicomObject dicomobject_75_) {
	DicomObject dicomobject_76_ = a;
	a = dicomobject_75_;
	return dicomobject_76_;
    }
    
    public void dumpVRs(OutputStream outputstream) throws IOException {
	dumpVRs(outputstream, false);
    }
    
    public void dumpVRs(OutputStream outputstream, boolean bool)
	throws IOException {
	DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
	if (bool && a != null)
	    a.dumpVRs(outputstream);
	Enumeration enumeration = this.a(false, 8193, true, true);
	while (enumeration.hasMoreElements()) {
	    VR vr = (VR) enumeration.nextElement();
	    dataoutputstream.writeBytes(DumpUtils.dumpVR(vr, 0));
	}
    }
}
