/* j - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.IOException;
import java.io.OutputStream;

class j extends k
{
    j(OutputStream outputstream) {
	super(outputstream);
    }
    
    j(OutputStream outputstream, int i) {
	super(outputstream, i);
    }
    
    int a(Object object) throws IOException {
	return this.a((String) object);
    }
    
    int b(Object object) throws IOException {
	int i = ((Integer) object).intValue();
	return this.b(i >>> 16) + this.b(i);
    }
    
    int c(Object object) throws IOException {
	return this.c(Float.floatToIntBits(((Float) object).floatValue()));
    }
    
    int d(Object object) throws IOException {
	return this.a(Double
			  .doubleToLongBits(((Double) object).doubleValue()));
    }
    
    int e(Object object) throws IOException {
	return this.c(((Long) object).intValue());
    }
    
    int f(Object object) throws IOException {
	return this.b(((Short) object).intValue());
    }
    
    int g(Object object) throws IOException {
	return this.c(((Long) object).intValue());
    }
    
    int h(Object object) throws IOException {
	return this.b(((Integer) object).intValue());
    }
    
    int i(Object object) throws IOException {
	return this.b(((Integer) object).intValue());
    }
    
    int b(Object object, int i) throws IOException {
	switch (i) {
	case 17:
	    return a(object);
	case 5:
	    return b(object);
	case 26:
	    return c(object);
	case 20:
	    return d(object);
	case 19:
	    return e(object);
	case 23:
	    return f(object);
	case 1:
	    return g(object);
	case 3:
	    return h(object);
	case 21:
	    return i(object);
	default:
	    return 0;
	}
    }
}
