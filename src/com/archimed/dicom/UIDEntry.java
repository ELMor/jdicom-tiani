/* UIDEntry - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import com.archimed.tool.n;

public class UIDEntry
{
    public static final int SOPClass = 1;
    public static final int WellknownSOPInstance = 2;
    public static final int TransferSyntax = 3;
    public static final int MetaSOPClass = 4;
    public static final int ApplicationContextName = 5;
    private int a;
    private String b;
    private String c;
    private String d;
    private int e;
    
    public UIDEntry(int i, String string, String string_0_, String string_1_,
		    int i_2_) {
	a = i;
	b = string;
	c = string_0_;
	d = string_1_;
	e = i_2_;
    }
    
    public int getConstant() {
	return a;
    }
    
    public String getValue() {
	return b;
    }
    
    public String getName() {
	return c;
    }
    
    public String getShortName() {
	return d;
    }
    
    public int getType() {
	return e;
    }
    
    public boolean equals(Object object) {
	if (!(object instanceof UIDEntry))
	    return false;
	return a == ((UIDEntry) object).a;
    }
    
    public String toString() {
	return b;
    }
    
    public String toLongString() {
	return n.fstr(b, 30) + n.fstr(d, 6) + n.fstr(c, 40);
    }
}
