/* g - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.IOException;
import java.io.InputStream;

class g extends h
{
    g(InputStream inputstream) {
	super(inputstream);
    }
    
    g(InputStream inputstream, int i) {
	super(inputstream, i);
    }
    
    String a() throws IOException {
	return this.d(4);
    }
    
    Integer b() throws IOException {
	return new Integer((this.k() << 16) + this.k());
    }
    
    Float c() throws IOException {
	return new Float(Float.intBitsToFloat(this.m()));
    }
    
    Double d() throws IOException {
	return new Double(Double.longBitsToDouble(this.o()));
    }
    
    Long e() throws IOException {
	return new Long((long) this.n());
    }
    
    Short f() throws IOException {
	return new Short((short) this.l());
    }
    
    Long g() throws IOException {
	return new Long((long) this.m());
    }
    
    Integer h() throws IOException {
	return new Integer(this.k());
    }
    
    Integer i() throws IOException {
	return new Integer(this.k());
    }
    
    Object b(int i) throws IOException {
	switch (i) {
	case 17:
	    return a();
	case 5:
	    return b();
	case 26:
	    return c();
	case 20:
	    return d();
	case 19:
	    return e();
	case 23:
	    return f();
	case 1:
	    return g();
	case 3:
	    return h();
	case 21:
	    return i();
	default:
	    return null;
	}
    }
}
