/* IntHashtableEnumerator - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.util.Enumeration;
import java.util.NoSuchElementException;

class IntHashtableEnumerator implements Enumeration
{
    boolean a;
    int b;
    IntHashtableEntry[] c;
    IntHashtableEntry d;
    
    IntHashtableEnumerator(IntHashtableEntry[] inthashtableentrys,
			   boolean bool) {
	c = inthashtableentrys;
	a = bool;
	b = inthashtableentrys.length;
    }
    
    public boolean hasMoreElements() {
	if (d != null)
	    return true;
	while (b-- > 0) {
	    if ((d = c[b]) != null)
		return true;
	}
	return false;
    }
    
    public Object nextElement() {
	if (d == null) {
	    while (b-- > 0 && (d = c[b]) == null) {
		/* empty */
	    }
	}
	if (d != null) {
	    IntHashtableEntry inthashtableentry = d;
	    d = inthashtableentry.d;
	    return (a ? (Object) new Integer(inthashtableentry.b)
		    : inthashtableentry.c);
	}
	throw new NoSuchElementException("IntHashtableEnumerator");
    }
}
