/* UID - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.util.Enumeration;
import java.util.Hashtable;

public class UID
{
    public static final int UnknownUID = 0;
    public static final int DICOMApplicationContextName = 20481;
    static Hashtable a = new Hashtable();
    
    public static UIDEntry getUIDEntry(int i) throws IllegalValueException {
	UIDEntry uidentry = (UIDEntry) a.get(new Integer(i));
	if (uidentry == null)
	    throw new IllegalValueException("UID not found");
	return uidentry;
    }
    
    public static UIDEntry getUIDEntry(String string)
	throws UnknownUIDException {
	Enumeration enumeration = a.elements();
	while (enumeration.hasMoreElements()) {
	    UIDEntry uidentry = (UIDEntry) enumeration.nextElement();
	    if (uidentry.getValue().equals(string))
		return uidentry;
	}
	throw new UnknownUIDException(string, "Unknown UID: '" + string + "'");
    }
    
    static UIDEntry a(String string) throws UnknownUIDException {
	Enumeration enumeration = a.elements();
	while (enumeration.hasMoreElements()) {
	    UIDEntry uidentry = (UIDEntry) enumeration.nextElement();
	    if (uidentry.getShortName().equals(string))
		return uidentry;
	}
	throw new UnknownUIDException(string, "Unknown UID: '" + string + "'");
    }
    
    public static void addEntry(int i, UIDEntry uidentry)
	throws DicomException {
	Integer integer = new Integer(i);
	if (a.contains(integer))
	    throw new DicomException("Entry with id = 0x"
				     + Integer.toHexString(i)
				     + " already present in UID");
	a.put(integer, uidentry);
    }
    
    public static String toString(int i) throws IllegalValueException {
	return getUIDEntry(i).getValue();
    }
    
    static {
	TransferSyntax.a();
	SOPClass.a();
	MetaSOPClass.a();
	SOPInstance.a();
	a.put(new Integer(20481),
	      new UIDEntry(20481, "1.2.840.10008.3.1.1.1",
			   "DICOM Application Context Name", "AC", 5));
    }
}
