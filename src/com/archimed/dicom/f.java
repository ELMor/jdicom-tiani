/* f - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.Vector;

class f extends g
{
    Vector a;
    
    f(InputStream inputstream) {
	super(inputstream);
    }
    
    f(InputStream inputstream, int i) {
	super(inputstream, i);
    }
    
    Vector a(int i, int i_0_) throws IOException {
	a = new Vector();
	this.resetCounter();
	for (int i_1_ = 0; i_1_ < i_0_; i_1_++)
	    a.addElement(this.b(i));
	return a;
    }
    
    String a(String string) {
	return string;
    }
    
    String b(String string) {
	return string;
    }
    
    String c(String string) {
	return string;
    }
    
    String d(String string) {
	return string;
    }
    
    String e(String string) {
	return string;
    }
    
    String f(String string) {
	return string;
    }
    
    Object g(String string) {
	try {
	    return new DTime(string);
	} catch (NumberFormatException numberformatexception) {
	    return new DTimeRange(string);
	}
    }
    
    String h(String string) {
	return string;
    }
    
    Object i(String string) {
	try {
	    return new DDateTime(string);
	} catch (NumberFormatException numberformatexception) {
	    return new DDateTimeRange(string);
	}
    }
    
    String j(String string) {
	return string;
    }
    
    Float k(String string) {
	return new Float(string);
    }
    
    Integer l(String string) {
	return new Integer(string);
    }
    
    Person m(String string) {
	return new Person(string);
    }
    
    Object n(String string) {
	try {
	    return new DDate(string);
	} catch (NumberFormatException numberformatexception) {
	    return new DDateRange(string);
	}
    }
    
    Object a(String string, int i) {
	switch (i) {
	case 4:
	    return a(string);
	case 9:
	    return b(string);
	case 16:
	    return k(string);
	case 11:
	    return n(string);
	case 15:
	    return l(string);
	case 6:
	    return c(string);
	case 18:
	    return d(string);
	case 14:
	    return m(string);
	case 7:
	    return e(string);
	case 13:
	    return f(string);
	case 12:
	    return g(string);
	case 27:
	    return h(string);
	case 28:
	    return i(string);
	case 2:
	    return j(string);
	default:
	    return null;
	}
    }
    
    Vector a(int i) throws IOException {
	a = new Vector();
	StringTokenizer stringtokenizer = new StringTokenizer(this.d(i), "\\");
	while (stringtokenizer.hasMoreTokens())
	    a.addElement(stringtokenizer.nextToken());
	return a;
    }
    
    Vector b(int i, int i_2_) throws IOException {
	Vector vector;
	if (i == 11 && i_2_ % 8 == 0) {
	    String string = this.d(i_2_).trim();
	    if (string.length() != i_2_)
		throw new IllegalArgumentException();
	    vector = new Vector();
	    for (int i_3_ = 0; i_3_ < i_2_ / 8; i_3_++)
		vector.addElement(string.substring(i_3_ * 8, (i_3_ + 1) * 8));
	} else
	    vector = a(i_2_);
	for (int i_4_ = 0; i_4_ < vector.size(); i_4_++) {
	    Object object = a(((String) vector.elementAt(i_4_)).trim(), i);
	    vector.setElementAt(object, i_4_);
	}
	return vector;
    }
    
    Vector c(int i, int i_5_) throws IOException {
	byte[] is = new byte[i_5_];
	this.readFully(is);
	if (i == 24 && this.j() == 8195)
	    DicomUtils.a(is);
	a = new Vector();
	a.addElement(is);
	return a;
    }
    
    Vector d(int i, int i_6_) throws IOException {
	int i_7_ = DDict.c(i);
	if (i_7_ != 0)
	    return a(i, i_6_ / i_7_);
	switch (i) {
	case 0:
	case 8:
	case 22:
	case 24:
	    return c(i, i_6_);
	default:
	    return b(i, i_6_);
	}
    }
}
