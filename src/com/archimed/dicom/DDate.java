/* DDate - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DDate
{
    private Calendar a;
    private boolean b = false;
    
    public DDate() {
	a = new GregorianCalendar();
	b = true;
    }
    
    public DDate(String string) throws NumberFormatException {
	if (string.equals(""))
	    b = true;
	else {
	    boolean bool = string.length() == 10;
	    if (string.length() != 8 && !bool)
		throw new NumberFormatException
			  ("Cannot parse string into DDate");
	    int i = bool ? 5 : 4;
	    int i_0_ = bool ? 8 : 6;
	    int i_1_ = Integer.parseInt(string.substring(0, 4));
	    int i_2_ = Integer.parseInt(string.substring(i, i + 2));
	    int i_3_ = Integer.parseInt(string.substring(i_0_));
	    a(i_1_, i_2_, i_3_);
	    a = new GregorianCalendar(i_1_, i_2_ - 1, i_3_);
	}
    }
    
    static void a(int i, int i_4_, int i_5_) {
	switch (i_4_) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
	    if (i_5_ <= 0 || i_5_ > 31)
		break;
	    return;
	case 4:
	case 6:
	case 9:
	case 11:
	    if (i_5_ <= 0 || i_5_ > 30)
		break;
	    return;
	case 2:
	    if (i_5_ <= 0 || i_5_ > 29)
		break;
	    return;
	}
	throw new NumberFormatException("Cannot parse string into DDate");
    }
    
    public String toString() {
	if (b)
	    return "";
	SimpleDateFormat simpledateformat
	    = new SimpleDateFormat("EEE dd MMM yyyy");
	return simpledateformat.format(a.getTime());
    }
    
    public String toString(DateFormat dateformat) {
	if (b)
	    return "";
	return dateformat.format(a.getTime());
    }
    
    public String toDICOMString() {
	if (b)
	    return "";
	SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMdd");
	return simpledateformat.format(a.getTime());
    }
    
    public boolean isEmpty() {
	return b;
    }
}
