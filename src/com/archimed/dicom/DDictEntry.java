/* DDictEntry - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class DDictEntry
{
    int a;
    int b;
    int c;
    String d;
    String e;
    
    public DDictEntry(int i, int i_0_, int i_1_, String string,
		      String string_2_) {
	a = i;
	b = i_0_;
	c = i_1_;
	d = string;
	e = string_2_;
    }
    
    public int getGroup() {
	return a;
    }
    
    public int getElement() {
	return b;
    }
    
    public String getDescription() {
	return d;
    }
    
    public int getType() {
	return c;
    }
    
    public String getVM() {
	return e;
    }
}
