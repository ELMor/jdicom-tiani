/* VR - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

class VR extends TagValue
{
    protected int dataLen = 0;
    protected int headerLen = 0;
    protected int dcm_type = 0;
    protected boolean empty = false;
    
    protected VR() {
	group = 0;
	element = 0;
	val = new Vector();
	dcm_type = 0;
	dataLen = 0;
	headerLen = 0;
	empty = true;
    }
    
    protected VR(int i, int i_0_, int i_1_) {
	group = i;
	element = i_0_;
	dcm_type = i_1_;
	empty = true;
    }
    
    protected VR(int i, int i_2_, int i_3_, Object object, int i_4_) {
	group = i;
	element = i_2_;
	val = new Vector();
	dcm_type = i_3_;
	val.addElement(object);
	dataLen = i_4_;
	empty = false;
	headerLen = 8;
    }
    
    protected int getDDType() {
	int i = DDict.lookupDDict(group, element);
	return i;
    }
    
    protected void writeVRHeader(k var_k) throws IOException, DicomException {
	if (Debug.DEBUG >= 5)
	    Debug.printMessage("VR.writeVRHeader", group, element);
	int i = var_k.a();
	boolean bool = var_k.b();
	boolean bool_5_ = false;
	byte[] is = { 0, 0 };
	var_k.b(group);
	var_k.b(element);
	if (group == 2)
	    i = 8194;
	int i_6_ = dataLen;
	if (empty)
	    i_6_ = 0;
	else if (dcm_type == 10 && bool)
	    i_6_ = -1;
	if ((dcm_type == 24 || dcm_type == 8 || dcm_type == 22)
	    && val.size() > 1) {
	    dcm_type = 8;
	    i_6_ = -1;
	}
	if (i == 8193)
	    var_k.c(i_6_);
	else {
	    String string = DDict.a(dcm_type);
	    var_k.a(string.substring(0, 2));
	    switch (dcm_type) {
	    case 0:
	    case 8:
	    case 10:
	    case 22:
	    case 24:
	    case 27:
		var_k.write(is);
		var_k.c(i_6_);
		break;
	    default:
		var_k.b(i_6_);
	    }
	}
    }
    
    private void a(i var_i, DicomObject dicomobject)
	throws IOException, DicomException {
	Enumeration enumeration
	    = dicomobject.a(false, var_i.a(), var_i.b(), true);
	while (enumeration.hasMoreElements()) {
	    VR vr_7_ = (VR) enumeration.nextElement();
	    vr_7_.writeVRHeader(var_i);
	    vr_7_.writeVRData(var_i);
	}
    }
    
    private void a(i var_i) throws DicomException, IOException {
	if (var_i.b()) {
	    for (int i = 0; i < val.size(); i++) {
		DicomObject dicomobject = (DicomObject) val.elementAt(i);
		var_i.b(65534);
		var_i.b(57344);
		var_i.c(-1);
		a(var_i, dicomobject);
		var_i.b(65534);
		var_i.b(57357);
		var_i.c(0);
	    }
	    var_i.b(65534);
	    var_i.b(57565);
	    var_i.c(0);
	} else {
	    for (int i = 0; i < val.size(); i++) {
		DicomObject dicomobject = (DicomObject) val.elementAt(i);
		var_i.b(65534);
		var_i.b(57344);
		var_i.c(b.a(dicomobject, var_i.a(), var_i.b()) - 8);
		a(var_i, dicomobject);
	    }
	}
    }
    
    private void b(i var_i) throws IOException, DicomException {
	for (int i = 0; i < val.size(); i++) {
	    byte[] is = (byte[]) val.elementAt(i);
	    var_i.b(65534);
	    var_i.b(57344);
	    var_i.c(is.length);
	    if (is.length != 0)
		var_i.write(is);
	}
	var_i.b(65534);
	var_i.b(57565);
	var_i.c(0);
    }
    
    protected void writeVRData(i var_i) throws IOException, DicomException {
	if (dataLen == 0 || empty) {
	    if (Debug.DEBUG >= 5)
		Debug.printMessage("VR.writeVRData <empty>", group, element);
	} else if (dcm_type == 10) {
	    if (Debug.DEBUG >= 5)
		Debug.printMessage("VR.writeVRData <sequence>", group,
				   element);
	    a(var_i);
	} else if ((dcm_type == 24 || dcm_type == 8 || dcm_type == 22)
		   && val.size() > 1) {
	    if (Debug.DEBUG >= 5)
		Debug.printMessage("VR.writeVRData <undefined length>", group,
				   element);
	    b(var_i);
	} else {
	    if (Debug.DEBUG >= 5)
		Debug.printMessage("VR.writeVRData <normal>", group, element);
	    var_i.d(val, dcm_type);
	}
    }
    
    protected boolean readVRHeader(h var_h)
	throws IOException, DicomException {
	int i = var_h.j();
	group = var_h.k();
	element = var_h.k();
	if (Debug.DEBUG >= 5) {
	    try {
		Debug.printMessage(("VR.readVRHeader (ts: "
				    + UID.getUIDEntry(var_h.j()).toString()
				    + ")"),
				   group, element);
	    } catch (IllegalValueException illegalvalueexception) {
		/* empty */
	    }
	}
	if (group == 2)
	    i = 8194;
	if (element == 0)
	    dcm_type = 1;
	else
	    dcm_type = DDict.getTypeCode(group, element);
	if (i == 8193) {
	    dataLen = var_h.m();
	    headerLen = 8;
	} else {
	    String string = var_h.d(2);
	    if (group != 65534 || element != 57357)
		dcm_type = DDict.a(string);
	    switch (dcm_type) {
	    case 0:
	    case 8:
	    case 10:
	    case 24:
	    case 27:
		var_h.skipBytes(2);
		dataLen = var_h.m();
		headerLen = 12;
		break;
	    default:
		dataLen = var_h.k();
		headerLen = 8;
	    }
	}
	int i_8_ = DDict.c(dcm_type);
	if (i_8_ != 0 && dataLen % i_8_ != 0) {
	    var_h.skipBytes(dataLen);
	    Debug.out.println
		("Data element: (" + group + "," + element + "): "
		 + DDict.getDescription(DDict.lookupDDict(group, element))
		 + " has an invalid length (" + dataLen
		 + ")\n    Skipping...");
	    return false;
	}
	return true;
    }
    
    private DicomObject a(f var_f) throws IOException, DicomException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.b = var_f.getOffset() - 4L;
	long l = (long) var_f.m();
	if (l != -1L) {
	    dicomobject.c = 0;
	    long l_9_ = var_f.getOffset() + l;
	    while (var_f.getOffset() != l_9_) {
		VR vr_10_ = new VR();
		if (vr_10_.readVRHeader(var_f) && vr_10_.readVRData(var_f)) {
		    dicomobject.c += vr_10_.headerLen;
		    dicomobject.c += vr_10_.dataLen;
		    dicomobject.a(vr_10_);
		}
	    }
	} else {
	    boolean bool = false;
	    dicomobject.c = 0;
	    while (!bool) {
		VR vr_11_ = new VR();
		if (vr_11_.readVRHeader(var_f)) {
		    if (vr_11_.group == 65534 && vr_11_.element == 57357)
			bool = true;
		    else if (vr_11_.readVRData(var_f)) {
			dicomobject.c += vr_11_.headerLen;
			dicomobject.c += vr_11_.dataLen;
			dicomobject.a(vr_11_);
		    }
		}
	    }
	}
	dicomobject.c += 16;
	return dicomobject;
    }
    
    private Vector b(f var_f) throws DicomException, IOException {
	Vector vector = new Vector();
	if (dataLen != -1) {
	    long l = var_f.getOffset() + (long) dataLen;
	    dataLen = 0;
	    while (var_f.getOffset() != l) {
		int i = var_f.k();
		int i_12_ = var_f.k();
		if (i == 65534 && i_12_ == 57344) {
		    DicomObject dicomobject = a(var_f);
		    if (dicomobject != null) {
			dataLen += dicomobject.c;
			vector.addElement(dicomobject);
		    }
		}
	    }
	} else {
	    boolean bool = false;
	    dataLen = 0;
	    while (!bool) {
		int i = var_f.k();
		int i_13_ = var_f.k();
		if (i == 65534 && i_13_ == 57344) {
		    DicomObject dicomobject = a(var_f);
		    if (dicomobject != null) {
			dataLen += dicomobject.c;
			vector.addElement(dicomobject);
		    }
		} else if (i == 65534 && i_13_ == 57565) {
		    var_f.skipBytes(4);
		    bool = true;
		} else
		    throw new DicomException("Unexpected tag read: (" + i + ","
					     + i_13_ + ") at offset "
					     + var_f.getOffset());
	    }
	}
	dataLen += 8;
	return vector;
    }
    
    private Vector c(f var_f) throws DicomException, IOException {
	Vector vector = new Vector();
	dataLen = 0;
	int i;
	int i_14_;
	for (;;) {
	    i = var_f.k();
	    i_14_ = var_f.k();
	    dataLen += 4;
	    if (i == 65534 && i_14_ == 57565) {
		var_f.skipBytes(4);
		dataLen += 4;
		return vector;
	    }
	    if (i != 65534 || i_14_ != 57344)
		break;
	    int i_15_ = var_f.m();
	    dataLen += 4;
	    byte[] is = new byte[i_15_];
	    var_f.readFully(is);
	    dataLen += i_15_;
	    vector.addElement(is);
	}
	throw new DicomException("Unexpected tag encountered: (" + i + ","
				 + i_14_ + ").");
    }
    
    protected boolean readVRData(f var_f) throws IOException, DicomException {
	if (dataLen == 0) {
	    if (Debug.DEBUG >= 5)
		Debug.printMessage("VR.readVRData <empty>", group, element);
	    empty = true;
	    val = new Vector();
	    return true;
	}
	empty = false;
	if (dataLen == -1) {
	    if (dcm_type == 24 || dcm_type == 8 || dcm_type == 22) {
		if (Debug.DEBUG >= 5)
		    Debug.printMessage("VR.readVRData <undefined length>",
				       group, element);
		val = c(var_f);
		if (val == null)
		    return false;
		return true;
	    }
	    dcm_type = 10;
	}
	if (dcm_type == 10) {
	    if (Debug.DEBUG >= 5)
		Debug.printMessage("VR.readVRData <sequence>", group, element);
	    val = b(var_f);
	    if (val == null)
		return false;
	    return true;
	}
	try {
	    if (Debug.DEBUG >= 5)
		Debug.printMessage("VR.readVRData <normal>", group, element);
	    val = var_f.d(dcm_type, dataLen);
	} catch (Exception exception) {
	    if (Debug.DEBUG >= 1) {
		Debug.out.print("Encoding error in data of +(");
		Debug.out.print(DumpUtils.a(group, 4) + ",");
		Debug.out.print(DumpUtils.a(element, 4) + "), ");
		Debug.out.println("tag excluded from dataset");
		if (Debug.DEBUG > 2)
		    exception.printStackTrace(Debug.out);
	    }
	    return false;
	}
	return true;
    }
}
