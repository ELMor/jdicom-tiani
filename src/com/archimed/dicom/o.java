/* o - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

class o
{
    private long a = 122L;
    private long b = 9223372036854775807L;
    
    private long a(InputStream inputstream) {
	long l = 0L;
	try {
	    BufferedReader bufferedreader
		= new BufferedReader(new InputStreamReader(inputstream));
	    for (int i = 0; i < 19; i++) {
		l *= 10L;
		String string = bufferedreader.readLine();
		long l_0_ = Long.parseLong(string) % a;
		l += l_0_;
	    }
	} catch (Throwable throwable) {
	    return 0L;
	}
	return l;
    }
    
    private long b() {
	long l = b - System.currentTimeMillis();
	return l / 86400000L;
    }
    
    private boolean c() {
	if (b == 9223372036854775807L)
	    return true;
	if (b == 0L)
	    return false;
	if (b > System.currentTimeMillis()) {
	    System.out
		.println("Your evaluation copy expires in " + b() + " days.");
	    return true;
	}
	return false;
    }
    
    private void d() {
	System.out.println("Your evaluation copy of " + Jdt.getVersion()
			   + " has expired.");
	System.out.println
	    ("Please contact SoftLink (www.softlink.be) for a license.");
    }
    
    void a() {
	InputStream inputstream
	    = this.getClass().getResourceAsStream("/jdt.key");
	b = a(inputstream);
	if (!c()) {
	    d();
	    System.exit(0);
	}
    }
}
