/* IntHashtableEntry - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

class IntHashtableEntry
{
    int a;
    int b;
    Object c;
    IntHashtableEntry d;
    
    protected Object clone() {
	IntHashtableEntry inthashtableentry_0_ = new IntHashtableEntry();
	inthashtableentry_0_.a = a;
	inthashtableentry_0_.b = b;
	inthashtableentry_0_.c = c;
	inthashtableentry_0_.d
	    = d != null ? (IntHashtableEntry) d.clone() : null;
	return inthashtableentry_0_;
    }
}
