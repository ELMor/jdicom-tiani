/* DDateTime - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DDateTime
{
    private Calendar a;
    private boolean b = false;
    
    public DDateTime() {
	b = true;
    }
    
    public DDateTime(String string) throws NumberFormatException {
	string = string.trim();
	int i = string.length();
	if (i == 0)
	    b = true;
	else {
	    int i_1_;
	    int i_2_;
	    int i_3_;
	    int i_4_;
	    int i_5_;
	    int i_6_;
	    int i_0_ = i_1_ = i_2_ = i_3_ = i_4_ = i_5_ = i_6_ = 0;
	    switch (Math.min(i, 15)) {
	    case 15:
		if (string.charAt(14) != '.')
		    throw new NumberFormatException
			      ("Cannot parse string into DDateTime");
		if (i > 15)
		    i_6_ = (int) (Float.parseFloat(string.substring(14))
				  * 1000.0F);
		/* fall through */
	    case 14:
		i_5_ = Integer.parseInt(string.substring(12, 14));
		/* fall through */
	    case 12:
		i_4_ = Integer.parseInt(string.substring(10, 12));
		/* fall through */
	    case 10:
		i_3_ = Integer.parseInt(string.substring(8, 10));
		/* fall through */
	    case 8:
		i_2_ = Integer.parseInt(string.substring(6, 8));
		i_1_ = Integer.parseInt(string.substring(4, 6));
		i_0_ = Integer.parseInt(string.substring(0, 4));
		DDate.a(i_0_, i_1_, i_2_);
		break;
	    default:
		throw new NumberFormatException
			  ("Cannot parse string into DDateTime");
	    }
	    a = new GregorianCalendar(i_0_, i_1_ - 1, i_2_);
	    a.set(11, i_3_);
	    a.set(12, i_4_);
	    a.set(13, i_5_);
	    a.set(14, i_6_);
	}
    }
    
    public String toString() {
	if (b)
	    return "";
	SimpleDateFormat simpledateformat
	    = new SimpleDateFormat("EEE dd MMM yyyy HH:mm:ss.SSS");
	return simpledateformat.format(a.getTime());
    }
    
    public String toString(DateFormat dateformat) {
	if (b)
	    return "";
	return dateformat.format(a.getTime());
    }
    
    public String toDICOMString() {
	if (b)
	    return "";
	SimpleDateFormat simpledateformat
	    = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
	return simpledateformat.format(a.getTime());
    }
    
    public boolean isEmpty() {
	return b;
    }
}
