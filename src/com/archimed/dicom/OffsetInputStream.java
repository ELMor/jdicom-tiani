/* OffsetInputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

class OffsetInputStream extends FilterInputStream
{
    private long a = 0L;
    private long b;
    protected long counter = 0L;
    
    public OffsetInputStream(InputStream inputstream) {
	this(inputstream, 0);
    }
    
    public OffsetInputStream(InputStream inputstream, int i) {
	super(inputstream);
	a = (long) i;
    }
    
    public int read() throws IOException {
	a++;
	return in.read();
    }
    
    public int read(byte[] is) throws IOException {
	return read(is, 0, is.length);
    }
    
    public int read(byte[] is, int i, int i_0_) throws IOException {
	b = (long) in.read(is, i, i_0_);
	a += (long) i + b;
	return (int) b;
    }
    
    public long skip(long l) throws IOException {
	b = in.skip(l);
	a += b;
	return b;
    }
    
    public boolean markSupported() {
	return false;
    }
    
    public void readFully(byte[] is) throws IOException {
	readFully(is, 0, is.length);
    }
    
    public void readFully(byte[] is, int i, int i_1_) throws IOException {
	a += (long) (i + i_1_);
	InputStream inputstream = in;
	int i_3_;
	for (int i_2_ = 0; i_2_ < i_1_; i_2_ += i_3_) {
	    i_3_ = inputstream.read(is, i + i_2_, i_1_ - i_2_);
	    if (i_3_ < 0)
		throw new EOFException();
	}
    }
    
    public int skipBytes(int i) throws IOException {
	InputStream inputstream = in;
	for (int i_4_ = 0; i_4_ < i;
	     i_4_ += (int) inputstream.skip((long) (i - i_4_))) {
	    /* empty */
	}
	a += (long) i;
	return i;
    }
    
    public long getOffset() {
	return a;
    }
    
    protected void resetCounter() {
	counter = a;
    }
    
    protected long getCounter() {
	if (a > counter)
	    return a - counter;
	return 0L;
    }
}
