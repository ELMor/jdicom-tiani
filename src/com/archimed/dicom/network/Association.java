/* Association - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.y;

public class Association
{
    private static final int a = 8;
    private static final int b = 9;
    public static final int PDATA_PDU = 10;
    public static final int RELEASE_REQUEST = 11;
    public static final int ABORT = 12;
    public static final int RELEASE_RESPONSE = 13;
    public static final int ASSOCIATE_REQUEST = 14;
    public static final int ASSOCIATE_ACKNOWLEDGE = 15;
    public static final int ASSOCIATE_REJECT = 16;
    static final String c = "1.2.826.0.1.3680043.2.60.0.1";
    static final String d = "softlink_jdt103";
    private boolean e = true;
    private boolean f = false;
    private String g;
    private String h;
    private int i;
    private InputStream j;
    private OutputStream k;
    private PushbackInputStream l;
    private Request m;
    private Acknowledge n;
    private ByteArrayOutputStream o;
    private ByteArrayOutputStream p;
    private AssociateRequestPdu q;
    private AssociateAcknowledgePdu r;
    private Vector s;
    private boolean t = false;
    private boolean u = false;
    private int v;
    private int w = 0;
    private static final SimpleDateFormat x
	= new SimpleDateFormat("yyyyMMddHHmmss.SSS");
    
    public Association(InputStream inputstream, OutputStream outputstream) {
	j = inputstream;
	k = outputstream;
	l = new PushbackInputStream(inputstream);
	o = new ByteArrayOutputStream();
	p = new ByteArrayOutputStream();
    }
    
    public void setGrouplens(boolean bool) {
	e = bool;
    }
    
    public void setSeqUndef(boolean bool) {
	f = bool;
    }
    
    public DicomObject receiveCommand()
	throws IOException, IllegalValueException, DicomException,
	       UnknownUIDException {
	byte[] is = receiveCommandAsByteArray();
	if (is != null) {
	    DicomObject dicomobject = new DicomObject();
	    dicomobject.read(new ByteArrayInputStream(is), true);
	    Object object = null;
	    return dicomobject;
	}
	return null;
    }
    
    public byte[] receiveCommandAsByteArray()
	throws IOException, IllegalValueException {
	if (t == true || a() == 8) {
	    byte[] is = o.toByteArray();
	    o.reset();
	    t = false;
	    if (Debug.dumpCmdsetIntoDir != null
		&& Debug.dumpCmdsetIntoDir.length() != 0)
		a(is, Debug.dumpCmdsetIntoDir, ".cmd.dcm");
	    return is;
	}
	return null;
    }
    
    public DicomObject receiveData() throws IOException, IllegalValueException,
					    DicomException,
					    UnknownUIDException {
	byte[] is = receiveDataAsByteArray();
	if (is != null) {
	    UIDEntry uidentry = getTransferSyntax((byte) v);
	    int i = a(is, uidentry);
	    if (i != 0)
		Debug.out.println
		    ("WARNING: received dataset contains (0002,xxxx) File Meta Elements [will be skipped]");
	    DicomObject dicomobject = new DicomObject();
	    dicomobject.read(new ByteArrayInputStream(is, i, is.length - i),
			     uidentry.getConstant(), true);
	    Object object = null;
	    return dicomobject;
	}
	return null;
    }
    
    private int a(byte[] is, UIDEntry uidentry) {
	switch (uidentry.getConstant()) {
	case 8195:
	    if (((is[0] & 0xff) << 8) + (is[1] & 0xff) == 2)
		return (12 + ((is[8] & 0xff) << 24) + ((is[9] & 0xff) << 32)
			+ ((is[10] & 0xff) << 8) + (is[11] & 0xff));
	    break;
	default:
	    if (((is[1] & 0xff) << 8) + (is[0] & 0xff) == 2)
		return (12 + ((is[11] & 0xff) << 24) + ((is[10] & 0xff) << 32)
			+ ((is[9] & 0xff) << 8) + (is[8] & 0xff));
	}
	return 0;
    }
    
    private void a(byte[] is, String string, String string_0_) {
	try {
	    File file = new File(string);
	    if (!file.exists())
		file.mkdirs();
	    File file_1_ = new File(file, x.format(new Date()) + string_0_);
	    FileOutputStream fileoutputstream = new FileOutputStream(file_1_);
	    try {
		fileoutputstream.write(is);
	    } finally {
		fileoutputstream.close();
	    }
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
    }
    
    public byte[] receiveDataAsByteArray()
	throws IOException, IllegalValueException {
	if (u == true || a() == 9) {
	    byte[] is = p.toByteArray();
	    p.reset();
	    u = false;
	    if (Debug.dumpDatasetIntoDir != null
		&& Debug.dumpDatasetIntoDir.length() != 0)
		a(is, Debug.dumpDatasetIntoDir, ".data.dcm");
	    return is;
	}
	return null;
    }
    
    public void sendAssociateResponse(Response response)
	throws IOException, IllegalValueException {
	if (response instanceof Abort) {
	    Abort abort = (Abort) response;
	    AbortPdu abortpdu
		= new AbortPdu(abort.getSource(), abort.getReason());
	    abortpdu.write(k);
	} else if (response instanceof Reject) {
	    Reject reject = (Reject) response;
	    AssociateRejectPdu associaterejectpdu
		= new AssociateRejectPdu(reject.getResult(),
					 reject.getSource(),
					 reject.getReason());
	    associaterejectpdu.write(k);
	} else if (response instanceof Acknowledge) {
	    n = (Acknowledge) response;
	    r = new AssociateAcknowledgePdu(n);
	    r.write(k);
	    s = r.getPresentationContextResponseItems();
	}
    }
    
    public void sendReleaseResponse()
	throws IOException, IllegalValueException {
	DataOutputStream dataoutputstream = new DataOutputStream(k);
	k.write(6);
	k.write(0);
	dataoutputstream.writeInt(4);
	dataoutputstream.writeInt(0);
    }
    
    public byte getPresentationContext(int i, int i_2_)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	if (uidentry.getType() != 4 && uidentry.getType() != 1)
	    throw new IllegalValueException
		      ("first argument should be Meta SOP Class or SOP Class");
	UIDEntry uidentry_3_ = UID.getUIDEntry(i_2_);
	if (uidentry_3_.getType() != 3)
	    throw new IllegalValueException
		      ("second argument should be Transfer Syntax");
	for (int i_4_ = 0; i_4_ < m.getPresentationContexts(); i_4_++) {
	    uidentry = m.getAbstractSyntax(i_4_);
	    if (uidentry.getConstant() == i) {
		byte i_5_ = m.getPresentationContextID(i_4_);
		for (int i_6_ = 0; i_6_ < n.getPresentationContexts();
		     i_6_++) {
		    byte i_7_ = n.getPresentationContextID(i_6_);
		    if (i_7_ == i_5_ && n.getResult(i_6_) == 0) {
			uidentry_3_ = n.getTransferSyntax(i_6_);
			if (uidentry_3_.getConstant() == i_2_)
			    return i_5_;
		    }
		}
	    }
	}
	throw new IllegalValueException
		  ("no accepted presentation context with specified abstract syntax");
    }
    
    public byte[] listAcceptedPresentationContexts(int i)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	if (uidentry.getType() != 4 && uidentry.getType() != 1)
	    throw new IllegalValueException
		      ("first argument should be Meta SOP Class or SOP Class");
	int i_8_ = 0;
	int i_9_ = m.getPresentationContexts();
	byte[] is = new byte[i_9_];
	for (int i_10_ = 0; i_10_ < i_9_; i_10_++) {
	    uidentry = m.getAbstractSyntax(i_10_);
	    if (uidentry.getConstant() == i) {
		byte i_11_ = m.getPresentationContextID(i_10_);
		for (int i_12_ = 0; i_12_ < n.getPresentationContexts();
		     i_12_++) {
		    byte i_13_ = n.getPresentationContextID(i_12_);
		    if (i_13_ == i_11_) {
			if (n.getResult(i_12_) == 0)
			    is[i_8_++] = i_11_;
			break;
		    }
		}
	    }
	}
	byte[] is_14_ = new byte[i_8_];
	System.arraycopy(is, 0, is_14_, 0, i_8_);
	return is_14_;
    }
    
    public UIDEntry[] listAcceptedTransferSyntaxes(int i)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	if (uidentry.getType() != 4 && uidentry.getType() != 1)
	    throw new IllegalValueException
		      ("first argument should be Meta SOP Class or SOP Class");
	int i_15_ = 0;
	int i_16_ = m.getPresentationContexts();
	UIDEntry[] uidentrys = new UIDEntry[i_16_];
	for (int i_17_ = 0; i_17_ < i_16_; i_17_++) {
	    uidentry = m.getAbstractSyntax(i_17_);
	    if (uidentry.getConstant() == i) {
		byte i_18_ = m.getPresentationContextID(i_17_);
		for (int i_19_ = 0; i_19_ < n.getPresentationContexts();
		     i_19_++) {
		    byte i_20_ = n.getPresentationContextID(i_19_);
		    if (i_20_ == i_18_) {
			if (n.getResult(i_19_) == 0)
			    uidentrys[i_15_++] = n.getTransferSyntax(i_19_);
			break;
		    }
		}
	    }
	}
	UIDEntry[] uidentrys_21_ = new UIDEntry[i_15_];
	System.arraycopy(uidentrys, 0, uidentrys_21_, 0, i_15_);
	return uidentrys_21_;
    }
    
    public byte getPresentationContext(int i) throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	if (uidentry.getType() != 4 && uidentry.getType() != 1)
	    throw new IllegalValueException
		      ("first argument should be Meta SOP Class or SOP Class");
	int i_22_ = m.getPresentationContexts();
	for (int i_23_ = 0; i_23_ < i_22_; i_23_++) {
	    uidentry = m.getAbstractSyntax(i_23_);
	    if (uidentry.getConstant() == i) {
		byte i_24_ = m.getPresentationContextID(i_23_);
		for (int i_25_ = 0; i_25_ < n.getPresentationContexts();
		     i_25_++) {
		    byte i_26_ = n.getPresentationContextID(i_25_);
		    if (i_26_ == i_24_ && n.getResult(i_25_) == 0)
			return i_24_;
		}
	    }
	}
	throw new IllegalValueException
		  ("no accepted presentation context with specified abstract syntax");
    }
    
    public void send
	(int i, DicomObject dicomobject, DicomObject dicomobject_27_)
	throws IOException, IllegalValueException, DicomException {
	byte i_28_ = getPresentationContext(i);
	sendInPresentationContext(i_28_, dicomobject, dicomobject_27_);
    }
    
    /**
     * @deprecated
     */
    public void send
	(byte i, DicomObject dicomobject, DicomObject dicomobject_29_)
	throws IOException, IllegalValueException, DicomException {
	sendInPresentationContext(i, dicomobject, dicomobject_29_);
    }
    
    public void sendInPresentationContext
	(byte i, DicomObject dicomobject, DicomObject dicomobject_30_)
	throws IOException, IllegalValueException, DicomException {
	UIDEntry uidentry = getTransferSyntax(i);
	if (uidentry == null)
	    Debug.out.println("NULL");
	ByteArrayOutputStream bytearrayoutputstream
	    = new ByteArrayOutputStream();
	y var_y = new y();
	PduOutputStream pduoutputstream
	    = new PduOutputStream(k, this.i, i, (byte) 1);
	dicomobject.write(pduoutputstream, false, 8193, true);
	pduoutputstream.close();
	if (dicomobject_30_ != null) {
	    pduoutputstream = new PduOutputStream(k, this.i, i, (byte) 0);
	    dicomobject_30_.write(pduoutputstream, false,
				  uidentry.getConstant(), f, e);
	    pduoutputstream.close();
	}
    }
    
    public UIDEntry getTransferSyntax(byte i) throws IllegalValueException {
	int i_31_ = -1;
	for (int i_32_ = 0; i_32_ < n.getPresentationContexts(); i_32_++) {
	    if (i == n.getPresentationContextID(i_32_))
		i_31_ = i_32_;
	}
	if (i_31_ == -1)
	    throw new IllegalValueException
		      ("presentation context with specified id not present in Acknowledge");
	if (n.getResult(i_31_) != 0)
	    throw new IllegalValueException
		      ("not an accepted presentation context");
	return n.getTransferSyntax(i_31_);
    }
    
    private void a(byte[] is, int i, int i_33_, byte i_34_, byte i_35_)
	throws IOException {
	int i_36_ = i;
	BufferedOutputStream bufferedoutputstream
	    = new BufferedOutputStream(k, 8192);
	DataOutputStream dataoutputstream
	    = new DataOutputStream(bufferedoutputstream);
	int i_37_ = i_33_;
	int i_38_;
	if (this.i != 0)
	    i_38_ = this.i - 6;
	else
	    i_38_ = i_37_;
	for (/**/; i_37_ > i_38_; i_37_ -= i_38_) {
	    dataoutputstream.writeByte(4);
	    dataoutputstream.writeByte(0);
	    dataoutputstream.writeInt(this.i);
	    dataoutputstream.writeInt(this.i - 4);
	    dataoutputstream.writeByte(i_34_);
	    dataoutputstream.writeByte(i_35_);
	    dataoutputstream.write(is, i_36_, i_38_);
	    i_36_ += i_38_;
	}
	dataoutputstream.writeByte(4);
	dataoutputstream.writeByte(0);
	dataoutputstream.writeInt(i_37_ + 6);
	dataoutputstream.writeInt(i_37_ + 2);
	dataoutputstream.writeByte(i_34_);
	dataoutputstream.writeByte(i_35_ + 2);
	dataoutputstream.write(is, i_36_, i_37_);
	bufferedoutputstream.flush();
    }
    
    public int peek() throws IOException, IllegalValueException {
	int i = l.read();
	if (i == -1)
	    throw new IOException("end of stream reached");
	l.unread(i);
	if (i == 1)
	    return 14;
	if (i == 2)
	    return 15;
	if (i == 3)
	    return 16;
	if (i == 4)
	    return 10;
	if (i == 5)
	    return 11;
	if (i == 6)
	    return 13;
	if (i == 7)
	    return 12;
	throw new IllegalValueException("Unexpected byte read: " + i);
    }
    
    private int a() throws IOException, IllegalValueException {
	DataInputStream datainputstream = new DataInputStream(l);
	DataOutputStream dataoutputstream = new DataOutputStream(k);
	int i = 4;
	do {
	    int i_39_ = l.read();
	    if (Debug.DEBUG > 3) {
		Debug.out.println("incoming PDU");
		Debug.out.println("PDU type: " + i_39_);
	    }
	    if (i_39_ != i)
		throw new IllegalValueException
			  ("first byte of PDATA-PDU not 0x04 but '" + i_39_
			   + "'");
	    datainputstream.skipBytes(1);
	    int i_40_ = datainputstream.readInt();
	    if (Debug.DEBUG > 3)
		Debug.out.println("PDU length: " + i_40_);
	    while (i_40_ > 0) {
		int i_41_ = datainputstream.readInt();
		if (Debug.DEBUG > 3)
		    Debug.out.println("item length: " + i_41_);
		byte[] is = new byte[i_41_ - 2];
		i_40_ = i_40_ - i_41_ - 4;
		v = datainputstream.readByte();
		if (Debug.DEBUG > 3)
		    Debug.out.println("presentation context id: " + v);
		byte i_42_ = datainputstream.readByte();
		if (Debug.DEBUG > 3)
		    Debug.out.println("header value: " + i_42_);
		datainputstream.readFully(is);
		if (i_42_ == 0)
		    p.write(is, 0, is.length);
		else if (i_42_ == 1)
		    o.write(is, 0, is.length);
		else if (i_42_ == 2) {
		    p.write(is, 0, is.length);
		    u = true;
		} else if (i_42_ == 3) {
		    o.write(is, 0, is.length);
		    t = true;
		} else
		    throw new IllegalValueException
			      ("illegal value for header of PDV item");
	    }
	    if (t)
		return 8;
	} while (!u);
	return 9;
    }
    
    public int getCurrentPresentationContext() {
	return v;
    }
    
    public UIDEntry getCurrentAbstractSyntax() throws IllegalValueException {
	for (int i = 0; i < m.getPresentationContexts(); i++) {
	    if (v == m.getPresentationContextID(i))
		return m.getAbstractSyntax(i);
	}
	throw new IllegalValueException
		  ("no abstract syntax in request found for received presentation context id: "
		   + v);
    }
    
    public Request receiveAssociateRequest()
	throws IOException, UnknownUIDException, IllegalValueException {
	q = new AssociateRequestPdu();
	q.read(l);
	i = q.getRequest().getMaxPduSize();
	m = q.getRequest();
	return m;
    }
    
    public Response receiveAssociateResponse()
	throws IOException, UnknownUIDException, IllegalValueException {
	if (Debug.DEBUG > 1)
	    Debug.out.println("Waiting for AssociationRsp");
	int i = l.read();
	l.unread(i);
	if (i == 2) {
	    if (Debug.DEBUG > 1)
		Debug.out.println("ASSOCIATE_ACKNOWLEDGE detected");
	    r = new AssociateAcknowledgePdu();
	    r.read(l);
	    this.i = r.getAcknowledge().getMaxPduSize();
	    s = r.getPresentationContextResponseItems();
	    n = r.getAcknowledge();
	    return r.getAcknowledge();
	}
	if (i == 3) {
	    if (Debug.DEBUG > 1)
		Debug.out.println("ASSOCIATE_REJECT detected");
	    AssociateRejectPdu associaterejectpdu = new AssociateRejectPdu();
	    associaterejectpdu.read(l);
	    return associaterejectpdu.getReject();
	}
	if (i == 7) {
	    if (Debug.DEBUG > 1)
		Debug.out.println("ABORT detected");
	    AbortPdu abortpdu = new AbortPdu();
	    abortpdu.read(l);
	    return abortpdu.getAbort();
	}
	throw new IllegalValueException
		  ("first byte of PDATA-PDU not 0x04 but '" + i + "'");
    }
    
    public void sendAssociateRequest(Request request)
	throws IOException, IllegalValueException {
	m = request;
	m.a("1.2.826.0.1.3680043.2.60.0.1");
	m.b("softlink_jdt103");
	q = new AssociateRequestPdu(m);
	ByteArrayOutputStream bytearrayoutputstream
	    = new ByteArrayOutputStream();
	DataOutputStream dataoutputstream
	    = new DataOutputStream(bytearrayoutputstream);
	q.write(dataoutputstream);
	k.write(bytearrayoutputstream.toByteArray());
    }
    
    public void sendReleaseRequest()
	throws IOException, IllegalValueException {
	DataOutputStream dataoutputstream = new DataOutputStream(k);
	k.write(5);
	k.write(0);
	dataoutputstream.writeInt(4);
	dataoutputstream.writeInt(0);
    }
    
    public void receiveReleaseRequest()
	throws IOException, IllegalValueException {
	DataInputStream datainputstream = new DataInputStream(l);
	int i = l.read();
	if (i != 5)
	    throw new IllegalValueException
		      ("wrong first byte for expected Release Request: " + i);
	l.read();
	datainputstream.readInt();
	datainputstream.readInt();
    }
    
    public void receiveReleaseResponse()
	throws IOException, IllegalValueException {
	DataInputStream datainputstream = new DataInputStream(l);
	int i = l.read();
	if (i != 6)
	    throw new IllegalValueException
		      ("wrong first byte for expected Release Response: " + i);
	l.read();
	datainputstream.readInt();
	datainputstream.readInt();
    }
    
    public void sendAbort(int i, int i_43_) throws IOException {
	AbortPdu abortpdu = new AbortPdu(i, i_43_);
	abortpdu.write(k);
    }
    
    public Abort receiveAbort() throws IllegalValueException, IOException {
	AbortPdu abortpdu = new AbortPdu();
	abortpdu.read(l);
	return abortpdu.getAbort();
    }
    
    public int getResultForAbstractSyntax(int i) throws IllegalValueException {
	return ResponsePolicy.getResultForAbstractSyntax(m, n, i);
    }
}
