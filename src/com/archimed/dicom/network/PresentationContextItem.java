/* PresentationContextItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;

class PresentationContextItem
{
    private byte a = 32;
    private int b;
    private Vector c;
    private AbstractSyntaxItem d;
    
    public PresentationContextItem() {
	/* empty */
    }
    
    public PresentationContextItem
	(int i, AbstractSyntaxItem abstractsyntaxitem, Vector vector) {
	b = i;
	d = abstractsyntaxitem;
	c = vector;
    }
    
    public int getID() {
	return b;
    }
    
    public int getLength() {
	return 4 + a();
    }
    
    public AbstractSyntaxItem getAbstractSyntaxItem() {
	return d;
    }
    
    public Vector getTransferSyntaxes() {
	return c;
    }
    
    public void addTransferSyntaxItem(TransferSyntaxItem transfersyntaxitem) {
	c.addElement(transfersyntaxitem);
    }
    
    private int a() {
	int i = 4;
	i += d.getLength();
	for (int i_0_ = 0; i_0_ < c.size(); i_0_++) {
	    TransferSyntaxItem transfersyntaxitem
		= (TransferSyntaxItem) c.elementAt(i_0_);
	    i += transfersyntaxitem.getLength();
	}
	return i;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(a());
	dataoutputstream.write(b);
	dataoutputstream.write(0);
	dataoutputstream.write(0);
	dataoutputstream.write(0);
	d.write(dataoutputstream);
	for (int i = 0; i < c.size(); i++)
	    ((TransferSyntaxItem) c.elementAt(i)).a(dataoutputstream);
    }
    
    int a(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("wrong item type of presentation context item");
	datainputstream.read();
	int i_1_ = datainputstream.readChar();
	int i_2_ = i_1_;
	b = datainputstream.read();
	datainputstream.read();
	datainputstream.read();
	datainputstream.read();
	i_1_ -= 4;
	d = new AbstractSyntaxItem();
	int i_3_ = d.read(datainputstream);
	i_1_ -= i_3_;
	c = new Vector();
	for (/**/; i_1_ > 0; i_1_ -= i_3_) {
	    TransferSyntaxItem transfersyntaxitem = new TransferSyntaxItem();
	    i_3_ = transfersyntaxitem.a(datainputstream);
	    c.addElement(transfersyntaxitem);
	}
	return i_2_ + 4;
    }
}
