/* ResponsePolicy - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;

public class ResponsePolicy
{
    public static Response prepareResponse
	(Request request, String string, String[] strings, int[] is, int i,
	 boolean bool)
	throws IllegalValueException {
	if (request.isPrivateApplicationContext())
	    return new Reject(Reject.REJECTED_PERMANENT,
			      Reject.DICOM_UL_SERVICE_USER,
			      (Reject
			       .USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED));
	if (string != null && !request.getCalledTitle().equals(string))
	    return new Reject(Reject.REJECTED_PERMANENT,
			      Reject.DICOM_UL_SERVICE_USER,
			      Reject.USER_CALLED_AETITLE_NOT_RECOGNIZED);
	boolean bool_0_ = true;
	if (strings != null) {
	    for (int i_1_ = 0; i_1_ < strings.length; i_1_++) {
		if (request.getCallingTitle().equals(strings[i_1_]) == true)
		    bool_0_ = false;
	    }
	    if (bool_0_)
		return new Reject(Reject.REJECTED_PERMANENT,
				  Reject.DICOM_UL_SERVICE_USER,
				  Reject.USER_CALLING_AETITLE_NOT_RECOGNIZED);
	}
	Acknowledge acknowledge = new Acknowledge();
	acknowledge.setCalledTitle(request.getCalledTitle());
	acknowledge.setCallingTitle(request.getCallingTitle());
    while_1_:
	for (int i_2_ = 0; i_2_ < request.getPresentationContexts(); i_2_++) {
	    UIDEntry uidentry = request.getAbstractSyntax(i_2_);
	    for (int i_3_ = 0; i_3_ < is.length; i_3_++) {
		if (uidentry.equals(UID.getUIDEntry(is[i_3_]))) {
		    for (int i_4_ = 0;
			 i_4_ < request.getTransferSyntaxes(i_2_); i_4_++) {
			UIDEntry uidentry_5_
			    = request.getTransferSyntax(i_2_, i_4_);
			if (uidentry_5_.getConstant() == 8193) {
			    acknowledge.addPresentationContext
				(request.getPresentationContextID(i_2_), 0, i);
			    continue while_1_;
			}
		    }
		    acknowledge.addPresentationContext
			(request.getPresentationContextID(i_2_), 4, i);
		    continue while_1_;
		}
	    }
	    acknowledge.addPresentationContext
		(request.getPresentationContextID(i_2_), 3, i);
	}
	if (bool) {
	    bool_0_ = true;
	    for (int i_6_ = 0; i_6_ < acknowledge.getPresentationContexts();
		 i_6_++) {
		if (acknowledge.getResult(i_6_) == 0)
		    bool_0_ = false;
	    }
	    if (bool_0_)
		return new Reject(Reject.REJECTED_PERMANENT,
				  Reject.DICOM_UL_SERVICE_USER,
				  Reject.USER_NO_REASON_GIVEN);
	}
	return acknowledge;
    }
    
    public static int getResultForAbstractSyntax
	(Request request, Acknowledge acknowledge, int i)
	throws IllegalValueException {
	int i_7_ = request.getPresentationContexts();
	for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
	    UIDEntry uidentry = request.getAbstractSyntax(i_8_);
	    if (uidentry.getConstant() == i) {
		byte i_9_ = request.getPresentationContextID(i_8_);
		for (int i_10_ = 0;
		     i_10_ < acknowledge.getPresentationContexts(); i_10_++) {
		    byte i_11_ = acknowledge.getPresentationContextID(i_10_);
		    if (i_9_ == i_11_)
			return acknowledge.getResult(i_10_);
		}
		throw new IllegalValueException
			  ("no matching presentation context id in request and acknowledge for specified abstract syntax");
	    }
	}
	throw new IllegalValueException("abstract syntax not in request");
    }
}
