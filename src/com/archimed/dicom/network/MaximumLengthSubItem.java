/* MaximumLengthSubItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;

class MaximumLengthSubItem extends SubItem
{
    private int a = 81;
    private int b;
    
    public MaximumLengthSubItem() {
	/* empty */
    }
    
    public MaximumLengthSubItem(int i) {
	b = i;
    }
    
    public int getLength() {
	return 8;
    }
    
    public int getMaxPduSize() {
	return b;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(4);
	dataoutputstream.writeInt(b);
    }
    
    public int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received Implementation Class UID Item  not "
		       + a);
	datainputstream.read();
	char c = datainputstream.readChar();
	b = datainputstream.readInt();
	return c + '\004';
    }
}
