/* AbstractSyntaxItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;

class AbstractSyntaxItem
{
    private byte a = 48;
    private UIDEntry b;
    
    public AbstractSyntaxItem(UIDEntry uidentry) {
	b = uidentry;
    }
    
    public AbstractSyntaxItem() {
	/* empty */
    }
    
    public UIDEntry getAbstractSyntax() {
	return b;
    }
    
    public int getLength() {
	return b.getValue().length() + 4;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(b.getValue().length());
	dataoutputstream.write(b.getValue().getBytes());
    }
    
    public int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("wrong itemtype of abstract syntax");
	datainputstream.read();
	char c = datainputstream.readChar();
	byte[] is = new byte[c];
	datainputstream.readFully(is);
	is = n.trimZeros(is);
	String string = new String(is);
	try {
	    b = UID.getUIDEntry(string);
	} catch (UnknownUIDException unknownuidexception) {
	    b = new UIDEntry(0, string, "unknown uid", "??", 1);
	}
	return '\004' + c;
    }
}
