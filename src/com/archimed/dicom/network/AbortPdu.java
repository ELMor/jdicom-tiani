/* AbortPdu - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;

import com.archimed.dicom.IllegalValueException;

class AbortPdu
{
    private int a = 7;
    private int b;
    private int c;
    private int d;
    
    public AbortPdu() {
	/* empty */
    }
    
    public AbortPdu(int i, int i_0_) {
	c = i;
	d = i_0_;
    }
    
    public Abort getAbort() {
	Abort abort = new Abort(c, d);
	return abort;
    }
    
    public void read(InputStream inputstream)
	throws IOException, IllegalValueException {
	PushbackInputStream pushbackinputstream
	    = new PushbackInputStream(inputstream);
	DataInputStream datainputstream
	    = new DataInputStream(pushbackinputstream);
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException("PDU-type field of Abort PDU not "
					    + a);
	inputstream.read();
	b = datainputstream.readInt();
	inputstream.read();
	c = inputstream.read();
	d = inputstream.read();
    }
    
    public void write(OutputStream outputstream) throws IOException {
	DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
	outputstream.write(a);
	outputstream.write(0);
	outputstream.write(0);
	outputstream.write(0);
	outputstream.write(0);
	dataoutputstream.write(4);
	outputstream.write(0);
	outputstream.write(0);
	outputstream.write(c);
	outputstream.write(d);
    }
}
