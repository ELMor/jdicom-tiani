/* ApplicationContextItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.n;

class ApplicationContextItem
{
    private byte a = 16;
    static final String b = "1.2.840.10008.3.1.1.1";
    private String c = "1.2.840.10008.3.1.1.1";
    
    int a() {
	return c.length() + 4;
    }
    
    public String getUid() {
	return c;
    }
    
    public static boolean isPrivate(String string) {
	return "1.2.840.10008.3.1.1.1".equals(string);
    }
    
    void a(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(c.length());
	dataoutputstream.write(c.getBytes());
    }
    
    protected int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	boolean bool = false;
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received Application Context Item  not "
		       + a);
	datainputstream.read();
	char c = datainputstream.readChar();
	byte[] is = new byte[c];
	datainputstream.readFully(is);
	is = n.trimZeros(is);
	this.c = new String(is);
	if (Debug.DEBUG > 3)
	    Debug.out
		.println("ApplicationContextItem [" + (int) c + "]=" + this.c);
	return '\004' + c;
    }
}
