/* ExtendedNegotiationSubItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;

class ExtendedNegotiationSubItem extends SubItem
{
    private int a = 86;
    private UIDEntry b;
    private byte[] c;
    
    public ExtendedNegotiationSubItem() {
	c = new byte[0];
    }
    
    public ExtendedNegotiationSubItem(UIDEntry uidentry, byte[] is) {
	c = new byte[0];
	if (is == null)
	    throw new NullPointerException();
    }
    
    public byte[] getApplicationInfo() {
	return c;
    }
    
    public UIDEntry getUIDEntry() {
	return b;
    }
    
    public int getLength() {
	return 6 + b.getValue().length() + c.length;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(2 + b.getValue().length() + c.length);
	dataoutputstream.writeChar(b.getValue().length());
	dataoutputstream.write(b.getValue().getBytes());
	dataoutputstream.write(c);
    }
    
    public int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException, UnknownUIDException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received Implementation Version Name Item  not "
		       + a);
	datainputstream.read();
	char c = datainputstream.readChar();
	char c_0_ = datainputstream.readChar();
	byte[] is = new byte[c_0_];
	datainputstream.read(is);
	is = n.trimZeros(is);
	String string = new String(is);
	try {
	    b = UID.getUIDEntry(string);
	} catch (UnknownUIDException unknownuidexception) {
	    b = new UIDEntry(0, string, "unknown uid", "??", 1);
	}
	this.c = new byte[c - c_0_ - '\002'];
	datainputstream.read(this.c);
	return '\004' + c;
    }
}
