/* Request - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.tool.n;

public class Request
{
    public static final int NO_SUPPORT_SCU_ROLE = 0;
    public static final int SUPPORT_SCU_ROLE = 1;
    public static final int NO_SUPPORT_SCP_ROLE = 0;
    public static final int SUPPORT_SCP_ROLE = 1;
    public static final int DEFAULT = -1;
    private String a;
    private String b;
    private String c;
    private Vector d;
    private Vector e;
    private Vector f;
    private String g;
    private String h;
    private Vector i;
    private int j = 32768;
    private int k = 1;
    private int l = 1;
    private Vector m;
    private Vector n;
    private Vector o;
    
    public Request() {
	e = new Vector();
	f = new Vector();
	d = new Vector();
	i = new Vector();
	k = 1;
	l = 1;
	m = new Vector();
	n = new Vector();
	o = new Vector();
	a("1.2.826.0.1.3680043.2.60.0.1");
	b("softlink_jdt103");
    }
    
    public String getCallingTitle() {
	return c;
    }
    
    public String getApplicationContextUid() {
	return a;
    }
    
    public boolean isPrivateApplicationContext() {
	return !ApplicationContextItem.isPrivate(a);
    }
    
    public void setApplicationContextUid(String string) {
	a = string;
    }
    
    public String getCalledTitle() {
	return b;
    }
    
    public void setCalledTitle(String string) {
	b = string;
    }
    
    public void setCallingTitle(String string) {
	c = string;
    }
    
    public String getImplementationClassUID() {
	return g;
    }
    
    public String getImplementationVersionName() {
	return h;
    }
    
    void a(String string) {
	g = string;
    }
    
    void b(String string) {
	h = string;
    }
    
    public int getMaxPduSize() {
	return j;
    }
    
    public void setMaxPduSize(int i) {
	j = i;
    }
    
    public void setMaxOperationsInvoked(int i) {
	k = i;
    }
    
    public int getMaxOperationsInvoked() {
	return k;
    }
    
    public void setMaxOperationsPerformed(int i) {
	l = i;
    }
    
    public int getMaxOperationsPerformed() {
	return l;
    }
    
    public final int getScuRole(int i) throws IllegalValueException {
	return getScuRole(UID.getUIDEntry(i));
    }
    
    public int getScuRole(UIDEntry uidentry) {
	int i = m.indexOf(uidentry);
	if (i == -1)
	    return -1;
	return ((Integer) n.elementAt(i)).intValue();
    }
    
    public UIDEntry getAbstractSyntaxForRole(int i) {
	return (UIDEntry) m.elementAt(i);
    }
    
    public int getRoles() {
	return m.size();
    }
    
    public int getScpRole(int i) throws IllegalValueException {
	return getScpRole(UID.getUIDEntry(i));
    }
    
    public int getScpRole(UIDEntry uidentry) {
	int i = m.indexOf(uidentry);
	if (i == -1)
	    return -1;
	return ((Integer) o.elementAt(i)).intValue();
    }
    
    public byte getPresentationContextID(int i) {
	return ((Byte) d.elementAt(i)).byteValue();
    }
    
    public int getPresentationContexts() {
	return d.size();
    }
    
    public UIDEntry getAbstractSyntax(int i) {
	return (UIDEntry) e.elementAt(i);
    }
    
    public int getTransferSyntaxes(int i) {
	return ((Vector) f.elementAt(i)).size();
    }
    
    public UIDEntry getTransferSyntax(int i, int i_0_) {
	return (UIDEntry) ((Vector) f.elementAt(i)).elementAt(i_0_);
    }
    
    public byte[] getExtendedNegotiationData(int i) {
	return (byte[]) this.i.elementAt(i);
    }
    
    public byte addPresentationContext(int i, int[] is)
	throws IllegalValueException {
	byte i_1_ = (byte) (2 * e.size() + 1);
	a(i_1_, i, is);
	return i_1_;
    }
    
    void a(byte i, int i_2_, int[] is) throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i_2_);
	UIDEntry[] uidentrys = new UIDEntry[is.length];
	for (int i_3_ = 0; i_3_ < is.length; i_3_++)
	    uidentrys[i_3_] = UID.getUIDEntry(is[i_3_]);
	a(i, uidentry, uidentrys);
    }
    
    void a(byte i, UIDEntry uidentry, UIDEntry[] uidentrys) {
	e.addElement(uidentry);
	m.addElement(uidentry);
	n.addElement(new Integer(-1));
	o.addElement(new Integer(-1));
	this.i.addElement(null);
	d.addElement(new Byte(i));
	Vector vector = new Vector();
	for (int i_4_ = 0; i_4_ < uidentrys.length; i_4_++)
	    vector.addElement(uidentrys[i_4_]);
	f.addElement(vector);
    }
    
    void a(UIDEntry uidentry, int i, int i_5_) {
	int i_6_ = e.indexOf(uidentry);
	if (i_6_ != -1) {
	    n.setElementAt(new Integer(i), i_6_);
	    o.setElementAt(new Integer(i_5_), i_6_);
	}
    }
    
    public void setExtendedNegotiationData(int i, byte[] is) {
	this.i.setElementAt(is, i);
    }
    
    public void setScuScpRoleSelection(int i, int i_7_, int i_8_)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	int i_9_ = m.indexOf(uidentry);
	if (i_9_ != -1) {
	    n.setElementAt(new Integer(i_7_), i_9_);
	    o.setElementAt(new Integer(i_8_), i_9_);
	} else {
	    m.addElement(uidentry);
	    n.addElement(new Integer(i_7_));
	    o.addElement(new Integer(i_8_));
	}
    }
    
    public String toString() {
	String string = "*** request ***\n";
	string += "application context UID: " + a + "\n";
	string += "called title: " + b + "\n";
	string += "calling title: " + c + "\n";
	string += "max pdu size: " + j + "\n";
	string += "max operation invoked: " + k + "\n";
	string += "max operation performed: " + l + "\n";
	string += "implementation class UID: " + g + "\n";
	string += "implementation version Name: " + h + "\n";
	string += (com.archimed.tool.n.fstr("abstract syntax", 32) + com.archimed.tool.n.fstr("scu", 4)
		   + com.archimed.tool.n.fstr("scp", 4) + "\n");
	for (int i = 0; i < getRoles(); i++) {
	    UIDEntry uidentry = getAbstractSyntaxForRole(i);
	    string += (com.archimed.tool.n.fstr(uidentry.getValue(), 32)
		       + com.archimed.tool.n.fstr("" + getScuRole(uidentry), 4)
		       + com.archimed.tool.n.fstr("" + getScpRole(uidentry), 4) + "\n");
	}
	string += (com.archimed.tool.n.fstr("nr", 5) + com.archimed.tool.n.fstr("abstract syntax", 32)
		   + com.archimed.tool.n.fstr("pcid", 6) + com.archimed.tool.n.fstr("description", 55) + "\n");
	for (int i = 0; i < getPresentationContexts(); i++) {
	    UIDEntry uidentry = getAbstractSyntax(i);
	    string += (com.archimed.tool.n.fstr(i + "", 5) + com.archimed.tool.n.fstr(uidentry.getValue(), 32)
		       + com.archimed.tool.n.fstr("" + getPresentationContextID(i), 6)
		       + com.archimed.tool.n.fstr(uidentry.getName(), 55) + "\n");
	    if (Debug.DEBUG > 1) {
		int i_10_ = getTransferSyntaxes(i);
		for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
		    UIDEntry uidentry_12_ = getTransferSyntax(i, i_11_);
		    string += (com.archimed.tool.n.fstr("  ts-" + i_11_, 11)
			       + com.archimed.tool.n.fstr(uidentry_12_.getValue(), 32)
			       + com.archimed.tool.n.fstr(uidentry_12_.getName(), 55) + "\n");
		}
	    }
	}
	string += "***************";
	return string;
    }
    
    public int indexOf(byte i) {
	return d.indexOf(new Byte(i));
    }
}
