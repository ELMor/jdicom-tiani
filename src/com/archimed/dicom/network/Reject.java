/* Reject - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

public class Reject extends Response
{
    public static int REJECTED_PERMANENT = 1;
    public static int REJECTED_TRANSIENT = 2;
    public static int DICOM_UL_SERVICE_USER = 1;
    public static int DICOM_UL_SERVICE_PROVIDER_ACSE = 2;
    public static int DICOM_UL_SERVICE_PROVIDER_PRESENTATION = 3;
    public static int USER_NO_REASON_GIVEN = 1;
    public static int USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED = 2;
    public static int USER_CALLING_AETITLE_NOT_RECOGNIZED = 3;
    public static int USER_CALLED_AETITLE_NOT_RECOGNIZED = 7;
    public static int ACSE_NO_REASON_GIVEN = 1;
    public static int ACSE_PROTOCOL_NOT_SUPPORTED = 2;
    public static int PRESENTATION_TEMPORARY_CONGESTION = 1;
    public static int PRESENTATION_LOCAL_LIMIT_EXCEEDED = 2;
    private int a;
    private int b;
    private int c;
    
    public Reject(int i, int i_0_, int i_1_) {
	a = i;
	b = i_0_;
	c = i_1_;
    }
    
    public int getResult() {
	return a;
    }
    
    public int getSource() {
	return b;
    }
    
    public int getReason() {
	return c;
    }
    
    public String toString() {
	String string = "[reject, result: ";
	if (a == REJECTED_PERMANENT)
	    string += "REJECTED_PERMANENT";
	else if (a == REJECTED_TRANSIENT)
	    string += "REJECTED_TRANSIENT";
	else
	    string += "UNKNOWN";
	string += ", source: ";
	if (b == DICOM_UL_SERVICE_USER)
	    string += "DICOM_UL_SERVICE_USER";
	else if (b == DICOM_UL_SERVICE_PROVIDER_ACSE)
	    string += "DICOM_UL_SERVICE_PROVIDER_ACSE";
	else if (b == DICOM_UL_SERVICE_PROVIDER_PRESENTATION)
	    string += "DICOM_UL_SERVICE_PROVIDER_PRESENTATION";
	else
	    string += "UNKNOWN";
	string += ", reason: ";
	if (c == USER_NO_REASON_GIVEN)
	    string += "NO_REASON_GIVEN";
	else if (c == USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED)
	    string += "APPLICATIONCONTEXTNAME_NOT_SUPPORTED";
	else if (c == USER_CALLING_AETITLE_NOT_RECOGNIZED)
	    string += "CALLING_AETITILE_NOT_RECOGNIZED";
	else if (c == USER_CALLED_AETITLE_NOT_RECOGNIZED)
	    string += "CALLED_AETITILE_NOT_RECOGNIZED";
	else if (c == ACSE_NO_REASON_GIVEN)
	    string += "NO_REASON_GIVEN";
	else if (c == ACSE_PROTOCOL_NOT_SUPPORTED)
	    string += "PROTOCOL_NOT_SUPPORTED";
	else if (c == PRESENTATION_TEMPORARY_CONGESTION)
	    string += "TEMPORARY_CONGESTION";
	else if (c == PRESENTATION_LOCAL_LIMIT_EXCEEDED)
	    string += "LOCAL_LIMIT_EXCEEDED";
	else
	    string += "UNKNOWN";
	string += "]";
	return string;
    }
}
