/* AssociateAcknowledgePdu - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;

class AssociateAcknowledgePdu
{
    private int a = 2;
    private int b;
    private int c;
    private String d;
    private String e;
    private ApplicationContextItem f;
    private UserInfoItem g;
    private PresentationContextResponseItem h;
    private Vector i;
    private Acknowledge j;
    
    public AssociateAcknowledgePdu() {
	i = new Vector();
    }
    
    public AssociateAcknowledgePdu(Acknowledge acknowledge)
	throws IllegalValueException {
	d = acknowledge.getCalledTitle();
	e = acknowledge.getCallingTitle();
	f = new ApplicationContextItem();
	this.i = new Vector();
	for (int i = 0; i < acknowledge.getPresentationContexts(); i++) {
	    UIDEntry uidentry = acknowledge.getTransferSyntax(i);
	    if (uidentry == null)
		h = (new PresentationContextResponseItem
		     (acknowledge.getPresentationContextID(i),
		      acknowledge.getResult(i), null));
	    else
		h = (new PresentationContextResponseItem
		     (acknowledge.getPresentationContextID(i),
		      acknowledge.getResult(i),
		      new TransferSyntaxItem(uidentry)));
	    this.i.addElement(h);
	}
	g = new UserInfoItem();
	ImplementationClassUIDSubItem implementationclassuidsubitem
	    = (new ImplementationClassUIDSubItem
	       (acknowledge.getImplementationClassUID()));
	ImplementationVersionNameSubItem implementationversionnamesubitem
	    = (new ImplementationVersionNameSubItem
	       (acknowledge.getImplementationVersionName()));
	MaximumLengthSubItem maximumlengthsubitem
	    = new MaximumLengthSubItem(acknowledge.getMaxPduSize());
	g.addSubItem(maximumlengthsubitem);
	g.addSubItem(implementationclassuidsubitem);
	for (int i = 0; i < acknowledge.a(); i++) {
	    UIDEntry uidentry = acknowledge.a(i);
	    int i_0_ = acknowledge.getScuRole(uidentry.getConstant());
	    int i_1_ = acknowledge.getScpRole(uidentry.getConstant());
	    if (i_0_ != -1 && i_1_ != -1) {
		ScuScpRoleSubItem scuscprolesubitem
		    = new ScuScpRoleSubItem(uidentry, i_0_, i_1_);
		g.addSubItem(scuscprolesubitem);
	    }
	}
	g.addSubItem(implementationversionnamesubitem);
	if (acknowledge.getMaxOperationsInvoked() != 1
	    || acknowledge.getMaxOperationsPerformed() != 1) {
	    AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem
		= new AsynchronousOperationsWindowSubItem();
	    asynchronousoperationswindowsubitem
		.a(acknowledge.getMaxOperationsInvoked());
	    asynchronousoperationswindowsubitem
		.b(acknowledge.getMaxOperationsPerformed());
	    g.addSubItem(asynchronousoperationswindowsubitem);
	}
	b = 68;
	b += f.a();
	for (int i = 0; i < this.i.size(); i++)
	    b += ((PresentationContextResponseItem) this.i.elementAt(i))
		     .getLength();
	b += g.getLength();
    }
    
    public Acknowledge getAcknowledge() {
	return j;
    }
    
    private void a() throws IllegalValueException {
	j = new Acknowledge();
	j.setCalledTitle(d);
	j.setCallingTitle(e);
	j.setApplicationContextUid(f.getUid());
	Vector vector = g.getSubItems();
	for (int i = 0; i < this.i.size(); i++) {
	    PresentationContextResponseItem presentationcontextresponseitem
		= (PresentationContextResponseItem) this.i.elementAt(i);
	    if (presentationcontextresponseitem.getResult() == 0)
		j.addPresentationContext((byte) presentationcontextresponseitem
						    .getID(),
					 presentationcontextresponseitem
					     .getResult(),
					 presentationcontextresponseitem
					     .getTransferSyntax
					     ().getConstant());
	    else
		j.addPresentationContext((byte) presentationcontextresponseitem
						    .getID(),
					 presentationcontextresponseitem
					     .getResult(),
					 8193);
	}
	for (int i = 0; i < vector.size(); i++) {
	    SubItem subitem = (SubItem) vector.elementAt(i);
	    if (subitem instanceof ImplementationClassUIDSubItem)
		j.a(((ImplementationClassUIDSubItem) subitem)
			.getImplementationClassUID());
	    else if (subitem instanceof ImplementationVersionNameSubItem)
		j.b(((ImplementationVersionNameSubItem) subitem)
			.getImplementationVersionName());
	    else if (subitem instanceof MaximumLengthSubItem)
		j.setMaxPduSize(((MaximumLengthSubItem) subitem)
				    .getMaxPduSize());
	    else if (subitem instanceof ScuScpRoleSubItem) {
		ScuScpRoleSubItem scuscprolesubitem
		    = (ScuScpRoleSubItem) subitem;
		j.setScuScpRoleSelection(scuscprolesubitem.getAbstractSyntax
					     ().getConstant(),
					 scuscprolesubitem.getScuRole(),
					 scuscprolesubitem.getScpRole());
	    } else if (subitem
		       instanceof AsynchronousOperationsWindowSubItem) {
		AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem
		    = (AsynchronousOperationsWindowSubItem) subitem;
		j.setMaxOperationsInvoked(asynchronousoperationswindowsubitem
					      .a());
		j.setMaxOperationsPerformed(asynchronousoperationswindowsubitem
						.b());
	    }
	}
    }
    
    public int getLength() {
	return b + 6;
    }
    
    public Vector getPresentationContextResponseItems() {
	return i;
    }
    
    public void write(OutputStream outputstream)
	throws IOException, IllegalValueException {
	DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
	outputstream.write(a);
	outputstream.write(0);
	dataoutputstream.writeInt(getLength() - 6);
	dataoutputstream.writeChar(1);
	dataoutputstream.writeChar(0);
	outputstream.write(d.getBytes());
	for (int i = 0; i < 16 - d.length(); i++)
	    outputstream.write(32);
	outputstream.write(e.getBytes());
	for (int i = 0; i < 16 - e.length(); i++)
	    outputstream.write(32);
	outputstream.write(new byte[32]);
	f.a(dataoutputstream);
	for (int i = 0; i < this.i.size(); i++) {
	    PresentationContextResponseItem presentationcontextresponseitem
		= (PresentationContextResponseItem) this.i.elementAt(i);
	    presentationcontextresponseitem.write(dataoutputstream);
	}
	g.write(dataoutputstream);
    }
    
    public void read(InputStream inputstream)
	throws IOException, IllegalValueException, UnknownUIDException {
	PushbackInputStream pushbackinputstream
	    = new PushbackInputStream(inputstream);
	DataInputStream datainputstream
	    = new DataInputStream(pushbackinputstream);
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("PDU-type field of Associate Acknowledge PDU not " + a);
	datainputstream.read();
	b = datainputstream.readInt();
	if (Debug.DEBUG > 3)
	    Debug.out.println("PDU length: " + b);
	c = datainputstream.readChar();
	b -= 2;
	inputstream.skip(66L);
	b -= 66;
	f = new ApplicationContextItem();
	b = b - f.read(datainputstream);
	while (b > 0) {
	    i = datainputstream.read();
	    if (i == 33) {
		pushbackinputstream.unread(33);
		h = new PresentationContextResponseItem();
		h.read(datainputstream);
		this.i.addElement(h);
		b = b - h.getLength();
	    } else if (i == 80) {
		pushbackinputstream.unread(80);
		g = new UserInfoItem();
		int i_2_ = g.read(pushbackinputstream);
		b = b - i_2_;
	    }
	    if (Debug.DEBUG > 3)
		Debug.out.println("Remaining PDU length: " + b);
	}
	a();
    }
}
