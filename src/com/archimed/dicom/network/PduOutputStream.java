/* PduOutputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class PduOutputStream extends FilterOutputStream
{
    public static final byte COMMAND = 1;
    public static final byte DATA = 0;
    private int a;
    private DataOutputStream b;
    private byte[] c;
    private int d;
    private byte e;
    private byte f;
    
    public PduOutputStream(OutputStream outputstream, int i, byte i_0_,
			   byte i_1_) {
	super(outputstream);
	b = new DataOutputStream(outputstream);
	if (i == 0)
	    i = 100000;
	else
	    a = i;
	e = i_1_;
	f = i_0_;
	c = new byte[i - 6];
    }
    
    public synchronized void write(int i) throws IOException {
	if (d >= c.length)
	    a();
	c[d++] = (byte) i;
    }
    
    public synchronized void write(byte[] is) throws IOException {
	write(is, 0, is.length);
    }
    
    public synchronized void write(byte[] is, int i, int i_2_)
	throws IOException {
	if (d >= c.length)
	    a();
	if (i_2_ <= c.length - d) {
	    System.arraycopy(is, i, c, d, i_2_);
	    d += i_2_;
	} else {
	    System.arraycopy(is, i, c, d, c.length - d);
	    i += c.length - d;
	    i_2_ -= c.length - d;
	    d += c.length - d;
	    a();
	    int i_3_ = i_2_ / c.length;
	    for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
		System.arraycopy(is, i, c, 0, c.length);
		i += c.length;
		i_2_ -= c.length;
		d += c.length;
		a();
	    }
	    if (i_2_ > 0) {
		System.arraycopy(is, i, c, 0, i_2_);
		d += i_2_;
	    }
	}
    }
    
    private void a() throws IOException {
	b.writeByte(4);
	b.writeByte(0);
	b.writeInt(d + 6);
	b.writeInt(d + 2);
	b.writeByte(f);
	b.writeByte(e);
	out.write(c, 0, d);
	d = 0;
    }
    
    public void close() throws IOException {
	if (d > 0) {
	    b.writeByte(4);
	    b.writeByte(0);
	    b.writeInt(d + 6);
	    b.writeInt(d + 2);
	    b.writeByte(f);
	    b.writeByte(e + 2);
	    out.write(c, 0, d);
	} else {
	    b.writeByte(4);
	    b.writeByte(0);
	    b.writeInt(6);
	    b.writeInt(2);
	    b.writeByte(f);
	    b.writeByte(e + 2);
	}
    }
}
