/* AssociateRequestPdu - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;

class AssociateRequestPdu
{
    private int a = 1;
    private char b = '\001';
    String c;
    String d;
    ApplicationContextItem e;
    Vector f;
    UserInfoItem g;
    Request h;
    
    public AssociateRequestPdu() {
	f = new Vector();
    }
    
    public AssociateRequestPdu(Request request) throws IllegalValueException {
	h = request;
	c = request.getCalledTitle();
	d = request.getCallingTitle();
	f = new Vector();
	e = new ApplicationContextItem();
	for (int i = 0; i < request.getPresentationContexts(); i++) {
	    UIDEntry uidentry = request.getAbstractSyntax(i);
	    AbstractSyntaxItem abstractsyntaxitem
		= new AbstractSyntaxItem(uidentry);
	    Vector vector = new Vector();
	    for (int i_0_ = 0; i_0_ < request.getTransferSyntaxes(i); i_0_++)
		vector.addElement
		    (new TransferSyntaxItem(request.getTransferSyntax(i,
								      i_0_)));
	    PresentationContextItem presentationcontextitem
		= new PresentationContextItem(request
						  .getPresentationContextID(i),
					      abstractsyntaxitem, vector);
	    f.addElement(presentationcontextitem);
	}
	g = new UserInfoItem();
	ImplementationClassUIDSubItem implementationclassuidsubitem
	    = (new ImplementationClassUIDSubItem
	       (request.getImplementationClassUID()));
	ImplementationVersionNameSubItem implementationversionnamesubitem
	    = (new ImplementationVersionNameSubItem
	       (request.getImplementationVersionName()));
	MaximumLengthSubItem maximumlengthsubitem
	    = new MaximumLengthSubItem(request.getMaxPduSize());
	g.addSubItem(maximumlengthsubitem);
	g.addSubItem(implementationclassuidsubitem);
	g.addSubItem(implementationversionnamesubitem);
	for (int i = 0; i < request.getRoles(); i++) {
	    UIDEntry uidentry = request.getAbstractSyntaxForRole(i);
	    int i_1_ = request.getScuRole(uidentry.getConstant());
	    int i_2_ = request.getScpRole(uidentry.getConstant());
	    if (i_1_ != -1 && i_2_ != -1) {
		ScuScpRoleSubItem scuscprolesubitem
		    = new ScuScpRoleSubItem(uidentry, i_1_, i_2_);
		g.addSubItem(scuscprolesubitem);
	    }
	}
	for (int i = 0; i < request.getPresentationContexts(); i++) {
	    byte[] is = request.getExtendedNegotiationData(i);
	    if (is != null) {
		UIDEntry uidentry = request.getAbstractSyntax(i);
		ExtendedNegotiationSubItem extendednegotiationsubitem
		    = new ExtendedNegotiationSubItem(uidentry, is);
		g.addSubItem(extendednegotiationsubitem);
	    }
	}
	if (request.getMaxOperationsInvoked() != 1
	    || request.getMaxOperationsPerformed() != 1) {
	    AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem
		= new AsynchronousOperationsWindowSubItem();
	    asynchronousoperationswindowsubitem
		.a(request.getMaxOperationsInvoked());
	    asynchronousoperationswindowsubitem
		.b(request.getMaxOperationsPerformed());
	    g.addSubItem(asynchronousoperationswindowsubitem);
	}
    }
    
    public Request getRequest() throws IllegalValueException {
	return h;
    }
    
    private void a() {
	h = new Request();
	h.setCalledTitle(c);
	h.setCallingTitle(d);
	h.setApplicationContextUid(e.getUid());
	Vector vector = g.getSubItems();
	for (int i = 0; i < f.size(); i++) {
	    PresentationContextItem presentationcontextitem
		= (PresentationContextItem) f.elementAt(i);
	    Vector vector_3_ = presentationcontextitem.getTransferSyntaxes();
	    UIDEntry uidentry = presentationcontextitem.getAbstractSyntaxItem
				    ().getAbstractSyntax();
	    UIDEntry[] uidentrys = new UIDEntry[vector_3_.size()];
	    for (int i_4_ = 0; i_4_ < vector_3_.size(); i_4_++)
		uidentrys[i_4_]
		    = ((TransferSyntaxItem) vector_3_.elementAt(i_4_))
			  .getTransferSyntax();
	    h.a((byte) presentationcontextitem.getID(), uidentry, uidentrys);
	}
	for (int i = 0; i < vector.size(); i++) {
	    SubItem subitem = (SubItem) vector.elementAt(i);
	    if (subitem instanceof ImplementationClassUIDSubItem)
		h.a(((ImplementationClassUIDSubItem) subitem)
			.getImplementationClassUID());
	    else if (subitem instanceof ImplementationVersionNameSubItem)
		h.b(((ImplementationVersionNameSubItem) subitem)
			.getImplementationVersionName());
	    else if (subitem instanceof MaximumLengthSubItem)
		h.setMaxPduSize(((MaximumLengthSubItem) subitem)
				    .getMaxPduSize());
	    else if (subitem instanceof ScuScpRoleSubItem) {
		ScuScpRoleSubItem scuscprolesubitem
		    = (ScuScpRoleSubItem) subitem;
		h.a(scuscprolesubitem.getAbstractSyntax(),
		    scuscprolesubitem.getScuRole(),
		    scuscprolesubitem.getScpRole());
	    } else if (subitem
		       instanceof AsynchronousOperationsWindowSubItem) {
		AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem
		    = (AsynchronousOperationsWindowSubItem) subitem;
		h.setMaxOperationsInvoked(asynchronousoperationswindowsubitem
					      .a());
		h.setMaxOperationsPerformed(asynchronousoperationswindowsubitem
						.b());
	    } else if (subitem instanceof ExtendedNegotiationSubItem) {
		ExtendedNegotiationSubItem extendednegotiationsubitem
		    = (ExtendedNegotiationSubItem) subitem;
		UIDEntry uidentry = extendednegotiationsubitem.getUIDEntry();
		for (int i_5_ = 0; i_5_ < h.getPresentationContexts();
		     i_5_++) {
		    if (h.getAbstractSyntax(i_5_).getValue()
			    .equals(uidentry.getValue()))
			h.setExtendedNegotiationData
			    (i_5_,
			     extendednegotiationsubitem.getApplicationInfo());
		}
	    }
	}
    }
    
    public String getCalledTitle() {
	return c;
    }
    
    public String getCallingTitle() {
	return d;
    }
    
    public ApplicationContextItem getApplicationContextItem() {
	return e;
    }
    
    public Vector getPresentationContextItems() {
	return f;
    }
    
    public UserInfoItem getUserInfoItem() {
	return g;
    }
    
    public void setApplicationContextItem
	(ApplicationContextItem applicationcontextitem) {
	e = applicationcontextitem;
    }
    
    public void setUserInfoItem(UserInfoItem userinfoitem) {
	g = userinfoitem;
    }
    
    public void addPresentationContextItem
	(PresentationContextItem presentationcontextitem) {
	f.addElement(presentationcontextitem);
    }
    
    public int getLength() {
	int i = 0;
	i += 74;
	i += e.a();
	for (int i_6_ = 0; i_6_ < f.size(); i_6_++)
	    i += ((PresentationContextItem) f.elementAt(i_6_)).getLength();
	i += g.getLength();
	return i;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeInt(getLength() - 6);
	dataoutputstream.writeChar(1);
	dataoutputstream.writeChar(0);
	dataoutputstream.write(c.getBytes());
	for (int i = 0; i < 16 - c.length(); i++)
	    dataoutputstream.write(32);
	dataoutputstream.write(d.getBytes());
	for (int i = 0; i < 16 - d.length(); i++)
	    dataoutputstream.write(32);
	dataoutputstream.write(new byte[32]);
	e.a(dataoutputstream);
	for (int i = 0; i < f.size(); i++)
	    ((PresentationContextItem) f.elementAt(i)).write(dataoutputstream);
	g.write(dataoutputstream);
    }
    
    public void read(InputStream inputstream)
	throws IOException, IllegalValueException, UnknownUIDException {
	PushbackInputStream pushbackinputstream
	    = new PushbackInputStream(inputstream);
	DataInputStream datainputstream
	    = new DataInputStream(pushbackinputstream);
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("PDU-type field of Associate Request PDU not " + a);
	datainputstream.read();
	int i_7_ = datainputstream.readInt();
	b = datainputstream.readChar();
	i_7_ -= 2;
	datainputstream.readChar();
	byte[] is = new byte[16];
	datainputstream.readFully(is);
	c = new String(is);
	c = c.trim();
	datainputstream.readFully(is);
	d = new String(is);
	d = d.trim();
	inputstream.read(new byte[32]);
	i_7_ -= 66;
	e = new ApplicationContextItem();
	int i_8_ = e.read(datainputstream);
	i_7_ -= i_8_;
	while (i_7_ > 0) {
	    i = datainputstream.read();
	    if (i == 32) {
		pushbackinputstream.unread(32);
		PresentationContextItem presentationcontextitem
		    = new PresentationContextItem();
		i_8_ = presentationcontextitem.a(datainputstream);
		f.addElement(presentationcontextitem);
		i_7_ -= i_8_;
	    } else if (i == 80) {
		pushbackinputstream.unread(80);
		g = new UserInfoItem();
		i_8_ = g.read(pushbackinputstream);
		i_7_ -= i_8_;
	    }
	}
	a();
    }
}
