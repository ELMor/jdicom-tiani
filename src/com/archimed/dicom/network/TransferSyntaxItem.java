/* TransferSyntaxItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;

class TransferSyntaxItem
{
    private byte a = 64;
    private UIDEntry b;
    
    public TransferSyntaxItem() {
	/* empty */
    }
    
    public TransferSyntaxItem(UIDEntry uidentry) {
	b = uidentry;
    }
    
    public UIDEntry getTransferSyntax() {
	return b;
    }
    
    public int getLength() {
	return b.getValue().length() + 4;
    }
    
    void a(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(b.getValue().length());
	dataoutputstream.write(b.getValue().getBytes());
    }
    
    int a(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != 64)
	    throw new IllegalValueException
		      ("itemtype field of received Transfer Syntax Item not "
		       + a);
	datainputstream.skip(1L);
	char c = datainputstream.readChar();
	if (Debug.DEBUG > 3)
	    Debug.out.println("TransferSyntaxItem length: " + (int) c);
	byte[] is = new byte[c];
	datainputstream.readFully(is);
	is = n.trimZeros(is);
	String string = new String(is);
	try {
	    b = UID.getUIDEntry(string);
	} catch (UnknownUIDException unknownuidexception) {
	    b = new UIDEntry(0, string, "unknown uid", "??", 3);
	}
	return '\004' + c;
    }
}
