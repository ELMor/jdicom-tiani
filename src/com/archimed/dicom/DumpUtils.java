/* DumpUtils - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.util.Enumeration;

class DumpUtils
{
    public static String objectToDescription(Object object, int i) {
	String string = "";
	if (object == null)
	    return null;
	switch (i) {
	case 2:
	case 4:
	case 6:
	case 7:
	case 9:
	case 13:
	case 17:
	case 18:
	case 27:
	    string = ((String) object).trim();
	    break;
	case 28:
	    if (object instanceof DDateTime)
		string = ((DDateTime) object).toDICOMString();
	    else
		string = ((DDateTimeRange) object).toDICOMString();
	    break;
	case 12:
	    if (object instanceof DTime)
		string = ((DTime) object).toDICOMString();
	    else
		string = ((DTimeRange) object).toDICOMString();
	    break;
	case 14:
	    string = ((Person) object).a('^');
	    break;
	case 11:
	    if (object instanceof DDate)
		string = ((DDate) object).toDICOMString();
	    else
		string = ((DDateRange) object).toDICOMString();
	    break;
	case 16:
	case 26:
	    string = ((Float) object).toString();
	    break;
	case 3:
	case 15:
	case 21:
	    string = ((Integer) object).toString();
	    break;
	case 23:
	    string = ((Short) object).toString();
	    break;
	case 1:
	case 19:
	    string = ((Long) object).toString();
	    break;
	case 20:
	    string = ((Double) object).toString();
	    break;
	case 0:
	    string = new String((byte[]) object);
	    break;
	case 8:
	case 22:
	case 24: {
	    string = "";
	    int i_0_ = ((byte[]) object).length;
	    if (i_0_ > 12)
		i_0_ = 12;
	    for (int i_1_ = 0; i_1_ < i_0_; i_1_++) {
		if (i_1_ != i_0_ - 1)
		    string += a(((byte[]) object)[i_1_] & 0xff, 2) + "\\";
		else
		    string += a(((byte[]) object)[i_1_] & 0xff, 2);
	    }
	    if (i_0_ == 12)
		string += "...";
	    break;
	}
	case 5: {
	    int i_2_ = ((Integer) object).intValue() >> 16;
	    int i_3_ = ((Integer) object).intValue() & 0xffff;
	    string = "(" + a(i_2_, 4) + "," + a(i_3_, 4) + ")";
	    break;
	}
	default:
	    return null;
	}
	boolean bool = true;
	for (int i_4_ = 0; i_4_ < string.length(); i_4_++) {
	    if (' ' > string.charAt(i_4_) && string.charAt(i_4_) <= '~') {
		bool = false;
		i_4_ = string.length();
	    }
	}
	if (bool)
	    return string;
	return "Not printable";
    }
    
    public static String seqToDescription(Object object, int i, int i_5_) {
	String string = "";
	if (object == null)
	    return null;
	switch (i) {
	case 10: {
	    string += "\n";
	    Enumeration enumeration
		= ((DicomObject) object).enumerateVRs(false, true);
	    while (enumeration.hasMoreElements())
		string += dumpVR((VR) enumeration.nextElement(), i_5_ + 1);
	    return string;
	}
	default:
	    return null;
	}
    }
    
    public static String dumpVR(VR vr, int i) {
	String string = "";
	boolean bool = vr.dcm_type == 10;
	int i_6_ = 6;
	for (int i_7_ = 0; i_7_ < i * i_6_; i_7_++)
	    string += " ";
	string += "(" + a(vr.group, 4) + "," + a(vr.element, 4) + ")";
	string += " " + DDict.a(vr.dcm_type).substring(0, 2) + " ";
	string += "[";
	String string_8_ = "";
	for (int i_9_ = 0; i_9_ < vr.val.size(); i_9_++) {
	    if (bool) {
		String string_10_
		    = seqToDescription(vr.val.elementAt(i_9_), vr.dcm_type, i);
		if (string_10_ != null)
		    string_8_ += (String) string_10_;
	    } else {
		String string_11_
		    = objectToDescription(vr.val.elementAt(i_9_), vr.dcm_type);
		if (string_11_ != null)
		    string_8_ += (String) string_11_;
	    }
	    if (i_9_ != vr.val.size() - 1) {
		if (bool) {
		    for (int i_12_ = 0; i_12_ < i * i_6_; i_12_++)
			string_8_ += " ";
		}
		string_8_ += "\\";
	    }
	}
	if (bool) {
	    for (int i_13_ = 0; i_13_ < i * i_6_; i_13_++)
		string_8_ += " ";
	} else if (string_8_.length() > DicomObject.dumpLineLen - i * i_6_) {
	    string_8_
		= string_8_.substring(0, DicomObject.dumpLineLen - i * i_6_);
	    string_8_ += "...";
	}
	string += (String) string_8_ + "]";
	int i_14_;
	if ((i_14_ = string.lastIndexOf("\n")) == -1)
	    i_14_ = 0;
	else
	    i_14_++;
	String string_15_ = string.substring(i_14_, string.length());
	for (int i_16_ = 0; i_16_ < 60 - string_15_.length(); i_16_++)
	    string += " ";
	string += "#  " + vr.dataLen;
	for (int i_17_ = 0;
	     i_17_ < 8 - new Integer(vr.dataLen).toString().length(); i_17_++)
	    string += " ";
	string += vr.val.size();
	for (int i_18_ = 0;
	     i_18_ < 3 - new Integer(vr.val.size()).toString().length();
	     i_18_++)
	    string += " ";
	String string_19_
	    = DDict.getDescription(DDict.lookupDDict(vr.group, vr.element));
	if (string_19_.equals("Undefined") && vr.group % 2 == 1)
	    string_19_ = "Proprietary Tag";
	string += (String) string_19_ + "\n";
	return string;
    }
    
    static String a(int i, int i_20_) {
	String string = "";
	String string_21_ = Integer.toHexString(i);
	if (i_20_ < string_21_.length())
	    string_21_ = string_21_.substring(0, i_20_);
	for (int i_22_ = 0; i_22_ < i_20_ - string_21_.length(); i_22_++)
	    string += "0";
	return string + string_21_;
    }
}
