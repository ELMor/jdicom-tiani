/* DDateRange - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.text.DateFormat;

public class DDateRange
{
    DDate a;
    DDate b;
    
    public DDateRange(DDate ddate, DDate ddate_0_) {
	if (ddate != null)
	    a = ddate;
	else
	    a = new DDate();
	if (ddate_0_ != null)
	    b = ddate_0_;
	else
	    b = new DDate();
    }
    
    public DDateRange(String string) throws NumberFormatException {
	int i = string.indexOf('-');
	if (i == -1)
	    throw new NumberFormatException
		      (string + " cannot parse this string into DDateRange");
	a = new DDate(string.substring(0, i));
	b = new DDate(string.substring(i + 1));
    }
    
    public String toString() {
	return a.toString() + " - " + b.toString();
    }
    
    public String toString(DateFormat dateformat) {
	return a.toString(dateformat) + " - " + b.toString(dateformat);
    }
    
    public String toDICOMString() {
	return a.toDICOMString() + "-" + b.toDICOMString();
    }
    
    public DDate getDate1() {
	if (!a.isEmpty())
	    return a;
	return null;
    }
    
    public DDate getDate2() {
	if (!b.isEmpty())
	    return b;
	return null;
    }
}
