/* l - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.tool;
import java.util.Enumeration;
import java.util.NoSuchElementException;

final class l implements Enumeration
{
    d a;
    int b;
    
    l(d var_d) {
	a = var_d;
	b = 0;
    }
    
    public boolean hasMoreElements() {
	return b < a.b;
    }
    
    public Object nextElement() {
	if (b < a.b)
	    return a.a[b++];
	throw new NoSuchElementException("ArrayListEnumerator");
    }
}
