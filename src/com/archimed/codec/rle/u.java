/* u - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.codec.rle;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class u
{
    static final int a = 128;
    int[] b;
    int c;
    int d = 0;
    int e = 0;
    int f = d * e;
    
    public void setDimension(int i, int i_0_) {
	d = i;
	e = i_0_;
	f = i * i_0_;
    }
    
    public void processHeader(InputStream inputstream) throws IOException {
	c = v.get4bytes(inputstream);
	b = new int[15];
	for (int i = 0; i < 15; i++)
	    b[i] = v.get4bytes(inputstream);
    }
    
    public byte[] decodeBytes(InputStream inputstream) throws IOException {
	int i = 0;
	boolean bool = false;
	byte[] is = new byte[f];
	while (i < f) {
	    int i_1_ = (byte) inputstream.read();
	    if (i_1_ >= 0 && i_1_ <= 127) {
		for (int i_2_ = 0; i_2_ < i_1_ + 1; i_2_++) {
		    if (i < f)
			is[i++] = (byte) inputstream.read();
		}
	    } else if (i_1_ <= -1 && i_1_ >= -127) {
		byte i_3_ = (byte) inputstream.read();
		for (int i_4_ = 0; i_4_ < -i_1_ + 1; i_4_++) {
		    if (i < f)
			is[i++] = i_3_;
		}
	    }
	}
	return is;
    }
    
    public byte[] encodeBytes(InputStream inputstream) {
	boolean bool = false;
	int i = 0;
	int i_5_ = 0;
	int i_6_ = 0;
	byte[] is = new byte[128];
	ByteArrayOutputStream bytearrayoutputstream
	    = new ByteArrayOutputStream();
	for (;;) {
	    int i_7_;
	    try {
		i_7_ = inputstream.read();
	    } catch (Exception exception) {
		break;
	    }
	    if (i_5_ == 0) {
		if (i_6_ == 0) {
		    i = i_7_;
		    i_6_++;
		} else if (i == i_7_) {
		    if (++i_6_ == 128) {
			bytearrayoutputstream.write(1 - i_6_);
			bytearrayoutputstream.write(i);
			i_6_ = 0;
		    }
		} else if (i_6_ > 2) {
		    bytearrayoutputstream.write(1 - i_6_);
		    bytearrayoutputstream.write(i);
		    i_6_ = 1;
		    i = i_7_;
		} else {
		    for (int i_8_ = 0; i_8_ < i_6_; i_8_++) {
			is[i_5_++] = (byte) i;
			if (i_5_ == 128) {
			    bytearrayoutputstream.write(i_5_ - 1);
			    bytearrayoutputstream.write(is, 0, i_5_);
			    i_5_ = 0;
			}
		    }
		    i_6_ = 0;
		    is[i_5_++] = (byte) i_7_;
		    if (i_5_ == 128) {
			bytearrayoutputstream.write(i_5_ - 1);
			bytearrayoutputstream.write(is, 0, i_5_);
			i_5_ = 0;
		    }
		}
	    } else if (i_6_ != 0) {
		if (i_7_ == i) {
		    if (++i_6_ == 128) {
			bytearrayoutputstream.write(i_5_ - 1);
			bytearrayoutputstream.write(is, 0, i_5_);
			bytearrayoutputstream.write(1 - i_6_);
			bytearrayoutputstream.write(i);
			i_5_ = i_6_ = 0;
		    }
		} else {
		    bytearrayoutputstream.write(i_5_ - 1);
		    bytearrayoutputstream.write(is, 0, i_5_);
		    bytearrayoutputstream.write(1 - i_6_);
		    bytearrayoutputstream.write(i);
		    i_5_ = 0;
		    i_6_ = 1;
		    i = i_7_;
		}
	    } else if (is[i_5_ - 1] == i_7_) {
		i = i_7_;
		i_6_ = 2;
		i_5_--;
	    } else {
		is[i_5_++] = (byte) i_7_;
		if (i_5_ == 128) {
		    bytearrayoutputstream.write(i_5_ - 1);
		    bytearrayoutputstream.write(is, 0, i_5_);
		}
	    }
	}
	return bytearrayoutputstream.toByteArray();
    }
    
    public byte[] read(InputStream inputstream) throws IOException {
	processHeader(inputstream);
	byte[] is = new byte[c * f];
	int i = 0;
	int i_9_ = 0;
	while (i_9_ < c) {
	    System.arraycopy(decodeBytes(inputstream), 0, is, i, f);
	    i_9_++;
	    i += f;
	}
	return is;
    }
    
    public byte[] write(InputStream inputstream) throws IOException {
	return null;
    }
}
