/* SCMEventManager - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.kcmultimedia.demo;
import java.util.Vector;

public class SCMEventManager extends Vector
{
    private static SCMEventManager h = null;
    
    private SCMEventManager() {
	/* empty */
    }
    
    public static SCMEventManager getInstance() {
	if (null == h)
	    h = new SCMEventManager();
	return h;
    }
    
    public void dispatchSCMEvent(int i) {
	SCMEvent scmevent = new SCMEvent(i);
	for (int i_0_ = 0; i_0_ < this.size(); i_0_++) {
	    Object object = this.get(i_0_);
	    if (null != object)
		((SCMEventListener) object).handleSCMEvent(scmevent);
	}
    }
    
    public void addSCMEventListener(SCMEventListener scmeventlistener) {
	if (!this.contains(scmeventlistener))
	    this.add(scmeventlistener);
    }
    
    public void removeSCMEventListener(SCMEventListener scmeventlistener) {
	this.remove(scmeventlistener);
    }
}
