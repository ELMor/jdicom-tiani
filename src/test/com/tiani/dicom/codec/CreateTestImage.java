/* CreateTestImage - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package test.com.tiani.dicom.codec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.codec.Compression;
import com.tiani.dicom.legacy.TianiInputStream;

public class CreateTestImage
{
    private final boolean compress;
    private final boolean inverse;
    private final int type;
    private final int block;
    private final int white;
    private final int bitsStored;
    private final int bitsAllocated;
    private final Random random;
    private static final byte[] _FMI_VERSION = { 0, 1 };
    
    public CreateTestImage(String[] strings) {
	if (strings.length < 4)
	    throw new IllegalArgumentException("missing arguments");
	if (strings.length > 4)
	    throw new IllegalArgumentException("to many parameters");
	compress = strings[0].startsWith("c");
	float f = Float.parseFloat(compress ? strings[0].substring(1)
				   : strings[0]);
	if (inverse = f < 0.0F)
	    f = -f;
	type = (int) f;
	block = (int) ((f - (float) type) * 10.0F);
	bitsStored = Integer.parseInt(strings[1]);
	if (bitsStored < 8 || bitsStored > 16)
	    throw new IllegalArgumentException
		      ("stored bits out of range [8,16] - " + bitsStored);
	bitsAllocated = bitsStored > 8 ? 16 : 8;
	white = 65535 >> 16 - bitsStored;
	random = new Random((long) type);
    }
    
    public static void main(String[] strings) {
	try {
	    CreateTestImage createtestimage = new CreateTestImage(strings);
	    DicomObject dicomobject = new DicomObject();
	    TianiInputStream tianiinputstream
		= new TianiInputStream(new FileInputStream(strings[2]));
	    try {
		dicomobject.read(tianiinputstream);
	    } finally {
		tianiinputstream.close();
	    }
	    createtestimage.process(dicomobject);
	    FileOutputStream fileoutputstream
		= new FileOutputStream(strings[3]);
	    try {
		dicomobject.write(fileoutputstream, true);
	    } finally {
		fileoutputstream.close();
	    }
	} catch (IllegalArgumentException illegalargumentexception) {
	    System.out.println(illegalargumentexception);
	    System.out.println();
	    System.out.println
		("Usage: CreateTestImage [c][-]N[.<block>] <bitsStored> <attribFile> <outFile> ");
	    System.out
		.println("                    c - compress jpeg lossless");
	    System.out.println("                    0 - black image");
	    System.out
		.println("                    1 - horizontal black to white");
	    System.out
		.println("                   -1 - horizontal white to black");
	    System.out
		.println("                    2 - vertical black to white");
	    System.out
		.println("                   -2 - vertical white to black");
	    System.out
		.println("                    3 - diagonal black to white");
	    System.out
		.println("                   -3 - diagonal white to black");
	    System.out.println("                    4 - white image");
	    System.out.println
		("                    5 - horizontal black - white - black - ..");
	    System.out.println
		("                   -5 - horizontal white - black - white - ..");
	    System.out.println
		("                    6 - vertical black - white - black - ..");
	    System.out.println
		("                   -6 - vertical white - black - white - ..");
	    System.out.println
		("                    7 - diagonal black - white - black - ..");
	    System.out.println
		("                   -7 - diagonal white - black - white - ..");
	    System.out.println("                    N - random noise");
	    System.out.println("                   -N - random black/white");
	    System.out.println("           <block> .0 - continue gradation");
	    System.out.println("           <block> .1 - 2 pixel block");
	    System.out.println("           <block> .2 - 4 pixel block");
	    System.out.println("           <block> .3 - 8 pixel block");
	    System.out.println("           <block> .4 - 16 pixel block");
	    System.out.println("           <block> .5 - 32 pixel block");
	    System.out.println("           <block> .6 - 64 pixel block");
	} catch (IOException ioexception) {
	    System.out.println(ioexception);
	} catch (Throwable throwable) {
	    throwable.printStackTrace(System.out);
	}
    }
    
    private void process(DicomObject dicomobject)
	throws DicomException, IllegalValueException, IOException {
	dicomobject.set(466, new Integer(256));
	dicomobject.set(467, new Integer(256));
	dicomobject.set(475, new Integer(bitsAllocated));
	dicomobject.set(476, new Integer(bitsStored));
	dicomobject.set(477, new Integer(bitsStored - 1));
	dicomobject.set(478, new Integer(0));
	dicomobject.set(462, "MONOCHROME2");
	dicomobject.deleteItem(463);
	dicomobject.set(1184, (bitsAllocated == 8 ? create8BitPixelData()
			       : create16BitPixelData()));
	int i = 8194;
	if (compress) {
	    Compression compression = new Compression(dicomobject);
	    compression.compress(i = 8197);
	}
	dicomobject.setFileMetaInformation
	    (createFileMetaInformation(dicomobject,
				       UID.getUIDEntry(i).getValue()));
    }
    
    private int block(int i) {
	return (inverse ? 255 - i : i) >> block << block;
    }
    
    private int bOrW(int i) {
	return ((inverse ? 255 - i : i) >> block & 0x1) == 0 ? 0 : white;
    }
    
    private int random() {
	return (inverse ? random.nextBoolean() ? 0 : white
		: random.nextInt(white));
    }
    
    private byte[] create8BitPixelData() {
	byte[] is = new byte[65536];
	switch (type) {
	case 0:
	    break;
	case 1:
	    for (int i = 0; i < 256; i++) {
		for (int i_0_ = 0; i_0_ < 256; i_0_++)
		    is[256 * i + i_0_] = (byte) block(i_0_);
	    }
	    break;
	case 2:
	    for (int i = 0; i < 256; i++) {
		for (int i_1_ = 0; i_1_ < 256; i_1_++)
		    is[256 * i + i_1_] = (byte) block(i);
	    }
	    break;
	case 3:
	    for (int i = 0; i < 256; i++) {
		for (int i_2_ = 0; i_2_ < 256; i_2_++)
		    is[256 * i + i_2_] = (byte) (block(i) + block(i_2_) >> 1);
	    }
	    break;
	case 4:
	    Arrays.fill(is, (byte) -1);
	    break;
	case 5:
	    for (int i = 0; i < 256; i++) {
		for (int i_3_ = 0; i_3_ < 256; i_3_++)
		    is[256 * i + i_3_] = (byte) bOrW(i_3_);
	    }
	    break;
	case 6:
	    for (int i = 0; i < 256; i++) {
		for (int i_4_ = 0; i_4_ < 256; i_4_++)
		    is[256 * i + i_4_] = (byte) bOrW(i);
	    }
	    break;
	case 7:
	    for (int i = 0; i < 256; i++) {
		for (int i_5_ = 0; i_5_ < 256; i_5_++)
		    is[256 * i + i_5_] = (byte) bOrW(i ^ i_5_);
	    }
	    break;
	default:
	    for (int i = 0; i < 256; i++) {
		for (int i_6_ = 0; i_6_ < 256; i_6_++)
		    is[256 * i + i_6_] = (byte) random();
	    }
	}
	return is;
    }
    
    private byte[] create16BitPixelData() {
	byte[] is = new byte[131072];
	int i = bitsStored - 8;
	switch (type) {
	case 0:
	    break;
	case 1:
	    for (int i_7_ = 0; i_7_ < 256; i_7_++) {
		for (int i_8_ = 0; i_8_ < 256; i_8_++) {
		    int i_9_ = (256 * i_7_ + i_8_) * 2;
		    int i_10_ = block(i_8_) << i;
		    is[i_9_] = (byte) i_10_;
		    is[i_9_ + 1] = (byte) (i_10_ >> 8);
		}
	    }
	    break;
	case 2:
	    for (int i_11_ = 0; i_11_ < 256; i_11_++) {
		for (int i_12_ = 0; i_12_ < 256; i_12_++) {
		    int i_13_ = (256 * i_11_ + i_12_) * 2;
		    int i_14_ = block(i_11_) << i;
		    is[i_13_] = (byte) i_14_;
		    is[i_13_ + 1] = (byte) (i_14_ >> 8);
		}
	    }
	    break;
	case 3:
	    for (int i_15_ = 0; i_15_ < 256; i_15_++) {
		for (int i_16_ = 0; i_16_ < 256; i_16_++) {
		    int i_17_ = (256 * i_15_ + i_16_) * 2;
		    int i_18_ = block(i_15_) + block(i_16_) << i - 1;
		    is[i_17_] = (byte) i_18_;
		    is[i_17_ + 1] = (byte) (i_18_ >> 8);
		}
	    }
	    break;
	case 4:
	    Arrays.fill(is, (byte) -1);
	    break;
	case 5:
	    for (int i_19_ = 0; i_19_ < 256; i_19_++) {
		for (int i_20_ = 0; i_20_ < 256; i_20_++) {
		    int i_21_ = (256 * i_19_ + i_20_) * 2;
		    int i_22_ = bOrW(i_20_);
		    is[i_21_] = (byte) i_22_;
		    is[i_21_ + 1] = (byte) (i_22_ >> 8);
		}
	    }
	    break;
	case 6:
	    for (int i_23_ = 0; i_23_ < 256; i_23_++) {
		for (int i_24_ = 0; i_24_ < 256; i_24_++) {
		    int i_25_ = (256 * i_23_ + i_24_) * 2;
		    int i_26_ = bOrW(i_23_);
		    is[i_25_] = (byte) i_26_;
		    is[i_25_ + 1] = (byte) (i_26_ >> 8);
		}
	    }
	    break;
	case 7:
	    for (int i_27_ = 0; i_27_ < 256; i_27_++) {
		for (int i_28_ = 0; i_28_ < 256; i_28_++) {
		    int i_29_ = (256 * i_27_ + i_28_) * 2;
		    int i_30_ = bOrW(i_27_ ^ i_28_);
		    is[i_29_] = (byte) i_30_;
		    is[i_29_ + 1] = (byte) (i_30_ >> 8);
		}
	    }
	    break;
	default:
	    for (int i_31_ = 0; i_31_ < 256; i_31_++) {
		for (int i_32_ = 0; i_32_ < 256; i_32_++) {
		    int i_33_ = (256 * i_31_ + i_32_) * 2;
		    int i_34_ = random();
		    is[i_33_] = (byte) i_34_;
		    is[i_33_ + 1] = (byte) (i_34_ >> 8);
		}
	    }
	}
	return is;
    }
    
    private static DicomObject createFileMetaInformation
	(DicomObject dicomobject, String string) throws DicomException {
	DicomObject dicomobject_35_ = new DicomObject();
	dicomobject_35_.set(28, _FMI_VERSION);
	dicomobject_35_.set(29, dicomobject.get(62));
	dicomobject_35_.set(30, dicomobject.get(63));
	dicomobject_35_.set(31, string);
	dicomobject_35_.set(32, "1.2.40.0.13.0.0.113");
	dicomobject_35_.set(33, "TIANI_JDICOM_113");
	return dicomobject_35_;
    }
}
